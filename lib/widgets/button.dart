import 'package:edubox_tryout/themes/themes.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final double width;
  final Widget child;
  final bool isDisabled;
  final Function onTap;
  final Color color;

  const MyButton({Key key, this.width, this.child, this.isDisabled, this.onTap, this.color}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(30.0),
      onTap: isDisabled ? null : onTap,
      child: Container(
        width: width ?? MediaQuery.of(context).size.width,
        height: 50.0,
        decoration: BoxDecoration(
          color: isDisabled ? Colors.grey[300] : color != null ? color : EduColors.primary,
          borderRadius: BorderRadius.circular(30.0)
        ),
        child: Center(child: child),
      ),
    );
  }
}