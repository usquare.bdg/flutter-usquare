import 'package:edubox_tryout/themes/themes.dart';
import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final Widget content;
  final List<Widget> actions;
  final String title;
  final MainAxisAlignment actionAlignment;

  const CustomDialog({Key key, this.title, this.content, this.actions, this.actionAlignment}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0)
      ),
      elevation: 2.0, 
      child: Padding(
        padding: EdgeInsets.only(
          top: 15.0,
          bottom: 15.0,
          left: 10.0, 
          right: 10.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,       
          crossAxisAlignment: CrossAxisAlignment.center, 
          mainAxisSize: MainAxisSize.min,  
          children: [
            title == null ? Container() :
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(title, style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                color: EduColors.black
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: content,
            ),
            actions != null ? Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: actionAlignment ?? MainAxisAlignment.end,
                children: actions
              )
            ) : Container()
          ]
        ),
      ),         
    );     
  }
}