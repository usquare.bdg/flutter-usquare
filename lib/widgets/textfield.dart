import 'package:edubox_tryout/themes/themes.dart';
import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  final String hintText;
  final Function onChanged, onSubmitted, validator;
  final bool obsecureText;
  final TextEditingController controller;
  final Widget suffix, prefix;
  final TextInputType keyboardType;
  final TextInputAction actionType;
  final FocusNode focusNode;
  final bool isEnabled;
  final String errorText;

  const MyTextField({Key key, this.hintText, this.onChanged, this.suffix, this.prefix, this.keyboardType, this.controller, this.obsecureText, this.actionType, this.onSubmitted, this.focusNode, this.validator, this.isEnabled = true, this.errorText}) :  assert(isEnabled != null);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Container(
        height: 50.0,
        child: TextFormField(
          focusNode: focusNode,
          controller: controller ?? null,              
          style: EduText().mediumRegular,
          onChanged: onChanged,
          decoration: InputDecoration(
            fillColor: !isEnabled ? Colors.grey[100] : Colors.transparent,
            filled: true,
            errorText: errorText,
            contentPadding: EdgeInsets.symmetric(
              vertical: 18.0, horizontal: 5.0),     
            hintText: hintText,
            hintStyle: TextStyle(
              fontSize: 14.0,
              color: Colors.grey[400]
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 1.0, color: Colors.grey[300]
              )
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 1.5, color: EduColors.primary
              )
            ),
            prefixIcon: prefix ?? null,
            suffixIcon: suffix ?? null,
          ),
          enabled: isEnabled,
          cursorColor: EduColors.primary,
          keyboardType: keyboardType ?? null,
          textInputAction: actionType ?? null,    
          textAlignVertical: TextAlignVertical.center,
          obscureText: obsecureText ?? false,
          onFieldSubmitted: onSubmitted,
          validator: validator,
        ),
      ),
    );
  }
}