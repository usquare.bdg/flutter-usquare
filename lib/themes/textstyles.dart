import 'package:edubox_tryout/themes/themes.dart';
import 'package:flutter/material.dart';

class EduText {
  TextStyle display1 = new TextStyle(
    fontSize: 34.0,
    fontWeight: FontWeight.bold,
    color: EduColors.black
  );

  TextStyle smallRegular = new TextStyle(
    fontSize: 12.0,    
    color: EduColors.black
  );

  TextStyle smallSemiBold = new TextStyle(
    fontSize: 12.0,    
    fontWeight: FontWeight.w600,
    color: EduColors.black
  );

  TextStyle smallBold = new TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.bold,
    color: EduColors.black
  );

  TextStyle mediumRegular = new TextStyle(
    fontSize: 14.0,    
    color: EduColors.black
  );

  TextStyle mediumSemiBold = new TextStyle(
    fontSize: 14.0,  
    fontWeight: FontWeight.w600,  
    color: EduColors.black
  );

  TextStyle mediumBold = new TextStyle(
    fontSize: 14.0,  
    fontWeight: FontWeight.bold,  
    color: EduColors.black
  );

  TextStyle largeRegular = new TextStyle(
    fontSize: 16.0,      
    color: EduColors.black
  );

  TextStyle largeSemiBold = new TextStyle(
    fontSize: 16.0,  
    fontWeight: FontWeight.w600,  
    color: EduColors.black
  );

  TextStyle largeBold = new TextStyle(
    fontSize: 16.0,  
    fontWeight: FontWeight.bold,  
    color: EduColors.black
  );

  TextStyle superLargeRegular = new TextStyle(
    fontSize: 20.0,
    color: EduColors.black
  );

  TextStyle superLargeMedium = new TextStyle(
    fontSize: 20.0,  
    fontWeight: FontWeight.w600,  
    color: EduColors.black
  );

  TextStyle superLargeBold = new TextStyle(
    fontSize: 20.0,  
    fontWeight: FontWeight.bold,  
    color: EduColors.black
  );  
}