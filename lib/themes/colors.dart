import 'package:flutter/material.dart';

class EduColors {
  static const MaterialColor primary = const MaterialColor(
      0xFF3cc8f8,
    {
      50: const Color(0xFF3cc8f8),
      100: const Color(0xFF3cc8f8),
      200: const Color(0xFF3cc8f8),
      300: const Color(0xFF3cc8f8),
      400: const Color(0xFF3cc8f8),
      500: const Color(0xFF3cc8f8),
      600: const Color(0xFF3cc8f8),
      700: const Color(0xFF3cc8f8),
      800: const Color(0xFF3cc8f8),
      900: const Color(0xFF3cc8f8),
    }
  );

  static const Color primaryDark = Color(0xFF099bdb);
  static const Color darkBlue = Color(0xFF099bdb);
  static const Color black = Color(0xFF333333);
  static const Color red = Color(0xFFff3333);
  static const Color green = Color(0xFF00B722);
  static const Color yellow = const Color(0xFFffcc00);
//  static const Color purple = const Color(0xFF6E4AD5);
  static const Color purple = const Color(0xFF099bdb);
}