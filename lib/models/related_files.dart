import 'package:edubox_tryout/models/content.dart';

class RelatedFiles {
  bool status;
  Data data;
  Meta meta;

  RelatedFiles({this.status, this.data, this.meta});

  RelatedFiles.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<Items> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  List<String> viewRoles;
  List<String> ancestors;
  List<FileIds> fileIds;
  String sId;
  String name;
  String content;
  String sluggedName;
  String publicity;
  String browserWindowTitle;
  String parentId;
  bool open;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;

  Items(
      {this.viewRoles,
        this.ancestors,
        this.fileIds,
        this.sId,
        this.name,
        this.content,
        this.sluggedName,
        this.publicity,
        this.browserWindowTitle,
        this.parentId,
        this.open,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy});

  Items.fromJson(Map<String, dynamic> json) {
    viewRoles = json['view-roles'].cast<String>();
    ancestors = json['ancestors'].cast<String>();
    if (json['file_ids'] != null) {
      fileIds = new List<FileIds>();
      json['file_ids'].forEach((v) {
        fileIds.add(new FileIds.fromJson(v));
      });
    }
    sId = json['_id'];
    name = json['name'];
    content = json['content'];
    sluggedName = json['slugged-name'];
    publicity = json['publicity'];
    browserWindowTitle = json['browser-window-title'];
    parentId = json['parent_id'];
    open = json['open'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['view-roles'] = this.viewRoles;
    data['ancestors'] = this.ancestors;
    if (this.fileIds != null) {
      data['file_ids'] = this.fileIds.map((v) => v.toJson()).toList();
    }
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['content'] = this.content;
    data['slugged-name'] = this.sluggedName;
    data['publicity'] = this.publicity;
    data['browser-window-title'] = this.browserWindowTitle;
    data['parent_id'] = this.parentId;
    data['open'] = this.open;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}
