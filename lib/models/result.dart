class ResultBody {
  String institutionId;
  String userId;
  String quizId;
  String rombelId;
  String studentId;
  List<StudentAnswersBody> studentAnswersBody;
  PeriodBody periodBody;
  String status;
  List<KdsBody> kds;
  int correct;
  int wrong;
  int unanswers;
  dynamic score;
  int remaining;
  String blockedStatus;
  bool isUploaded;

  ResultBody(
      {this.institutionId,
      this.quizId,
      this.rombelId,
      this.studentId,
      this.studentAnswersBody,
      this.periodBody,
      this.status,
      this.kds,
      this.correct,
      this.wrong,
      this.isUploaded,
      this.userId,
      this.unanswers,
      this.score,
      this.remaining,
      this.blockedStatus});

  ResultBody.fromJson(Map<String, dynamic> json) {
    institutionId = json['institution_id'];
    quizId = json['quiz_id'];
    userId = json['user_id'];
    rombelId = json['rombel_id'];
    isUploaded = json['isUploaded'];
    studentId = json['student_id'];
    if (json['student_answers'] != null) {
      studentAnswersBody = new List<StudentAnswersBody>();
      json['student_answers'].forEach((v) {
        studentAnswersBody.add(new StudentAnswersBody.fromJson(v));
      });
    }
    periodBody =
        json['period'] != null ? new PeriodBody.fromJson(json['period']) : null;
    status = json['status'];
    if (json['kds'] != null) {
      kds = new List<KdsBody>();
      json['kds'].forEach((v) {
        kds.add(new KdsBody.fromJson(v));
      });
    }
    if (json['correct'] == null) correct = 0;
      else correct = json['correct'];
    wrong = json['wrong'];
    unanswers = json['unanswers'];
    score = json['score'];
    remaining = json['remaining'];
    blockedStatus = json['blocked_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['institution_id'] = this.institutionId;
    data['quiz_id'] = this.quizId;
    data['rombel_id'] = this.rombelId;
    data['student_id'] = this.studentId;
    data['isUploaded'] = this.isUploaded;
    data['user_id'] = this.userId;
    if (this.studentAnswersBody != null) {
      data['student_answers'] =
          this.studentAnswersBody.map((v) => v.toJson()).toList();
    }
    if (this.periodBody != null) {
      data['period'] = this.periodBody.toJson();
    }
    data['status'] = this.status;
    if (this.kds != null) {
      data['kds'] = this.kds.map((v) => v.toJson()).toList();
    }
    data['correct'] = this.correct;
    data['wrong'] = this.wrong;
    data['unanswers'] = this.unanswers;
    data['score'] = this.score;
    data['remaining'] = this.remaining;
    data['blocked_status'] = this.blockedStatus;
    return data;
  }
}

class StudentAnswersBody {
  String id;
  String answer;
  String state;
  String type;
  bool isCorrect;

  StudentAnswersBody({this.id, this.answer, this.state, this.isCorrect, this.type});

  StudentAnswersBody.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    answer = json['answer'];
    state = json['state'];
    isCorrect = json['isCorrect'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['answer'] = this.answer;
    data['state'] = this.state;
    data['isCorrect'] = this.isCorrect;
    data['type'] = this.type;

    return data;
  }
}

class PeriodBody {
  int year;
  int semester;

  PeriodBody({this.year, this.semester});

  PeriodBody.fromJson(Map<String, dynamic> json) {
    year = json['year'];
    semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['year'] = this.year;
    data['semester'] = this.semester;
    return data;
  }
}

class KdsBody {
  String hello;

  KdsBody({this.hello});

  KdsBody.fromJson(Map<String, dynamic> json) {
    hello = json['hello'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['hello'] = this.hello;
    return data;
  }
}

class Answer {  
  String quizId;
  int position;  
  String answer;
  bool isAnswered;

  Answer({this.position, this.answer, this.quizId, this.isAnswered});

  Answer.fromJson(Map<String, dynamic> json) {
    position = json['position'];    
    answer = json['answer'];
    quizId = json['quizId'];
    isAnswered = json['isAnswered'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position'] = this.position;    
    data['answer'] = this.answer;
    data['quizId'] = this.quizId;
    data['isAnswered'] = this.isAnswered;

    return data;
  }
}