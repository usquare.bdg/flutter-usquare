import 'package:edubox_tryout/models/quiz_list.dart';

class Sync {
  String lessonName;
  List<QuizIds> quizids;

  Sync({this.lessonName, this.quizids});
}

class QuizIds {
  bool isSelected;
  bool isDownloaded;
  bool isFinished;
  Items items;

  QuizIds({this.isSelected, this.isDownloaded, this.items, this.isFinished});
}