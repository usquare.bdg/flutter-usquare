class QuizPass {
  bool status;
  Data data;
  Meta meta;

  QuizPass({this.status, this.data, this.meta});

  QuizPass.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<Items> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String sId;
  InstitutionId institutionId;
  QuizId quizId;
  String passcode;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  UserId userId;
  String id;

  Items(
      {this.sId,
        this.institutionId,
        this.quizId,
        this.passcode,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy,
        this.userId,
        this.id});

  Items.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    institutionId = json['institution_id'] != null
        ? new InstitutionId.fromJson(json['institution_id'])
        : null;
    quizId =
    json['quiz_id'] != null ? new QuizId.fromJson(json['quiz_id']) : null;
    passcode = json['passcode'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    userId =
    json['user_id'] != null ? new UserId.fromJson(json['user_id']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.institutionId != null) {
      data['institution_id'] = this.institutionId.toJson();
    }
    if (this.quizId != null) {
      data['quiz_id'] = this.quizId.toJson();
    }
    data['passcode'] = this.passcode;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    if (this.userId != null) {
      data['user_id'] = this.userId.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class InstitutionId {
  String sId;
  Zenius zenius;
  int entrant;
  String name;
  String type;
  String country;
  String province;
  String city;
  String district;
  String postalCode;
  int phone;
  String email;
  String address;
  Director director;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String logo;
  String updatedBy;
  String prefix;

  InstitutionId(
      {this.sId,
        this.zenius,
        this.entrant,
        this.name,
        this.type,
        this.country,
        this.province,
        this.city,
        this.district,
        this.postalCode,
        this.phone,
        this.email,
        this.address,
        this.director,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.logo,
        this.updatedBy,
        this.prefix});

  InstitutionId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    zenius =
    json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    entrant = json['entrant'];
    name = json['name'];
    type = json['type'];
    country = json['country'];
    province = json['province'];
    city = json['city'];
    district = json['district'];
    postalCode = json['postal_code'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    director = json['director'] != null
        ? new Director.fromJson(json['director'])
        : null;
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    logo = json['logo'];
    updatedBy = json['updated_by'];
    prefix = json['prefix'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['entrant'] = this.entrant;
    data['name'] = this.name;
    data['type'] = this.type;
    data['country'] = this.country;
    data['province'] = this.province;
    data['city'] = this.city;
    data['district'] = this.district;
    data['postal_code'] = this.postalCode;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    if (this.director != null) {
      data['director'] = this.director.toJson();
    }
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['logo'] = this.logo;
    data['updated_by'] = this.updatedBy;
    data['prefix'] = this.prefix;
    return data;
  }
}

class Zenius {
  int premiumQuota;
  String status;
  String slug;

  Zenius({this.premiumQuota, this.status, this.slug});

  Zenius.fromJson(Map<String, dynamic> json) {
    premiumQuota = json['premium_quota'];
    status = json['status'];
    slug = json['slug'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium_quota'] = this.premiumQuota;
    data['status'] = this.status;
    data['slug'] = this.slug;
    return data;
  }
}

class Director {
  String name;
  String ktpNumber;
  String hp;
  String email;

  Director({this.name, this.ktpNumber, this.hp, this.email});

  Director.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    ktpNumber = json['ktp_number'];
    hp = json['hp'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['ktp_number'] = this.ktpNumber;
    data['hp'] = this.hp;
    data['email'] = this.email;
    return data;
  }
}

class QuizId {
  String sId;
  List<String> lessonIds;
  List<String> questionIds;
  List<String> lessonGroup;
  List<String> grade;
  List<String> department;
  String title;
  int endTime;
  String quizType;
  String institutionId;
  String status;
  Period period;
  bool randomSeq;
  bool isPublic;
  bool showScoreOption;
  String passcode;
  String kodePembuka;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;

  QuizId(
      {this.sId,
        this.lessonIds,
        this.questionIds,
        this.lessonGroup,
        this.grade,
        this.department,
        this.title,
        this.endTime,
        this.quizType,
        this.institutionId,
        this.status,
        this.period,
        this.randomSeq,
        this.isPublic,
        this.showScoreOption,
        this.passcode,
        this.kodePembuka,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy});

  QuizId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    lessonIds = json['lesson_ids'].cast<String>();
    questionIds = json['question_ids'].cast<String>();
    lessonGroup = json['lesson_group'].cast<String>();
    grade = json['grade'].cast<String>();
    department = json['department'].cast<String>();
    title = json['title'];
    endTime = json['end_time'];
    quizType = json['quiz_type'];
    institutionId = json['institution_id'];
    status = json['status'];
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    randomSeq = json['random_seq'];
    isPublic = json['is_public'];
    showScoreOption = json['show_score_option'];
    passcode = json['passcode'];
    kodePembuka = json['kode_pembuka'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['lesson_ids'] = this.lessonIds;
    data['question_ids'] = this.questionIds;
    data['lesson_group'] = this.lessonGroup;
    data['grade'] = this.grade;
    data['department'] = this.department;
    data['title'] = this.title;
    data['end_time'] = this.endTime;
    data['quiz_type'] = this.quizType;
    data['institution_id'] = this.institutionId;
    data['status'] = this.status;
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['random_seq'] = this.randomSeq;
    data['is_public'] = this.isPublic;
    data['show_score_option'] = this.showScoreOption;
    data['passcode'] = this.passcode;
    data['kode_pembuka'] = this.kodePembuka;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class Period {
  int year;
  int semester;

  Period({this.year, this.semester});

  Period.fromJson(Map<String, dynamic> json) {
    year = json['year'];
    semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['year'] = this.year;
    data['semester'] = this.semester;
    return data;
  }
}

class UserId {
  String sId;
  Zenius zenius;
  List<int> role;
  List<String> studentAtInstitutions;
  List<Null> teacherAtInstitutions;
  List<Null> adminAtInstitutions;
  List<String> rombelIds;
  String username;
  String email;
  String password;
  String displayName;
  String createdBy;
  String createdAt;
  String updatedAt;
  String institutionId;
  int iV;
  String updatedBy;
  String idNumber;
  String phone;

  UserId(
      {this.sId,
        this.zenius,
        this.role,
        this.studentAtInstitutions,
        this.teacherAtInstitutions,
        this.adminAtInstitutions,
        this.rombelIds,
        this.username,
        this.email,
        this.password,
        this.displayName,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.institutionId,
        this.iV,
        this.updatedBy,
        this.idNumber,
        this.phone});

  UserId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    zenius =
    json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    role = json['role'].cast<int>();
    studentAtInstitutions = json['student_at_institutions'].cast<String>();
    if (json['teacher_at_institutions'] != null) {
//      teacherAtInstitutions = new List<Null>();
//      json['teacher_at_institutions'].forEach((v) {
//        teacherAtInstitutions.add(new Null.fromJson(v));
//      });
    }
    if (json['admin_at_institutions'] != null) {
//      adminAtInstitutions = new List<Null>();
//      json['admin_at_institutions'].forEach((v) {
//        adminAtInstitutions.add(new Null.fromJson(v));
//      });
    }
    rombelIds = json['rombel_ids'].cast<String>();
    username = json['username'];
    email = json['email'];
    password = json['password'];
    displayName = json['display_name'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    institutionId = json['institution_id'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    idNumber = json['id_number'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['role'] = this.role;
    data['student_at_institutions'] = this.studentAtInstitutions;
    if (this.teacherAtInstitutions != null) {
//      data['teacher_at_institutions'] =
//          this.teacherAtInstitutions.map((v) => v.toJson()).toList();
    }
    if (this.adminAtInstitutions != null) {
//      data['admin_at_institutions'] =
//          this.adminAtInstitutions.map((v) => v.toJson()).toList();
    }
    data['rombel_ids'] = this.rombelIds;
    data['username'] = this.username;
    data['email'] = this.email;
    data['password'] = this.password;
    data['display_name'] = this.displayName;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['institution_id'] = this.institutionId;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    data['id_number'] = this.idNumber;
    data['phone'] = this.phone;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}