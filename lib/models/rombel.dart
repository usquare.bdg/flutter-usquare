class Rombel {
  bool status;
  Data data;  

  Rombel({this.status, this.data});

  Rombel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String rombelType;
  List<String> lessonIds;
  String sId;
  String name;
  String department;
  String grade;
  int periode;
  String curriculum;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;

  Data(
      {this.rombelType,
      this.lessonIds,
      this.sId,
      this.name,
      this.department,
      this.grade,
      this.periode,
      this.curriculum,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV});

  Data.fromJson(Map<String, dynamic> json) {
    rombelType = json['rombel_type'];
    lessonIds = json['lesson_ids'].cast<String>();
    sId = json['_id'];
    name = json['name'];
    department = json['department'];
    grade = json['grade'];
    periode = json['periode'];
    curriculum = json['curriculum'];    
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rombel_type'] = this.rombelType;
    data['lesson_ids'] = this.lessonIds;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['department'] = this.department;
    data['grade'] = this.grade;
    data['periode'] = this.periode;
    data['curriculum'] = this.curriculum;    
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}