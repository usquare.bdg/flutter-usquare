class AssignmentResult {
  bool status;
  Data data;
  Meta meta;

  AssignmentResult({this.status, this.data, this.meta});

  AssignmentResult.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<Items> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String sId;
  List<String> fileIds;
  String institutionId;
  AssignmentId assignmentId;
  RombelId rombelId;
  StudentId studentId;
  String title;
  String content;
  Period period;
  String status;
  int score;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String id;

  Items(
      {this.sId,
        this.fileIds,
        this.institutionId,
        this.assignmentId,
        this.rombelId,
        this.studentId,
        this.title,
        this.content,
        this.period,
        this.status,
        this.score,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.id});

  Items.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    fileIds = json['file_ids'].cast<String>();
    institutionId = json['institution_id'];
    assignmentId = json['assignment_id'] != null
        ? new AssignmentId.fromJson(json['assignment_id'])
        : null;
    rombelId = json['rombel_id'] != null
        ? new RombelId.fromJson(json['rombel_id'])
        : null;
    studentId = json['student_id'] != null
        ? new StudentId.fromJson(json['student_id'])
        : null;
    title = json['title'];
    content = json['content'];
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    status = json['status'];
    score = json['score'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['file_ids'] = this.fileIds;
    data['institution_id'] = this.institutionId;
    if (this.assignmentId != null) {
      data['assignment_id'] = this.assignmentId.toJson();
    }
    if (this.rombelId != null) {
      data['rombel_id'] = this.rombelId.toJson();
    }
    if (this.studentId != null) {
      data['student_id'] = this.studentId.toJson();
    }
    data['title'] = this.title;
    data['content'] = this.content;
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['status'] = this.status;
    data['score'] = this.score;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['id'] = this.id;
    return data;
  }
}

class AssignmentId {
  String sId;
  List<String> lessonIds;
  List<Null> fileIds;
  List<Null> lessonGroup;
  List<Null> grade;
  List<Null> department;
  String institutionId;
  String title;
  String content;
  String dueDate;
  Period period;
  String status;
  String assignmentType;
  String addToSummary;
  String recipient;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  String deletedAt;
  String deletedBy;

  AssignmentId(
      {this.sId,
        this.lessonIds,
        this.fileIds,
        this.lessonGroup,
        this.grade,
        this.department,
        this.institutionId,
        this.title,
        this.content,
        this.dueDate,
        this.period,
        this.status,
        this.assignmentType,
        this.addToSummary,
        this.recipient,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy,
        this.deletedAt,
        this.deletedBy});

  AssignmentId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    lessonIds = json['lesson_ids'].cast<String>();
    if (json['file_ids'] != null) {
      fileIds = new List<Null>();
      json['file_ids'].forEach((v) {
//        fileIds.add(new Null.fromJson(v));
      });
    }
    if (json['lesson_group'] != null) {
      lessonGroup = new List<Null>();
      json['lesson_group'].forEach((v) {
//        lessonGroup.add(new Null.fromJson(v));
      });
    }
    if (json['grade'] != null) {
      grade = new List<Null>();
      json['grade'].forEach((v) {
//        grade.add(new Null.fromJson(v));
      });
    }
    if (json['department'] != null) {
      department = new List<Null>();
      json['department'].forEach((v) {
//        department.add(new Null.fromJson(v));
      });
    }
    institutionId = json['institution_id'];
    title = json['title'];
    content = json['content'];
    dueDate = json['due_date'];
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    status = json['status'];
    assignmentType = json['assignment_type'];
    addToSummary = json['add_to_summary'];
    recipient = json['recipient'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    deletedAt = json['deleted_at'];
    deletedBy = json['deleted_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['lesson_ids'] = this.lessonIds;
    if (this.fileIds != null) {
//      data['file_ids'] = this.fileIds.map((v) => v.toJson()).toList();
    }
    if (this.lessonGroup != null) {
//      data['lesson_group'] = this.lessonGroup.map((v) => v.toJson()).toList();
    }
    if (this.grade != null) {
//      data['grade'] = this.grade.map((v) => v.toJson()).toList();
    }
    if (this.department != null) {
//      data['department'] = this.department.map((v) => v.toJson()).toList();
    }
    data['institution_id'] = this.institutionId;
    data['title'] = this.title;
    data['content'] = this.content;
    data['due_date'] = this.dueDate;
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['status'] = this.status;
    data['assignment_type'] = this.assignmentType;
    data['add_to_summary'] = this.addToSummary;
    data['recipient'] = this.recipient;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    data['deleted_at'] = this.deletedAt;
    data['deleted_by'] = this.deletedBy;
    return data;
  }
}

class Period {
  int year;
  int semester;

  Period({this.year, this.semester});

  Period.fromJson(Map<String, dynamic> json) {
    year = json['year'];
    semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['year'] = this.year;
    data['semester'] = this.semester;
    return data;
  }
}

class RombelId {
  String sId;
  String rombelType;
  List<String> lessonIds;
  String name;
  String department;
  String grade;
  int periode;
  String curriculum;
  String teacherId;
  String institutionId;
  String createdBy;
  String createdAt;
  String updatedAt;
  String teacherName;
  int iV;

  RombelId(
      {this.sId,
        this.rombelType,
        this.lessonIds,
        this.name,
        this.department,
        this.grade,
        this.periode,
        this.curriculum,
        this.teacherId,
        this.institutionId,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.teacherName,
        this.iV});

  RombelId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    rombelType = json['rombel_type'];
    lessonIds = json['lesson_ids'].cast<String>();
    name = json['name'];
    department = json['department'];
    grade = json['grade'];
    periode = json['periode'];
    curriculum = json['curriculum'];
    teacherId = json['teacher_id'];
    institutionId = json['institution_id'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    teacherName = json['teacher_name'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['rombel_type'] = this.rombelType;
    data['lesson_ids'] = this.lessonIds;
    data['name'] = this.name;
    data['department'] = this.department;
    data['grade'] = this.grade;
    data['periode'] = this.periode;
    data['curriculum'] = this.curriculum;
    data['teacher_id'] = this.teacherId;
    data['institution_id'] = this.institutionId;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['teacher_name'] = this.teacherName;
    data['__v'] = this.iV;
    return data;
  }
}

class StudentId {
  String sId;
  Zenius zenius;
  List<int> role;
  List<String> studentAtInstitutions;
  List<Null> teacherAtInstitutions;
  List<Null> adminAtInstitutions;
  List<String> rombelIds;
  String username;
  String email;
  String password;
  String displayName;
  String createdBy;
  String createdAt;
  String updatedAt;
  String institutionId;
  int iV;
  String updatedBy;
  String phone;

  StudentId(
      {this.sId,
        this.zenius,
        this.role,
        this.studentAtInstitutions,
        this.teacherAtInstitutions,
        this.adminAtInstitutions,
        this.rombelIds,
        this.username,
        this.email,
        this.password,
        this.displayName,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.institutionId,
        this.iV,
        this.updatedBy,
        this.phone});

  StudentId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    zenius =
    json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    role = json['role'].cast<int>();
    studentAtInstitutions = json['student_at_institutions'].cast<String>();
    if (json['teacher_at_institutions'] != null) {
      teacherAtInstitutions = new List<Null>();
      json['teacher_at_institutions'].forEach((v) {
//        teacherAtInstitutions.add(new Null.fromJson(v));
      });
    }
    if (json['admin_at_institutions'] != null) {
      adminAtInstitutions = new List<Null>();
      json['admin_at_institutions'].forEach((v) {
//        adminAtInstitutions.add(new Null.fromJson(v));
      });
    }
    rombelIds = json['rombel_ids'].cast<String>();
    username = json['username'];
    email = json['email'];
    password = json['password'];
    displayName = json['display_name'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    institutionId = json['institution_id'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['role'] = this.role;
    data['student_at_institutions'] = this.studentAtInstitutions;
    if (this.teacherAtInstitutions != null) {
//      data['teacher_at_institutions'] =
//          this.teacherAtInstitutions.map((v) => v.toJson()).toList();
    }
    if (this.adminAtInstitutions != null) {
//      data['admin_at_institutions'] =
//          this.adminAtInstitutions.map((v) => v.toJson()).toList();
    }
    data['rombel_ids'] = this.rombelIds;
    data['username'] = this.username;
    data['email'] = this.email;
    data['password'] = this.password;
    data['display_name'] = this.displayName;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['institution_id'] = this.institutionId;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    data['phone'] = this.phone;
    return data;
  }
}

class Zenius {
  bool isActive;
  bool isPremium;

  Zenius({this.isActive, this.isPremium});

  Zenius.fromJson(Map<String, dynamic> json) {
    isActive = json['is_active'];
    isPremium = json['is_premium'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_active'] = this.isActive;
    data['is_premium'] = this.isPremium;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}
