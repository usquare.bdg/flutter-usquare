import 'quiz.dart';

class Question {
  bool status;
  QuestionIds data;

  Question({this.status, this.data});

  Question.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new QuestionIds.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}