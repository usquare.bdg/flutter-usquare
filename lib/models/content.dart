class EduboxContent {
  bool status;
  Data data;
  Meta meta;

  EduboxContent({this.status, this.data, this.meta});

  EduboxContent.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  String sluggedName;
  List<Sub> sub;
  String content;
  String name;
  String publicity;
  List<String> viewRoles;
  String contentsSequence;
  int id;
  String urlPath;
  List<Contents> contents;
  List<FileIds> fileIds;
  String browserWindowTitle;

  Data(
      {this.sluggedName,
        this.sub,
        this.content,
        this.name,
        this.publicity,
        this.viewRoles,
        this.contentsSequence,
        this.id,
        this.urlPath,
        this.contents,
        this.fileIds,
        this.browserWindowTitle});

  Data.fromJson(Map<String, dynamic> json) {
    sluggedName = json['slugged-name'];
    if (json['sub'] != null) {
      sub = new List<Sub>();
      json['sub'].forEach((v) {
        sub.add(new Sub.fromJson(v));
      });
    }
    if (json['file_ids'] != null) {
      fileIds = new List<FileIds>();
      json['file_ids'].forEach((v) {
        fileIds.add(new FileIds.fromJson(v));
      });
    }
    content = json['content'];
    name = json['name'];
    publicity = json['publicity'];
    viewRoles = json['view-roles'].cast<String>();
    contentsSequence = json['contents-sequence'];
    id = json['id'];
    urlPath = json['url-path'];
    if (json['contents'] != null) {
      contents = new List<Contents>();
      json['contents'].forEach((v) {
        contents.add(new Contents.fromJson(v));
      });
    }
    browserWindowTitle = json['browser-window-title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slugged-name'] = this.sluggedName;
    if (this.sub != null) {
      data['sub'] = this.sub.map((v) => v.toJson()).toList();
    }
    data['content'] = this.content;
    data['name'] = this.name;
    data['publicity'] = this.publicity;
    data['view-roles'] = this.viewRoles;
    data['contents-sequence'] = this.contentsSequence;
    data['id'] = this.id;
    data['url-path'] = this.urlPath;
    if (this.contents != null) {
      data['contents'] = this.contents.map((v) => v.toJson()).toList();
    }
    if (this.fileIds != null) {
      data['file_ids'] = this.fileIds.map((v) => v.toJson()).toList();
    }
    data['browser-window-title'] = this.browserWindowTitle;
    return data;
  }
}

class Sub {
  String sId;
  String sluggedName;
  String content;
  String name;
  String publicity;
  List<String> viewRoles;
  int id;
  String urlPath;
  List<Contents> contents;
  List<Sub> sub;
  String parentId;
  String browserWindowTitle;

  Sub(
      {this.sluggedName,
        this.sId,
        this.content,
        this.name,
        this.publicity,
        this.viewRoles,
        this.id,
        this.sub,
        this.parentId,
        this.urlPath,
        this.contents,
        this.browserWindowTitle});

  Sub.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    sluggedName = json['slugged-name'];
    content = json['content'];
    name = json['name'];
    publicity = json['publicity'];
    parentId = json['parent_id'];
    viewRoles = json['view-roles'].cast<String>();
    id = json['id'];
    urlPath = json['url-path'];
    if (json['sub'] != null) {
      sub = new List<Sub>();
      json['sub'].forEach((v) {
        sub.add(new Sub.fromJson(v));
      });
    }
    if (json['contents'] != null) {
      contents = new List<Contents>();
      json['contents'].forEach((v) {
        contents.add(new Contents.fromJson(v));
      });
    }
    browserWindowTitle = json['browser-window-title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slugged-name'] = this.sluggedName;
    data['content'] = this.content;
    data['name'] = this.name;
    data['publicity'] = this.publicity;
    data['view-roles'] = this.viewRoles;
    data['id'] = this.id;
    data['parent_id'] = this.parentId;
    data['_id'] = this.sId;
    data['url-path'] = this.urlPath;
    if (this.contents != null) {
      data['contents'] = this.contents.map((v) => v.toJson()).toList();
    }
    if (this.sub != null) {
      data['sub'] = this.sub.map((v) => v.toJson()).toList();
    }
    data['browser-window-title'] = this.browserWindowTitle;
    return data;
  }
}

class Contents {
  int id;
  String name;
  String sluggedName;
  String content;
  String browserWindowTitle;
  String urlPath;
  String publicity;
  List<String> viewRoles;

  Contents(
      {this.id,
        this.name,
        this.sluggedName,
        this.content,
        this.browserWindowTitle,
        this.urlPath,
        this.publicity,
        this.viewRoles});

  Contents.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    sluggedName = json['slugged-name'];
    content = json['content'];
    browserWindowTitle = json['browser-window-title'];
    urlPath = json['url-path'];
    publicity = json['publicity'];
    viewRoles = json['view-roles'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slugged-name'] = this.sluggedName;
    data['content'] = this.content;
    data['browser-window-title'] = this.browserWindowTitle;
    data['url-path'] = this.urlPath;
    data['publicity'] = this.publicity;
    data['view-roles'] = this.viewRoles;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class FileIds {
  String deletedAt;
  String deletedBy;
  String sId;
  String name;
  String url;
  String filetype;
  String createdBy;
  DateTime createdAt;
  String updatedAt;
  FileMeta meta;
  bool isDownloaded;
  int iV;

  FileIds(
      {this.deletedAt,
        this.deletedBy,
        this.sId,
        this.name,
        this.meta,
        this.url,
        this.filetype,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV});

  FileIds.fromJson(Map<String, dynamic> json) {
    meta = json['meta'] != null ? new FileMeta.fromJson(json['meta']) : null;
    deletedAt = json['deleted_at'];
    deletedBy = json['deleted_by'];
    sId = json['_id'];
    name = json['name'];
    url = json['url'];
    filetype = json['filetype'];
    createdBy = json['created_by'];
    createdAt = DateTime.parse(json['created_at']);
    updatedAt = json['updated_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    data['deleted_at'] = this.deletedAt;
    data['deleted_by'] = this.deletedBy;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['url'] = this.url;
    data['filetype'] = this.filetype;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt.toString();
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class FileMeta {
  String title;
  String description;
  int duration;
  int pages;
  String thumbnail;

  FileMeta(
      {this.title,
        this.description,
        this.duration,
        this.pages,
        this.thumbnail});

  FileMeta.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    duration = json['duration'];
    pages = json['pages'];
    thumbnail = json['thumbnail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['description'] = this.description;
    data['duration'] = this.duration;
    data['pages'] = this.pages;
    data['thumbnail'] = this.thumbnail;
    return data;
  }
}

class PlaylistContent {
  bool status;
  List<ContentData> data;

  PlaylistContent({this.status, this.data});

  PlaylistContent.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<ContentData>();
      json['data'].forEach((v) {
        data.add(new ContentData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ContentData {
  String videoTitle;
  int duration;
  String filename;
  bool isDownloaded;

  ContentData({this.videoTitle, this.duration, this.filename});

  ContentData.fromJson(Map<String, dynamic> json) {
    videoTitle = json['video-title'];
    duration = json['duration'];
    filename = json['filename'];
    isDownloaded = json['isDownloaded'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video-title'] = this.videoTitle;
    data['duration'] = this.duration;
    data['filename'] = this.filename;
    data['isDownloaded'] = this.isDownloaded;
    return data;
  }
}