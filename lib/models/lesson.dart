class Lesson {
  bool status;
  Data data;
  Meta meta;

  Lesson({this.status, this.data, this.meta});

  Lesson.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<LessonItems> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<LessonItems>();
      json['items'].forEach((v) {
        items.add(new LessonItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LessonItems {
  String lessonType;
  String sId;
  String name;
  RombelId rombelId;
  String slug;
  String lessonGroup;
  TeacherId teacherId;
  String institutionId;
  int periode;
  String createdBy;
  String createdAt;
  String updatedAt;
  String materialId;
  int iV;

  LessonItems(
      {this.lessonType,
      this.sId,
      this.name,
      this.rombelId,
      this.slug,
      this.lessonGroup,
      this.teacherId,
      this.institutionId,
      this.periode,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.materialId,
      this.iV});

  LessonItems.fromJson(Map<String, dynamic> json) {
    lessonType = json['lesson_type'];
    sId = json['_id'];
    name = json['name'];
    rombelId = json['rombel_id'] != null
        ? new RombelId.fromJson(json['rombel_id'])
        : null;
    slug = json['slug'];
    lessonGroup = json['lesson_group'];
    teacherId = json['teacher_id'] != null
        ? new TeacherId.fromJson(json['teacher_id'])
        : null;
    institutionId = json['institution_id'];
    periode = json['periode'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    materialId = json['material_id'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lesson_type'] = this.lessonType;
    data['_id'] = this.sId;
    data['name'] = this.name;
    if (this.rombelId != null) {
      data['rombel_id'] = this.rombelId.toJson();
    }
    data['slug'] = this.slug;
    data['lesson_group'] = this.lessonGroup;
    if (this.teacherId != null) {
      data['teacher_id'] = this.teacherId.toJson();
    }
    data['institution_id'] = this.institutionId;
    data['periode'] = this.periode;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['material_id'] = this.materialId;
    data['__v'] = this.iV;
    return data;
  }
}

class RombelId {
  String rombelType;
  List<String> lessonIds;
  String sId;
  String name;
  String department;
  String grade;
  int periode;
  String curriculum;
  String teacherId;
  String institutionId;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String teacherName;

  RombelId(
      {this.rombelType,
      this.lessonIds,
      this.sId,
      this.name,
      this.department,
      this.grade,
      this.periode,
      this.curriculum,
      this.teacherId,
      this.institutionId,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.teacherName});

  RombelId.fromJson(Map<String, dynamic> json) {
    rombelType = json['rombel_type'];
    lessonIds = json['lesson_ids'].cast<String>();
    sId = json['_id'];
    name = json['name'];
    department = json['department'];
    grade = json['grade'];
    periode = json['periode'];
    curriculum = json['curriculum'];
    teacherId = json['teacher_id'];
    institutionId = json['institution_id'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    teacherName = json['teacher_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rombel_type'] = this.rombelType;
    data['lesson_ids'] = this.lessonIds;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['department'] = this.department;
    data['grade'] = this.grade;
    data['periode'] = this.periode;
    data['curriculum'] = this.curriculum;
    data['teacher_id'] = this.teacherId;
    data['institution_id'] = this.institutionId;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['teacher_name'] = this.teacherName;
    return data;
  }
}

class TeacherId {
  Zenius zenius;
  List<int> role;
  List<Null> studentAtInstitutions;
  List<String> teacherAtInstitutions;
  List<Null> adminAtInstitutions;
  List<Null> rombelIds;
  String sId;
  String username;
  String email;
  String password;
  String displayName;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;

  TeacherId(
      {this.zenius,
      this.role,
      this.studentAtInstitutions,
      this.teacherAtInstitutions,
      this.adminAtInstitutions,
      this.rombelIds,
      this.sId,
      this.username,
      this.email,
      this.password,
      this.displayName,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV});

  TeacherId.fromJson(Map<String, dynamic> json) {
    zenius =
        json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    role = json['role'].cast<int>();
    // if (json['student_at_institutions'] != null) {
    //   studentAtInstitutions = new List<Null>();
    //   json['student_at_institutions'].forEach((v) {
    //     studentAtInstitutions.add(new Null.fromJson(v));
    //   });
    // }
    teacherAtInstitutions = json['teacher_at_institutions'].cast<String>();
    // if (json['admin_at_institutions'] != null) {
    //   adminAtInstitutions = new List<Null>();
    //   json['admin_at_institutions'].forEach((v) {
    //     adminAtInstitutions.add(new Null.fromJson(v));
    //   });
    // }
    // if (json['rombel_ids'] != null) {
    //   rombelIds = new List<Null>();
    //   json['rombel_ids'].forEach((v) {
    //     rombelIds.add(new Null.fromJson(v));
    //   });
    // }
    sId = json['_id'];
    username = json['username'];
    email = json['email'];
    password = json['password'];
    displayName = json['display_name'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['role'] = this.role;
    // if (this.studentAtInstitutions != null) {
    //   data['student_at_institutions'] =
    //       this.studentAtInstitutions.map((v) => v.toJson()).toList();
    // }
    data['teacher_at_institutions'] = this.teacherAtInstitutions;
    // if (this.adminAtInstitutions != null) {
    //   data['admin_at_institutions'] =
    //       this.adminAtInstitutions.map((v) => v.toJson()).toList();
    // }
    // if (this.rombelIds != null) {
    //   data['rombel_ids'] = this.rombelIds.map((v) => v.toJson()).toList();
    // }
    data['_id'] = this.sId;
    data['username'] = this.username;
    data['email'] = this.email;
    data['password'] = this.password;
    data['display_name'] = this.displayName;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Zenius {
  bool isActive;
  bool isPremium;

  Zenius({this.isActive, this.isPremium});

  Zenius.fromJson(Map<String, dynamic> json) {
    isActive = json['is_active'];
    isPremium = json['is_premium'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_active'] = this.isActive;
    data['is_premium'] = this.isPremium;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}