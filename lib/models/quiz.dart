class Quiz {
  bool status;
  DataQuiz data;
  Meta meta;

  Quiz({this.status, this.data, this.meta});

  Quiz.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new DataQuiz.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class DataQuiz {
  String sId;
  List<LessonIds> lessonIds;
  List<QuestionIds> questionIds;
  InstitutionId institutionId;
  String title;
  Period period;
  String quizType;
  int endTime;
  int totalQuestion;
  String status;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  List<ResultQuiz> resultQuiz;
  bool isUploaded;

  DataQuiz(
      {this.sId,
      this.lessonIds,
      this.questionIds,
      this.institutionId,
      this.isUploaded,
      this.title,
      this.period,
      this.quizType,
      this.endTime,
      this.totalQuestion,
      this.status,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.updatedBy,
      this.resultQuiz});

  DataQuiz.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    if (json['lesson_ids'] != null) {
      lessonIds = new List<LessonIds>();
      json['lesson_ids'].forEach((v) {
        lessonIds.add(new LessonIds.fromJson(v));
      });
    }
    if (json['question_ids'] != null) {
      questionIds = new List<QuestionIds>();
      json['question_ids'].forEach((v) {
        questionIds.add(new QuestionIds.fromJson(v));
      });
    }
    institutionId = json['institution_id'] != null
        ? new InstitutionId.fromJson(json['institution_id'])
        : null;
    title = json['title'];
    period =
        json['period'] != null ? new Period.fromJson(json['period']) : null;
    quizType = json['quiz_type'];
    endTime = json['end_time'];
    totalQuestion = json['total_question'];
    status = json['status'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    isUploaded = json['isUploaded'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    if (json['result'] != null) {
      resultQuiz = new List<ResultQuiz>();
      json['result'].forEach((v) {
        resultQuiz.add(new ResultQuiz.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.lessonIds != null) {
      data['lesson_ids'] = this.lessonIds.map((v) => v.toJson()).toList();
    }
    if (this.questionIds != null) {
      data['question_ids'] = this.questionIds.map((v) => v.toJson()).toList();
    }
    if (this.institutionId != null) {
      data['institution_id'] = this.institutionId.toJson();
    }
    data['title'] = this.title;
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['quiz_type'] = this.quizType;
    data['end_time'] = this.endTime;
    data['total_question'] = this.totalQuestion;
    data['status'] = this.status;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isUploaded'] = this.isUploaded;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    if (this.resultQuiz != null) {
      data['result'] = this.resultQuiz.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LessonIds {
  String sId;
  String lessonType;
  String name;
  String rombelId;
  String slug;
  String lessonGroup;
  String teacherId;
  String institutionId;
  int periode;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  ZeniusMaterial zeniusMaterial;
  ZeniusContent zeniusContent;

  LessonIds(
      {this.sId,
      this.lessonType,
      this.name,
      this.rombelId,
      this.slug,
      this.lessonGroup,
      this.teacherId,
      this.institutionId,
      this.periode,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.updatedBy,
      this.zeniusMaterial,
      this.zeniusContent});

  LessonIds.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    lessonType = json['lesson_type'];
    name = json['name'];
    rombelId = json['rombel_id'];
    slug = json['slug'];
    lessonGroup = json['lesson_group'];
    teacherId = json['teacher_id'];
    institutionId = json['institution_id'];
    periode = json['periode'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    zeniusMaterial = json['zenius_material'] != null
        ? new ZeniusMaterial.fromJson(json['zenius_material'])
        : null;
    zeniusContent = json['zenius_content'] != null
        ? new ZeniusContent.fromJson(json['zenius_content'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['lesson_type'] = this.lessonType;
    data['name'] = this.name;
    data['rombel_id'] = this.rombelId;
    data['slug'] = this.slug;
    data['lesson_group'] = this.lessonGroup;
    data['teacher_id'] = this.teacherId;
    data['institution_id'] = this.institutionId;
    data['periode'] = this.periode;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    if (this.zeniusMaterial != null) {
      data['zenius_material'] = this.zeniusMaterial.toJson();
    }
    if (this.zeniusContent != null) {
      data['zenius_content'] = this.zeniusContent.toJson();
    }
    return data;
  }
}

class ZeniusMaterial {
  String sluggedName;
  List<Null> sub;
  String content;
  String name;
  String publicity;
  List<String> viewRoles;
  String contentsSequence;
  int id;
  String urlPath;
  List<Contents> contents;
  String browserWindowTitle;

  ZeniusMaterial(
      {this.sluggedName,
      this.sub,
      this.content,
      this.name,
      this.publicity,
      this.viewRoles,
      this.contentsSequence,
      this.id,
      this.urlPath,
      this.contents,
      this.browserWindowTitle});

  ZeniusMaterial.fromJson(Map<String, dynamic> json) {
    sluggedName = json['slugged-name'];
    if (json['sub'] != null) {
      sub = new List<Null>();
      json['sub'].forEach((v) {
        // sub.add(new Null.fromJson(v));
      });
    }
    content = json['content'];
    name = json['name'];
    publicity = json['publicity'];
    viewRoles = json['view-roles'].cast<String>();
    contentsSequence = json['contents-sequence'];
    id = json['id'];
    urlPath = json['url-path'];
    if (json['contents'] != null) {
      contents = new List<Contents>();
      json['contents'].forEach((v) {
        contents.add(new Contents.fromJson(v));
      });
    }
    browserWindowTitle = json['browser-window-title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slugged-name'] = this.sluggedName;
    if (this.sub != null) {
      // data['sub'] = this.sub.map((v) => v.toJson()).toList();
    }
    data['content'] = this.content;
    data['name'] = this.name;
    data['publicity'] = this.publicity;
    data['view-roles'] = this.viewRoles;
    data['contents-sequence'] = this.contentsSequence;
    data['id'] = this.id;
    data['url-path'] = this.urlPath;
    if (this.contents != null) {
      data['contents'] = this.contents.map((v) => v.toJson()).toList();
    }
    data['browser-window-title'] = this.browserWindowTitle;
    return data;
  }
}

class Contents {
  int id;
  String name;
  String sluggedName;
  String content;
  String browserWindowTitle;
  String urlPath;
  String publicity;
  List<String> viewRoles;

  Contents(
      {this.id,
      this.name,
      this.sluggedName,
      this.content,
      this.browserWindowTitle,
      this.urlPath,
      this.publicity,
      this.viewRoles});

  Contents.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    sluggedName = json['slugged-name'];
    content = json['content'];
    browserWindowTitle = json['browser-window-title'];
    urlPath = json['url-path'];
    publicity = json['publicity'];
    viewRoles = json['view-roles'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slugged-name'] = this.sluggedName;
    data['content'] = this.content;
    data['browser-window-title'] = this.browserWindowTitle;
    data['url-path'] = this.urlPath;
    data['publicity'] = this.publicity;
    data['view-roles'] = this.viewRoles;
    return data;
  }
}

class ZeniusContent {
  List<Video> video;

  ZeniusContent({this.video});

  ZeniusContent.fromJson(Map<String, dynamic> json) {
    if (json['video'] != null) {
      video = new List<Video>();
      json['video'].forEach((v) {
        video.add(new Video.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.video != null) {
      data['video'] = this.video.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Video {
  String videoTitle;
  int duration;
  String filename;

  Video({this.videoTitle, this.duration, this.filename});

  Video.fromJson(Map<String, dynamic> json) {
    videoTitle = json['video-title'];
    duration = json['duration'];
    filename = json['filename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video-title'] = this.videoTitle;
    data['duration'] = this.duration;
    data['filename'] = this.filename;
    return data;
  }
}

class QuestionIds {
  String sId;
  String quizId;
  String userId;
  List<Null> tags;
  String text;
  Choices choices;
  String keyAnswer;        
  String institutionId;    
  String createdBy;
  String type;
  int iV;

  QuestionIds(
      {this.sId,
      this.quizId,
      this.tags,
      this.text,
      this.choices,
      this.keyAnswer,      
      this.institutionId,    
      this.type,
      this.userId,
      this.createdBy,      
      this.iV});

  QuestionIds.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    quizId = json['quiz_id'];
    if (json['tags'] != null) {
      tags = new List<Null>();
      json['tags'].forEach((v) {
        // tags.add(new Null.fromJson(v));
      });
    }
    text = json['text'];
    userId = json['user_id'];
    choices =
        json['choices'] != null ? new Choices.fromJson(json['choices']) : null;
    keyAnswer = json['key_answer'];  
    type = json['type'];
    institutionId = json['institution_id'];        
    createdBy = json['created_by'];  
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['quiz_id'] = this.quizId;
    if (this.tags != null) {
      // data['tags'] = this.tags.map((v) => v.toJson()).toList();
    }
    data['text'] = this.text;
    if (this.choices != null) {
      data['choices'] = this.choices.toJson();
    }
    data['key_answer'] = this.keyAnswer;
    data['user_id'] = this.userId;
    data['institution_id'] = this.institutionId;    
    data['created_by'] = this.createdBy;
    data['type'] = this.type;
    data['__v'] = this.iV;
    return data;
  }
}

class Choices {
  String a;
  String b;
  String c;
  String d;
  String e;

  Choices({this.a, this.b, this.c, this.d});

  Choices.fromJson(Map<String, dynamic> json) {
    a = json['A'];
    b = json['B'];
    c = json['C'];
    d = json['D'];
    e = json['E'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['A'] = this.a;
    data['B'] = this.b;
    data['C'] = this.c;
    data['D'] = this.d;
    data['E'] = this.e;
    return data;
  }
}

class Period {
  int year;
  int semester;

  Period({this.year, this.semester});

  Period.fromJson(Map<String, dynamic> json) {
    year = json['year'];
    semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['year'] = this.year;
    data['semester'] = this.year;
    return data;
  }
}

class ContentData {
  String contentName;
  String contentPrivilege;
  String cgPrivilege;
  String imXPqLabel;
  List<Null> chapter;
  int answerVideoId;
  List<String> label;
  int contentId;
  String pqPicture;
  int cgId;
  String problemDisplayFormatPreference;
  String answerType;
  String subject;
  int pqId;

  ContentData(
      {this.contentName,
      this.contentPrivilege,
      this.cgPrivilege,
      this.imXPqLabel,
      this.chapter,
      this.answerVideoId,
      this.label,
      this.contentId,
      this.pqPicture,
      this.cgId,
      this.problemDisplayFormatPreference,
      this.answerType,
      this.subject,
      this.pqId});

  ContentData.fromJson(Map<String, dynamic> json) {
    contentName = json['content-name'];
    contentPrivilege = json['content-privilege'];
    cgPrivilege = json['cg-privilege'];
    imXPqLabel = json['im_x_pq-label'];
    if (json['chapter'] != null) {
      chapter = new List<Null>();
      json['chapter'].forEach((v) {
        // chapter.add(new Null.fromJson(v));
      });
    }
    answerVideoId = json['answer-video-id'];
    label = json['label'].cast<String>();
    contentId = json['content-id'];
    pqPicture = json['pq-picture'];
    cgId = json['cg-id'];
    problemDisplayFormatPreference = json['problem-display-format-preference'];
    answerType = json['answer-type'];
    subject = json['subject'];
    pqId = json['pq-id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content-name'] = this.contentName;
    data['content-privilege'] = this.contentPrivilege;
    data['cg-privilege'] = this.cgPrivilege;
    data['im_x_pq-label'] = this.imXPqLabel;
    if (this.chapter != null) {
      // data['chapter'] = this.chapter.map((v) => v.toJson()).toList();
    }
    data['answer-video-id'] = this.answerVideoId;
    data['label'] = this.label;
    data['content-id'] = this.contentId;
    data['pq-picture'] = this.pqPicture;
    data['cg-id'] = this.cgId;
    data['problem-display-format-preference'] =
        this.problemDisplayFormatPreference;
    data['answer-type'] = this.answerType;
    data['subject'] = this.subject;
    data['pq-id'] = this.pqId;
    return data;
  }
}

class InstitutionId {
  String sId;
  Zenius zenius;
  int entrant;
  String name;
  String type;
  String country;
  String province;
  String city;
  String district;
  String postalCode;
  int phone;
  String email;
  String address;
  Director director;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String logo;
  String updatedBy;

  InstitutionId(
      {this.sId,
      this.zenius,
      this.entrant,
      this.name,
      this.type,
      this.country,
      this.province,
      this.city,
      this.district,
      this.postalCode,
      this.phone,
      this.email,
      this.address,
      this.director,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.logo,
      this.updatedBy});

  InstitutionId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    zenius =
        json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    entrant = json['entrant'];
    name = json['name'];
    type = json['type'];
    country = json['country'];
    province = json['province'];
    city = json['city'];
    district = json['district'];
    postalCode = json['postal_code'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    director = json['director'] != null
        ? new Director.fromJson(json['director'])
        : null;
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    logo = json['logo'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['entrant'] = this.entrant;
    data['name'] = this.name;
    data['type'] = this.type;
    data['country'] = this.country;
    data['province'] = this.province;
    data['city'] = this.city;
    data['district'] = this.district;
    data['postal_code'] = this.postalCode;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    if (this.director != null) {
      data['director'] = this.director.toJson();
    }
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['logo'] = this.logo;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class Zenius {
  int premiumQuota;
  String status;

  Zenius({this.premiumQuota, this.status});

  Zenius.fromJson(Map<String, dynamic> json) {
    premiumQuota = json['premium_quota'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium_quota'] = this.premiumQuota;
    data['status'] = this.status;
    return data;
  }
}

class Director {
  String name;
  String ktpNumber;
  String hp;
  String email;

  Director({this.name, this.ktpNumber, this.hp, this.email});

  Director.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    ktpNumber = json['ktp_number'];
    hp = json['hp'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['ktp_number'] = this.ktpNumber;
    data['hp'] = this.hp;
    data['email'] = this.email;
    return data;
  }
}

class ResultQuiz {
  Period period;
  List<StudentAnswers> studentAnswers;
  List<Null> kds;
  String sId;
  String quizId;
  String rombelId;
  String studentId;
  dynamic remaining;
  String status;
  String blockedStatus;
  dynamic score;
  String institutionId;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  int correct;
  int unanswers;
  String updatedBy;
  int wrong;

  ResultQuiz(
      {this.period,
      this.studentAnswers,
      this.kds,
      this.sId,
      this.quizId,
      this.rombelId,
      this.studentId,
      this.remaining,
      this.status,
      this.blockedStatus,
      this.score,
      this.institutionId,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.correct,
      this.unanswers,
      this.updatedBy,
      this.wrong});

  ResultQuiz.fromJson(Map<String, dynamic> json) {
    period =
        json['period'] != null ? new Period.fromJson(json['period']) : null;
    if (json['student_answers'] != null) {
      studentAnswers = new List<StudentAnswers>();
      json['student_answers'].forEach((v) {
        studentAnswers.add(new StudentAnswers.fromJson(v));
      });
    }
    if (json['kds'] != null) {
      kds = new List<Null>();
      json['kds'].forEach((v) {
        // kds.add(new Null.fromJson(v));
      });
    }
    sId = json['_id'];
    quizId = json['quiz_id'];
    rombelId = json['rombel_id'];
    studentId = json['student_id'];
    remaining = json['remaining'];
    status = json['status'];
    blockedStatus = json['blocked_status'];
    score = json['score'];
    institutionId = json['institution_id'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    correct = json['correct'];
    unanswers = json['unanswers'];
    updatedBy = json['updated_by'];
    wrong = json['wrong'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    if (this.studentAnswers != null) {
      data['student_answers'] =
          this.studentAnswers.map((v) => v.toJson()).toList();
    }
    if (this.kds != null) {
      // data['kds'] = this.kds.map((v) => v.toJson()).toList();
    }
    data['_id'] = this.sId;
    data['quiz_id'] = this.quizId;
    data['rombel_id'] = this.rombelId;
    data['student_id'] = this.studentId;
    data['remaining'] = this.remaining;
    data['status'] = this.status;
    data['blocked_status'] = this.blockedStatus;
    data['score'] = this.score;
    data['institution_id'] = this.institutionId;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['correct'] = this.correct;
    data['unanswers'] = this.unanswers;
    data['updated_by'] = this.updatedBy;
    data['wrong'] = this.wrong;
    return data;
  }
}

class StudentAnswers {
  String id;
  String answer;
  String state;

  StudentAnswers({this.id, this.answer, this.state});

  StudentAnswers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    answer = json['answer'];
    state = json['state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['answer'] = this.answer;
    data['state'] = this.state;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}