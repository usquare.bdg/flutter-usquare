class Assignment {
  bool status;
  Data data;
  Meta meta;

  Assignment({this.status, this.data, this.meta});

  Assignment.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<Items> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String sId;
  List<LessonIds> lessonIds;
  List<Null> fileIds;
  List<Null> lessonGroup;
  List<Null> grade;
  List<Null> department;
  String institutionId;
  String title;
  String content;
  DateTime dueDate;
  Period period;
  String status;
  String assignmentType;
  String addToSummary;
  String recipient;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  String id;

  Items(
      {this.sId,
        this.lessonIds,
        this.fileIds,
        this.lessonGroup,
        this.grade,
        this.department,
        this.institutionId,
        this.title,
        this.content,
        this.dueDate,
        this.period,
        this.status,
        this.assignmentType,
        this.addToSummary,
        this.recipient,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy,
        this.id});

  Items.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    if (json['lesson_ids'] != null) {
      lessonIds = new List<LessonIds>();
      json['lesson_ids'].forEach((v) {
        lessonIds.add(new LessonIds.fromJson(v));
      });
    }
    if (json['file_ids'] != null) {
      fileIds = new List<Null>();
//      json['file_ids'].forEach((v) {
//        fileIds.add(new Null.fromJson(v));
//      });
    }
    if (json['lesson_group'] != null) {
      lessonGroup = new List<Null>();
//      json['lesson_group'].forEach((v) {
//        lessonGroup.add(new Null.fromJson(v));
//      });
    }
    if (json['grade'] != null) {
      grade = new List<Null>();
//      json['grade'].forEach((v) {
//        grade.add(new Null.fromJson(v));
//      });
    }
    if (json['department'] != null) {
      department = new List<Null>();
//      json['department'].forEach((v) {
//        department.add(new Null.fromJson(v));
//      });
    }
    institutionId = json['institution_id'];
    title = json['title'];
    content = json['content'];
    dueDate = DateTime.parse(json['due_date']);
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    status = json['status'];
    assignmentType = json['assignment_type'];
    addToSummary = json['add_to_summary'];
    recipient = json['recipient'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.lessonIds != null) {
      data['lesson_ids'] = this.lessonIds.map((v) => v.toJson()).toList();
    }
    if (this.fileIds != null) {
//      data['file_ids'] = this.fileIds.map((v) => v.toJson()).toList();
    }
    if (this.lessonGroup != null) {
//      data['lesson_group'] = this.lessonGroup.map((v) => v.toJson()).toList();
    }
    if (this.grade != null) {
//      data['grade'] = this.grade.map((v) => v.toJson()).toList();
    }
    if (this.department != null) {
//      data['department'] = this.department.map((v) => v.toJson()).toList();
    }
    data['institution_id'] = this.institutionId;
    data['title'] = this.title;
    data['content'] = this.content;
    data['due_date'] = this.dueDate.toString();
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['status'] = this.status;
    data['assignment_type'] = this.assignmentType;
    data['add_to_summary'] = this.addToSummary;
    data['recipient'] = this.recipient;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    data['id'] = this.id;
    return data;
  }
}

class LessonIds {
  String sId;
  String lessonType;
  String name;
  String rombelId;
  String slug;
  String lessonGroup;
  String teacherId;
  String institutionId;
  int periode;
  String createdBy;
  String createdAt;
  String updatedAt;
  String teacherName;
  int iV;

  LessonIds(
      {this.sId,
        this.lessonType,
        this.name,
        this.rombelId,
        this.slug,
        this.lessonGroup,
        this.teacherId,
        this.institutionId,
        this.periode,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.teacherName,
        this.iV});

  LessonIds.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    lessonType = json['lesson_type'];
    name = json['name'];
    rombelId = json['rombel_id'];
    slug = json['slug'];
    lessonGroup = json['lesson_group'];
    teacherId = json['teacher_id'];
    institutionId = json['institution_id'];
    periode = json['periode'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    teacherName = json['teacher_name'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['lesson_type'] = this.lessonType;
    data['name'] = this.name;
    data['rombel_id'] = this.rombelId;
    data['slug'] = this.slug;
    data['lesson_group'] = this.lessonGroup;
    data['teacher_id'] = this.teacherId;
    data['institution_id'] = this.institutionId;
    data['periode'] = this.periode;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['teacher_name'] = this.teacherName;
    data['__v'] = this.iV;
    return data;
  }
}

class Period {
  int year;
  int semester;

  Period({this.year, this.semester});

  Period.fromJson(Map<String, dynamic> json) {
    year = json['year'];
    semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['year'] = this.year;
    data['semester'] = this.semester;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class AssignmentBody {
  String institutionId;
  String assignmentId;
  String rombelId;
  String studentId;
  String title;
  String content;
  String fileIds;
  Period period;
  String status;
  int score;

  AssignmentBody(
      {this.institutionId,
        this.assignmentId,
        this.rombelId,
        this.studentId,
        this.title,
        this.content,
        this.fileIds,
        this.period,
        this.status,
        this.score});

  AssignmentBody.fromJson(Map<String, dynamic> json) {
    institutionId = json['institution_id'];
    assignmentId = json['assignment_id'];
    rombelId = json['rombel_id'];
    studentId = json['student_id'];
    title = json['title'];
    content = json['content'];
    fileIds = json['file_ids'];
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    status = json['status'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['institution_id'] = this.institutionId;
    data['assignment_id'] = this.assignmentId;
    data['rombel_id'] = this.rombelId;
    data['student_id'] = this.studentId;
    data['title'] = this.title;
    data['content'] = this.content;
    data['file_ids'] = this.fileIds;
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['status'] = this.status;
    data['score'] = this.score;
    return data;
  }
}