  class Login {
    final bool auth;
    final String token;
    final String id;

    Login({this.auth, this.token, this.id});

    factory Login.fromJson(Map<String, dynamic> json){
      return Login(
        auth: json['auth'],
        token: json['token'],
        id: json['_id']
      );
    }
  }

class User {
    bool status;
    Data data;
    Meta meta;

    User({this.status, this.data, this.meta});

    User.fromJson(Map<String, dynamic> json) {
      status = json['status'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
      meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['status'] = this.status;
      if (this.data != null) {
        data['data'] = this.data.toJson();
      }
      if (this.meta != null) {
        data['meta'] = this.meta.toJson();
      }
      return data;
    }
  }

  class Data {
    String sId;
    Zenius zenius;
    List<int> role;
    List<StudentAtInstitutions> studentAtInstitutions;
    List<StudentAtInstitutions> teacherAtInstitutions;
    List<StudentAtInstitutions> adminAtInstitutions;
    List<RombelIds> rombelIds;
    String username;
    String email;
    String password;
    String displayName;
    String phoneNumber;
    String createdBy;
    String createdAt;
    String updatedAt;
    int iV;

    Data(
        {this.sId,
          this.zenius,
          this.role,
          this.studentAtInstitutions,
          this.teacherAtInstitutions,
          this.adminAtInstitutions,
          this.rombelIds,
          this.username,
          this.email,
          this.password,
          this.phoneNumber,
          this.displayName,
          this.createdBy,
          this.createdAt,
          this.updatedAt,
          this.iV});

    Data.fromJson(Map<String, dynamic> json) {
      sId = json['_id'];
      zenius =
      json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
      role = json['role'].cast<int>();
      if (json['student_at_institutions'] != null) {
        studentAtInstitutions = new List<StudentAtInstitutions>();
        json['student_at_institutions'].forEach((v) {
          studentAtInstitutions.add(new StudentAtInstitutions.fromJson(v));
        });
      }
      if (json['teacher_at_institutions'] != null) {
        teacherAtInstitutions = new List<StudentAtInstitutions>();
        json['teacher_at_institutions'].forEach((v) {
          // teacherAtInstitutions.add(new Null.fromJson(v));
        });
      }
      if (json['admin_at_institutions'] != null) {
        adminAtInstitutions = new List<StudentAtInstitutions>();
        json['admin_at_institutions'].forEach((v) {
          // adminAtInstitutions.add(new Null.fromJson(v));
        });
      }
      if (json['rombel_ids'] != null) {
        rombelIds = new List<RombelIds>();
        json['rombel_ids'].forEach((v) {
          rombelIds.add(new RombelIds.fromJson(v));
        });
      }
      username = json['username'];
      email = json['email'];
      password = json['password'];
      displayName = json['display_name'];
      createdBy = json['created_by'];
      createdAt = json['created_at'];
      updatedAt = json['updated_at'];
      iV = json['__v'];
      phoneNumber = json['phone'];
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['_id'] = this.sId;
      if (this.zenius != null) {
        data['zenius'] = this.zenius.toJson();
      }
      data['role'] = this.role;
      if (this.studentAtInstitutions != null) {
        data['student_at_institutions'] =
            this.studentAtInstitutions.map((v) => v.toJson()).toList();
      }
      if (this.teacherAtInstitutions != null) {
         data['teacher_at_institutions'] =
             this.teacherAtInstitutions.map((v) => v.toJson()).toList();
      }
      if (this.adminAtInstitutions != null) {
         data['admin_at_institutions'] =
              this.adminAtInstitutions.map((v) => v.toJson()).toList();
      }
      if (this.rombelIds != null) {
        data['rombel_ids'] = this.rombelIds.map((v) => v.toJson()).toList();
      }
      data['username'] = this.username;
      data['email'] = this.email;
      data['password'] = this.password;
      data['display_name'] = this.displayName;
      data['created_by'] = this.createdBy;
      data['created_at'] = this.createdAt;
      data['updated_at'] = this.updatedAt;
      data['phone'] = this.phoneNumber;
      data['__v'] = this.iV;
      return data;
    }
  }

  class Zenius {
    bool isActive;
    bool isPremium;
    String status;

    Zenius({this.isActive, this.isPremium, this.status});

    Zenius.fromJson(Map<String, dynamic> json) {
      isActive = json['is_active'];
      isPremium = json['is_premium'];
      status = json['status'];
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['is_active'] = this.isActive;
      data['is_premium'] = this.isPremium;
      data['status'] = this.status;
      return data;
    }
  }

  class StudentAtInstitutions {
    String sId;
    Zenius zenius;
    int entrant;
    String name;
    String type;
    String country;
    String province;
    String city;
    String district;
    String postalCode;
    int phone;
    String email;
    String address;
    Director director;
    String createdBy;
    String createdAt;
    String updatedAt;
    int iV;
    String logo;
    String updatedBy;

    StudentAtInstitutions(
        {this.sId,
          this.zenius,
          this.entrant,
          this.name,
          this.type,
          this.country,
          this.province,
          this.city,
          this.district,
          this.postalCode,
          this.phone,
          this.email,
          this.address,
          this.director,
          this.createdBy,
          this.createdAt,
          this.updatedAt,
          this.iV,
          this.logo,
          this.updatedBy});

    StudentAtInstitutions.fromJson(Map<String, dynamic> json) {
      sId = json['_id'];
      zenius =
      json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
      entrant = json['entrant'];
      name = json['name'];
      type = json['type'];
      country = json['country'];
      province = json['province'];
      city = json['city'];
      district = json['district'];
      postalCode = json['postal_code'];
      phone = json['phone'];
      email = json['email'];
      address = json['address'];
      director = json['director'] != null
          ? new Director.fromJson(json['director'])
          : null;
      createdBy = json['created_by'];
      createdAt = json['created_at'];
      updatedAt = json['updated_at'];
      iV = json['__v'];
      logo = json['logo'];
      updatedBy = json['updated_by'];
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['_id'] = this.sId;
      if (this.zenius != null) {
        data['zenius'] = this.zenius.toJson();
      }
      data['entrant'] = this.entrant;
      data['name'] = this.name;
      data['type'] = this.type;
      data['country'] = this.country;
      data['province'] = this.province;
      data['city'] = this.city;
      data['district'] = this.district;
      data['postal_code'] = this.postalCode;
      data['phone'] = this.phone;
      data['email'] = this.email;
      data['address'] = this.address;
      if (this.director != null) {
        data['director'] = this.director.toJson();
      }
      data['created_by'] = this.createdBy;
      data['created_at'] = this.createdAt;
      data['updated_at'] = this.updatedAt;
      data['__v'] = this.iV;
      data['logo'] = this.logo;
      data['updated_by'] = this.updatedBy;
      return data;
    }
  }

  class ZeniusData {
    int premiumQuota;
    String status;

    ZeniusData({this.premiumQuota, this.status});

    ZeniusData.fromJson(Map<String, dynamic> json) {
      premiumQuota = json['premium_quota'];
      status = json['status'];
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['premium_quota'] = this.premiumQuota;
      data['status'] = this.status;
      return data;
    }
  }

  class Director {
    String name;
    String ktpNumber;
    String hp;
    String email;

    Director({this.name, this.ktpNumber, this.hp, this.email});

    Director.fromJson(Map<String, dynamic> json) {
      name = json['name'];
      ktpNumber = json['ktp_number'];
      hp = json['hp'];
      email = json['email'];
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['name'] = this.name;
      data['ktp_number'] = this.ktpNumber;
      data['hp'] = this.hp;
      data['email'] = this.email;
      return data;
    }
  }

  class RombelIds {
    String sId;
    String rombelType;
    List<String> lessonIds;
    String name;
    String department;
    String grade;
    int periode;
    String curriculum;
    String teacherId;
    String institutionId;
    String createdBy;
    String createdAt;
    String updatedAt;
    int iV;
    String teacherName;

    RombelIds(
        {this.sId,
          this.rombelType,
          this.lessonIds,
          this.name,
          this.department,
          this.grade,
          this.periode,
          this.curriculum,
          this.teacherId,
          this.institutionId,
          this.createdBy,
          this.createdAt,
          this.updatedAt,
          this.iV,
          this.teacherName});

    RombelIds.fromJson(Map<String, dynamic> json) {
      sId = json['_id'];
      rombelType = json['rombel_type'];
      lessonIds = json['lesson_ids'].cast<String>();
      name = json['name'];
      department = json['department'];
      grade = json['grade'];
      periode = json['periode'];
      curriculum = json['curriculum'];
      teacherId = json['teacher_id'];
      institutionId = json['institution_id'];
      createdBy = json['created_by'];
      createdAt = json['created_at'];
      updatedAt = json['updated_at'];
      iV = json['__v'];
      teacherName = json['teacher_name'];
    }

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['_id'] = this.sId;
      data['rombel_type'] = this.rombelType;
      data['lesson_ids'] = this.lessonIds;
      data['name'] = this.name;
      data['department'] = this.department;
      data['grade'] = this.grade;
      data['periode'] = this.periode;
      data['curriculum'] = this.curriculum;
      data['teacher_id'] = this.teacherId;
      data['institution_id'] = this.institutionId;
      data['created_by'] = this.createdBy;
      data['created_at'] = this.createdAt;
      data['updated_at'] = this.updatedAt;
      data['__v'] = this.iV;
      data['teacher_name'] = this.teacherName;
      return data;
    }
  }

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class CreateStudentBody {
  String email;
  String username;
  String password;
  String displayName;
  String institutionId;
  List<String> rombelIds;

  CreateStudentBody({this.email, this.username, this.password, this.displayName, this.institutionId, this.rombelIds});

  CreateStudentBody.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    username = json['username'];
    password = json['password'];
    displayName = json['display_name'];
    institutionId = json['institution_id'];
    rombelIds = json['rombel_ids'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['username'] = this.username;
    data['password'] = this.password;
    data['display_name'] = this.displayName;
    data['rombel_ids'] = this.rombelIds;
    data['institution_id'] = this.institutionId;
    return data;
  }
}