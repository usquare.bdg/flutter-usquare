class Files {
  bool status;
  Data data;
  Meta meta;

  Files({this.status, this.data, this.meta});

  Files.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<Items> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  Null deletedAt;
  Null deletedBy;
  String sId;
  FileMeta meta;
  String name;
  String url;
  String filetype;
  String createdBy;
  DateTime createdAt;
  String updatedAt;
  int iV;

  Items(
      {this.deletedAt,
        this.deletedBy,
        this.sId,
        this.meta,
        this.name,
        this.url,
        this.filetype,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV});

  Items.fromJson(Map<String, dynamic> json) {
    deletedAt = json['deleted_at'];
    deletedBy = json['deleted_by'];
    sId = json['_id'];
    meta = json['meta'] != null ? new FileMeta.fromJson(json['meta']) : null;
    name = json['name'];
    url = json['url'];
    filetype = json['filetype'];
    createdBy = json['created_by'];
    createdAt = DateTime.parse(json['created_at']);
    updatedAt = json['updated_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deleted_at'] = this.deletedAt;
    data['deleted_by'] = this.deletedBy;
    data['_id'] = this.sId;
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    data['name'] = this.name;
    data['url'] = this.url;
    data['filetype'] = this.filetype;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt.toString();
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class FileMeta {
  String title;
  String description;
  String thumbnail;
  int duration;
  int pages;

  FileMeta(
      {this.title,
        this.description,
        this.thumbnail,
        this.duration,
        this.pages});

  FileMeta.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    thumbnail = json['thumbnail'];
    duration = json['duration'];
    pages = json['pages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['description'] = this.description;
    data['thumbnail'] = this.thumbnail;
    data['duration'] = this.duration;
    data['pages'] = this.pages;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class CreateFileRes {
  bool status;
  FileData data;
  Meta meta;

  CreateFileRes({this.status, this.data, this.meta});

  CreateFileRes.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new FileData.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class FileData {
  String institutionId;
  String name;
  String filetype;
  String createdBy;
  String sId;
  String createdAt;
  int iV;

  FileData(
      {this.institutionId,
        this.name,
        this.filetype,
        this.createdBy,
        this.sId,
        this.createdAt,
        this.iV});

  FileData.fromJson(Map<String, dynamic> json) {
    institutionId = json['institution_id'];
    name = json['name'];
    filetype = json['filetype'];
    createdBy = json['created_by'];
    sId = json['_id'];
    createdAt = json['created_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['institution_id'] = this.institutionId;
    data['name'] = this.name;
    data['filetype'] = this.filetype;
    data['created_by'] = this.createdBy;
    data['_id'] = this.sId;
    data['created_at'] = this.createdAt;
    data['__v'] = this.iV;
    return data;
  }
}
