import 'package:edubox_tryout/models/quiz.dart';

class QuizList {
	bool status;
	Data data;
	Meta meta;

	QuizList({this.status, this.data, this.meta});

	QuizList.fromJson(Map<String, dynamic> json) {
		status = json['status'];
		data = json['data'] != null ? new Data.fromJson(json['data']) : null;
		meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = this.status;
		if (this.data != null) {
      data['data'] = this.data.toJson();
    }
		if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
		return data;
	}
}

class Data {
	int count;
	List<Items> items;

	Data({this.count, this.items});

	Data.fromJson(Map<String, dynamic> json) {
		count = json['count'];
		if (json['items'] != null) {
			items = new List<Items>();
			json['items'].forEach((v) { items.add(new Items.fromJson(v)); });
		}
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['count'] = this.count;
		if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
		return data;
	}
}

class Items {
	String userId;
	String sId;
	List<LessonIds> lessonIds;
	List<StudentIds> studentIds;
	List<String> questionIds;
	List<QuestionIds> questionsIds;
	String lessonName;
	String institutionId;
	String title;
	Period period;
	String quizType;
	int endTime;
	int totalQuestion;
	String status;
	String createdBy;
	String createdAt;
	String updatedAt;
	int iV;
	String updatedBy;
	String passcode;
	String type;
	String openingCode;
	Result result;
	bool isBlocked;
	bool isOpened;
	bool isUploaded;
	bool randomSeq;
  bool showScoreOption;

	Items({
		this.sId,
		this.lessonIds,
		this.studentIds,
		this.questionIds,
		this.institutionId,
		this.title,
		this.period,
		this.quizType,
		this.endTime,
		this.totalQuestion,
		this.status,
		this.createdBy,
		this.createdAt,
		this.updatedAt,
		this.iV,
		this.type,
		this.openingCode,
		this.lessonName,
		this.passcode,
		this.isBlocked,
		this.isOpened,
		this.updatedBy,
		this.randomSeq,
		this.result,
		this.isUploaded,
    this.showScoreOption,
		this.userId
	});

	Items.fromJson(Map<String, dynamic> json) {
		sId = json['_id'];
		userId = json['user_id'];
		if (json['lesson_ids'] != null) {
			lessonIds = new List<LessonIds>();
			json['lesson_ids'].forEach((v) { lessonIds.add(new LessonIds.fromJson(v)); });
		}
		if (json['student_ids'] != null) {
			studentIds = new List<StudentIds>();
			json['student_ids'].forEach((v) {
				studentIds.add(new StudentIds.fromJson(v));
			});
		}
		if (json['question_ids'] != null) {
			questionIds = json['question_ids'].cast<String>();
		}
		institutionId = json['institution_id'];
		title = json['title'];
    showScoreOption = json['show_score_option'];
		period = json['period'] != null ? new Period.fromJson(json['period']) : null;
		quizType = json['quiz_type'];
		endTime = json['end_time'];
		totalQuestion = json['total_question'];
		status = json['status'];
		createdBy = json['created_by'];
		lessonName = json['lesson_name'];
		createdAt = json['created_at'];
		updatedAt = json['updated_at'];
		openingCode = json['kode_pembuka'];
		passcode = json['passcode'];
		isBlocked = json['isBlocked'];
		isOpened = json['isOpened'];
		type = json['type'];
		iV = json['__v'];
		updatedBy = json['updated_by'];
		result = json['result'] != null ? new Result.fromJson(json['result']) : null;
		isUploaded = json['isUploaded'];
		if (json['questions_ids'] != null) {
			questionsIds = new List<QuestionIds>();
			json['questions_ids'].forEach((v) {
				questionsIds.add(new QuestionIds.fromJson(v));
			});
		}
		randomSeq = json['random_seq'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['_id'] = this.sId;
		data['user_id'] = this.userId;
		if (this.lessonIds != null) {
      data['lesson_ids'] = this.lessonIds.map((v) => v.toJson()).toList();
    }
		if (this.studentIds != null) {
			data['student_ids'] = this.studentIds.map((v) => v.toJson()).toList();
		}
		if (this.questionsIds != null) {
			data['questions_ids'] = this.questionsIds.map((v) => v.toJson()).toList();
		}
		data['question_ids'] = this.questionIds;
		data['institution_id'] = this.institutionId;
		data['lesson_name'] = this.lessonName;
		data['title'] = this.title;
		if (this.period != null) {
      data['period'] = this.period.toJson();
    }
		data['quiz_type'] = this.quizType;
		data['end_time'] = this.endTime;
		data['total_question'] = this.totalQuestion;
		data['status'] = this.status;
		data['created_by'] = this.createdBy;
		data['created_at'] = this.createdAt;
		data['updated_at'] = this.updatedAt;
    data['show_score_option'] = this.showScoreOption;
		data['__v'] = this.iV;
		data['updated_by'] = this.updatedBy;
		data['type'] = this.type;
		data['isBlocked'] = this.isBlocked;
		data['isOpened'] = this.isOpened;
		data['kode_pembuka'] = this.openingCode;
		data['passcode'] = this.passcode;
		if (this.result != null) {
      data['result'] = this.result.toJson();
    }
		data['isUploaded'] = this.isUploaded;
		data['random_seq'] = this.randomSeq;
		return data;
	}
}

class LessonIds {
	String sId;
	String lessonType;
	String name;
	String rombelId;
	String slug;
	String lessonGroup;
	String teacherId;
	String institutionId;
	int periode;
	String createdBy;
	String createdAt;
	String updatedAt;
	int iV;
	String updatedBy;
	ZeniusMaterial zeniusMaterial;	

	LessonIds({this.sId, this.lessonType, this.name, this.rombelId, this.slug, this.lessonGroup, this.teacherId, this.institutionId, this.periode, this.createdBy, this.createdAt, this.updatedAt, this.iV, this.updatedBy, this.zeniusMaterial});

	LessonIds.fromJson(Map<String, dynamic> json) {
		sId = json['_id'];
		lessonType = json['lesson_type'];
		name = json['name'];
		rombelId = json['rombel_id'];
		slug = json['slug'];
		lessonGroup = json['lesson_group'];
		teacherId = json['teacher_id'];
		institutionId = json['institution_id'];
		periode = json['periode'];
		createdBy = json['created_by'];
		createdAt = json['created_at'];
		updatedAt = json['updated_at'];
		iV = json['__v'];
		updatedBy = json['updated_by'];
		zeniusMaterial = json['zenius_material'] != null ? new ZeniusMaterial.fromJson(json['zenius_material']) : null;		
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['_id'] = this.sId;
		data['lesson_type'] = this.lessonType;
		data['name'] = this.name;
		data['rombel_id'] = this.rombelId;
		data['slug'] = this.slug;
		data['lesson_group'] = this.lessonGroup;
		data['teacher_id'] = this.teacherId;
		data['institution_id'] = this.institutionId;
		data['periode'] = this.periode;
		data['created_by'] = this.createdBy;
		data['created_at'] = this.createdAt;
		data['updated_at'] = this.updatedAt;
		data['__v'] = this.iV;
		data['updated_by'] = this.updatedBy;
		if (this.zeniusMaterial != null) {
      data['zenius_material'] = this.zeniusMaterial.toJson();
    }
		return data;
	}
}

class StudentIds {
	String sId;
	Zenius zenius;
	List<int> role;
	List<String> studentAtInstitutions;
	List<Null> teacherAtInstitutions;
	List<Null> adminAtInstitutions;
	List<String> rombelIds;
	String username;
	String email;
	String password;
	String displayName;
	String createdBy;
	String createdAt;
	String updatedAt;
	String institutionId;
	int iV;
	String updatedBy;

	StudentIds(
			{this.sId,
				this.zenius,
				this.role,
				this.studentAtInstitutions,
				this.teacherAtInstitutions,
				this.adminAtInstitutions,
				this.rombelIds,
				this.username,
				this.email,
				this.password,
				this.displayName,
				this.createdBy,
				this.createdAt,
				this.updatedAt,
				this.institutionId,
				this.iV,
				this.updatedBy});

	StudentIds.fromJson(Map<String, dynamic> json) {
		sId = json['_id'];
		zenius =
		json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
		role = json['role'].cast<int>();
		studentAtInstitutions = json['student_at_institutions'].cast<String>();
//		if (json['teacher_at_institutions'] != null) {
//			teacherAtInstitutions = new List<Null>();
//			json['teacher_at_institutions'].forEach((v) {
//				teacherAtInstitutions.add(new Null.fromJson(v));
//			});
//		}
//		if (json['admin_at_institutions'] != null) {
//			adminAtInstitutions = new List<Null>();
//			json['admin_at_institutions'].forEach((v) {
//				adminAtInstitutions.add(new Null.fromJson(v));
//			});
//		}
		rombelIds = json['rombel_ids'].cast<String>();
		username = json['username'];
		email = json['email'];
		password = json['password'];
		displayName = json['display_name'];
		createdBy = json['created_by'];
		createdAt = json['created_at'];
		updatedAt = json['updated_at'];
		institutionId = json['institution_id'];
		iV = json['__v'];
		updatedBy = json['updated_by'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['_id'] = this.sId;
		if (this.zenius != null) {
			data['zenius'] = this.zenius.toJson();
		}
		data['role'] = this.role;
		data['student_at_institutions'] = this.studentAtInstitutions;
//		if (this.teacherAtInstitutions != null) {
//			data['teacher_at_institutions'] =
//					this.teacherAtInstitutions.map((v) => v.toJson()).toList();
//		}
//		if (this.adminAtInstitutions != null) {
//			data['admin_at_institutions'] =
//					this.adminAtInstitutions.map((v) => v.toJson()).toList();
//		}
		data['rombel_ids'] = this.rombelIds;
		data['username'] = this.username;
		data['email'] = this.email;
		data['password'] = this.password;
		data['display_name'] = this.displayName;
		data['created_by'] = this.createdBy;
		data['created_at'] = this.createdAt;
		data['updated_at'] = this.updatedAt;
		data['institution_id'] = this.institutionId;
		data['__v'] = this.iV;
		data['updated_by'] = this.updatedBy;
		return data;
	}
}

class ZeniusMaterial {
	String sluggedName;
	List<Null> sub;
	String content;
	String name;
	String publicity;
	List<String> viewRoles;
	String contentsSequence;
	int id;
	String urlPath;
	List<Contents> contents;
	String browserWindowTitle;

	ZeniusMaterial({this.sluggedName, this.sub, this.content, this.name, this.publicity, this.viewRoles, this.contentsSequence, this.id, this.urlPath, this.contents, this.browserWindowTitle});

	ZeniusMaterial.fromJson(Map<String, dynamic> json) {
		sluggedName = json['slugged-name'];
		if (json['sub'] != null) {
			sub = new List<Null>();
			// json['sub'].forEach((v) { sub.add(new Null.fromJson(v)); });
		}
		content = json['content'];
		name = json['name'];
		publicity = json['publicity'];
//		viewRoles = json['view-roles'].cast<String>();
		contentsSequence = json['contents-sequence'];
		id = json['id'];
		urlPath = json['url-path'];
		if (json['contents'] != null) {
			contents = new List<Contents>();
			json['contents'].forEach((v) { contents.add(new Contents.fromJson(v)); });
		}
		browserWindowTitle = json['browser-window-title'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['slugged-name'] = this.sluggedName;
		if (this.sub != null) {
      // data['sub'] = this.sub.map((v) => v.toJson()).toList();
    }
		data['content'] = this.content;
		data['name'] = this.name;
		data['publicity'] = this.publicity;
//		data['view-roles'] = this.viewRoles;
		data['contents-sequence'] = this.contentsSequence;
		data['id'] = this.id;
		data['url-path'] = this.urlPath;
		if (this.contents != null) {
      data['contents'] = this.contents.map((v) => v.toJson()).toList();
    }
		data['browser-window-title'] = this.browserWindowTitle;
		return data;
	}
}

class Contents {
	int id;
	String name;
	String sluggedName;
	String content;
	String browserWindowTitle;
	String urlPath;
	String publicity;
	List<String> viewRoles;

	Contents({this.id, this.name, this.sluggedName, this.content, this.browserWindowTitle, this.urlPath, this.publicity, this.viewRoles});

	Contents.fromJson(Map<String, dynamic> json) {
		id = json['id'];
		name = json['name'];
		sluggedName = json['slugged-name'];
		content = json['content'];
		browserWindowTitle = json['browser-window-title'];
		urlPath = json['url-path'];
		publicity = json['publicity'];
		viewRoles = json['view-roles'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = this.id;
		data['name'] = this.name;
		data['slugged-name'] = this.sluggedName;
		data['content'] = this.content;
		data['browser-window-title'] = this.browserWindowTitle;
		data['url-path'] = this.urlPath;
		data['publicity'] = this.publicity;
		data['view-roles'] = this.viewRoles;
		return data;
	}
}

class Period {
	int year;

	Period({this.year});

	Period.fromJson(Map<String, dynamic> json) {
		year = json['year'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['year'] = this.year;
		return data;
	}
}

class Result {
	PeriodResult periodResult;
	List<StudentAnswers> studentAnswers;
	List<Kds> kds;
	String sId;
	String institutionId;
	String quizId;
	String rombelId;
	String studentId;
	String status;
	int correct;
	int wrong;
	int unanswers;
	dynamic score;
	dynamic remaining;
	String blockedStatus;
	String createdBy;
	String createdAt;
	String updatedAt;
	int iV;

	Result({this.periodResult, this.studentAnswers, this.kds, this.sId, this.institutionId, this.quizId, this.rombelId, this.studentId, this.status, this.correct, this.wrong, this.unanswers, this.score, this.remaining, this.blockedStatus, this.createdBy, this.createdAt, this.updatedAt, this.iV});

	Result.fromJson(Map<String, dynamic> json) {
		periodResult = json['period'] != null ? new PeriodResult.fromJson(json['period']) : null;
		if (json['student_answers'] != null) {
			studentAnswers = new List<StudentAnswers>();
			json['student_answers'].forEach((v) { studentAnswers.add(new StudentAnswers.fromJson(v)); });
		}
		if (json['kds'] != null) {
			kds = new List<Kds>();
			json['kds'].forEach((v) { kds.add(new Kds.fromJson(v)); });
		}
		sId = json['_id'];
		institutionId = json['institution_id'];
		quizId = json['quiz_id'];
		rombelId = json['rombel_id'];
		studentId = json['student_id'];
		status = json['status'];
		correct = json['correct'];
		wrong = json['wrong'];
		unanswers = json['unanswers'];
		score = json['score'];
		remaining = json['remaining'];
		blockedStatus = json['blocked_status'];
		createdBy = json['created_by'];
		createdAt = json['created_at'];
		updatedAt = json['updated_at'];
		iV = json['__v'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.periodResult != null) {
      data['period'] = this.periodResult.toJson();
    }
		if (this.studentAnswers != null) {
      data['student_answers'] = this.studentAnswers.map((v) => v.toJson()).toList();
    }
		if (this.kds != null) {
      data['kds'] = this.kds.map((v) => v.toJson()).toList();
    }
		data['_id'] = this.sId;
		data['institution_id'] = this.institutionId;
		data['quiz_id'] = this.quizId;
		data['rombel_id'] = this.rombelId;
		data['student_id'] = this.studentId;
		data['status'] = this.status;
		data['correct'] = this.correct;
		data['wrong'] = this.wrong;
		data['unanswers'] = this.unanswers;
		data['score'] = this.score;
		data['remaining'] = this.remaining;
		data['blocked_status'] = this.blockedStatus;
		data['created_by'] = this.createdBy;
		data['created_at'] = this.createdAt;
		data['updated_at'] = this.updatedAt;
		data['__v'] = this.iV;
		return data;
	}
}

class PeriodResult {
	int year;
	int semester;

	PeriodResult({this.year, this.semester});

	PeriodResult.fromJson(Map<String, dynamic> json) {
		year = json['year'];
		semester = json['semester'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['year'] = this.year;
		data['semester'] = this.semester;
		return data;
	}
}

class StudentAnswers {
	String id;
	String answer;
	String state;

	StudentAnswers({this.id, this.answer, this.state});

	StudentAnswers.fromJson(Map<String, dynamic> json) {
		id = json['id'];
		answer = json['answer'];
		state = json['state'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = this.id;
		data['answer'] = this.answer;
		data['state'] = this.state;
		return data;
	}
}

class Kds {
	String hello;

	Kds({this.hello});

	Kds.fromJson(Map<String, dynamic> json) {
		hello = json['hello'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['hello'] = this.hello;
		return data;
	}
}

class Meta {
	String message;
	int code;

	Meta({this.message, this.code});

	Meta.fromJson(Map<String, dynamic> json) {
		message = json['message'];
		code = json['code'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['message'] = this.message;
		data['code'] = this.code;
		return data;
	}
}