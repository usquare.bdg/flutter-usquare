class QuizResult {
  bool status;
  Data data;
  Meta meta;

  QuizResult({this.status, this.data, this.meta});

  QuizResult.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int count;
  List<Items> items;

  Data({this.count, this.items});

  Data.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  Period period;
  List<StudentAnswers> studentAnswers;
  List<Null> kds;
  String sId;
  QuizId quizId;
  RombelId rombelId;
  StudentId studentId;
  String remaining;
  String status;
  String blockedStatus;
  dynamic score;
  String institutionId;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  int correct;
  int unanswers;
  int wrong;

  Items(
      {this.period,
        this.studentAnswers,
        this.kds,
        this.sId,
        this.quizId,
        this.rombelId,
        this.studentId,
        this.remaining,
        this.status,
        this.blockedStatus,
        this.score,
        this.institutionId,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy,
        this.correct,
        this.unanswers,
        this.wrong});

  Items.fromJson(Map<String, dynamic> json) {
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    if (json['student_answers'] != null) {
      studentAnswers = new List<StudentAnswers>();
      json['student_answers'].forEach((v) {
        studentAnswers.add(new StudentAnswers.fromJson(v));
      });
    }
    if (json['kds'] != null) {
      kds = new List<Null>();
      json['kds'].forEach((v) {
//        kds.add(new Null.fromJson(v));
      });
    }
    sId = json['_id'];
    quizId =
    json['quiz_id'] != null ? new QuizId.fromJson(json['quiz_id']) : null;
    rombelId = json['rombel_id'] != null
        ? new RombelId.fromJson(json['rombel_id'])
        : null;
    studentId = json['student_id'] != null
        ? new StudentId.fromJson(json['student_id'])
        : null;
    remaining = json['remaining'];
    status = json['status'];
    blockedStatus = json['blocked_status'];
    score = json['score'];
    institutionId = json['institution_id'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    correct = json['correct'];
    unanswers = json['unanswers'];
    wrong = json['wrong'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    if (this.studentAnswers != null) {
      data['student_answers'] =
          this.studentAnswers.map((v) => v.toJson()).toList();
    }
    if (this.kds != null) {
//      data['kds'] = this.kds.map((v) => v.toJson()).toList();
    }
    data['_id'] = this.sId;
    if (this.quizId != null) {
      data['quiz_id'] = this.quizId.toJson();
    }
    if (this.rombelId != null) {
      data['rombel_id'] = this.rombelId.toJson();
    }
    if (this.studentId != null) {
      data['student_id'] = this.studentId.toJson();
    }
    data['remaining'] = this.remaining;
    data['status'] = this.status;
    data['blocked_status'] = this.blockedStatus;
    data['score'] = this.score;
    data['institution_id'] = this.institutionId;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    data['correct'] = this.correct;
    data['unanswers'] = this.unanswers;
    data['wrong'] = this.wrong;
    return data;
  }
}

class Period {
  int year;
  int semester;

  Period({this.year, this.semester});

  Period.fromJson(Map<String, dynamic> json) {
    year = json['year'];
    semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['year'] = this.year;
    data['semester'] = this.semester;
    return data;
  }
}

class StudentAnswers {
  String id;
  String answer;
  String state;
  bool isCorrect;
  String type;

  StudentAnswers({this.id, this.answer, this.state});

  StudentAnswers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    answer = json['answer'];
    state = json['state'];
    isCorrect = json['isCorrect'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['answer'] = this.answer;
    data['state'] = this.state;
    data['isCorrect'] = this.isCorrect;
    data['type'] = this.type;
    return data;
  }
}

class QuizId {
  Period period;
  List<String> lessonIds;
  List<String> questionIds;
  List<String> lessonGroup;
  List<String> grade;
  List<String> department;
  String sId;
  String title;
  int endTime;
  String quizType;
  String institutionId;
  String status;
  bool randomSeq;
  bool isPublic;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  String passcode;
  String kodePembuka;
  bool showScoreOption;

  QuizId(
      {this.period,
        this.lessonIds,
        this.questionIds,
        this.lessonGroup,
        this.grade,
        this.department,
        this.sId,
        this.title,
        this.endTime,
        this.quizType,
        this.institutionId,
        this.status,
        this.randomSeq,
        this.isPublic,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy,
        this.passcode,
        this.kodePembuka,
        this.showScoreOption});

  QuizId.fromJson(Map<String, dynamic> json) {
    period =
    json['period'] != null ? new Period.fromJson(json['period']) : null;
    lessonIds = json['lesson_ids'].cast<String>();
    questionIds = json['question_ids'].cast<String>();
    lessonGroup = json['lesson_group'].cast<String>();
    grade = json['grade'].cast<String>();
    department = json['department'].cast<String>();
    sId = json['_id'];
    title = json['title'];
    endTime = json['end_time'];
    quizType = json['quiz_type'];
    institutionId = json['institution_id'];
    status = json['status'];
    randomSeq = json['random_seq'];
    isPublic = json['is_public'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    passcode = json['passcode'];
    kodePembuka = json['kode_pembuka'];
    showScoreOption = json['show_score_option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.period != null) {
      data['period'] = this.period.toJson();
    }
    data['lesson_ids'] = this.lessonIds;
    data['question_ids'] = this.questionIds;
    data['lesson_group'] = this.lessonGroup;
    data['grade'] = this.grade;
    data['department'] = this.department;
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['end_time'] = this.endTime;
    data['quiz_type'] = this.quizType;
    data['institution_id'] = this.institutionId;
    data['status'] = this.status;
    data['random_seq'] = this.randomSeq;
    data['is_public'] = this.isPublic;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    data['passcode'] = this.passcode;
    data['kode_pembuka'] = this.kodePembuka;
    data['show_score_option'] = this.showScoreOption;
    return data;
  }
}

class RombelId {
  String rombelType;
  List<String> lessonIds;
  String sId;
  String name;
  String department;
  String grade;
  int periode;
  String curriculum;
  String teacherId;
  String institutionId;
  String createdBy;
  String createdAt;
  String updatedAt;
  String teacherName;
  int iV;

  RombelId(
      {this.rombelType,
        this.lessonIds,
        this.sId,
        this.name,
        this.department,
        this.grade,
        this.periode,
        this.curriculum,
        this.teacherId,
        this.institutionId,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.teacherName,
        this.iV});

  RombelId.fromJson(Map<String, dynamic> json) {
    rombelType = json['rombel_type'];
    lessonIds = json['lesson_ids'].cast<String>();
    sId = json['_id'];
    name = json['name'];
    department = json['department'];
    grade = json['grade'];
    periode = json['periode'];
    curriculum = json['curriculum'];
    teacherId = json['teacher_id'];
    institutionId = json['institution_id'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    teacherName = json['teacher_name'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rombel_type'] = this.rombelType;
    data['lesson_ids'] = this.lessonIds;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['department'] = this.department;
    data['grade'] = this.grade;
    data['periode'] = this.periode;
    data['curriculum'] = this.curriculum;
    data['teacher_id'] = this.teacherId;
    data['institution_id'] = this.institutionId;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['teacher_name'] = this.teacherName;
    data['__v'] = this.iV;
    return data;
  }
}

class StudentId {
  Zenius zenius;
  List<int> role;
  List<String> studentAtInstitutions;
  List<Null> teacherAtInstitutions;
  List<Null> adminAtInstitutions;
  List<String> rombelIds;
  String sId;
  String username;
  String email;
  String password;
  String displayName;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String institutionId;
  String updatedBy;
  String phone;

  StudentId(
      {this.zenius,
        this.role,
        this.studentAtInstitutions,
        this.teacherAtInstitutions,
        this.adminAtInstitutions,
        this.rombelIds,
        this.sId,
        this.username,
        this.email,
        this.password,
        this.displayName,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.institutionId,
        this.updatedBy,
        this.phone});

  StudentId.fromJson(Map<String, dynamic> json) {
    zenius =
    json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    role = json['role'].cast<int>();
    studentAtInstitutions = json['student_at_institutions'].cast<String>();
    if (json['teacher_at_institutions'] != null) {
      teacherAtInstitutions = new List<Null>();
      json['teacher_at_institutions'].forEach((v) {
//        teacherAtInstitutions.add(new Null.fromJson(v));
      });
    }
    if (json['admin_at_institutions'] != null) {
      adminAtInstitutions = new List<Null>();
      json['admin_at_institutions'].forEach((v) {
//        adminAtInstitutions.add(new Null.fromJson(v));
      });
    }
    rombelIds = json['rombel_ids'].cast<String>();
    sId = json['_id'];
    username = json['username'];
    email = json['email'];
    password = json['password'];
    displayName = json['display_name'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    institutionId = json['institution_id'];
    updatedBy = json['updated_by'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['role'] = this.role;
    data['student_at_institutions'] = this.studentAtInstitutions;
    if (this.teacherAtInstitutions != null) {
//      data['teacher_at_institutions'] =
//          this.teacherAtInstitutions.map((v) => v.toJson()).toList();
    }
    if (this.adminAtInstitutions != null) {
//      data['admin_at_institutions'] =
//          this.adminAtInstitutions.map((v) => v.toJson()).toList();
    }
    data['rombel_ids'] = this.rombelIds;
    data['_id'] = this.sId;
    data['username'] = this.username;
    data['email'] = this.email;
    data['password'] = this.password;
    data['display_name'] = this.displayName;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['institution_id'] = this.institutionId;
    data['updated_by'] = this.updatedBy;
    data['phone'] = this.phone;
    return data;
  }
}

class Zenius {
  bool isActive;
  bool isPremium;

  Zenius({this.isActive, this.isPremium});

  Zenius.fromJson(Map<String, dynamic> json) {
    isActive = json['is_active'];
    isPremium = json['is_premium'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_active'] = this.isActive;
    data['is_premium'] = this.isPremium;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}
