import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart' as prefix0;
import 'package:edubox_tryout/models/assignment.dart';
import 'package:edubox_tryout/models/assignment_result.dart';
import 'package:edubox_tryout/models/file.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/models/lesson.dart';
import 'package:edubox_tryout/models/question.dart';
import 'package:edubox_tryout/models/quiz.dart';
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/quiz_pass.dart';
import 'package:edubox_tryout/models/quiz_result.dart';
import 'package:edubox_tryout/models/related_files.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/models/rombel.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiClient {
  List<String> whiteList = [
    'a2y4fajar',
    'a2y4febri',
    'a2y4qc'
  ];

  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  
  Future saveUrl(url) async {
    SharedPreferences _prefs = await prefs;
    _prefs.setString('baseUrl', url);
  }

  Future setFirstTimeOpen(firstTimeOpen) async {
    SharedPreferences _prefs = await prefs;
    _prefs.setBool('firstTimeOpen', firstTimeOpen);
  }

  Future setRated() async {
    SharedPreferences _prefs = await prefs;
    _prefs.setBool('hasRated', true);
  }

  Future saveToken(Login login) async {
    SharedPreferences _prefs = await prefs;
    _prefs.setString('token', login.token);
    _prefs.setString('userId', login.id);
    _prefs.setBool('isLoggedIn', true);
  }

  Future saveIsPremium(String isPremium) async {
    SharedPreferences _prefs = await prefs;
    _prefs.setString('isPremium', isPremium);
  }

  Future saveProfile(String profile) async {
    SharedPreferences _prefs = await prefs;
    _prefs.setString('listRombel', profile);
  }

  Future<List<String>> getListRombel() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getStringList('listRombel');
  }

  Future<String> getUserId() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getString('userId');
  }

  Future saveUsername(username) async {
    SharedPreferences _prefs = await prefs;
    return _prefs.setString('username', username);
  }

  Future<String> getUsername() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getString('username');
  }

  Future<String> getToken() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getString('token');
  }

  Future<String> getUrl() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getString('baseUrl') ?? 'https://api.edubox.cloud';
  }

  Future<bool> getFirstTimeOpen() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getBool('firstTimeOpen') ?? false;
  }

  Future<bool> isLoggedIn() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getBool('isLoggedIn') ?? false;
  }

  Future<String> isPremium() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getString('isPremium') ?? "";
  }

  Future<bool> hasRated() async {
    SharedPreferences _prefs = await prefs;
    return _prefs.getBool('hasRated') ?? false;
  }

  Future<dynamic> getAuthHeader() async {
    SharedPreferences _prefs = await prefs;
    String token = _prefs.getString('token');
    return {
      'Content-Type' : 'application/json',
      'Authorization' : '$token'
    };
  }

  dynamic getDefaultHeader() {
    return {
      'Content-Type' : 'application/json'
    };
  }

  Future<bool> logIn(String username, String password) async {
    Response response = await Client().post(
      '${await getUrl()}/auth/login',
      headers: getDefaultHeader(),
      body: json.encode({
        'username' : '$username',
        'password' : '$password'
      })              
    );
    if (response.statusCode == 200) {
      Login login = new Login.fromJson(json.decode(response.body));
      if(login.auth){
        saveToken(login);
      }
    }
    print(response.body);
    return response.statusCode == 200;
  }

  Future logOut() async {
    SharedPreferences _prefs = await prefs;
    await _prefs.remove('token');
    await _prefs.remove('isLoggedIn');
    await _prefs.remove('userId');
    await _prefs.remove('username');
    await _prefs.remove('isPremium');
  }

  Future<User> getProfile() async {        
    User user;  

    Response response = await Client().get(
      '${await getUrl()}/auth/me',
      headers: await getAuthHeader()
    );
    if (response.statusCode == 200) {
      user = new User.fromJson(json.decode(response.body));
      List<String> list = [];

      saveIsPremium(user.data.studentAtInstitutions[0].zenius.status);
    }    
    return user;
  }

  Future<Lesson> getLessons(String rombelId) async {    
    String url = '${await getUrl()}/lesson?rombel_id=$rombelId&page_size=-1';
    Lesson lesson;

    Response response = await Client().get(
      url,
      headers: await getAuthHeader(),      
    ).catchError((e) {
      print(e);
    });
    if (response.statusCode == 200) {
      lesson = new Lesson.fromJson(json.decode(response.body));
    }
    return lesson;  
  }

  Future<bool> checkApi(url) async {
    try {
      bool status = false;

      Response response = await Client().get(
          '$url/api'
      ).timeout(Duration(seconds: 15));

      Map<String, dynamic> api = jsonDecode(response.body);
      print('check api : ${api['status']}');

      try {
        status = api['status'];
      } catch(e){
        print(e);
      }
      return status;
    } on TimeoutException catch(e) {
      print(e);
      return false;
    }
  }

  Future<Lesson> getNoAuthLessons() async {
    String url = '${await getUrl()}/lesson/open?page_size=-1&lesson_group=lainnya';
    Lesson lesson;

    Response response = await Client().get(
      url,
      headers: await getAuthHeader(),
    ).catchError((e) {
      print(e);
    });

    if (response.statusCode == 200) {
      lesson = new Lesson.fromJson(json.decode(response.body));
    }
    return lesson;
  }

  Future<QuizList> getTryOut({String department, lessonGroup, grade, type}) async {
    try {
      final username = await getUsername();
      String url;
      var account = whiteList.where((i) => i == username).toList();
      if (account.length != 0) {
        url = '${await getUrl()}/quiz?is_public=true&page_size=-1';
      } else {
        url = '${await getUrl()}/quiz?status=published&is_public=true&grade=$grade&department=$department&lesson_group=$lessonGroup&page_size=-1';
      }
      QuizList quizList;

      Response response = await Client().get(
          url,
          headers: await getAuthHeader()
      ).catchError((e){
        print(e);
      });    
      if (response.statusCode == 200) {
        quizList = new QuizList.fromJson(json.decode(response.body));
      }
      return quizList;
    } catch(e){
      print(e);
      return null;
    }
  }

  Future<QuizList> getQuizList({String institutionId, String lessonId}) async {
    try {
      final username = await getUsername();
      String url;
      var account = whiteList.where((i) => i == username).toList();

      if (account.length != 0) {
        url = '${await getUrl()}/quiz?institution_id=$institutionId&lesson_id=$lessonId&page_size=100';
      } else {
        url = '${await getUrl()}/quiz?institution_id=$institutionId&lesson_id=$lessonId&status=published&page_size=100';
      }

      QuizList quizList;

      Response response = await Client().get(
          url,
          headers: await getAuthHeader()
      );
//      print("url quiz list $url ${await getAuthHeader()}");

      if (response.statusCode == 200) {
        quizList = new QuizList.fromJson(json.decode(response.body));
        quizList.data.items.forEach((i) {
          print(i.questionIds.length);
        });
        return quizList;
      } else {
        return null;
      }

    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Quiz> getQuizById(String id) async {    
    String url = '${await getUrl()}/quiz/$id';
    Quiz quiz;
    
    Response response = await Client().get(
      url,
      headers: await getAuthHeader()
    ).catchError((e) {
      print('error : $e');
    });
    if (response.statusCode == 200) {
      quiz = new Quiz.fromJson(json.decode(response.body));      
      return quiz;
    } else {
      return null;
    }    
  }

  Future<Question> getQuestion(String id) async {
    try {
      Question question;
      String url = '${await getUrl()}/question/$id';
      Response response = await Client().get(
        url,
        headers: await getAuthHeader()
      ).catchError((e){
        print('get Question error : $e');
      });

      if(response.statusCode == 200){
        question = Question.fromJson(json.decode(response.body));
        return question;
      } else {
        return null;
      }
    } catch(e){
      print(e);
      return null;
    }
  }

  Future<Rombel> getRombel(String rombelId) async {
    try {
      String url = '${await getUrl()}/rombel/$rombelId';
      Rombel rombel;

      Response response = await Client().get(
        url,
        headers: await getAuthHeader(),        
      ).catchError((e){
        print('get Rombel error : $e');
      });

      if (response.statusCode == 200) {
        rombel = new Rombel.fromJson(json.decode(response.body));
        return rombel;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<int> createResult(ResultBody body) async {    
    try {      
      String url = '${await getUrl()}/result';
      Response response = await Client().post(
        url,
        headers: await getAuthHeader(),
        body: json.encode(body.toJson())
      ).catchError((e){
        print('error create result : $e');
      });

      print(response.body);
      print('Create result : ${response.statusCode}');
      return response.statusCode;
    } catch (e) {
      print('error : $e');
      return null;
    }
  }

  Future<bool> createStudent(CreateStudentBody body) async {
    try {
      String baseUrl = await getUrl();
      String url1 = '$baseUrl/user/register/student';
      String url2 = 'http://apizenpres.box/user/register/student';
      Response response1;
      Response response2;

      response1 = await Client().post(
        url1,
        headers: await getDefaultHeader(),
        body: json.encode(body.toJson())
      );

      print('Create student to api.edu.box : ${response1.statusCode}');

      if (baseUrl == 'http://api.edu.box') {
        if (response1.statusCode == 201) {
          response2 = await Client().post(
              url2,
              headers: await getDefaultHeader(),
              body: json.encode(body.toJson())
          );

          print('Create student to apizenpres.box : ${response2.statusCode}');
        }

        return response1.statusCode == 201 && response2.statusCode == 201;
      } else {
        return response1.statusCode == 201;
      }
    } catch (e) {
      print('create student error : $e');
      return false;
    }
  }

  Future<EduboxContent> getNoAuthChapter(String materialId) async {
    try {
      EduboxContent chapter;
      print('$materialId ');
      String url = '${await getUrl()}/material/open/$materialId';
      Response response = await Client().get(
        url,
        headers: await getAuthHeader(),
      ).catchError((e) {
        print(e);
      });
      print("Response chapter $url");
      if (response.statusCode == 200) {
        chapter = new EduboxContent.fromJson(jsonDecode(response.body));
        return chapter;
      } else {
        return null;
      }
    } catch (e) {
      print('get content error : $e');
      return null;
    }
  }

  Future<EduboxContent> getContent(String lessonId) async {
    try {
      EduboxContent eduboxContent;
      String url = '${await getUrl()}/lesson/content/$lessonId';
      Response response = await Client().get(
        url,
        headers: await getAuthHeader(),
      );

      if (response.statusCode == 200) {
        eduboxContent = new EduboxContent.fromJson(jsonDecode(response.body));
        return eduboxContent;
      } else {
        return null;
      }
    } catch (e) {
      print('get content error : $e');
      return null;
    }
  }

  Future<EduboxContent> getNoAuthPlayListContent({String subBabId}) async {
    try {
      String url = '${await getUrl()}/material/open/$subBabId';
      EduboxContent playlistContent;
      Response response = await Client().get(
        url,
      );

      print("url material $url");
      if (response.statusCode == 200) {
        playlistContent = new EduboxContent.fromJson(jsonDecode(response.body));
        return playlistContent;
      } else {
        return null;
      }
    } catch (e) {
      print('get playlist error : $e');
      return null;
    }
  }

  Future<bool> downloadFile(FileIds item) async {
    Response response = await Client().get(
      '${await getUrl()}/file/${item.sId}');

    var bytes = response.bodyBytes;
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file;
    file = new File('$dir/${item.name}');
    await file.writeAsBytes(bytes);

    print('Download konten : ${response.statusCode}');

    return response.statusCode == 200;
  }

  Future<bool> downloadThumbnail(FileIds item) async {
    Response response = await Client().get(
      '${await getUrl()}/file/${item.meta.thumbnail}'
    );

    var bytes = response.bodyBytes;
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/${item.meta.thumbnail}.jpg');
    await file.writeAsBytes(bytes);

    print('Download thumbnail : ${response.statusCode}');

    return response.statusCode == 200;
  }
  
  Future<Files> getLatestFile(String type, int size) async {
    try {
      Response response = await Client().get(
          '${await getUrl()}/file?filetype=$type&orders=desc created_at&page_size=$size');

      Files files;
      if (response.statusCode == 200) {
        files = Files.fromJson(json.decode(response.body));
        return files;
      } else {
        return null;
      }
    } catch (e) {
      print('error get latestfile : $e');
      return null;
    }
  }

  Future<RelatedFiles> getRelatedFiles(String fileId) async {
    try {
      Response response = await Client().get(
          '${await getUrl()}/material/open?file_ids=$fileId');

      RelatedFiles files;
      if (response.statusCode == 200) {
        files = RelatedFiles.fromJson(json.decode(response.body));
        return files;
      } else {
        return null;
      }
    } catch (e) {
      print('error get related files : $e');
      return null;
    }
  }

  Future<Assignment> getAssignment(String institutionId, lessonId) async {
    try {
      Response response = await Client().get(
        '${await getUrl()}/assignment?institution_id=$institutionId&lesson_id=$lessonId&status=open',
        headers: await getAuthHeader()
      );

      if (response.statusCode == 200) {
        Assignment assignment = Assignment.fromJson(json.decode(response.body));
        return assignment;
      } else {
        return null;
      }
    } catch(e) {
      print(e);
      return null;
    }
  }

  Future<bool> createAssignmentResult(AssignmentBody body) async {
    try {
      Response response = await Client().post(
          '${await getUrl()}/assignment-result',
          headers: await getAuthHeader(),
          body: json.encode(body.toJson())
      );

      print('Create Assignment Result : ${response.statusCode} ==> ${response.body}');

      return response.statusCode == 201;
    } catch(e) {
      print(e);
      return false;
    }
  }

  Future<bool> updateAssignmentResult(AssignmentBody body, String id) async {
    try {
      Response response = await Client().put(
        '${await getUrl()}/assignment-result/$id',
        headers: await getAuthHeader(),
        body: json.encode(body.toJson())
      );

      print('Upadte Assignment Result : ${response.statusCode} ==> ${response.body}');

      return response.statusCode == 200;
    } catch(e) {
      print(e);
      return false;
    }
  }

  Future<AssignmentResult> getAssignmentResult(String assignmentId, studentId) async {
    try {
      Response response = await Client().get(
        '${await getUrl()}/assignment-result?assignment_id=$assignmentId&student_id=$studentId',
        headers: await getAuthHeader()
      );

      if (response.statusCode == 200) {
        return AssignmentResult.fromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch(e) {
     print('error to get Assignment Result : $e');
     return null;
    }
  }

  Future<CreateFileRes> createFile(File files, String institutionId) async {
    try {
      var dio = new prefix0.Dio();

      prefix0.FormData formData = new prefix0.FormData.fromMap({
        "file": await prefix0.MultipartFile.fromFile(files.path),
        "institution_id": institutionId,
      });

      final response = await dio.post(
        '${await getUrl()}/file',
        data: formData,
        options: prefix0.Options(
          headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Authorization' : await getToken()
          }
        )
      );

      print('Create File : ${response.statusCode} ==> ${response.data}');

      if (response.statusCode == 201) {
        return CreateFileRes.fromJson(response.data);
      } else {
        return null;
      }
    } catch(e) {
      print('error uploading file : $e');
      return null;
    }
  }

  Future<bool> deleteFile(String id) async {
    try {
      var dio = new prefix0.Dio();

      final response = await dio.delete(
        '${await getUrl()}/file/$id',
        options: prefix0.Options(
          headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Authorization' : await getToken()
          }
        )
      );

      print('Delete File : ${response.statusCode} ==> ${response.data}');

      return response.statusCode == 202;
    } catch(e) {
      print('error updating file : $e');
      return false;
    }
  }

  Future<bool> updateUser(String id, displayName, email, phone) async {
    try {
      Response response = await Client().put(
        '${await getUrl()}/user/$id',
        headers: await getAuthHeader(),
        body: json.encode({
          'display_name' : displayName,
          'email' : email,
          'phone' : phone,
        })
      );

      print(response.statusCode);

      return response.statusCode == 200;
    } catch(e) {
      print(e);
      return false;
    }
  }

  Future<bool> updatePassword(String id, password) async {
    try {
      Response response = await Client().put(
          '${await getUrl()}/user/$id',
          headers: await getAuthHeader(),
          body: json.encode({
            'password' : password,
          })
      );

      print(response.statusCode);

      return response.statusCode == 200;
    } catch(e) {
      print(e);
      return false;
    }
  }

  Future<QuizResult> getQuizResult(quizId) async {
    try {
      Response response = await Client().get(
        '${await getUrl()}/result?quiz_id=$quizId&student_id=${await getUserId()}',
        headers: await getAuthHeader()
      );

      if (response.statusCode == 200) {
        return QuizResult.fromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      print('Error fetching quiz result : $e');
      return null;
    }
  }

  Future<QuizPass> getQuizPass(quizId) async {
    try {
      Response response = await Client().get(
        '${await getUrl()}/quiz-pass?quiz_id=$quizId',
        headers: await getAuthHeader()
      );

      if (response.statusCode == 200) {
        return QuizPass.fromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      print('failed to get quiz pass : $e');
      return null;
    }
  }

  Future<bool> updateQuizPass(id) async {
    try {
      Response response = await Client().put(
        '${await getUrl()}/quiz-pass/$id',
        body: json.encode({
          'user_id' : await getUserId()
        }),
        headers: await getAuthHeader()
      );

      print(response.body);

      return response.statusCode == 200;
    } catch (e) {
      print('failed to update quizpass: $e');
      return false;
    }
  }
}