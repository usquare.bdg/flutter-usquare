import 'package:edubox_tryout/models/lesson.dart';
import 'package:edubox_tryout/models/quiz.dart';
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/content.dart' as edubox;
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/app_database.dart';
import 'package:sembast/sembast.dart';

class DbDao {  
  static const String STORE_USER = 'user';
  static const String STORE_QUIZ = 'quiz';
  static const String STORE_RESULT_BODY = 'result_body';
  static const String STORE_QUESTION = 'question';
  static const String STORE_LESSON = 'lesson';
  static const String STORE_CONTENT = 'content';
  
  final _userStore = intMapStoreFactory.store(STORE_USER);  
  final _quizStore = intMapStoreFactory.store(STORE_QUIZ);
  final _resultBody = intMapStoreFactory.store(STORE_RESULT_BODY);
  final _questionStore = intMapStoreFactory.store(STORE_QUESTION);
  final _lessonStore = intMapStoreFactory.store(STORE_LESSON);
  final _contentStore = intMapStoreFactory.store(STORE_CONTENT);

  Future<Database> get _db async => await AppDatabase.instance.database;

  // INSERT  
  Future insertUser(User user) async {
    await _userStore.add(await _db, user.toJson());
  }

  Future insertQuiz(Items quiz) async {
    await _quizStore.add(await _db, quiz.toJson());
  }

  Future insertResultBody(ResultBody body) async {
    await _resultBody.add(await _db, body.toJson());
  }

  Future insertQuestion(QuestionIds question) async {
    await _questionStore.add(await _db, question.toJson());
  }

  Future insertLesson(LessonItems lesson) async {
    await _lessonStore.add(await _db, lesson.toJson());
  }

  Future insertContent(edubox.FileIds file) async {
    await _contentStore.add(await _db, file.toJson());
  }

  // UPDATE
  Future updateQuiz(Items quiz, String id) async {
    final _finder = Finder(
      filter: Filter.and([
        Filter.equals('_id', id),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    await _quizStore.update(
      await _db,       
      quiz.toJson(),
      finder: _finder
    );
  }

  Future updateUser(User user) async {
    print(user.toJson());
    await _userStore.update(
      await _db,
      user.toJson(),
    );
  }

  Future updateResultBody(ResultBody result, String quizId) async {
    final finder = Finder(
        filter: Filter.and([
        Filter.equals('quiz_id', quizId),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    await _resultBody.update(
      await _db,
      result.toJson(),
      finder: finder
    );
  }

  // DELETE  
  Future deleteUser() async {
    await _userStore.delete(await _db);
  }

  Future deleteAllResultBody() async {
    await _resultBody.delete(await _db);
  }

  Future deleteResultBody(String id) async {
    final _finder = Finder(
      filter: Filter.and([
        Filter.equals('quiz_id', id),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    await _resultBody.delete(
      await _db,
      finder: _finder
    );
  }

  Future deleteQuiz(String id) async {
    final _finder = Finder(
      filter: Filter.and([
        Filter.equals('_id', id),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    await _quizStore.delete(
      await _db,
      finder: _finder
    );
  }

  Future deleteAllQuiz() async {
    await _quizStore.delete(await _db);
  }

  Future deleteQuestions(String quizId) async {
    final _finder = Finder(
      filter: Filter.and([
        Filter.equals('quiz_id', quizId),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    await _questionStore.delete(
      await _db,
      finder: _finder
    );
  }

  Future deleteAllQuestions() async {
    await _questionStore.delete(await _db);
  }

  Future deleteLesson() async {
    await _lessonStore.delete(await _db);
  }

  Future deleteContent() async {
    await _contentStore.delete(await _db);
  }

  // GET
  Future<User> getUser() async {
    User _user = new User();
    final recordSnapshot = await _userStore.find(await _db);
    
    if (recordSnapshot.isEmpty) {
      return null;
    } else {
      _user = new User.fromJson(recordSnapshot[0].value);    
      return _user;    
    }
  }

  Future<Items> getQuiz(String id) async {
    Items _items = new Items();

    final finder = Finder(
      filter: Filter.and([
        Filter.equals('_id', id),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    final recordSnapshot = await _quizStore.find(
      await _db,
      finder: finder
    );    
    
    if (recordSnapshot.isEmpty) {
      print('null');
      return null;
    } else {
      print(recordSnapshot[0].value);
      _items = new Items.fromJson(recordSnapshot[0].value);  
      return _items;
    }    
  }

  Future<List<Items>> getAllQuiz() async {    
    try {
      List<Items> _listQuiz = new List<Items>();
      final finder = Finder(
        filter: Filter.equals('user_id', await ApiClient().getUserId())
      );
      final recordSnapshot = await _quizStore.find(await _db, finder: finder);

      if (recordSnapshot.isEmpty) {
        return null;
      } else {
        recordSnapshot.forEach((quiz) {      
          _listQuiz.add(new Items.fromJson(quiz.value));
        });        
        return _listQuiz;
      }
    } catch (e) {
      print('error get all quiz : $e');
      return null;
    }
  }

  Future<ResultBody> getResultBody(String quizId) async {
    ResultBody _body = new ResultBody();
    final finder = Finder(
      filter: Filter.and([
        Filter.equals('quiz_id', quizId),
        Filter.equals('user_id', await ApiClient().getUserId())
      ])
    );
    final recordSnapshot = await _resultBody.find(
      await _db,
      finder: finder
    );    
    if (recordSnapshot.isEmpty) {
      return null;
    } else {
      _body = new ResultBody.fromJson(recordSnapshot[0].value);
      return _body;
    }
  }

  Future<List<QuestionIds>> getAllQuestions(String quizId, userId) async {
    List<QuestionIds> _listQuestions = new List<QuestionIds>();
    final finder = Finder(
      filter: Filter.and([
        Filter.equals('quiz_id', quizId),
        Filter.equals('user_id', userId)
      ])
    );
    final recordSnapshot = await _questionStore.find(
      await _db,
      finder: finder
    );
    if (recordSnapshot.isEmpty) {
      return null;
    } else {
      recordSnapshot.forEach((question) {
        _listQuestions.add(new QuestionIds.fromJson(question.value));
      });
      return _listQuestions;
    }
  }

  Future<List<LessonItems>> getLessons() async {
    List<LessonItems> _listLessons = new List<LessonItems>();
    final recordSnapshot = await _lessonStore.find(
      await _db,
    );
    if (recordSnapshot.isEmpty) {
      return null;
    } else {
      recordSnapshot.forEach((lesson) {
        _listLessons.add(LessonItems.fromJson(lesson.value));
      });
      return _listLessons;
    }
  }

  Future<List<edubox.FileIds>> getContent() async {
    List<edubox.FileIds> _listFile = List<edubox.FileIds>();
    final recordSnapshot = await _contentStore.find(
      await _db,
    );
    if (recordSnapshot.isEmpty) {
      return null;
    } else {
      recordSnapshot.forEach((content) {
        _listFile.add(edubox.FileIds.fromJson(content.value));
      });
      return _listFile;
    }
  }

  Future<edubox.FileIds> getSingleContent(String id) async {
    edubox.FileIds _file = new edubox.FileIds();
    final finder = Finder(filter: Filter.equals('_id', id));
    final recordSnapshot = await _contentStore.find(
      await _db,
      finder: finder
    );
    if (recordSnapshot.isEmpty) {
      return null;
    } else {
      _file = edubox.FileIds.fromJson(recordSnapshot[0].value);
      return _file;
    }
  }
}