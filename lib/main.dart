import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/main_bloc/bloc.dart';
import 'package:edubox_tryout/main_bloc/simple_bloc_delegate.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/others/splashsceen.dart';
import 'package:edubox_tryout/view/others/update_warning_screen.dart';
import 'package:edubox_tryout/view/main_screen/main_screen.dart';
import 'package:edubox_tryout/view/select_server/select_server_screen.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'utils/firebase_admob.dart';

bool isFirstTimeOpen = true;

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();

  Crashlytics.instance.enableInDevMode = false;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  final currentPage;

  const MyApp({Key key, this.currentPage}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

Future<dynamic> backgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }
}

class _MyAppState extends State<MyApp> {
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  MainBloc _mainBloc;
//  InterstitialAd myInterstitial;

  @override
  void initState() {
    _initFirebaseMessaging();
    _mainBloc = MainBloc();
    if (isFirstTimeOpen) {
      _mainBloc.add(StartApp());
    }


//    myInterstitial = buildInterstitialAd()..load();

    super.initState();
  }

  /*InterstitialAd buildInterstitialAd() {
    return InterstitialAd(
      adUnitId: InterstitialAd.testAdUnitId,
      listener: (MobileAdEvent event) {
        if (event == MobileAdEvent.failedToLoad) {
          myInterstitial..load();
        } else if (event == MobileAdEvent.closed) {
          myInterstitial = buildInterstitialAd()..load();
        }
        print(event);
      },
    );
  }*/

  /*void showInterstitialAd() {
    myInterstitial..show();
  }

  void showRandomInterstitialAd() {
    Random r = new Random();
    bool value = r.nextBool();

    if (value == true) {
      myInterstitial..show();
    }
  }*/

  void _initFirebaseMessaging() {
    _firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg){
        print('called launch');
        return;
      },
      onResume: (Map<String, dynamic> msg){
        print('called resume');
        return;
      },
      onMessage: (Map<String, dynamic> msg){
        print('called message');
        return;
      },
      onBackgroundMessage: backgroundMessageHandler,
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
        sound: true,
        alert: true,
        badge: true
      )
    );
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings setting){
      print('Ios Setting register');
    });
    _firebaseMessaging.getToken().then((v){
      print('token firebase $v');
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Strings.appName,
      theme: ThemeData(
        primarySwatch: EduColors.primary,
        canvasColor: Colors.white,
        iconTheme: IconThemeData(
          color: EduColors.black
        ),
        fontFamily: 'Montserrat'
      ),
      debugShowCheckedModeBanner: false,
      home: BlocBuilder(
        bloc: _mainBloc,
        builder: (context, state) {
          if (state is StartingApp) {
            return SplashScreen();
          }
          if (state is UpdateWarning) {
//            return SelectServerScreen();
            return UpdateWarningScreen(
              forceUpdate: state.forceUpdate,
              currentVersion: state.currentVersion,
              latestVersion: state.latestVersion,
            );
          }
          if (state is AppStarted) {
//            return SelectServerScreen();
          }

          return MainScreen(currentPage: widget.currentPage,);
        },
      )
    );
  } 
}