
class Strings {
  static const String appName = "USquare";
  static const String baseUrl = "http://api.usquare.id";
  static const String aboutTitle = "Tentang Aplikasi";
  static const String aboutDesc = "USquare adalah aplikasi penilaian digital untuk "
      "mendukung proses Pembelajaran Jarak Jauh (PJJ). \nUSquare dikembangkan dari Edubox PRO.\n\n\n"
      "Saran dan masukan silakan disampaikan ke admin@usquare.id";
}