import 'package:edubox_tryout/services/db_dao.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'dart:async';
import 'package:flutter/services.dart';

class FbAnalytics {
  FirebaseAnalytics _analytics;

  Future<void> sendAnalytics(String event) async {
    _analytics = FirebaseAnalytics();

    try {
      final user = await DbDao().getUser();
      String _userName = 'Unauthenticated';
      String _userAtInstitution = 'Unauthenticated';

      if (user != null) {
        _userName = user.data.displayName;
        _userAtInstitution = user.data.studentAtInstitutions[0].name;

        await _analytics.setUserId(user.data.sId);
        await _analytics.setUserProperty(name: 'user_id', value: user.data.sId);
        await _analytics.setUserProperty(name: 'display_name', value: user.data.displayName);
        await _analytics.setUserProperty(name: 'institution_name', value: user.data.studentAtInstitutions[0].name);
        await _analytics.setUserProperty(name: 'rombel', value: user.data.rombelIds[0].name);
      }

      await _analytics.logEvent(
        name: event,
        parameters: <String, dynamic> {
          'display_name' : _userName,
          'institution_name' : _userAtInstitution
        }
      );
    } on PlatformException {
      print('Failed to send event : $event');
    }
  }

  Future<void> setCurrentScreen(String screen) async {
    _analytics = FirebaseAnalytics();

    try {
      await _analytics.setCurrentScreen(screenName: screen);
    } on PlatformException {
      print('Failed to setCurrentScreen : $screen');
    }
  }
}