import 'dart:async';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:flutter/services.dart';
import 'package:native_mixpanel/native_mixpanel.dart';

class MixpanelUtils {
  Mixpanel _mixpanel;

  Future<void> initPlatformState(String activity) async {
    _mixpanel = Mixpanel(
      shouldLogEvents: true,
      isOptedOut: false
    );

    User user = await DbDao().getUser();
    Map<String, dynamic> properties = {'Nama' : '${user.data.displayName}', 'Sekolah' : '${user.data.studentAtInstitutions[0].name}'};

    try {
      await _mixpanel.initialize('0a7b02cec58daf9a5e3b8c7f461c5c4d');
      await _mixpanel.identify('${user.data.displayName}_${user.data.studentAtInstitutions[0].name}');
      await _mixpanel.track('$activity', properties);

    } on PlatformException {
      print('Failed to send event : $activity');
    }
  }
}