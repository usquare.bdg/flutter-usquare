import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';

class Ads {

  /*BannerAd myBanner;
  InterstitialAd interstitialAd;
  String adUnitIdIOs = "ca-app-pub-5076078415456216~5135288019";
  String adUnitIdAndroid = "ca-app-pub-5076078415456216~1115222340";
  String bannerIOs = "ca-app-pub-5076078415456216/9198941518";
  String bannerAndroid = "ca-app-pub-5076078415456216/6709311879";
  String interstitialAndroid = "ca-app-pub-5076078415456216/6987086427";

  //Testing id
//  String bannerAndroid = "ca-app-pub-3940256099942544/6300978111";
//  String interstitialAndroid = "ca-app-pub-3940256099942544/1033173712";

  BannerAd initBannerAd(){
    if (Platform.isAndroid) {
      FirebaseAdMob.instance.initialize(appId: adUnitIdAndroid);
    } else {
      FirebaseAdMob.instance.initialize(appId: adUnitIdIOs);
    }
    myBanner = Ads().showBanner();
    myBanner..load().then((loaded) {
      if (loaded) {
        myBanner..show();
      } else {
        myBanner..dispose();
      }
    });

    return myBanner;
  }

  InterstitialAd initInterstitialAd() {
    if (Platform.isAndroid) {
      FirebaseAdMob.instance.initialize(appId: adUnitIdAndroid);
    } else {
      FirebaseAdMob.instance.initialize(appId: adUnitIdIOs);
    }
    interstitialAd = Ads().showInterstitial();
    interstitialAd..load().then((value) {
      if (value) interstitialAd..show();
      else interstitialAd..dispose();
    });

    return interstitialAd;
  }

  BannerAd showBanner(){
    return BannerAd(
      // Replace the testAdUnitId with an ad unit id from the AdMob dash.
      // https://developers.google.com/admob/android/test-ads
      // https://developers.google.com/admob/ios/test-ads
      adUnitId: Platform.isAndroid ? bannerAndroid : bannerIOs,
      size: AdSize.smartBanner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        if (event == MobileAdEvent.failedToLoad) {
          myBanner..load();
        } else if (event == MobileAdEvent.closed) {
          myBanner = showBanner()..load();
        }
        print("BannerAd event is $event");
      },
    );
  }

  InterstitialAd showInterstitial(){
    return InterstitialAd(
      adUnitId: Platform.isAndroid ? interstitialAndroid : "",
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        if (event == MobileAdEvent.failedToLoad) {
          interstitialAd..load();
        } else if (event == MobileAdEvent.closed) {
          interstitialAd = showInterstitial()..load();
        }
        print("InterstitialAd event is $event");
      },
    );
  }

  MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>[],
    contentUrl: 'https://flutter.io',
    childDirected: true,
    designedForFamilies: true,
    gender: MobileAdGender.unknown, // or MobileAdGender.female, MobileAdGender.unknown
    testDevices: <String>["1E5A95D004315D8FC91273ABDEB5F92D", "437DA53402BC1D3F05F7A0451BB8246E"]//], // Android emulators are considered test devices
  );*/
}