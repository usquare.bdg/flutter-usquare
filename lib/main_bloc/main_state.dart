import 'package:equatable/equatable.dart';

abstract class MainState extends Equatable {
  const MainState();
}

class InitialMainState extends MainState {
  @override
  List<Object> get props => [];
}

class AppStarted extends MainState {
  @override
  List<Object> get props => [];
}

class UpdateWarning extends MainState {
  final bool forceUpdate;
  final String currentVersion, latestVersion;

  UpdateWarning({this.forceUpdate, this.currentVersion, this.latestVersion});
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class StartingApp extends MainState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}