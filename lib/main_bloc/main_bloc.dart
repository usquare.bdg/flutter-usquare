import 'dart:async';
import 'dart:io' show Platform;

import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:package_info/package_info.dart';
import './bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  @override
  MainState get initialState => InitialMainState();

  @override
  Stream<MainState> mapEventToState(MainEvent event,) async* {
    if (event is StartApp) {
      yield* _mapStartAppToState();
    }
  }

  Stream<MainState> _mapStartAppToState() async* {
    yield StartingApp();
    await Future.delayed(Duration(seconds: 2));

    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    await remoteConfig.fetch(expiration: const Duration(seconds: 0));
    await remoteConfig.activateFetched();

    String latestVersion;
    bool forceUpdate;

    if (Platform.isAndroid) {
      latestVersion =  remoteConfig.getString('latest_android_version');
      forceUpdate = remoteConfig.getBool('android_force_update');
    } else {
      latestVersion =  remoteConfig.getString('latest_ios_version');
      forceUpdate = remoteConfig.getBool('ios_force_update');
    }

    print('Current version ${packageInfo.version}');
    print('Latest version $latestVersion');
    print('Forced $forceUpdate');

    if (latestVersion != packageInfo.version) {
      yield UpdateWarning(currentVersion: packageInfo.version, latestVersion: latestVersion, forceUpdate: forceUpdate);
    } else {
      yield AppStarted();
    }
  }
}