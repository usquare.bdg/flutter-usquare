import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/models/file.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserState extends Equatable {}

@immutable
abstract class LatestVideoState extends Equatable {}

@immutable
abstract class LatestBookState extends Equatable {}

@immutable
abstract class LatestAudioState extends Equatable {}

// User
class InitUserState extends UserState {
  @override
  List<Object> get props => [];
}

class LoadingUser extends UserState {
  @override
  List<Object> get props => [];
}

class UserLoaded extends UserState {
  final User user;

  UserLoaded(this.user);

  @override
  List<Object> get props => [user];
}

class LoadUserFailed extends UserState{
  @override
  List<Object> get props => [];
}

class LoggedOut extends UserState {
  @override
  List<Object> get props => [];
}

class PageRefreshed extends UserState {
  @override
  List<Object> get props => [];
}

// Latest Video
class InitLatestVideoState extends LatestVideoState {
  @override
  List<Object> get props => [];
}

class LatestVideoLoaded extends LatestVideoState {
  final Files files;

  LatestVideoLoaded(this.files);
  @override
  List<Object> get props => [];
}

class LoadLatestVideoFailed extends LatestVideoState {
  @override
  List<Object> get props => [];
}

class LatestVideoIsEmpty extends LatestVideoState {
  @override
  List<Object> get props => [];
}

class SelectedVideo extends LatestVideoState {
  final FileIds nowPlaying;
  final List<FileIds> listVideo;

  SelectedVideo(this.nowPlaying, this.listVideo);
  @override
  List<Object> get props => [];
}

class FailedToLoadVideo extends LatestVideoState {
  @override
  List<Object> get props => [];
}

class LoadingVideo extends LatestVideoState {
  final bool isLoadingPlaylist;

  LoadingVideo(this.isLoadingPlaylist);
  @override
  List<Object> get props => [];
}

// Latest Book
class InitLatestBookState extends LatestBookState {
  @override
  List<Object> get props => [];
}

class LatestBookLoaded extends LatestBookState {
  final Files files;

  LatestBookLoaded(this.files);
  @override
  List<Object> get props => [];
}

class LoadLatestBookFailed extends LatestBookState {
  @override
  List<Object> get props => [];
}

class LatestBookIsEmpty extends LatestBookState {
  @override
  List<Object> get props => [];
}

class SelectedBook extends LatestBookState {
  final FileIds file;
  final List<FileIds> listBook;

  SelectedBook(this.file, this.listBook);
  @override
  List<Object> get props => [];
}

class FailedToLoadBook extends LatestBookState {
  @override
  List<Object> get props => [];
}

class LoadingBook extends LatestBookState {
  final bool isLoadingPlaylist;

  LoadingBook(this.isLoadingPlaylist);
  @override
  List<Object> get props => [];
}


// Latest Audio
class InitLatestAudioState extends LatestAudioState {
  @override
  List<Object> get props => [];
}

class LatestAudioLoaded extends LatestAudioState {
  final Files files;

  LatestAudioLoaded(this.files);
  @override
  List<Object> get props => [];
}

class LoadLatestAudioFailed extends LatestAudioState {
  @override
  List<Object> get props => [];
}

class LatestAudioIsEmpty extends LatestAudioState {
  @override
  List<Object> get props => [];
}

class SelectedAudio extends LatestAudioState {
  final FileIds nowPlaying;
  final List<FileIds> listAudio;

  SelectedAudio(this.nowPlaying, this.listAudio);
  @override
  List<Object> get props => [];
}

class FailedToLoadAudio extends LatestAudioState {
  @override
  List<Object> get props => [];
}

class LoadingAudio extends LatestAudioState {
  final bool isLoadingPlaylist;

  LoadingAudio(this.isLoadingPlaylist);
  @override
  List<Object> get props => [];
}
