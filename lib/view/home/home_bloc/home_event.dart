import 'package:edubox_tryout/models/file.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserEvent extends Equatable {}

@immutable
abstract class LatestVideoEvent extends Equatable {}

@immutable
abstract class LatestBookEvent extends Equatable {}

@immutable
abstract class LatestAudioEvent extends Equatable {}

// User
class LoadUser extends UserEvent {
  @override
  List<Object> get props => [];
}

class LogOut extends UserEvent {
  @override
  List<Object> get props => [];
}

// LatestVideo
class LoadLatestVideo extends LatestVideoEvent {
  final int pageSize;

  LoadLatestVideo(this.pageSize);
  @override
  List<Object> get props => [];
}

class SelectVideo extends LatestVideoEvent {
  final Items item;

  SelectVideo(this.item);
  @override
  List<Object> get props => [];
}

// LatestBook
class LoadLatestBook extends LatestBookEvent {
  final int pageSize;

  LoadLatestBook(this.pageSize);
  @override
  List<Object> get props => [];
}

class SelectBook extends LatestBookEvent {
  final Items item;

  SelectBook(this.item);
  @override
  List<Object> get props => [];
}

// LatestAudio
class LoadLatestAudio extends LatestAudioEvent {
  final int pageSize;

  LoadLatestAudio(this.pageSize);
  @override
  List<Object> get props => [];
}

class SelectAudio extends LatestAudioEvent {
  final Items item;

  SelectAudio(this.item);
  @override
  List<Object> get props => [];
}
