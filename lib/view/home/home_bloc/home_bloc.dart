import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import './bloc.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  @override
  UserState get initialState => InitUserState();

  @override
  Stream<UserState> mapEventToState(
      UserEvent event,
      ) async* {
    if (event is LoadUser) {
      yield* _loadUserToState();
    } else if (event is LogOut) {
      yield* _logOutToState();
    }
  }

  Stream<UserState> _loadUserToState() async* {
    yield LoadingUser();
    try {
      final user = await DbDao().getUser();
      print(user.data.sId);
      print(await ApiClient().getUserId());
      if (user != null) {
        yield UserLoaded(user);
      } else {
        yield LoadUserFailed();
      }
    } catch (e) {
      print(e);
      yield LoadUserFailed();
    }
  }

  Stream<UserState> _logOutToState() async* {
    try {
//      await DbDao().deleteAllQuiz();
//      await DbDao().deleteAllResultBody();
//      await DbDao().deleteAllQuestions();
//      await DbDao().deleteContent();
      await DbDao().deleteUser();
      await ApiClient().logOut();

      yield LoggedOut();
    } catch (e) {
      print(e);
    }
  }
}

class LatestVideoBloc extends Bloc<LatestVideoEvent, LatestVideoState> {
  @override
  LatestVideoState get initialState => InitLatestVideoState();

  @override
  Stream<LatestVideoState> mapEventToState(
      LatestVideoEvent event,
      ) async* {
    if (event is LoadLatestVideo) {
      yield* _loadLatestVideoToState(event);
    } else if (event is SelectVideo) {
      yield* _selectVideoToState(event);
    }
  }

  Stream<LatestVideoState> _loadLatestVideoToState(event) async* {
    yield LoadingVideo(false);
    try {
      final result = await ApiClient().getLatestFile('video/mp4', event.pageSize);
      if (result != null) {
        if (result.data.items != null) {
          if (result.data.items.length > 0) {
            result.data.items.removeWhere((item) => item.meta == null);
            yield LatestVideoLoaded(result);
          } else {
            yield LatestVideoIsEmpty();
          }
        }
      } else {
        yield LoadLatestVideoFailed();
      }
    } catch (e) {
      print('Load latestbook failed : $e');
      yield LoadLatestVideoFailed();
    }
  }

  Stream<LatestVideoState> _selectVideoToState(event) async* {
    yield LoadingVideo(true);
    try {
      List<FileIds> listVideo = new List<FileIds>();

      final fileInDb = await DbDao().getSingleContent(event.item.sId);

      FileMeta newMeta = new FileMeta();
      newMeta.title = event.item.meta.title;
      newMeta.thumbnail = event.item.meta.thumbnail;
      newMeta.pages = event.item.meta.pages;
      newMeta.duration = event.item.meta.duration;
      newMeta.description = event.item.meta.description;
      FileIds newFile = new FileIds();
      newFile.sId = event.item.sId;
      newFile.name = event.item.name;
      newFile.createdAt = event.item.createdAt;
      newFile.meta = newMeta;
      newFile.isDownloaded = false;
      if (fileInDb != null) newFile.isDownloaded = true;

      final relatedVideos = await ApiClient().getRelatedFiles(event.item.sId);
      final listFileDb = await DbDao().getContent();

      if (relatedVideos != null) {
        if (relatedVideos.data.items.length > 0) {
          for (var item in relatedVideos.data.items) {
            if (item.fileIds.length > 0) {
              for (var file in item.fileIds) {
                file.isDownloaded = false;

                if (listFileDb != null) {
                  for (var fileDb in listFileDb) {
                    if (file.sId == fileDb.sId) {
                      file.isDownloaded = true;
                    }
                  }
                }

                if (file.filetype == 'video/mp4') {
                  listVideo.add(file);
                }
              }

              listVideo.removeWhere((i) => i.meta == null);
              yield SelectedVideo(newFile, listVideo);
            }
          }
        }
      } else {
        yield FailedToLoadVideo();
      }
    } catch (e) {
      print('failed to open video : $e');
      yield FailedToLoadVideo();
    }
  }
}

class LatestBookBloc extends Bloc<LatestBookEvent, LatestBookState> {
  @override
  LatestBookState get initialState => InitLatestBookState();

  @override
  Stream<LatestBookState> mapEventToState(
      LatestBookEvent event,
      ) async* {
    if (event is LoadLatestBook) {
      yield* _loadLatestBookToState(event);
    } else if (event is SelectBook) {
      yield* _selectBookToState(event);
    }
  }

  Stream<LatestBookState> _loadLatestBookToState(event) async* {
    yield LoadingBook(false);
    try {
      final result = await ApiClient().getLatestFile('application/pdf', event.pageSize);
      if (result != null) {
        if (result.data.items != null) {
          if (result.data.items.length > 0) {
            result.data.items.removeWhere((item) => item.meta == null);
            yield LatestBookLoaded(result);
          } else {
            yield LatestBookIsEmpty();
          }
        }
      } else {
        yield LoadLatestBookFailed();
      }
    } catch (e) {
      print('Load latestbook failed : $e');
      yield LoadLatestBookFailed();
    }
  }

  Stream<LatestBookState> _selectBookToState(event) async* {
    yield LoadingBook(true);
    try {
      List<FileIds> listBook = new List<FileIds>();

      final fileInDb = await DbDao().getSingleContent(event.item.sId);

      FileMeta newMeta = new FileMeta();
      newMeta.title = event.item.meta.title;
      newMeta.thumbnail = event.item.meta.thumbnail;
      newMeta.pages = event.item.meta.pages;
      newMeta.duration = event.item.meta.duration;
      newMeta.description = event.item.meta.description;
      FileIds newFile = new FileIds();
      newFile.sId = event.item.sId;
      newFile.name = event.item.name;
      newFile.createdAt = event.item.createdAt;
      newFile.meta = newMeta;
      newFile.isDownloaded = false;
      if (fileInDb != null) newFile.isDownloaded = true;

      final relatedVideos = await ApiClient().getRelatedFiles(event.item.sId);
      final listFileDb = await DbDao().getContent();

      if (relatedVideos != null) {
        if (relatedVideos.data.items.length > 0) {
          for (var item in relatedVideos.data.items) {
            if (item.fileIds.length > 0) {
              for (var file in item.fileIds) {
                file.isDownloaded = false;

                if (listFileDb != null) {
                  for (var fileDb in listFileDb) {
                    if (file.sId == fileDb.sId) {
                      file.isDownloaded = true;
                    }
                  }
                }

                if (file.filetype == 'application/pdf') {
                  listBook.add(file);
                }
              }

              listBook.removeWhere((i) => i.meta == null);
              yield SelectedBook(newFile, listBook);
            }
          }
        }
      } else {
        yield FailedToLoadBook();
      }
    } catch (e) {
      print('failed to open video : $e');
      yield FailedToLoadBook();
    }
  }
}

class LatestAudioBloc extends Bloc<LatestAudioEvent, LatestAudioState> {
  @override
  LatestAudioState get initialState => InitLatestAudioState();

  @override
  Stream<LatestAudioState> mapEventToState(
      LatestAudioEvent event,
      ) async* {
    if (event is LoadLatestAudio) {
      yield* _loadLatestAudioToState(event);
    } else if (event is SelectAudio) {
      yield* _selectAudioToState(event);
    }
  }

  Stream<LatestAudioState> _loadLatestAudioToState(event) async* {
    yield LoadingAudio(false);
    try {
      final result = await ApiClient().getLatestFile('audio/mp3', event.pageSize);
      if (result != null) {
        if (result.data.items != null) {
          if (result.data.items.length > 0) {
            result.data.items.removeWhere((item) => item.meta == null);
            yield LatestAudioLoaded(result);
          } else {
            yield LatestAudioIsEmpty();
          }
        }
      } else {
        yield LoadLatestAudioFailed();
      }
    } catch (e) {
      print('Load latestAudio failed : $e');
      yield LoadLatestAudioFailed();
    }
  }

  Stream<LatestAudioState> _selectAudioToState(event) async* {
    yield LoadingAudio(true);
    try {
      List<FileIds> listVideo = new List<FileIds>();

      final fileInDb = await DbDao().getSingleContent(event.item.sId);

      FileMeta newMeta = new FileMeta();
      newMeta.title = event.item.meta.title;
      newMeta.thumbnail = event.item.meta.thumbnail;
      newMeta.pages = event.item.meta.pages;
      newMeta.duration = event.item.meta.duration;
      newMeta.description = event.item.meta.description;
      FileIds newFile = new FileIds();
      newFile.sId = event.item.sId;
      newFile.name = event.item.name;
      newFile.createdAt = event.item.createdAt;
      newFile.meta = newMeta;
      newFile.isDownloaded = false;
      if (fileInDb != null) newFile.isDownloaded = true;

      final relatedVideos = await ApiClient().getRelatedFiles(event.item.sId);
      final listFileDb = await DbDao().getContent();

      if (relatedVideos != null) {
        if (relatedVideos.data.items.length > 0) {
          for (var item in relatedVideos.data.items) {
            if (item.fileIds.length > 0) {
              for (var file in item.fileIds) {
                file.isDownloaded = false;

                if (listFileDb != null) {
                  for (var fileDb in listFileDb) {
                    if (file.sId == fileDb.sId) {
                      file.isDownloaded = true;
                    }
                  }
                }

                if (file.filetype == 'audio/mp3') {
                  listVideo.add(file);
                }
              }

              listVideo.removeWhere((i) => i.meta == null);
              yield SelectedAudio(newFile, listVideo);
            }
          }
        }
      } else {
        yield FailedToLoadAudio();
      }
    } catch (e) {
      print('failed to open audio : $e');
      yield FailedToLoadAudio();
    }
  }
}