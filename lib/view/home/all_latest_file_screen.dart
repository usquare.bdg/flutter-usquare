import 'package:bloc/bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox_tryout/models/file.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/content/list_content/list_content_screen.dart';
import 'package:edubox_tryout/view/content/open_book/open_book_screen.dart';
import 'package:edubox_tryout/view/content/play_audio/play_audio_screen.dart';
import 'package:edubox_tryout/view/content/play_video/play_video_screen.dart';
import 'package:edubox_tryout/view/home/home_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shimmer/shimmer.dart';

class AllLatestFileScreen extends StatefulWidget {
  final String title, type, baseUrl;

  const AllLatestFileScreen({Key key, this.title, this.type, this.baseUrl})
      : super(key: key);
  @override
  _AllLatestFileScreenState createState() => _AllLatestFileScreenState();
}

class _AllLatestFileScreenState extends State<AllLatestFileScreen> {
  Bloc _bloc;
  Files _listFile;
  bool _isLoading = true;
  bool _isFailed = false;
  bool _isEmpty = false;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('All Latest File Screen');
    MixpanelUtils().initPlatformState('All Latest File Screen');

    _listFile = new Files();
    _initBloc();
    super.initState();
  }

  void _initBloc() {
    if (widget.type == 'mp4') {
      _bloc = LatestVideoBloc()..add(LoadLatestVideo(-1));
    } else if (widget.type == 'mp3') {
      _bloc = LatestAudioBloc()..add(LoadLatestAudio(-1));
    } else {
      _bloc = LatestBookBloc()..add(LoadLatestBook(-1));
    }
  }

  void _onRefresh() {
    if (widget.type == 'mp4') {
      _bloc.add(LoadLatestVideo(-1));
    } else if (widget.type == 'mp3') {
      _bloc.add(LoadLatestAudio(-1));
    } else {
      _bloc.add(LoadLatestBook(-1));
    }
  }

  void _loadVideoState(state) {
    if (state is LatestVideoIsEmpty) {
      setState(() {
        _isFailed = false;
        _isEmpty = true;
        _isLoading = false;
      });
    }
    if (state is LoadLatestVideoFailed) {
      setState(() {
        _isFailed = true;
        _isEmpty = false;
        _isLoading = false;
      });
    }
    if (state is LatestVideoLoaded) {
      setState(() {
        _listFile = state.files;
        _isFailed = false;
        _isEmpty = false;
        _isLoading = false;
      });
    }
    if (state is LoadingVideo) {
      if (state.isLoadingPlaylist) {
        _loadingFile();
      } else {
        setState(() {
          _isLoading = true;
        });
      }
    }
    if (state is SelectedVideo) {
      Navigator.of(context, rootNavigator: true).pop();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PlayVideoScreen(
                    babName: 'Tidak diketahui',
                    subBabName: 'Tidak diketahui',
                    baseUrl: widget.baseUrl,
                    nowPlaying: state.nowPlaying,
                    listVideo: state.listVideo,
                  )));
    }
    if (state is FailedToLoadVideo) {
      Navigator.of(context, rootNavigator: true).pop();
      MyToast().toast('Gagal mengambil data');
    }
  }

  void _loadAudioState(state) {
    if (state is LatestAudioIsEmpty) {
      setState(() {
        _isFailed = false;
        _isEmpty = true;
        _isLoading = false;
      });
    }
    if (state is LoadLatestAudioFailed) {
      setState(() {
        _isFailed = true;
        _isEmpty = false;
        _isLoading = false;
      });
    }
    if (state is LatestAudioLoaded) {
      setState(() {
        _listFile = state.files;
        _isFailed = false;
        _isEmpty = false;
        _isLoading = false;
      });
    }
    if (state is LoadingVideo) {
      if (state.isLoadingPlaylist) {
        _loadingFile();
      } else {
        setState(() {
          _isLoading = true;
        });
      }
    }
    if (state is LoadingAudio) {
      if (state.isLoadingPlaylist) {
        _loadingFile();
      } else {
        setState(() {
          _isLoading = true;
        });
      }
    }
    if (state is FailedToLoadAudio) {
      Navigator.of(context, rootNavigator: true).pop();
      MyToast().toast('Gagal mengambil data');
    }
    if (state is SelectedAudio) {
      Navigator.of(context, rootNavigator: true).pop();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PlayAudioScreen(
                    subBabName: 'Tidak diketahui',
                    baseUrl: widget.baseUrl,
                    nowPlaying: state.nowPlaying,
                    listAudio: state.listAudio,
                  )));
    }
  }

  void _loadBookState(state) {
    if (state is LatestBookIsEmpty) {
      setState(() {
        _isFailed = false;
        _isEmpty = true;
        _isLoading = false;
      });
    }
    if (state is LoadLatestBookFailed) {
      setState(() {
        _isFailed = true;
        _isEmpty = false;
        _isLoading = false;
      });
    }
    if (state is LatestBookLoaded) {
      setState(() {
        _listFile = state.files;
        _isFailed = false;
        _isEmpty = false;
        _isLoading = false;
      });
    }
    if (state is LoadingVideo) {
      if (state.isLoadingPlaylist) {
        _loadingFile();
      } else {
        setState(() {
          _isLoading = true;
        });
      }
    }
    if (state is LoadingBook) {
      if (state.isLoadingPlaylist) {
        _loadingFile();
      } else {
        setState(() {
          _isLoading = true;
        });
      }
    }
    if (state is FailedToLoadBook) {
      Navigator.of(context, rootNavigator: true).pop();
      MyToast().toast('Gagal mengambil data');
    }
    if (state is SelectedBook) {
      Navigator.of(context, rootNavigator: true).pop();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => OpenBookScreen(
                    listBook: state.listBook,
                    file: state.file,
                  )));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [EduColors.primary, EduColors.primaryDark],
            stops: [0.0, 1.0],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            tileMode: TileMode.clamp),
        title: Text(
          widget.title,
          style: EduText().mediumBold.copyWith(color: Colors.white),
        ),
      ),
      body: BlocListener(
        bloc: _bloc,
        listener: (context, state) {
          if (widget.type == 'mp4') {
            _loadVideoState(state);
          } else if (widget.type == 'mp3') {
            _loadAudioState(state);
          } else {
            _loadBookState(state);
          }
        },
        child: _isLoading
            ? _loadingView()
            : _isFailed
                ? _loadFailedView()
                : _isEmpty
                    ? _listEmptyView()
                    : ListView.builder(
                        itemCount: _listFile.data.items.length,
                        padding: EdgeInsets.only(
                            top: widget.type == 'mp3' ? 8.0 : 16.0,
                            left: 8.0,
                            right: 8.0),
                        itemBuilder: (context, index) {
                          if (widget.type == 'mp4') {
                            return _videoItem(index);
                          } else if (widget.type == 'mp3') {
                            return _audioItem(index);
                          } else {
                            return _bookItem(index);
                          }
                        },
                      ),
      ),
    );
  }

  Widget _videoItem(index) {
    return InkWell(
      onTap: () {
//        _bloc.add(SelectVideo(_listFile.data.items[index]));
      },
      child: Padding(
        padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
            top: 10.0,
            bottom: index == _listFile.data.items.length - 1 ? 25.0 : 10.0),
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 70.0,
            child: Row(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    _listFile.data.items[index].meta.thumbnail != ""
                        ? CachedNetworkImage(
                            imageUrl:
                                '${widget.baseUrl}/file/${_listFile.data.items[index].meta.thumbnail}',
                            imageBuilder: (context, imageProvider) => Container(
                                  height: 70.0,
                                  width: 120.0,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover,
                                      )),
                                ),
                            placeholder: (context, url) =>
                                _loadingImage('video'),
                            errorWidget: (context, url, error) =>
                                _imageError('video'))
                        : Container(
                            width: 120.0,
                            height: 70.0,
                            color: Colors.grey[300],
                            child: Center(
                              child: Icon(
                                Icons.play_circle_outline,
                                size: 36.0,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 4.0),
                        color: Colors.black54,
                        child: Center(
                          child:
                              _listFile.data.items[index].meta.duration != null
                                  ? Text(
                                      '${(_listFile.data.items[index].meta.duration / 60).round()}.${(_listFile.data.items[index].meta.duration % 60).toString().padLeft(2, '0')}',
                                      style: EduText().smallSemiBold.copyWith(
                                          color: Colors.white, fontSize: 10.0),
                                    )
                                  : Text(
                                      '0.00',
                                      style: EduText().smallSemiBold.copyWith(
                                          color: Colors.white, fontSize: 10.0),
                                    ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  width: 10.0,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        _listFile.data.items[index].meta.title,
                        style: EduText().mediumSemiBold,
                        maxLines: 2,
                      ),
//                    SizedBox(height: 2.0,),
//                    Text(_listVideo[index].speaker, style: EduText().smallRegular)
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  Widget _audioItem(index) {
    return InkWell(
      onTap: () {
        _bloc.add(SelectAudio(_listFile.data.items[index]));
      },
      child: Padding(
        padding: EdgeInsets.only(
            left: 0.0,
            right: 8.0,
            top: 8.0,
            bottom: index == _listFile.data.items.length - 1 ? 25.0 : 8.0),
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 70.0,
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: 60.0,
                  height: 60.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey[300],
                  ),
                  child: Center(
                    child: Icon(
                      Icons.play_circle_filled,
                      size: 36.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Expanded(
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        _listFile.data.items[index].meta.title,
                        style: EduText().mediumSemiBold,
                        maxLines: 2,
                      )),
                ),
                SizedBox(
                  width: 15.0,
                ),
                _listFile.data.items[index].meta.duration != null
                    ? Text(
                        '${(_listFile.data.items[index].meta.duration / 60).round()}.${(_listFile.data.items[index].meta.duration % 60).toString().padLeft(2, '0')}',
                        style: EduText().smallSemiBold)
                    : Text(
                        '0.0',
                        style: EduText().smallSemiBold,
                      )
              ],
            )),
      ),
    );
  }

  Widget _bookItem(index) {
    return InkWell(
      onTap: () {
//        _bloc.add(SelectBook(_listFile.data.items[index]));
      },
      child: Padding(
        padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
            top: 10.0,
            bottom: index == _listFile.data.items.length - 1 ? 25.0 : 10.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 90.0,
          child: Row(
            children: <Widget>[
              Container(
                width: 60.0,
                height: 90.0,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: _listFile.data.items[index].meta.thumbnail != ""
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: CachedNetworkImage(
                            imageUrl:
                                '${widget.baseUrl}/file/${_listFile.data.items[index].meta.thumbnail}',
                            imageBuilder: (context, imageProvider) => Container(
                                  width: 60.0,
                                  height: 90.0,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(5.0),
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover,
                                      )),
                                ),
                            placeholder: (context, url) =>
                                _loadingImage('book'),
                            errorWidget: (context, url, error) =>
                                _imageError('book')))
                    : Center(
                        child: Icon(
                          Icons.book,
                          size: 36.0,
                          color: Colors.grey,
                        ),
                      ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _listFile.data.items[index].meta.title,
                      style: EduText().mediumSemiBold,
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text('${_listFile.data.items[index].meta.pages} halaman',
                        style: EduText().smallRegular),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _loadingFile() {
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: 'Tunggu Sebentar',
          content: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: SpinKitThreeBounce(
              color: EduColors.primary,
              size: 24.0,
            ),
          ),
        );
      },
    );
  }

  Widget _loadFailedView() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/no-wifi.png', width: 80.0),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Text(
                'Kamu sedang offline',
                style: EduText().mediumSemiBold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
              child: Text(
                'Lihat hasil download untuk menikmati konten\ntanpa internet.',
                style: EduText().smallRegular,
                textAlign: TextAlign.center,
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListContentScreen(
                              subBabName: 'Tidak diketahui',
                              subBabId: 'Tidak diketahui',
                              babName: 'Tidak diketahui',
                              onlyDownloadedFile: true,
                            )));
              },
              color: EduColors.primary,
              child: Text(
                'Lihat hasil download',
                style: EduText().smallSemiBold.copyWith(color: Colors.white),
              ),
            ),
            SizedBox(height: 5.0),
            InkWell(
              onTap: () {
                _onRefresh();
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Text(
                  'Muat ulang',
                  style: EduText()
                      .smallSemiBold
                      .copyWith(color: EduColors.primary),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _listEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/empty.png', width: 80.0),
          SizedBox(height: 40.0),
          Container(
            child: Text(
              'Tidak ada data',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _loadingView() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: SpinKitRing(
            color: EduColors.primary,
            size: 50.0,
            lineWidth: 3.0,
          ),
        ));
  }

  Widget _loadingImage(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
        child: Shimmer.fromColors(
          child: Text(Strings.appName,
              style: EduText()
                  .mediumBold
                  .copyWith(fontSize: type == 'video' ? 14.0 : 8.0)),
          period: Duration(milliseconds: 500),
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[200],
        ),
      ),
    );
  }

  Widget _imageError(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
          child: Icon(
        Icons.error,
        color: Colors.grey,
      )),
    );
  }
}
