import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox_tryout/models/file.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/content/list_content/list_content_screen.dart';
import 'package:edubox_tryout/view/content/open_book/open_book_screen.dart';
import 'package:edubox_tryout/view/content/play_audio/play_audio_screen.dart';
import 'package:edubox_tryout/view/content/play_video/play_video_screen.dart';
import 'package:edubox_tryout/view/home/all_latest_file_screen.dart';
import 'package:edubox_tryout/view/home/home_bloc/bloc.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/view/main_screen/main_screen_bloc/bloc.dart';
import 'package:edubox_tryout/view/register_student/register_student_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  AnimationController _controller, _controllerListAudio;
  Animation _fadeAnimation, _listAudioAnimation,  _appBarColorTween, _elevationTween, _appBarTitleColorTween;
  UserBloc _userBloc;
  User _user;
  Files _listVideo;
  Files _listBook;
  Files _listAudio;
  LatestVideoBloc _latestVideoBloc;
  LatestBookBloc _latestBookBloc;
  LatestAudioBloc _latestAudioBloc;
  bool _showAction = false;
  bool _isLoadingUser = true;
  bool _isLoadingVideo = true;
  bool _fetchVideoIsFailed = false;
  bool _videoIsEmpty = false;
  bool _isLoadingBook = true;
  bool _fetchBookIsFailed = false;
  bool _bookIsEmpty = false;
  bool _isLoadingAudio = true;
  bool _fetchAudioIsFailed = false;
  bool _audioIsEmpty = false;
  bool _isOffline = false;
  String _baseUrl = '';
  List<String> _displayName = List<String>();

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Home Screen');

    _getUrl().then((url) {
      _baseUrl = url;
    });

    // ANIMATION
    _controllerListAudio = AnimationController(vsync: this, duration: Duration(seconds: 0));
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 0));
    _fadeAnimation = Tween(begin: 0.0, end: 1.0).animate(_controllerListAudio);
    _elevationTween = Tween(begin: 0.0, end: 4.0).animate(_controller);
    _appBarColorTween = ColorTween(begin: Colors.transparent, end: Colors.white).animate(_controller);
    _appBarTitleColorTween = ColorTween(begin: Colors.white, end: EduColors.black).animate(_controller);
    _listAudioAnimation = Tween(begin: 1.0, end: 0.0).animate(_controllerListAudio);

    // INIT
    _user = User();
    _listBook = new Files();
    _listVideo = new Files();
    _listAudio = new Files();
    _initBloc();
    _displayName = List<String>();

    super.initState();
  }

  bool _scrollListAudioListener(ScrollNotification scrollInfo) {
    if (scrollInfo.metrics.axis == Axis.horizontal) {
      _controllerListAudio.animateTo(scrollInfo.metrics.pixels / 50);
    }
  }

  bool _scrollListener(ScrollNotification scrollInfo) {
    if (scrollInfo.metrics.axis == Axis.vertical) {
      _controller.animateTo(scrollInfo.metrics.pixels / 100);
    }
  }

  void _initBloc() {
    _userBloc = UserBloc()
      ..add(LoadUser());
    _latestVideoBloc = LatestVideoBloc()
      ..add(LoadLatestVideo(5));
    _latestBookBloc = LatestBookBloc()
      ..add(LoadLatestBook(5));
    _latestAudioBloc = LatestAudioBloc()
      ..add(LoadLatestAudio(5));
  }

  Future _onRefresh() async {
    setState(() {
      _isOffline = false;
    });
    _userBloc.add(LoadUser());
    _latestVideoBloc.add(LoadLatestVideo(5));
    _latestAudioBloc.add(LoadLatestAudio(5));
    _latestBookBloc.add(LoadLatestBook(5));
  }

  Future _getUrl() async {
    return await ApiClient().getUrl();
  }

  Future _launchURL() async {
    String url = 'http://zenpres.box';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _controllerListAudio.dispose();
    super.dispose();
  }

  Future _moveToLoginScreen() async {
    final result = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => LoginScreen()
    ));

    if (result) {
      _onRefresh();
    }
  }

  Future _moveToPlayVideoScreen(state) async {
    final result = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => PlayVideoScreen(
          babName: 'Tidak diketahui',
          subBabName: 'Tidak diketahui',
          baseUrl: _baseUrl,
          nowPlaying: state.nowPlaying,
          listVideo: state.listVideo,
        )
    ));

    if (result) {
      _userBloc.add(LoadUser());
    }
  }

  Future _moveToPlayAudioScreen(state) async {
    final result = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => PlayAudioScreen(
          subBabName: 'Tidak diketahui',
          baseUrl: _baseUrl,
          nowPlaying: state.nowPlaying,
          listAudio: state.listAudio,
        )
    ));

    if (result) {
      _userBloc.add(LoadUser());
    }
  }

  void _profileDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) => CustomDialog(
          content: Column(
            children: <Widget>[
              Container(
                width: 60.0,
                height: 60.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: EduColors.primaryDark,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Image.asset('assets/user.png'),
                ),
              ),
              SizedBox(height: 30.0,),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text(_user.data.displayName.toUpperCase(), style: EduText().mediumSemiBold,),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text(_user.data.email, style: EduText().smallRegular,),
              ),
              SizedBox(height: 20.0,),
              FlatButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                  BlocProvider.of<MainScreenBloc>(context).add(LoadProfileScreen());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Lihat Profil', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),),
                  ],
                ),
                color: EduColors.primary,
              ),
              SizedBox(height: 5.0,),
              FlatButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                  _logOutDialog();
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Keluar', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.red
                    ),),
                    SizedBox(width: 5.0,),
                    Icon(Icons.exit_to_app, color: EduColors.red, size: 20.0,)
                  ],
                ),
              )
            ],
          ),
        )
    );
  }

  void _logOutDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: Strings.appName,
            content: Padding(
              padding: const EdgeInsets.only(
                  bottom: 16.0),
              child: Text('Apakah kamu yakin ingin keluar ?', style: EduText().mediumRegular),
            ),
            actionAlignment: MainAxisAlignment.center,
            actions: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 100.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(
                          width: 1.5,
                          color: EduColors.primary
                      )
                  ),
                  child: Center(
                      child: Text('Batal', style: EduText().smallSemiBold.copyWith(
                          color: EduColors.primary
                      ),)
                  ),
                ),
              ),
              SizedBox(width: 10.0,),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  _userBloc.add(LogOut());
                },
                child: Container(
                  width: 100.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: EduColors.primary,
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  child: Center(
                      child: Text('Yakin', style: EduText().smallSemiBold.copyWith(
                          color: Colors.white
                      ),)
                  ),
                ),
              ),
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _onRefresh,
            child: ScrollConfiguration(
              behavior: MyBehavior(),
              child: NotificationListener<ScrollNotification>(
                onNotification: _scrollListener,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      _headerView(),
                      _latestVideos(),
                      _latestAudio(),
                      _latestBooks(),
                      _offlineView(),
                      SizedBox(height: 30.0)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 80,
            child: AnimatedBuilder(
              animation: _controller,
              builder: (ctx, child) {
                return AppBar(
                  backgroundColor: _appBarColorTween.value,
                  elevation: _elevationTween.value,
                  iconTheme: IconThemeData(color: Colors.black),
                  title: Row(
                    children: <Widget>[
                      Text(Strings.appName, style: EduText().largeBold.copyWith(
                          color: _appBarTitleColorTween.value
                      )),
                      SizedBox(width: 3.0,),
                      /*Container(
                        width: 30.0,
                        height: 25.0,
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: EduColors.red,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20.0),
                              bottomRight: Radius.circular(20.0),
                              topLeft: Radius.circular(20.0)
                          ),
                          gradient: LinearGradient(
                              colors: [Colors.red, EduColors.red],
                              stops: [0.0, 1.0],
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              tileMode: TileMode.clamp
                          ),
                        ),
                        child: Center(
                          child: Text('Go', style: EduText().smallBold.copyWith(
                              color: Colors.white
                          )),
                        ),
                      ),*/
                    ],
                  ),
                  actions: <Widget>[
                    _isLoadingUser ? Container() : _action()
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _action() {
    if (_user == null) {
      return Row(
        children: <Widget>[
          _baseUrl != 'http://api.edu.box' ? InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => RegisterStudentScreen()
              ));
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Container(
                width: 60.0,
                height: 35.0,
                child: Center(
                  child: Text('Daftar', style: EduText().smallSemiBold.copyWith(
                      color: _appBarTitleColorTween.value,
                      decoration: TextDecoration.underline
                  ),
                  ),
                ),
              ),
            ),
          ) : Container(),
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: InkWell(
              onTap: () {
                _moveToLoginScreen();
              },
              child: Container(
                width: 80.0,
                height: 35.0,
                color: EduColors.primaryDark,
                child: Center(
                  child: Text('Masuk', style: EduText().smallSemiBold.copyWith(
                      color: Colors.white
                  ),),
                ),
              ),
            ),
          ),
        ],
      );
    } else {
      return AnimatedBuilder(
        animation: _controller,
        builder: (context, child) {
          return Row(
            children: <Widget>[
              InkWell(
                onTap: () => _profileDialog(context),
                child: Row(
                  children: <Widget>[
                    Text(_user.data.displayName.split(' ')[0].toLowerCase(), style: EduText().smallSemiBold.copyWith(
                      color: _appBarTitleColorTween.value,
                    ),),
                    SizedBox(width: 8.0),
                    Container(
                        width: 30.0,
                        height: 30.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.black38
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: Image.asset('assets/user.png', fit: BoxFit.cover,),
                        )
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10.0),
            ],
          );
        },
      );
    }
  }

  Widget _headerView() {
    return BlocListener(
      bloc: _userBloc,
      listener: (context, state) {
        if (state is LoadingUser) {
          setState(() {
            _isLoadingUser = true;
          });
        }
        if (state is UserLoaded) {
          setState(() {
            _user = state.user;
            if (state.user != null) {
              _displayName = state.user.data.displayName.split(' ');
            }
            _isLoadingUser = false;
          });
        }
        if (state is LoadUserFailed) {
          setState(() {
            _user = null;
            _isLoadingUser = false;
          });
        }
        if (state is LoggedOut) {
          _userBloc.add(LoadUser());
        }
      },
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: MyClipper(),
            child: Container(
              height: 310.0,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [EduColors.primary, EduColors.purple],
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    stops: [0.2, 1.0],
                    tileMode: TileMode.clamp
                ),
              ),
              child: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 80.0, left: 16.0, right: 16.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _isLoadingUser ? Container() : _user != null ? Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Halo,', style: EduText().mediumSemiBold.copyWith(
                                  color: Colors.white
                              ),),
                              Text('${_user.data.displayName.toUpperCase()}', style: EduText().largeBold.copyWith(
                                color: Colors.white,
                              ),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(height: 10.0,),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.school, color: Colors.white, size: 16.0,),
                                  SizedBox(width: 5.0),
                                  Text('${_user.data.studentAtInstitutions[0].name}', style: EduText().smallRegular.copyWith(
                                    color: Colors.white,
                                  ),)
                                ],
                              ),
                            ],
                          ),
                        ) : Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Selamat datang di ${Strings.appName}', style: EduText().mediumSemiBold.copyWith(
                                color: Colors.white,
                              )),
                              SizedBox(height: 10.0,),
                              Text('Silakan masuk untuk mendapatkan akses unduh ujian, unduh konten dan mengumpulkan tugas.', style: EduText().smallRegular.copyWith(
                                  color: Colors.white
                              ),)
                            ],
                          ),
                        ),
                        Builder(
                          builder: (context) {
                            if (_isLoadingUser) return Container();
                            else if (_user != null && _user.data.studentAtInstitutions[0].logo != null) {
                              return Padding(
                                  padding: const EdgeInsets.only(left: 8.0, top: 0.0),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5.0),
                                    child: CachedNetworkImage(
                                        imageUrl: '$_baseUrl/file/${_user.data.studentAtInstitutions[0].logo}',
                                        imageBuilder: (context, imageProvider) => Container(
                                          height: 50.0,
                                          width: 65.0,
                                          decoration: BoxDecoration(
                                              color: Colors.grey[300],
                                              image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.fill,
                                              )
                                          ),
                                        ),
                                        placeholder: (context, url) => Container(),
                                        errorWidget: (context, url, error) => Icon(Icons.error)
                                    ),
                                  )
                              );
                            }
                            else return Container();
                          },
                        )
                      ],
                    ),
                  )
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(
              height: 110.0,
              margin: EdgeInsets.only(top: 200.0),
              padding: EdgeInsets.only(top: 15.0),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey[300],
                      blurRadius: 4.0,
                      offset: Offset(0.0, 5.0)
                  ),
                ],
              ),
              child: Row(
                children: <Widget>[
                  _menuItem(
                    title: 'Ujian',
                    image: 'assets/exam.png',
                    onTap: () => BlocProvider.of<MainScreenBloc>(context).add(LoadTryOutScreen()),
                  ),

                  _menuItem(
                    title: 'Tugas',
                    image: 'assets/assignment.png',
                    onTap: () => BlocProvider.of<MainScreenBloc>(context).add(LoadTaskScreen()),
                  ),
                  _menuItem(
                    title: 'Belajar',
                    image: 'assets/study.png',
                    onTap: () => BlocProvider.of<MainScreenBloc>(context).add(LoadContentScreen(0)),
                  ),
                  _menuItem(
                      title: 'Unduhan',
                      image: 'assets/download.png',
                      onTap: () => Navigator.push(context, MaterialPageRoute(
                          builder: (context) => ListContentScreen(
                            subBabName: 'Tidak diketahui',
                            subBabId: 'Tidak diketahui',
                            babName: 'Tidak diketahui',
                            onlyDownloadedFile: true,
                          )
                      ))
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _menuItem({
    String title,
    String image,
    Function onTap,
  }) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            children: <Widget>[
              Container(
                width: 50.0,
                height: 50.0,
                decoration: BoxDecoration(
                    color: EduColors.primaryDark,
                    shape: BoxShape.circle
                ),
                padding: EdgeInsets.all(10.0),
                child: Image.asset(image),
              ),
              SizedBox(height: 8.0,),
              Text('$title', style: EduText().smallSemiBold,
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _zenpresBox() {
    return _baseUrl == 'http://api.edu.box' ? InkWell(
      onTap: () => _launchURL(),
      child: Padding(
        padding: EdgeInsets.only(right: 16.0, left: 16.0, top: 16.0),
        child: Container(
          height: 50.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            gradient: LinearGradient(
                colors: [EduColors.primary, EduColors.purple],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                stops: [0.2, 1.0],
                tileMode: TileMode.clamp
            ),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[300],
                  blurRadius: 10.0,
                  offset: Offset(0.0, 3.0)
              )
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/default_category_icon.png', width: 25.0,),
              SizedBox(width: 10.0),
              Text('Pelajaran Sekolah', style: EduText().mediumSemiBold.copyWith(
                  color: Colors.white
              ),),
            ],
          ),
        ),
      ),
    ) : Container();
  }

  Widget _latestAudio() {
    return BlocListener(
      bloc: _latestAudioBloc,
      listener: (context, state) {
        if (state is LatestAudioIsEmpty) {
          setState(() {
            _audioIsEmpty = true;
            _isOffline = false;
            _fetchAudioIsFailed = false;
            _isLoadingAudio = false;
          });
        }
        if (state is LoadLatestAudioFailed) {
          setState(() {
            _isOffline = true;
            _fetchAudioIsFailed = true;
            _audioIsEmpty = false;
            _isLoadingAudio = false;
          });
        }
        if (state is LatestAudioLoaded) {
          setState(() {
            _listAudio = state.files;
            _isOffline = false;
            _fetchAudioIsFailed = false;
            _audioIsEmpty = false;
            _isLoadingAudio = false;
          });
        }
        if (state is LoadingAudio) {
          if (state.isLoadingPlaylist) {
            _loadingFile();
          } else {
            setState(() {
              _isLoadingAudio = true;
            });
          }
        }
        if (state is FailedToLoadAudio) {
          Navigator.of(context, rootNavigator: true).pop();
          MyToast().toast('Gagal mengambil data');
        }
        if (state is SelectedAudio) {
          Navigator.of(context, rootNavigator: true).pop();
          _moveToPlayAudioScreen(state);
        }
      },
      child: _isLoadingAudio ? _loadingView() : !_isOffline ? Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Stack(
          children: <Widget>[
            Container(
                height: 160.0,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
//                gradient: LinearGradient(
//                    colors: [EduColors.primary, EduColors.primary],
////                    colors: [Colors.white, EduColors.primary],
//                    stops: [0.4, 1.0],
//                    begin: Alignment.topCenter,
//                    end: Alignment.bottomCenter,
//                    tileMode: TileMode.clamp
//                ),
                    color: EduColors.primaryDark
                ),
                child: AnimatedBuilder(
                  animation: _controllerListAudio,
                  builder: (context, child) {
                    return Opacity(
                      opacity: _listAudioAnimation.value,
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 24.0),
                            child: Text('Audio\nTerbaru', style: EduText().largeBold.copyWith(
                                color: _listAudioAnimation.value == 0.0 ? Colors.transparent : Colors.white
                            ),
                              textAlign: TextAlign.center,
                            ),
                          )
                      ),
                    );
                  },
                )
            ),
            !_audioIsEmpty ? Container(
              height: 160.0,
              padding: EdgeInsets.symmetric(
                  vertical: 10.0
              ),
              child: NotificationListener<ScrollNotification>(
                onNotification: _scrollListAudioListener,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _listAudio.data.items.length,
                  padding: EdgeInsets.only(left: 100.0),
                  itemBuilder: (context, index) {
                    return _latestAudioItem(index);
                  },
                ),
              ),
            ) : Positioned.fill(
              child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 100.0),
                    child: _listEmptyView('audio'),
                  )
              ),
            ),
          ],
        ),
      ) : Container(),
    );
  }

  Widget _latestAudioItem(index) {
    return Row(
      children: <Widget>[
        InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => _latestAudioBloc.add(SelectAudio(_listAudio.data.items[index])),
          child: Padding(
            padding: EdgeInsets.only(
                left: 16.0, top: 8.0, bottom: 8.0,
                right: index == 4 ? 16.0 : 0.0
            ),
            child: Container(
              width: 100.0,
              height: 180.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      blurRadius: 10.0,
                      offset: Offset(0.0, 3.0)
                  )
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Container(
                        width: 100.0,
                        padding: EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(5.0),
                              topRight: Radius.circular(5.0),
                            )
                        ),
                        child: Image.asset('assets/audio.png')
                    ),
                  ),
                  Container(
                    height: 60.0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(_listAudio.data.items[index].meta.title,
                        style: EduText().smallSemiBold.copyWith(fontSize: 10.0),
                        maxLines: 3,),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Visibility(
          visible: index == 4 ? true : false,
          child: InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => AllLatestFileScreen(
                    baseUrl: _baseUrl,
                    title: 'Audio Terbaru',
                    type: 'mp3',
                  )
              ));
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 12.0),
              child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                      color: EduColors.primaryDark,
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  padding: EdgeInsets.all(5.0),
                  child: Center(
                    child: Text('Lihat\nSemua',
                      style: EduText().smallSemiBold.copyWith(
                          color: Colors.white
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _latestVideos() {
    return BlocListener(
      bloc: _latestVideoBloc,
      listener: (context, state) {
        if (state is LatestVideoIsEmpty) {
          setState(() {
            _isOffline = false;
            _fetchVideoIsFailed = false;
            _videoIsEmpty = true;
            _isLoadingVideo = false;
          });
        }
        if (state is LoadLatestVideoFailed) {
          setState(() {
            _isOffline = true;
            _fetchVideoIsFailed = true;
            _videoIsEmpty = false;
            _isLoadingVideo = false;
          });
        }
        if (state is LatestVideoLoaded) {
          setState(() {
            _listVideo = state.files;
            _isOffline = false;
            _fetchVideoIsFailed = false;
            _videoIsEmpty = false;
            _isLoadingVideo = false;
          });
        }
        if (state is LoadingVideo) {
          if (state.isLoadingPlaylist) {
            _loadingFile();
          } else {
            setState(() {
              _isLoadingVideo = true;
            });
          }
        }
        if (state is SelectedVideo) {
          Navigator.of(context, rootNavigator: true).pop();
          _moveToPlayVideoScreen(state);
        }
        if (state is FailedToLoadVideo) {
          Navigator.of(context, rootNavigator: true).pop();
          MyToast().toast('Gagal mengambil data');
        }
      },
      child: _isLoadingVideo ? _loadingView() : !_isOffline ? Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 24.0, bottom: 8.0,
                left: 16.0, right: 16.0
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Video Terbaru', style: EduText().largeBold,),
                InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => AllLatestFileScreen(
                          baseUrl: _baseUrl,
                          title: 'Video Terbaru',
                          type: 'mp4',
                        )
                    ));
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Center(
                      child: Text('Lihat semua', style: EduText().smallRegular.copyWith(
                        color: EduColors.primary,
                      ),),
                    ),
                  ),
                )
              ],
            ),
          ),
          _videoIsEmpty ? _listEmptyView('video') : Container(
            height: 150.0,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _listVideo.data.items.length,
              itemBuilder: (context, index) {
                return _latestVideoItem(index);
              },
            ),
          )
        ],
      ) : Container(),
    );
  }

  Widget _latestVideoItem(index) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => _latestVideoBloc.add(SelectVideo(_listVideo.data.items[index])),
      child: Padding(
        padding: EdgeInsets.only(
            left: 16.0, top: 8.0,
            right: index == 4 ? 16.0 : 0.0
        ),
        child: Container(
          width: 200.0,
          height: 100.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Builder(
                    builder: (context) {
                      if (_listVideo.data.items[index].meta.thumbnail != null && _listVideo.data.items[index].meta.thumbnail != "") {
                        return CachedNetworkImage(
                            imageUrl: '$_baseUrl/file/${_listVideo.data.items[index].meta.thumbnail}',
                            imageBuilder: (context, imageProvider) => Container(
                              height: 100.0,
                              width: 200.0,
                              decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  )
                              ),
                            ),
                            placeholder: (context, url) => _loadingImage('video'),
                            errorWidget: (context, url, error) => _imageError('video')
                        );
                      } else {
                        return Container(
                          width: 200.0,
                          height: 100.0,
                          color: Colors.grey[300],
                          child: Center(
                            child: Icon(Icons.play_circle_outline, size: 40.0, color: Colors.grey,),
                          ),
                        );
                      }
                    },
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 2.0,
                          horizontal: 4.0
                      ),
                      color: Colors.black54,
                      child: Center(
                        child: Text(
                          '${(_listVideo.data.items[index].meta.duration / 60).round()}.${(_listVideo.data.items[index].meta.duration % 60).toString().padLeft(2, '0')}',
                          style: EduText().smallSemiBold.copyWith(
                              color: Colors.white,
                              fontSize: 10.0
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Container(
                  width: 200.0,
                  height: 40.0,
                  padding: EdgeInsets.only(top: 8.0),
                  child: Text(
                    _listVideo.data.items[index].meta.title, style: EduText().smallSemiBold,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  )
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _latestBooks() {
    return BlocListener(
      bloc: _latestBookBloc,
      listener: (context, state) {
        if (state is LatestBookIsEmpty) {
          setState(() {
            _bookIsEmpty = true;
            _isOffline = false;
            _fetchBookIsFailed = false;
            _isLoadingBook = false;
          });
        }
        if (state is LoadLatestBookFailed) {
          setState(() {
            _isOffline = true;
            _fetchBookIsFailed = true;
            _bookIsEmpty = false;
            _isLoadingBook = false;
          });
        }
        if (state is LatestBookLoaded) {
          setState(() {
            _listBook = state.files;
            _isOffline = false;
            _fetchBookIsFailed = false;
            _bookIsEmpty = false;
            _isLoadingBook = false;
          });
        }
        if (state is LoadingBook) {
          if (state.isLoadingPlaylist) {
            _loadingFile();
          } else {
            setState(() {
              _isLoadingBook = true;
            });
          }
        }
        if (state is FailedToLoadBook) {
          Navigator.of(context, rootNavigator: true).pop();
          MyToast().toast('Gagal mengambil data');
        }
        if (state is SelectedBook) {
          Navigator.of(context, rootNavigator: true).pop();
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => OpenBookScreen(
                listBook: state.listBook,
                file: state.file,
              )
          ));
        }
      },
      child: _isLoadingBook ? _loadingView() : !_isOffline ? Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 24.0, bottom: 8.0,
                left: 16.0, right: 16.0
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Buku Terbaru', style: EduText().largeBold,),
                InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => AllLatestFileScreen(
                          baseUrl: _baseUrl,
                          title: 'Buku Terbaru',
                          type: 'book',
                        )
                    ));
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Center(
                      child: Text('Lihat semua', style: EduText().smallRegular.copyWith(
                        color: EduColors.primary,
                      ),),
                    ),
                  ),
                )
              ],
            ),
          ),
          _bookIsEmpty ? _listEmptyView('buku') : Container(
            height: 230.0,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _listBook.data.items.length,
              itemBuilder: (context, index) {
                return _latestBookItem(index);
              },
            ),
          )
        ],
      ) : Container(),
    );
  }

  Widget _latestBookItem(index) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => _latestBookBloc.add(SelectBook(_listBook.data.items[index])),
      child: Padding(
        padding: EdgeInsets.only(
            left: 16.0, top: 8.0,
            right: index == 4 ? 16.0 : 0.0
        ),
        child: Container(
          width: 120.0,
          constraints: BoxConstraints(
              minHeight: 235.0
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: CachedNetworkImage(
                      imageUrl: '$_baseUrl/file/${_listBook.data.items[index].meta.thumbnail}',
                      imageBuilder: (context, imageProvider) => Container(
                        width: 120.0,
                        height: 160.0,
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(5.0),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            )
                        ),
                      ),
                      placeholder: (context, url) => _loadingImage('book'),
                      errorWidget: (context, url, error) => _imageError('book')
                  )
              ),
              Container(
                width: 200.0,
                height: 60.0,
                padding: EdgeInsets.only(top: 8.0),
                child: Text(
                  _listBook.data.items[index].meta.title, style: EduText().smallSemiBold,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _imageError(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
          child: Icon(Icons.error, color: Colors.grey,)
      ),
    );
  }

  Widget _loadingImage(type) {
    return Container(
      width: type == 'video' ? 200.0 : 100.0,
      height: type == 'video' ? 100 : 235,
      color: Colors.grey[200],
      child: Center(
        child: Shimmer.fromColors(
          child: Text(Strings.appName, style: EduText().mediumBold.copyWith(
              fontSize: type == 'video' ? 22.0 : 14.0
          )),
          period: Duration(milliseconds: 500),
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[200],
        ),
      ),
    );
  }

  Widget _loadingView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
              top: 24.0, bottom: 8.0,
              left: 16.0, right: 16.0
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Shimmer.fromColors(
                child: Container(
                  width: 130.0,
                  height: 30.0,
                  color: Colors.white,
                ),
                period: Duration(milliseconds: 500),
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[200],
              ),
              Shimmer.fromColors(
                child: Container(
                  width: 60.0,
                  height: 30.0,
                  color: Colors.white,
                ),
                period: Duration(milliseconds: 500),
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[200],
              ),
            ],
          ),
        ),
        Container(
          height: 150.0,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 5,
            itemBuilder: (context, index) {
              return Column(
                children: <Widget>[
                  Shimmer.fromColors(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 16.0, top: 8.0,
                          right: index == 4 ? 16.0 : 0.0
                      ),
                      child: Container(
                        width: 200.0,
                        height: 100.0,
                        color: Colors.white,
                      ),
                    ),
                    period: Duration(milliseconds: 500),
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[200],
                  ),
                  Shimmer.fromColors(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 16.0, top: 16.0,
                          right: index == 4 ? 16.0 : 0.0
                      ),
                      child: Container(
                        width: 200.0,
                        height: 25.0,
                        color: Colors.white,
                      ),
                    ),
                    period: Duration(milliseconds: 500),
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[200],
                  ),
                ],
              );
            },
          ),
        )
      ],
    );
  }

  void _loadingFile() {
    showDialog(
        context: context,
        builder: (context) {
          return CustomDialog(
            title: 'Tunggu Sebentar',
            content: Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: SpinKitThreeBounce(
                color: EduColors.primary,
                size: 24.0,
              ),
            ),
          );
        },
        barrierDismissible: false
    );
  }

  Widget _offlineView() {
    return _isOffline ? Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Image.asset('assets/no-wifi.png', width: 80.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Text('Kamu sedang offline', style: EduText().mediumSemiBold,),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
                child: Text('Lihat unduhan untuk menikmati konten\ntanpa internet.',
                  style: EduText().smallRegular,
                  textAlign: TextAlign.center,
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ListContentScreen(
                        subBabName: 'Tidak diketahui',
                        subBabId: 'Tidak diketahui',
                        babName: 'Tidak diketahui',
                        onlyDownloadedFile: true,
                      )
                  ));
                },
                color: EduColors.primary,
                child: Text('Lihat unduhan', style: EduText().smallSemiBold.copyWith(
                    color: Colors.white
                ),),
              ),
              SizedBox(height: 5.0),
              InkWell(
                onTap: () {
                  _onRefresh();
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  child: Text('Muat ulang', style: EduText().smallSemiBold.copyWith(
                      color: EduColors.primary
                  ),),
                ),
              )
            ],
          ),
        ),
      ),
    ) : Container();
  }

  Widget _listEmptyView(String type) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        width: double.infinity,
        height: 130.0,
        decoration: BoxDecoration(
          color: Colors.grey[100],
//          border: Border.all(width: 1.0, color: Colors.grey[300])
        ),
        child: Center(
          child: Text('Tidak ada $type', style: EduText().smallRegular,),
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height - 65);
    path.quadraticBezierTo(size.width / 2, size.height, size.width, size.height - 65);
    path.lineTo(size.width, 0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
