import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/models/assignment.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import './bloc.dart';

class AssignmentBloc extends Bloc<AssignmentEvent, AssignmentState> {
  @override
  AssignmentState get initialState => InitialTaskState();

  @override
  Stream<AssignmentState> mapEventToState(
    AssignmentEvent event,
  ) async* {
    if (event is LoadAssignment) {
      yield* _loadTaskToState(event);
    } else if (event is SubmitAssignment) {
      yield* _submitAssignmentToState(event);
    } else if (event is LoadAssignmentResult) {
      yield* _loadAssignmentResultToState(event);
    }
  }

  Stream<AssignmentState> _loadAssignmentResultToState(event) async* {
    yield Loading();

    try {
      final user = await DbDao().getUser();
      final result = await ApiClient().getAssignmentResult(event.id, user.data.sId);

      if (result != null) {
        if (result.data.items.length > 0) {
          yield AssignmentResultLoaded(true, result);
        } else {
          yield AssignmentResultLoaded(false);
        }
      } else yield LoadAssignmentResultFailed();
    } catch (e) {
      print(e);
      yield LoadAssignmentResultFailed();
    }
  }

  Stream<AssignmentState> _submitAssignmentToState(event) async* {
    yield Submitting();
    try {
      final user = await DbDao().getUser();
      event.body.rombelId = user.data.rombelIds.last.sId;
      event.body.studentId = user.data.sId;

      final getAssignment = await ApiClient().getAssignmentResult(event.id, user.data.sId);

      if (getAssignment != null) {
        if (getAssignment.data.items.length > 0) {

          if (event.file != null) {
            if (event.file.lengthSync() > 10000000) {
              yield FileOverSized();
            } else {
              await ApiClient().deleteFile(event.body.fileIds);

              final uploadFile = await ApiClient().createFile(event.file, user.data.studentAtInstitutions[0].sId);

              if (uploadFile != null) {
                event.body.fileIds = uploadFile.data.sId;

                final update = await ApiClient().updateAssignmentResult(event.body, getAssignment.data.items[0].sId);

                if (update) {
                  yield AssignmentSubmitted();
                } else {
                  yield SubmitAssignmentFailed();
                }
              }
            }
          } else {
            final update = await ApiClient().updateAssignmentResult(event.body, getAssignment.data.items[0].sId);

            if (update) {
              yield AssignmentSubmitted();
            } else {
              yield SubmitAssignmentFailed();
            }
          }
        } else {
          if (event.file != null) {
            if (event.file.lengthSync() > 10000000) {
              yield FileOverSized();
            } else {
              final uploadFile = await ApiClient().createFile(event.file, user.data.studentAtInstitutions[0].sId);

              if (uploadFile != null) {
                event.body.fileIds = uploadFile.data.sId;
                final result = await ApiClient().createAssignmentResult(event.body);

                if (result) {
                  yield AssignmentSubmitted();
                } else {
                  yield SubmitAssignmentFailed();
                }
              } else {
                yield SubmitAssignmentFailed();
              }
            }
          } else {
            final result = await ApiClient().createAssignmentResult(event.body);

            if (result) {
              yield AssignmentSubmitted();
            } else {
              yield SubmitAssignmentFailed();
            }
          }
        }
      } else {
        yield SubmitAssignmentFailed();
      }
    } catch(e) {
      print('failed to upload : $e');
      yield SubmitAssignmentFailed();
    }
  }

  Stream<AssignmentState> _loadTaskToState(event) async* {
    yield LoadingAssignment();
    try {
      final user = await DbDao().getUser();

      if (user != null) {
        String institutionId = user.data.studentAtInstitutions[0].sId;
        String rombelId = user.data.rombelIds.last.sId;

        final lesson = await ApiClient().getLessons(rombelId);

        if (lesson != null) {
          Assignment assignment;

          if (event.lessonId != null) {
            assignment = await ApiClient().getAssignment(institutionId, event.lessonId);
          } else {
            assignment = await ApiClient().getAssignment(institutionId, lesson.data.items[0].sId);
          }

          if (assignment != null) {
            yield AssignmentLoaded(
              assignment: assignment,
              lesson: lesson
            );
          } else {
            yield LoadAssignmentFailed();
          }
        } else {
          yield LoadAssignmentFailed();
        }
      } else {
        yield UserNotFound();
      }
    } catch(e) {
      print('Load task failed : $e');
      yield LoadAssignmentFailed();
    }
  }
}
