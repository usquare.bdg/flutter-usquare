import 'dart:io';

import 'package:edubox_tryout/models/assignment.dart';
import 'package:equatable/equatable.dart';

abstract class AssignmentEvent extends Equatable {
  const AssignmentEvent();
}

class LoadAssignment extends AssignmentEvent {
  final String lessonId;

  LoadAssignment({this.lessonId});
  @override
  List<Object> get props => null;
}

class SubmitAssignment extends AssignmentEvent {
  final AssignmentBody body;
  final File file;
  final String id;

  SubmitAssignment(this.body, this.file, this.id);
  @override
  List<Object> get props => null;
}

class LoadAssignmentResult extends AssignmentEvent {
  final String id;

  LoadAssignmentResult(this.id);
  @override
  // TODO: implement props
  List<Object> get props => null;
}