import 'package:edubox_tryout/models/assignment.dart';
import 'package:edubox_tryout/models/assignment_result.dart';
import 'package:edubox_tryout/models/lesson.dart';
import 'package:equatable/equatable.dart';

abstract class AssignmentState extends Equatable {
  const AssignmentState();
}

class InitialTaskState extends AssignmentState {
  @override
  List<Object> get props => [];
}

class UserNotFound extends AssignmentState {
  @override
  List<Object> get props => [];
}

class LoadingAssignment extends AssignmentState {
  @override
  List<Object> get props => [];
}

class AssignmentLoaded extends AssignmentState {
  final Lesson lesson;
  final Assignment assignment;

  AssignmentLoaded({this.lesson, this.assignment});

  @override
  List<Object> get props => [lesson, assignment];
}

class LoadAssignmentFailed extends AssignmentState {
  @override
  List<Object> get props => [];
}

class Submitting extends AssignmentState {
  @override
  List<Object> get props => [];
}

class AssignmentSubmitted extends AssignmentState {
  @override
  List<Object> get props => [];
}

class SubmitAssignmentFailed extends AssignmentState {
  final bool hasUploaded;

  SubmitAssignmentFailed([this.hasUploaded = false]);
  @override
  List<Object> get props => [];
}

class Loading extends AssignmentState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class AssignmentResultLoaded extends AssignmentState {
  final bool hasResult;
  final AssignmentResult result;

  AssignmentResultLoaded(this.hasResult, [this.result]);
  @override
  // TODO: implement props
  List<Object> get props => [hasResult];
}

class LoadAssignmentResultFailed extends AssignmentState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class FileOverSized extends AssignmentState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}