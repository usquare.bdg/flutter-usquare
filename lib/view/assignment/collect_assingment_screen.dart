import 'dart:io';

import 'package:edubox_tryout/models/assignment.dart';
import 'package:edubox_tryout/models/assignment_result.dart' as result;
import 'package:edubox_tryout/themes/colors.dart';
import 'package:edubox_tryout/themes/textstyles.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/assignment/assignment_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/button.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class CollectTaskScreen extends StatefulWidget {
  final Items assignment;

  const CollectTaskScreen({Key key, this.assignment}) : super(key: key);

  @override
  _CollectTaskScreenState createState() => _CollectTaskScreenState();
}

class _CollectTaskScreenState extends State<CollectTaskScreen> {
  TextEditingController _contentController = new TextEditingController();
  YoutubePlayerController _controller;
  FocusNode _contentFocus = new FocusNode();
  AssignmentBloc _assignmentBloc;
  result.AssignmentResult _assignmentResult;
  File _file;
  bool _isLoading = false;
  bool _isSubmitting = false;
  bool _showLoading = false;
  bool _showSuccessIndicator = false;
  bool _showFailedIndicator = false;
  bool _hasResult = false;
  String _fileName = 'Tidak ada file';
  String _fileSize = '';
  String _btnText = 'Kumpulkan';
  String _formattedDate;
  Icon _btnIcon = Icon(Icons.send, color: Colors.white, size: 20.0,);

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Collect Assignment Screen');
    MixpanelUtils().initPlatformState('Collect Assignment Screen');

    _assignmentBloc = new AssignmentBloc();
    _assignmentResult = new result.AssignmentResult();
    _assignmentBloc.add(LoadAssignmentResult(widget.assignment.sId));
    _formattedDate = DateFormat('dd MMMM yyyy HH:mm').format(widget.assignment.dueDate);
    _initYoutubePlayer();
    print(widget.assignment.sId);
    super.initState();
  }

  String _getVideoId() {
    int startIndex = widget.assignment.content.indexOf('https://www.youtube.com/');
    int lastIndex = widget.assignment.content.lastIndexOf('"></iframe>');
    var url = widget.assignment.content.substring(startIndex, lastIndex);
    var videoId = YoutubePlayer.convertUrlToId(url);

    return videoId;
  }

  void _alertDialog(String text) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Text(text, textAlign: TextAlign.center,),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: ()=> Navigator.of(dialogContext).pop(),
              color: EduColors.primary,
              child: Text('Oke', style: EduText().smallSemiBold.copyWith(
                color: Colors.white
              ),),
            )
          ],
        );
      }
    );
  }

  void _initYoutubePlayer() {
    if (widget.assignment.content.contains('https://www.youtube.com')) {
      _controller = YoutubePlayerController(
        initialVideoId: _getVideoId(),
        flags: YoutubePlayerFlags(
          autoPlay: false,
          mute: false,
        ),
      );
    }
  }

  void _openFileExplorer() async {
    _file = await FilePicker.getFile();

    print(_file.lengthSync());

    setState(() {
      _fileName = _file != null ? _file.path.split('/').last : 'Tidak ada file';
      if (_file.lengthSync() >= 1000000) _fileSize = '(${(_file.lengthSync() / 1000000).toStringAsFixed(1)} MB)';
      else if (_file.lengthSync() >= 1000000000) _fileSize = '(${(_file.lengthSync() / 1000000).toStringAsFixed(1)} GB)';
      else if (_file.lengthSync() <= 1000000) _fileSize = '(${(_file.lengthSync() / 1000).toStringAsFixed(1)} KB)';
    });
  }

  Future _launchURL(url) async {
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _submitAssignment() {
    FbAnalytics().sendAnalytics('submit_assignment');
    MixpanelUtils().initPlatformState('Submit Assignment');

    var now = DateTime.now();
    var due = widget.assignment.dueDate;

    String formattedNow = DateFormat('dd MM yyyy').format(now);
    String formattedDue = DateFormat('dd MM yyyy').format(due);

    int currentHour = int.parse(DateFormat('HH').format(now));
    int currentMinute = int.parse(DateFormat('mm').format(now));
    int dueHour = int.parse(DateFormat('HH').format(due));
    int dueMinute = int.parse(DateFormat('mm').format(due));

    print('Current = $currentHour:$currentMinute <==> Due = $dueHour:$dueMinute');


    if (now.isAfter(due)) {
      _alertDialog('Batas waktu pengumpulan sudah berakhir!');
    } else if (formattedNow == formattedDue) {
       if (currentHour > dueHour) {
         _alertDialog('Batas waktu pengumpulan sudah berakhir!');
       } else if (currentHour == dueHour) {
         if (currentMinute > dueMinute) {
           _alertDialog('Batas waktu pengumpulan sudah berakhir!');
         } else {
           _onSubmit();
         }
       } else {
         _onSubmit();
       }
    } else {
      _onSubmit();
    }
  }

  void _onSubmit() {
    AssignmentBody _body = new AssignmentBody();

    _body.institutionId = widget.assignment.institutionId;
    _body.assignmentId = widget.assignment.sId;
    _body.rombelId = '';
    _body.studentId = '';
    _body.title = widget.assignment.title;
    _body.content = _contentController.text;
    _body.fileIds = '';
    if (_hasResult) _body.fileIds = _assignmentResult.data.items[0].fileIds[0];
    _body.period = widget.assignment.period;
    _body.status = 'finished';
    _body.score = 0;
    if (_hasResult) _body.score = _assignmentResult.data.items[0].score;

    _assignmentBloc.add(SubmitAssignment(_body, _file, widget.assignment.sId));
  }
  
  void _confirmationDialog() {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.only(
                bottom: 16.0),
            child: Text('Apakah kamu yakin ?', style: EduText().mediumRegular),
          ),
          actionAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(dialogContext);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        width: 1.5,
                        color: EduColors.primary
                    )
                ),
                child: Center(
                    child: Text('Batal', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                    ),)
                ),
              ),
            ),
            SizedBox(width: 10.0,),
            InkWell(
              onTap: () {
                Navigator.pop(dialogContext);
                _submitAssignment();
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    color: EduColors.primary,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                child: Center(
                    child: Text('Yakin', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),)
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  void _dialogImageZoomed(bytes){
    showDialog(
      context: context,
      builder: (ctx){
        return PhotoView(
          imageProvider: MemoryImage(bytes),
          heroAttributes: const PhotoViewHeroAttributes(
              tag: "image zoom",
              transitionOnUserGestures: true
          ),
          onTapUp: (ctx, img, controller){
            Navigator.of(ctx).pop();
          },
          backgroundDecoration: BoxDecoration(
              color: Colors.transparent
          ),
          minScale: PhotoViewComputedScale.contained * 1.0,
        );
      }
    );
  }

  void _scoreDialog() {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text('Nilai ${widget.assignment.title} kamu adalah', textAlign: TextAlign.center,),
                SizedBox(height: 10.0,),
                Text(_assignmentResult.data.items[0].score.toString(), style: EduText().display1,),
                SizedBox(height: 10.0,),
                Text('Terima kasih sudah mengumpulkan tugasnya', textAlign: TextAlign.center,),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(dialogContext).pop(),
              color: EduColors.primary,
              child: Text('Oke', style: EduText().smallSemiBold.copyWith(color: Colors.white),),
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.assignment.title, style: EduText().mediumBold,),
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData().copyWith(
          color: EduColors.black
        ),
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: <Widget>[
          BlocListener(
            bloc: _assignmentBloc,
            listener: (context, state) {
              if (state is Loading) {
                setState(()=> _isLoading = true);
              }
              if (state is AssignmentResultLoaded) {
                setState(() {
                  _hasResult = state.hasResult;
                  _assignmentResult = state.result;

                  if (_hasResult) {
                    _btnText = 'Edit';
                    _btnIcon = Icon(Icons.edit, color: Colors.white, size: 20.0,);

                    if (state.result.data.items[0].score != null && state.result.data.items[0].score != 0) {
                      _btnText = 'Lihat Nilai';
                      _btnIcon = Icon(Icons.score, color: Colors.white, size: 20.0,);
                    }

                    _contentController = TextEditingController(text: state.result.data.items[0].content);
                    if (state.result.data.items[0].fileIds[0] != null && state.result.data.items[0].fileIds[0] != "")
                      _fileName = state.result.data.items[0].fileIds[0];
                  }

                  _isLoading = false;
                });
              }
              if (state is LoadAssignmentResultFailed) {
                setState(() {
                  _assignmentResult = null;
                  _isLoading = false;

                  MyToast().toast('Terjadi kesalahan');
                  Navigator.pop(context);
                });
              }
              if (state is Submitting) {
                setState(() {
                  _isSubmitting = true;
                  _showLoading = true;
                  _showSuccessIndicator = false;
                  _hasResult = false;
                  _showFailedIndicator = false;
                });
              }
              if (state is SubmitAssignmentFailed) {
                setState(() {
                  _showLoading = false;
                  _showFailedIndicator = true;
                  _hasResult = false;
                  _showSuccessIndicator = false;
                });

                MyToast().toast('Terjadi kesalahan');

                Future.delayed(const Duration(seconds: 1), () {
                  setState(() {
                    _isSubmitting = false;
                  });
                });
              }
              if (state is AssignmentSubmitted) {
                setState(() {
                  _showLoading = false;
                  _showFailedIndicator = false;
                  _hasResult = false;
                  _showSuccessIndicator = true;
                });

                Future.delayed(const Duration(seconds: 1), () {
                  setState(() {
                    _isSubmitting = false;
                  });

                  Navigator.pop(context);
                });
              }
              if (state is FileOverSized) {
                setState(() {
                  _showLoading = false;
                  _showFailedIndicator = false;
                  _hasResult = false;
                  _showSuccessIndicator = false;
                  _isSubmitting = false;
                });
                _alertDialog('File yang dilampirkan tidak boleh lebih dari 10 MB');
              }
            },
            child: Builder(
              builder: (context) {
                if (_isLoading) return _loadingView();
                else return _contentView();
              },
            )
          ),
          Visibility(
            visible: _isSubmitting,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.transparent
            ),
          ),
          Visibility(
            visible: _isSubmitting,
            child: Align(
              alignment: Alignment.center,
              child: Container(
                width: 160.0,
                height: 120.0,
                decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _loading(),
                    _successIndicator(),
                    _failedIndicator()
                  ],
                )
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: !_isLoading ? Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: MyButton(
          onTap: () {
            _contentFocus.unfocus();

            if (_hasResult && _assignmentResult.data.items[0].score != null && _assignmentResult.data.items[0].score != 0) {
              FbAnalytics().sendAnalytics('see_assignment_score');
              MixpanelUtils().initPlatformState('See Assignment Score');
              _scoreDialog();
            } else {
              _confirmationDialog();
            }
          },
          color: EduColors.green,
          isDisabled: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(_btnText , style: EduText().mediumSemiBold.copyWith(
                color: Colors.white
              ),),
              SizedBox(width: 5.0,),
              _btnIcon
            ],
          ),
        ),
      ): Container(
        height: 50.0,
      )
    );
  }

  Widget _contentView() {
    return ListView(
      padding: EdgeInsets.only(
        right: 16.0, left: 16.0, top: 16.0),
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(4.0),
              border: Border.all(width: 1.0, color: Colors.grey[400])
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.access_time, size: 20.0,),
                    SizedBox(width: 5.0,),
                    Text('Batas waktu', style: EduText().mediumSemiBold,)
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(_formattedDate),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.assignment, size: 20.0,),
                    SizedBox(width: 5.0,),
                    Text('Tugas', style: EduText().mediumSemiBold,)
                  ],
                ),
              ),
              Html(
                data: """${widget.assignment.content}""",
                onImageTap: (img){
                  UriData data = Uri.parse(img).data;
                  var bytes = data.contentAsBytes();
                  _dialogImageZoomed(bytes);
                },
                onLinkTap: (url) {
                  _launchURL(url);
                },
              ),
              Builder(
                builder: (context) {
                  if (widget.assignment.content.contains('https://www.youtube.com/')) {
                    return YoutubePlayer(
                      controller: _controller,
                      showVideoProgressIndicator: true,
                      liveUIColor: EduColors.primary,
                      progressColors: ProgressBarColors(
                        playedColor: EduColors.primary,
                        handleColor: EduColors.primaryDark
                      ),
                    );
                  } else return Container();
                },
              )
            ],
          ),
        ),
        SizedBox(height: 30.0,),
        Text('Silakan isi form dibawah!', style: EduText().mediumSemiBold,),
        TextFormField(
          controller: _contentController,
          focusNode: _contentFocus,
          decoration: InputDecoration(
              hintText: 'Ketik disini',
              hintStyle: TextStyle(
                  fontSize: 14.0
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 20.0)
          ),
          maxLines: null,
          keyboardType: TextInputType.multiline,
        ),
        SizedBox(height: 10.0,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                child: Text(_fileName, style: EduText().smallRegular, overflow: TextOverflow.ellipsis,)
            ),
            Text(_fileSize, style: EduText().smallRegular),
            SizedBox(width: 10.0,),
            FlatButton(
              onPressed: () {
                _openFileExplorer();
              },
              color: EduColors.primary,
              child: Row(
                children: <Widget>[
                  Icon(Icons.attach_file, size: 16, color: Colors.white,),
                  SizedBox(width: 5.0,),
                  Text('Lampirkan file', style: EduText().smallSemiBold.copyWith(
                      color: Colors.white
                  ),)
                ],
              ),
            ),
          ],
        ),
        SizedBox(height: 30.0,),
      ],
    );
  }

  Widget _loading() {
    return Visibility(
      visible: _showLoading,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitDualRing(
            color: Colors.white,
            size: 30.0,
            lineWidth: 3.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text('Harap tunggu..', style: EduText().smallRegular.copyWith(
                color: Colors.white
            )),
          )
        ],
      ),
    );
  }

  Widget _successIndicator() {
    return Visibility(
      visible: _showSuccessIndicator,
      child: Column(
        children: <Widget>[
          Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
                border: Border.all(
                    width: 1.5, color: Colors.white
                ),
                shape: BoxShape.circle
            ),
            child: Icon(Icons.check, color: Colors.white,)
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(_hasResult ? 'Berhasil mengedit' : 'Berhasil mengunggah', style: EduText().smallRegular.copyWith(
                color: Colors.white
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _failedIndicator() {
    return Visibility(
      visible: _showFailedIndicator,
      child: Column(
        children: <Widget>[
          Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
                border: Border.all(
                    width: 1.5, color: Colors.white
                ),
                shape: BoxShape.circle
            ),
            child: Icon(Icons.close, color: Colors.white,)
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(_hasResult ? 'Gagal mengedit tugas' : 'Gagal mengumpulkan', style: EduText().smallRegular.copyWith(
              color: Colors.white
            ),
            textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _loadingView() {
    return Center(
      child: SpinKitRing(
        color: EduColors.primary,
        size: 50.0,
        lineWidth: 3.0,
      )
    );
  }
}
