import 'package:edubox_tryout/models/assignment.dart';
import 'package:edubox_tryout/models/lesson.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/assignment/assignment_bloc/bloc.dart';
import 'package:edubox_tryout/view/assignment/collect_assingment_screen.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ListTaskScreen extends StatefulWidget {
  final String sId;

  const ListTaskScreen({Key key, this.sId}) : super(key: key);

  @override
  _ListTaskScreenState createState() => _ListTaskScreenState();
}

class _ListTaskScreenState extends State<ListTaskScreen> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  AssignmentBloc _assignmentBloc;
  Lesson _lesson;
  LessonItems _lessonItems;
  Assignment _assignment;
  bool _isError = false;
  bool _isLoading = false;
  bool _initializing = true;
  bool _userNotFound = false;
  String sId;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Assignment List Screen');
    MixpanelUtils().initPlatformState('Assignment List Screen');

    _lesson = new Lesson();
    _assignment = new Assignment();
    _assignmentBloc = new AssignmentBloc();
    sId = widget.sId;
    if (sId != null && sId != "") {
      _assignmentBloc.add(LoadAssignment(
          lessonId: sId));
    } else {
      _assignmentBloc.add(LoadAssignment());
    }

    super.initState();
  }

  Future<void> _onRefresh() async {
    if (_isError || _userNotFound) {
      setState(() {
        _initializing = true;
      });
    }

    if (_lessonItems != null) {
      return _assignmentBloc.add(LoadAssignment(lessonId: _lessonItems.sId));
    } else {
      return _assignmentBloc.add(LoadAssignment());
    }
  }

  Future _moveToLoginScreen() async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));

    if (result) {
      await Future.delayed(Duration(milliseconds: 500));
      _onRefresh();
    }
  }

  void _selectLessonDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: 'Pilih pelajaran',
            content: Container(
              padding: EdgeInsets.only(top: 10.0),
              height: MediaQuery.of(context).size.height / 2,
              child: ListView.builder(
                itemCount: _lesson.data.items.length,
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {
//                    print("sId ${_lesson.data.items[index].sId}");
                        _assignmentBloc.add(LoadAssignment(
                            lessonId: _lesson.data.items[index].sId));
                        setState(() {
                          _lessonItems = _lesson.data.items[index];
                        });
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 50.0,
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                _lesson.data.items[index].name,
                                style: EduText().mediumRegular.copyWith(
                                    fontWeight: _lessonItems.sId ==
                                            _lesson.data.items[index].sId
                                        ? FontWeight.w600
                                        : FontWeight.normal),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            _lessonItems.sId == _lesson.data.items[index].sId
                                ? Container(
                                    width: 25.0,
                                    height: 25.0,
                                    decoration: BoxDecoration(
                                        color: EduColors.primaryDark,
                                        shape: BoxShape.circle),
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 16.0,
                                    ),
                                  )
                                : Container()
                          ],
                        ),
                      ));
                },
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: widget.sId != null
            ? PreferredSize(
                child: Container(),
                preferredSize: Size(0.0, 16.0),
              )
            : AppBar(
                backgroundColor: Colors.white,
                title: Row(
                  children: <Widget>[
                    Text(
                      'Daftar Tugas',
                      style: EduText().largeBold,
                    ),
                  ],
                ),
                elevation: 0.0,
              ),
        body: BlocListener(
          bloc: _assignmentBloc,
          listener: (context, state) {
            if (state is LoadingAssignment) {
              setState(() {
                _isLoading = true;
              });
            }
            if (state is UserNotFound) {
              setState(() {
                _isError = false;
                _initializing = false;
                _userNotFound = true;
              });
            }
            if (state is LoadAssignmentFailed) {
              setState(() {
                _isError = true;
                _initializing = false;
                _userNotFound = false;
                _isLoading = false;
              });
            }
            if (state is AssignmentLoaded) {
              setState(() {
                _lesson = state.lesson;
                if (_lessonItems == null)
                  _lessonItems = state.lesson.data.items[0];
                _assignment = state.assignment;

                _userNotFound = false;
                _isError = false;
                _initializing = false;
                _isLoading = false;
              });
            }
          },
          child: _initializing
              ? _loadingView()
              : _isError
                  ? _errorView()
                  : _userNotFound
                      ? _userNotFoundView()
                      : RefreshIndicator(
                          key: _refreshIndicatorKey,
                          onRefresh: _onRefresh,
                          child: ScrollConfiguration(
                            behavior: MyBehavior(),
                            child: ListView(
                              physics: AlwaysScrollableScrollPhysics(),
                              children: <Widget>[
                                _header(),
                                _isLoading ? _loadingView2() : _content()
                              ],
                            ),
                          ),
                        ),
        ));
  }

  Widget _header() {
    return Visibility(
      visible: sId != null && sId != "" ? false : true,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
        child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              height: 100.0,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [EduColors.primary, EduColors.primaryDark],
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: [0.2, 1.0],
                      tileMode: TileMode.clamp),
                  borderRadius: BorderRadius.circular(10.0)),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Center(
                  child: Text(
                    _lessonItems.name,
                    style:
                        EduText().mediumSemiBold.copyWith(color: Colors.white),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Container(
                height: 50.0,
                margin: EdgeInsets.only(top: 75.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(50.0),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 5.0,
                        offset: Offset(0.0, 4.0))
                  ],
                ),
                child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: _isError
                      ? null
                      : () {
                          _selectLessonDialog();
                        },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.stars,
                        color: EduColors.primaryDark,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        'Pilih Pelajaran',
                        style: EduText().mediumSemiBold,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _content() {
    if (_assignment.data.items.length > 0) {
      return _listTaskView();
    } else {
      return _listEmptyView();
    }
  }

  Widget _listTaskView() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: _assignment.data.items.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
          child: InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CollectTaskScreen(
                            assignment: _assignment.data.items[index],
                          )));
            },
            child: Container(
                height: 60.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 5.0,
                        offset: Offset(0.0, 4.0))
                  ],
                ),
                child: ListTile(
                  title: Text(
                    _assignment.data.items[index].title,
                    style: EduText().mediumSemiBold,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: Icon(Icons.arrow_forward_ios),
                )),
          ),
        );
      },
    );
  }

  Widget _listEmptyView() {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height / 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/empty.png', width: 60.0),
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: Text(
              'Tidak ada tugas',
              style: EduText().mediumSemiBold,
            ),
          ),
          widget.sId != null
              ? Container()
              : Padding(
                  padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
                  child: Text(
                    'Cari tugas di pelajaran lain.',
                    style: EduText().smallRegular,
                    textAlign: TextAlign.center,
                  ),
                ),
          widget.sId != null
              ? Container()
              : FlatButton(
                  onPressed: () {
                    _selectLessonDialog();
                  },
                  color: EduColors.primary,
                  child: Text(
                    'Pilih pelajaran lain',
                    style:
                        EduText().smallSemiBold.copyWith(color: Colors.white),
                  ),
                )
        ],
      ),
    );
  }

  Widget _loadingView() {
    return Center(
        child: SpinKitRing(
      color: EduColors.primary,
      size: 50.0,
      lineWidth: 3.0,
    ));
  }

  Widget _loadingView2() {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height / 2,
      child: Center(
          child: SpinKitRing(
        color: EduColors.primary,
        size: 50.0,
        lineWidth: 3.0,
      )),
    );
  }

  Widget _userNotFoundView() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Kamu belum masuk ke ${Strings.appName}',
            style: EduText().mediumSemiBold,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
            child: Text(
              'Ayo masuk untuk melihat tugas-tugas dan mengumpulkannya.',
              style: EduText().smallRegular,
              textAlign: TextAlign.center,
            ),
          ),
          FlatButton(
            onPressed: () {
              _moveToLoginScreen();
            },
            color: EduColors.primary,
            child: Text(
              'Masuk',
              style: EduText().smallSemiBold.copyWith(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  Widget _errorView() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/no-wifi.png', width: 80.0),
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: Text(
              'Kamu sedang offline',
              style: EduText().mediumSemiBold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
            child: Text(
              'Silakan periksa kembali internetmu.',
              style: EduText().smallRegular,
              textAlign: TextAlign.center,
            ),
          ),
          FlatButton(
            onPressed: () {
              _onRefresh();
            },
            color: EduColors.primary,
            child: Text(
              'Muat ulang',
              style: EduText().smallSemiBold.copyWith(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
