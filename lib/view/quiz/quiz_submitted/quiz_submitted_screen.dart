import 'package:edubox_tryout/main.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/material.dart';

class QuizSubmittedScreen extends StatefulWidget {
  final int unanswered, incorrect, correct;
  final double score, essayScore;
  final String quizType;

  const QuizSubmittedScreen({Key key, this.unanswered, this.incorrect, this.correct, this.score, this.quizType, this.essayScore}) : super(key: key);
  @override
  _QuizSubmittedScreenState createState() => _QuizSubmittedScreenState();
}

class _QuizSubmittedScreenState extends State<QuizSubmittedScreen> {
  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Quiz Score Screen');
    MixpanelUtils().initPlatformState('Quiz Score Screen');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
          builder: (context) => MyApp(currentPage: 1,)
        ), (Route<dynamic> route) => false);
        return null;
      },
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22.0),
          child: Align(
            alignment: Alignment.center,
            child: Card(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 22.0),
                    width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        topRight: Radius.circular(5.0)
                      )
                    ),
                    child: Column(
                      children: [
                        Builder(
                          builder: (ctx) {
                            if (widget.quizType == "7") {
                              return Container(
                                padding: EdgeInsets.symmetric(vertical: 30.0),
                                child: Text('Hasil Ujian', style: EduText().largeBold),
                              );
                            } else {
                              return Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.symmetric(vertical: 30.0),
                                    child: Text('Hasil Ujian', style: EduText().largeBold),
                                  ),
                                  Container(
                                    width: 120.0,
                                    height: 120.0,
                                    padding: EdgeInsets.symmetric(vertical: 8.0),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: EduColors.black,
                                          width: 2.0
                                        ),
                                        shape: BoxShape.circle
                                    ),
                                    child: Center(
                                      child: Text('${widget.score.toStringAsFixed(1)}', style: EduText().display1.copyWith(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 42.0,
                                        color: EduColors.black
                                      )),
                                    ),
                                  ),
                                ],
                              );
                            }
                          },
                        ),
                        widget.quizType != '7' ? Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Nilai PG', style: EduText().mediumRegular,),
                              Text('${widget.score.toStringAsFixed(1)}', style: EduText().mediumSemiBold,),
                            ],
                          ),
                        ) : Container(),
                        widget.quizType == '7' ? Container() : Divider(),
                        widget.quizType != '7' ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Nilai Essay', style: EduText().mediumRegular,),
                            Text('${widget.essayScore.toStringAsFixed(1)}', style: EduText().mediumSemiBold,),
                          ],
                        ) : Container(),
                        widget.quizType == '7' ? Container() : Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Benar (PG)', style: EduText().mediumRegular),
                            Text('${widget.correct}', style: EduText().mediumSemiBold),
                          ],
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Salah (PG)', style: EduText().mediumRegular),
                            Text('${widget.incorrect}', style: EduText().mediumSemiBold),
                          ],
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Tidak dijawab', style: EduText().mediumRegular,),
                            Text('${widget.unanswered}', style: EduText().mediumSemiBold,),
                          ],
                        ),
                        SizedBox(height: 20.0)
                      ]
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                          builder: (context) => MyApp(currentPage: 1,)
                      ), (Route<dynamic> route) => false);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50.0,
                      decoration: BoxDecoration(
                        color: EduColors.primary,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(5.0),
                          bottomRight: Radius.circular(5.0)
                        )
                      ),
                      child: Center(
                        child: Text('Oke', style: EduText().mediumSemiBold.copyWith(                      
                          color: Colors.white
                        ),),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class QuizFinishedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 180.0,
              height: 180.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: EduColors.green,
                  width: 4.0
                )
              ),
              child: Center(
                child: Icon(Icons.check, color: EduColors.green, size: 100.0,)
              ),
            ),
            SizedBox(height: 40.0),
            Text('Ujian Selesai !', style: EduText().largeBold),
            SizedBox(height: 20.0),
            Container(              
              child: Text('Ujian telah selesai dikerjakan, tekan tombol dibawah untuk menuju ke halaman Daftar Ujian.',
                style: EduText().mediumRegular,
                textAlign: TextAlign.center,),
            )
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: MyButton(
          color: EduColors.primary,
          onTap: () {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
              builder: (context) => MyApp(currentPage: 1,)
            ), (Route<dynamic> route) => false);
          },
          isDisabled: false,
          child: Text('Oke', style: EduText().mediumSemiBold.copyWith(color: Colors.white)),
        ),
      )
    );
  }
}