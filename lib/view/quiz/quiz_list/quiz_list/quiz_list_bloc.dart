import 'dart:async';
import 'dart:io' show Platform;

import 'package:bloc/bloc.dart';
import 'package:device_apps/device_apps.dart';
import 'package:edubox_tryout/models/quiz.dart';
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/list_packages.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import './bloc.dart';

class QuizListBloc extends Bloc<QuizListEvent, QuizListState> {
  @override
  QuizListState get initialState => InitialQuizListState();

  @override
  Stream<QuizListState> mapEventToState(
    QuizListEvent event,
  ) async* {
    if (event is LoadQuizList) {
      yield* _loadQuizListToState();
    } else if (event is StartQuiz) {
      yield* _startQuizToState(event);
    } else if (event is LoadResult) {
      yield* _loadResultToState(event);
    } else if (event is UploadResult) {
      yield* _uploadResultToState(event);
    } else if (event is DeleteQuiz) {
      yield* _deleteQuizToState(event);
    } else if (event is UpdateQuizPass) {
      yield* _updateQuizPassToState(event);
    }
  }

  Stream<QuizListState> _updateQuizPassToState(event) async* {
    yield FooState();

    try {
      String quizPassId = '';
      final quizPass = await ApiClient().getQuizPass(event.quizId);
      final userId = await ApiClient().getUserId();

      if (quizPass != null) {
        bool isCorrect = false;
        bool hasRedeemed = false;

        for (var item in quizPass.data.items) {
          if (event.passcode == item.passcode) {
            isCorrect = true;
            quizPassId = item.sId;

            if (item.userId != null) {
              if (userId != item.userId.sId) {
                print('$userId <===> ${item.userId.sId}');
                hasRedeemed = true;
              }
            }
          }
        }

        if (isCorrect) {
          if (hasRedeemed) {
            yield QuizPassHasRedeemed();
          } else {
            final result = await ApiClient().updateQuizPass(quizPassId);

            if (result) {
              yield QuizPassUpdated();
            } else {
              yield QuizPassError();
            }
          }
        } else {
          yield PasscodeIncorrect();
        }
      } else {
        yield QuizPassError();
      }
    } catch (e) {
      print('error : $e');
      yield QuizPassError();
    }
  }

  Stream<QuizListState> _deleteQuizToState(event) async* {
    try {
      await DbDao().deleteQuiz(event.quizData.sId);
      await DbDao().deleteQuestions(event.quizData.sId);
      await DbDao().deleteResultBody(event.quizData.sId);

      yield QuizDeleted(event.quizData.title);
    } catch (e) {
      print('delete error : $e');
    }
  }

  Stream<QuizListState> _uploadResultToState(event) async* {
    yield Uploading();
    try {
      FbAnalytics().sendAnalytics('upload_result');
      MixpanelUtils().initPlatformState('Upload Result');
      
      final resultDb = await DbDao().getResultBody(event.quizData.sId);
      final quiz = await ApiClient().getQuizById(event.quizData.sId);
      final hasRated = await ApiClient().hasRated();
      bool showDialog = false;

      if (!hasRated && resultDb.score > 60.0) {
        showDialog = true;
      }

      if (quiz != null) {
        if (quiz.data.status != null) {
          if (quiz.data.status == 'published') {
            final statusCreate = await ApiClient().createResult(resultDb);

            if (statusCreate == 201) {
              Items quiz = event.quizData;
              quiz.isUploaded = true;
              await DbDao().updateQuiz(quiz, quiz.sId);

              resultDb.isUploaded = true;
              await DbDao().updateResultBody(resultDb, resultDb.quizId);

              await Future.delayed(Duration(seconds: 1));
              yield ResultUploaded(showDialog);
            } else if (statusCreate == 500) {
              yield AlreadyUploaded();
            } else {
              yield UploadFailed();
            }
          } else {
            yield QuizClosed();
          }
        } else {
          yield QuizClosed();
        }
      } else {
        yield UploadFailed();
      }
    } catch (e) {
      print('upload failed $e');
      yield UploadFailed();
    }
  }

  Stream<QuizListState> _loadResultToState(event) async* {
    yield StartingQuiz();

    try {
      final result = await ApiClient().getQuizResult(event.id);

      if (result != null) {
        final listEssay = result.data.items[0].studentAnswers.where((i) => i.type != null && i.type == 'essay').toList();

        int totalScore = 0;
        double finalEssayScore = 0.0;

        if (listEssay.length > 0) {
          for (var essay in listEssay) {
            if (essay.state != null && essay.state != 'belum' && essay.state != 'dijawab' && essay.state != '') {
              totalScore = totalScore + int.parse(essay.state);
            }
          }

          finalEssayScore = totalScore / listEssay.length;
        }

        final wrong = result.data.items[0].wrong - listEssay.length;
        final count = wrong + result.data.items[0].correct;
        final pgScore = (100 / count) * result.data.items[0].correct;

        yield ResultLoaded(
          correct: result.data.items[0].correct,
          incorrect: wrong,
          unanswered: result.data.items[0].unanswers,
          score: pgScore,
          essayScore: finalEssayScore,
          type: event.type
        );
      } else {
        yield LoadResultFailed();
      }
    } catch (e) {
      print(e);
      yield LoadResultFailed();
    }
  }

  Stream<QuizListState> _startQuizToState(event) async* {
    yield StartingQuiz();
    try {
      List<StudentAnswersBody> listAnswers = new List<StudentAnswersBody>();
      final resultBody = await DbDao().getResultBody(event.quiz.sId);  
      final user = await DbDao().getUser();
      final List<QuestionIds> listQuestions = await DbDao().getAllQuestions(event.quiz.sId, user.data.sId);

      bool isInstalled = false;
      if (Platform.isAndroid) {
        for (var packageName in ListPackages().packages) {
          final status = await DeviceApps.isAppInstalled(packageName);
          if (status) isInstalled = true;
        }
      }
      
      if (isInstalled) {
        yield FloatingAppsDetected();
      } else {
        Items quiz = event.quiz;

        if (quiz.randomSeq) {
          listQuestions.shuffle();
        }

        quiz.status = 'ongoing';
        quiz.questionsIds = listQuestions;
        quiz.isOpened = true;

        if (resultBody != null) {
          print('asda : ${resultBody.quizId}');
          if (quiz.randomSeq) {
            for (var question in listQuestions) {
              for (var answer in resultBody.studentAnswersBody) {
                if (answer.id == question.sId) {
                  listAnswers.add(answer);
                }
              }
            }
          } else {
            for (var answer in resultBody.studentAnswersBody) {
              listAnswers.add(answer);
            }
          }

          quiz.isBlocked = true;
          await DbDao().updateQuiz(quiz, quiz.sId);

          yield QuizStarted(
            list: listAnswers,
            quizData: quiz,
            resultBody: resultBody
          );
        } else {
          for (var question in listQuestions) {
            StudentAnswersBody body = new StudentAnswersBody();
            body.id = question.sId;
            body.answer = null;
            body.state = 'belum';
            body.isCorrect = false;
            body.type = question.type;

            listAnswers.add(body);
          }

          PeriodBody periodBody = new PeriodBody();
          periodBody.year = quiz.period.year;
          KdsBody kd = new KdsBody();
          kd.hello = 'world';
          ResultBody resultBody = new ResultBody();
          resultBody.institutionId = user.data.studentAtInstitutions[0].sId;
          resultBody.quizId = quiz.sId;
          resultBody.rombelId = user.data.rombelIds.last.sId;
          resultBody.studentId = user.data.sId;
          resultBody.studentAnswersBody = listAnswers;
          resultBody.periodBody = periodBody;
          resultBody.status = 'draft';
          resultBody.kds = [kd];
          resultBody.correct = null;
          resultBody.wrong = null;
          resultBody.unanswers = null;
          resultBody.score = null;
          resultBody.remaining = null;
          resultBody.blockedStatus = 'free';
          resultBody.isUploaded = false;
          resultBody.userId = user.data.sId;

          if (quiz.questionsIds.length > 0) {
            await DbDao().insertResultBody(resultBody);
            await DbDao().updateQuiz(quiz, quiz.sId);

            yield QuizStarted(
              list: listAnswers,
              quizData: quiz,
              resultBody: resultBody
            );
          } else {
            yield StartQuizFailed();
          }
        }
      }
    } catch (e) {
      print('error starting quiz : $e');
      yield StartQuizFailed();
    }
  }

  Stream<QuizListState> _loadQuizListToState() async* {
    yield LoadingListQuiz();
    final user = await DbDao().getUser();
    try {
      final listQuiz = await DbDao().getAllQuiz();
      if (listQuiz != null) {
        yield QuizListLoaded(listQuiz, user);
      } else {
        yield QuizListEmpty(user);
      }
    } catch (e) {
      print('catch: $e');      
      yield QuizListEmpty(user);
    }
  }
}
