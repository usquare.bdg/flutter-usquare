import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class QuizListState extends Equatable {
  const QuizListState();
}

class InitialQuizListState extends QuizListState {
  @override
  List<Object> get props => [];
}

class QuizListLoaded extends QuizListState {  
  final List<Items> quizList;
  final User user;

  QuizListLoaded(this.quizList, this.user);

  @override
  List<Object> get props => [quizList];
}

class LoadFailed extends QuizListState {
  @override  
  List<Object> get props => null;
}

class QuizListEmpty extends QuizListState {
  final User user;

  QuizListEmpty(this.user);

  @override  
  List<Object> get props => null;
}

class LoadingListQuiz extends QuizListState {
  @override  
  List<Object> get props => null;
}

class QuizStarted extends QuizListState {
  final Items quizData;  
  final ResultBody resultBody;
  final List<StudentAnswersBody> list;

  QuizStarted({this.quizData, this.resultBody, this.list});
  
  @override  
  List<Object> get props => [];
}

class FloatingAppsDetected extends QuizListState {
  @override
  List<Object> get props => [];
}

class StartQuizFailed extends QuizListState {
  @override
  List<Object> get props => [];
}

class StartingQuiz extends QuizListState {
  @override  
  List<Object> get props => null;
}

class ResultLoaded extends QuizListState {
  final int correct, incorrect, unanswered;
  final double score, essayScore;
  final String type;

  ResultLoaded({this.correct, this.incorrect, this.unanswered, this.score, this.type, this.essayScore});
  @override  
  List<Object> get props => null;
}

class LoadResultFailed extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class Uploading extends QuizListState {
  @override  
  List<Object> get props => null;
}

class ResultUploaded extends QuizListState {
  final bool showDialog;

  ResultUploaded(this.showDialog);
  @override  
  List<Object> get props => null;
}

class UploadFailed extends QuizListState {
  @override  
  List<Object> get props => null;
}

class AlreadyUploaded extends QuizListState {
  @override  
  List<Object> get props => null;
}

class QuizClosed extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class QuizDeleted extends QuizListState {
  final String quizTitle;

  QuizDeleted(this.quizTitle);
  @override
  List<Object> get props => null;
}

class QuizPassUpdated extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class PasscodeIncorrect extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class QuizPassError extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class QuizPassHasRedeemed extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class FooState extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class ProfileIncomplete extends QuizListState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}