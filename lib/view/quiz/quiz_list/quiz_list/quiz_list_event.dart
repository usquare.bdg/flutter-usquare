import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:equatable/equatable.dart';

abstract class QuizListEvent extends Equatable {
  const QuizListEvent();
}

class LoadQuizList extends QuizListEvent {
  @override  
  List<Object> get props => null;
}

class StartQuiz extends QuizListEvent {
  final Items quiz;

  StartQuiz(this.quiz);

  @override  
  List<Object> get props => null;
}

class LoadResult extends QuizListEvent {
  final String id, type;

  LoadResult(this.id, this.type);
  @override  
  List<Object> get props => null;
}

class UploadResult extends QuizListEvent {  
  final Items quizData;

  UploadResult(this.quizData);
  @override  
  List<Object> get props => null;
}

class DeleteQuiz extends QuizListEvent {
  final Items quizData;

  DeleteQuiz(this.quizData);
  @override
  List<Object> get props => [];
}

class UpdateQuizPass extends QuizListEvent {
  final String passcode;
  final String quizId;

  UpdateQuizPass({this.passcode, this.quizId});
  @override
  // TODO: implement props
  List<Object> get props => null;
}