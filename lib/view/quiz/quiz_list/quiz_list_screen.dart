import 'dart:io' show Platform;

import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/main_screen/main_screen_bloc/bloc.dart';
import 'package:edubox_tryout/view/quiz/do_quiz/do_quiz_screen.dart';
import 'package:edubox_tryout/view/quiz/quiz_list/quiz_list/bloc.dart';
import 'package:edubox_tryout/view/quiz/quiz_submitted/quiz_submitted_screen.dart';
import 'package:edubox_tryout/view/sync/sync_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:launch_review/launch_review.dart';
import 'package:rxdart/rxdart.dart' as Rx;

class QuizListScreen extends StatefulWidget {
  @override
  _QuizListScreenState createState() => _QuizListScreenState();
}

class _QuizListScreenState extends State<QuizListScreen> with TickerProviderStateMixin {
  final Rx.BehaviorSubject<int> _expandedIndex = Rx.BehaviorSubject<int>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode _openingCodeFocus = FocusNode();
  AnimationController _controller;
  Animation _fadeAnimation;
  QuizListBloc _quizListBloc;
  List<Items> _quizList;
  User _user;
  bool _isLoading = true;
  bool _isStartingQuiz = false;
  bool _isEmpty = false;
  bool _isUploading = false;
  bool _showLoading = false;
  bool _showSuccessIndicator = false;
  bool _showFailedIndicator = false;
  String _failedStatusText = '';

  Future _moveToSyncScreen(type) async {
    final result = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => SyncScreen(type: type, context: _scaffoldKey.currentContext,)
    ));

    if (result) {
      _quizListBloc.add(LoadQuizList());
    }
  }

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _fadeAnimation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _quizList = new List<Items>();
    _quizListBloc = new QuizListBloc();
    _quizListBloc.add(LoadQuizList());
    _user = new User();

    FbAnalytics().setCurrentScreen('Quiz List Screen');
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> _onRefresh() async {
    return _quizListBloc.add(LoadQuizList());
  }

  Future<void> _onLaunchRating() async {
    await ApiClient().setRated();

    LaunchReview.launch(
      androidAppId: 'pinisi.eduboxgo.io',
      iOSAppId: '1485393841'
    );
  }

  void _warningDialog() {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return CustomDialog(
          title: 'Al-Irsyad',
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Tidak bisa memulai ujian!', textAlign:  TextAlign.center,),
              Text('Device yang digunakan terdeteksi menggunakan Floating App.', textAlign: TextAlign.center,),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(dialogContext).pop(),
              child: Text('Tutup', style: EduText().smallSemiBold.copyWith(color: Colors.white),),
              color: EduColors.primary,
            )
          ],
        );
      }
    );
  }

  void _deleteDialog(item) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          title: 'Al-Irsyad',
          content: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text('Apakah kamu yakin ingin menghapus ${item.title} ?', textAlign: TextAlign.center,),
          ),
          actionAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        width: 1.5,
                        color: EduColors.primary
                    )
                ),
                child: Center(
                    child: Text('Batal', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                    ),)
                ),
              ),
            ),
            SizedBox(width: 10.0,),
            InkWell(
              onTap: () {
                FbAnalytics().sendAnalytics('delete_quiz');
                MixpanelUtils().initPlatformState('Delete Quiz');

                _quizListBloc.add(DeleteQuiz(item));
                Navigator.pop(context);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    color: EduColors.primary,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                child: Center(
                    child: Text('Yakin', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),)
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  void _profileIncompleteDialog() {
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return CustomDialog(
            title: 'Al-Irsyad',
            content: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text('Lengkapi profil untuk memulai ujian!', textAlign: TextAlign.center,),
            ),
            actionAlignment: MainAxisAlignment.center,
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                  BlocProvider.of<MainScreenBloc>(context).add(LoadProfileScreen());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Lengkapi Profil', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),),
                  ],
                ),
                color: EduColors.primary,
              ),
            ],
          );
        }
    );
  }

  void _choiceDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          title: 'Pilih tipe ujian',
          content: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    Navigator.pop(context);
                    _moveToSyncScreen('Ujian Sekolah');
                  },
                  child: Card(
                    child: Container(
                      height: 40.0,
                      child: Center(
                        child: Text('Ujian Sekolah', style: EduText().smallSemiBold,),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5.0,),
                InkWell(
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    Navigator.pop(context);
                    _moveToSyncScreen('Try Out');
                  },
                  child: Card(
                    child: Container(
                      height: 40.0,
                      child: Center(
                        child: Text('Ujian Bersama / Try Out', style: EduText().smallSemiBold,),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }
    );
  }

  void _ratingDialog(platform) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return CustomDialog(
          title: 'Al-Irsyad',
          content: Column(
            children: <Widget>[
//              Image.asset('assets/EduboxGo4.png', width: 60.0,),
              SizedBox(height: 20.0,),
              Text('Terima kasih telah menggunakan aplikasi Al-Irsyad', textAlign: TextAlign.center,),
              SizedBox(height: 5.0,),
              Text('Beri kami ulasan dan bintang di $platform', textAlign: TextAlign.center,),
              SizedBox(height: 20.0,),
              FlatButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                  _onLaunchRating();
                },
                color: EduColors.primary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Beri ulasan', style: EduText().smallSemiBold.copyWith(color: Colors.white),),
                  ],
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                },
                color: Colors.grey[200],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Jangan sekarang', style: EduText().smallSemiBold),
                  ],
                ),
              ),
            ],
          ),
        );
      },
      barrierDismissible: false
    );
  }

  void _openingCodeDialog(item) {
    bool _isIncorrect = false;
    bool _hasRedeemed = false;
    bool _onProcess = false;
    TextEditingController _quizPassController = new TextEditingController();

    print(item.sId);
    showDialog(
      context: _scaffoldKey.currentContext,
      builder: (BuildContext dialogContext) {
        FocusScope.of(dialogContext).requestFocus(_openingCodeFocus);

        return StatefulBuilder(
          builder: (BuildContext context, Function setDialogState) {
            return BlocListener(
              bloc: _quizListBloc,
              listener: (context, state) {
                if (state is PasscodeIncorrect) {
                  setDialogState(() {
                    _isIncorrect = true;
                    _onProcess = false;
                    _hasRedeemed = false;
                  });
                }
                if (state is QuizPassHasRedeemed) {
                  setDialogState(() {
                    _hasRedeemed = true;
                    _isIncorrect = false;
                    _onProcess = false;
                  });
                }
                if (state is QuizPassError) {
                  setDialogState(() {
                    _isIncorrect = false;
                    _onProcess = false;
                    _hasRedeemed = false;
                  });
                  Navigator.pop(dialogContext);
                  MyToast().toast('Terjadi kesalahan');
                }
                if (state is QuizPassUpdated) {
                  setDialogState(() {
                    _isIncorrect = false;
                    _onProcess = false;
                    _hasRedeemed = false;
                  });

                  Navigator.pop(dialogContext);
                  _quizListBloc.add(StartQuiz(item));
                }
              },
              child: CustomDialog(
                title: 'Al-Irsyad',
                content: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Ujian dikunci! Buka kunci untuk memulai ujian.'),
                        SizedBox(height: 5),
                        Form(
                          key: _formKey,
                          child: MyTextField(
                            focusNode: _openingCodeFocus,
                            controller: _quizPassController,
                            obsecureText: false,
                            hintText: 'Masukkan kode pembuka',
                            errorText: _isIncorrect ? 'Kode pembuka salah!' : _hasRedeemed ? 'Kode sudah pernah digunakan!' : null,
                            validator: (val) {
                              if (val != item.openingCode) {
                                return 'Kode pembuka salah!';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    )
                ),
                actionAlignment: MainAxisAlignment.end,
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      if (item.quizType == '7') {
                        setDialogState(() {
                          _onProcess = true;
                          _isIncorrect = false;
                          _hasRedeemed = false;
                        });
                        _quizListBloc.add(UpdateQuizPass(
                          passcode: _quizPassController.text,
                          quizId: item.sId
                        ));
                      } else {
                        if (_formKey.currentState.validate()) {
                          Navigator.pop(context);
                          _quizListBloc.add(StartQuiz(item));
                        }
                      }
                    },
                    color: EduColors.primary,
                    child: !_onProcess ? Row(
                      children: <Widget>[
                        Text('Buka', style: EduText().smallSemiBold.copyWith(
                            color: Colors.white
                        ),),
                        SizedBox(width: 2.0,),
                        Icon(Icons.vpn_key, color: Colors.white, size: 18.0,),
                      ],
                    ) : Container(
                      width: 25.0,
                      height: 25.0,
                      padding: EdgeInsets.all(5.0),
                      child: CircularProgressIndicator(backgroundColor: Colors.white),
                    )
                  )
                ],
              ),
            );
          },
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          children: <Widget>[
            Text('Daftar Ujian', style: EduText().largeBold,),
          ],
        ),
        elevation: 0.0,
      ),
      body: Stack(
        children: <Widget>[
          RefreshIndicator(
            key: _refreshIndicatorKey,        
            onRefresh: _onRefresh,
            child: ScrollConfiguration(
              behavior: MyBehavior(),
              child: ListView(
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  _header(),
                  _content()
                ],
              ),
            ),
          ),
          Visibility(
            visible: _isUploading,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.transparent
            ),
          ),
          Visibility(
            visible: _isUploading,
            child: Align(
              alignment: Alignment.center,
              child: Container(
                width: 160.0,
                height: 120.0,
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(5.0)
                ),
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _loading(),
                    _successIndicator(),
                    _failedIndicator()
                  ],
                )
              ),
            ),
          )
        ],
      ),
    );
  }
  
  Widget _header() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(8.0),
            height: 125.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [EduColors.primary, EduColors.primaryDark],
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  stops: [0.2, 1.0],
                  tileMode: TileMode.clamp
              ),
              borderRadius: BorderRadius.circular(10.0)
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle
                          ),
                          child: Center(
                            child: AnimatedOpacity(
                              opacity: _isLoading ? 0.0 : 1.0,
                              duration: Duration(milliseconds: 500),
                              child: Text('${_quizList.length}', style: EduText().largeBold),
                            )
                          ),
                        ),
                        SizedBox(height: 5.0,),
                        Text('Diunduh', style: EduText().smallSemiBold.copyWith(
                          color: Colors.white,
                          fontSize: 10.0
                        ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle
                          ),
                          child: Center(
                              child: AnimatedOpacity(
                                opacity: _isLoading ? 0.0 : 1.0,
                                duration: Duration(milliseconds: 500),
                                child: Text('${_quizList.where((i) => i.status == 'finished').length}', style: EduText().largeBold),
                              )
                          ),
                        ),
                        SizedBox(height: 5.0,),
                        Text('Dikerjakan', style: EduText().smallSemiBold.copyWith(
                            color: Colors.white,
                            fontSize: 10.0
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle
                          ),
                          child: Center(
                            child: AnimatedOpacity(
                              opacity: _isLoading ? 0.0 : 1.0,
                              duration: Duration(milliseconds: 500),
                              child: Text('${_quizList.where((i) => i.isUploaded).length}', style: EduText().largeBold),
                            )
                          ),
                        ),
                        SizedBox(height: 5.0,),
                        Text('Diunggah', style: EduText().smallSemiBold.copyWith(
                          color: Colors.white,
                          fontSize: 10.0
                        ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Container(
              height: 50.0,
              margin: EdgeInsets.only(top: 100.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[300],
                    blurRadius: 5.0,
                    offset: Offset(0.0, 4.0)
                  )
                ],
              ),
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  _choiceDialog();
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.cloud_download, color: EduColors.primaryDark,),
                    SizedBox(width: 5.0,),
                    Text('Unduh Ujian', style: EduText().mediumSemiBold,)
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _content() {
    return BlocListener(
      bloc: _quizListBloc,
      listener: (context, state) {
        if (state is LoadingListQuiz) {
          setState(() {
            _isLoading = true;
          });
        }
        if (state is LoadFailed) {
          setState(() {
            _isLoading = false;
            print('failed');
          });
        }
        if (state is QuizListEmpty) {
          setState(() {
            _quizList = [];
            _isLoading = false;
            _isEmpty = true;
            print('empty list');
          });
        }
        if (state is QuizListLoaded) {
          _controller.forward();
          setState(() {
            _quizList = state.quizList;
            _user = state.user;
            _isLoading = false;
            _isEmpty = false;
          });
        }
        if (state is StartingQuiz) {
          setState(() {
            _isStartingQuiz = true;
          });
        }
        if (state is QuizStarted) {
          setState(() {
            _isStartingQuiz = false;
          });
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => DoQuizScreen(
              listAnswer: state.list,
              quiz: state.quizData,
              resultBody: state.resultBody
            )
          ));
        }
        if (state is FloatingAppsDetected) {
          setState(() {
            _isStartingQuiz = false;
          });
          _warningDialog();
        }
        if (state is StartQuizFailed) {
          setState(() {
            _isStartingQuiz = false;
          });
          MyToast().toast('Terjadi kesalahan');
        }
        if (state is ResultLoaded) {
          setState(() {
            _isStartingQuiz = false;
          });
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => QuizSubmittedScreen(
              correct: state.correct,
              incorrect: state.incorrect,
              score: state.score,
              unanswered: state.unanswered,
              essayScore: state.essayScore,
              quizType: state.type,
            )
          ));
        }
        if (state is LoadResultFailed) {
          setState(() {
            _isStartingQuiz = false;
          });
          MyToast().toast('Terjadi kesalahan');
        }
        if (state is Uploading) {
          setState(() {
            _isUploading = true;
            _showLoading = true;
            _showFailedIndicator = false;
            _showSuccessIndicator = false;
          });
        }
        if (state is UploadFailed) {
          setState(() {
            _showLoading = false;
            _failedStatusText = 'Gagal mengunggah';
            _showFailedIndicator = true;
            _showSuccessIndicator = false;
          });

          Fluttertoast.showToast(
            msg: "Terjadi kesalahan",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.grey,
            textColor: Colors.white,
            fontSize: 16.0
          );

          Future.delayed(const Duration(seconds: 1), () {
            setState(() {
              _isUploading = false;
            });
          });
        }
        if (state is AlreadyUploaded) {
          setState(() {
            _showLoading = false;
            _failedStatusText = 'Hasil sudah diunggah';
            _showFailedIndicator = true;
            _showSuccessIndicator = false;
          });

          Future.delayed(const Duration(seconds: 1), () {
            setState(() {
              _isUploading = false;
            });
          });
        }
        if (state is QuizClosed) {
          setState(() {
            _showLoading = false;
            _failedStatusText = 'Ujian telah ditutup';
            _showFailedIndicator = true;
            _showSuccessIndicator = false;
          });

          Future.delayed(const Duration(seconds: 1), () {
            setState(() {
              _isUploading = false;
            });
          });
        }
        if (state is ResultUploaded) {
          setState(() {
            _showLoading = false;
            _showFailedIndicator = false;
            _showSuccessIndicator = true;
          });

          Future.delayed(const Duration(seconds: 1), () {
            setState(() {
              _isUploading = false;
            });
          });

          if (state.showDialog) {
            Future.delayed(const Duration(seconds: 1), () {
              var platform = 'Play Store';
              if (Platform.isIOS) platform = 'App Store';

              _ratingDialog(platform);
            });
          }
        }
        if (state is QuizDeleted) {
          _onRefresh();
          MyToast().toast('Berhasil menghapus ${state.quizTitle}');
        }
      },
      child: _isLoading ? _loadingView() : _isEmpty ? _viewQuizEmpty() : _viewQuizList()
    );
  }

  Widget _viewQuizEmpty() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/empty.png',
            width: 60.0
          ),
          SizedBox(height: 20.0),
          Text('Tidak ada ujian',
            style: EduText().mediumSemiBold,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 5.0,),
          Text('Silakan unduh terlebih dahulu!',
            style: EduText().smallRegular,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0,),
          FlatButton(
            onPressed: () {
              _choiceDialog();
            },
            color: EduColors.primary,
            child: Text('Unduh ujian', style: EduText().smallSemiBold.copyWith(
              color: Colors.white
            ),),
          )
        ],
      ),
    );
  }

  Widget _viewQuizList() {
    return  AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Opacity(
          opacity: _fadeAnimation.value,
          child: Container(
            padding: const EdgeInsets.symmetric(
                horizontal: 16.0),
            child: Column(
              children: <Widget>[
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: _quizList.length,
                  itemBuilder: (context, index) {
                    var item = _quizList[index];
                    String statusText;
                    String buttonText;
                    Color statusColor;

                    if (item.status == 'draft') {
                      statusText = 'Belum dikerjakan';
                      buttonText = 'Mulai';
                      statusColor = EduColors.red;
                    } else if (item.status == 'ongoing') {
                      statusText = 'Sedang mengerjakan';
                      buttonText = 'Lanjutkan';
                      statusColor = EduColors.yellow;
                    } else if (item.status == 'finished') {
                      statusText = 'Sudah dikerjakan';
                      buttonText = 'Lihat hasil';
                      statusColor = EduColors.green;
                    }

                    return StreamBuilder(
                        stream: _expandedIndex,
                        builder: (context, snapshot) {
                          return _viewQuizListItem(
                            item: item,
                            index: index,
                            snapshot: snapshot,
                            statusColor: statusColor,
                            statusText: statusText,
                            buttonText: buttonText
                          );
                        }
                    );
                  },
                ),
                SizedBox(height: 30.0,)
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _viewQuizListItem({
    Items item,
    int index,
    AsyncSnapshot snapshot,
    String statusText, buttonText,
    Color statusColor
    }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 6.0),
      child: Container(
        constraints: BoxConstraints(
          minHeight: 60.0
        ),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey[300],
              blurRadius: 8.0,
              offset: Offset(0.0, 3.0)
            ),
          ],
          borderRadius: BorderRadius.circular(10.0)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () {
                if (snapshot.data == index) {
                  _expandedIndex.sink.add(-1);
                } else {
                  _expandedIndex.sink.add(index);
                }
              },
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0),
                    bottomLeft: snapshot.data != index ? Radius.circular(10.0) : Radius.circular(0.0),
                    bottomRight: snapshot.data != index ? Radius.circular(10.0) : Radius.circular(0.0)
                  ),
                ),
                height: 70.0,
                padding: EdgeInsets.symmetric(
                  horizontal: 16.0
                ),
                child: Row(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          width: 30.0,
                          height: 25.0,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: 25.0,
                            height: 25.0,
                            child: Image.asset('assets/quiz.png', fit: BoxFit.cover),
                          ),
                        ),
                        item.isUploaded && statusText == 'Sudah dikerjakan' ? Positioned(
                          top: 15.0,
                          right: 0.0,
                          child: Container(
                            width: 20.0,
                            height: 20.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: EduColors.green
                            ),
                            child: Icon(Icons.check, color: Colors.white, size: 16.0,),
                          ),
                        ) : Container()
                      ],
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 12.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(item.title,
                              style: EduText().mediumSemiBold,
                              overflow: TextOverflow.ellipsis
                            ),
                            !item.isUploaded && statusText == 'Sudah dikerjakan' ?
                              Text('Hasil belum diunggah', style: EduText().smallRegular.copyWith(
                                color: EduColors.red
                              ),)
                            : Container()
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 35.0,
                      height: 35.0,
                      child: Icon(
                        snapshot.data == index ? Icons.keyboard_arrow_up
                        : Icons.keyboard_arrow_down, size: 32.0,)
                    ),
                  ],
                ),
              ),
            ),
            AnimatedSize(
              vsync: this,
              duration: Duration(milliseconds: 300),
              curve: Curves.fastOutSlowIn,
              child: StreamBuilder(
                stream: _expandedIndex,
                builder: (context, snapshot) {
                  if (snapshot.data == index) {
                    return InkWell(
                      onTap: () {
                        if (snapshot.data == index) {
                          _expandedIndex.sink.add(-1);
                        } else {
                          _expandedIndex.sink.add(index);
                        }
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        constraints: BoxConstraints(
                          minHeight: 150.0
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(10.0),
                            bottomLeft: Radius.circular(10.0)
                          )
                        ),
                        padding: EdgeInsets.all(12.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                bottom: 22.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Pelajaran', style: EduText().mediumRegular.copyWith(
                                          color: Colors.black54)
                                        ),
                                        Builder(
                                          builder: (context) {
                                            if (item.lessonName != null) {
                                              return Text('${item.lessonName}',style: EduText().mediumRegular);
                                            } else {
                                              return Text('${item.lessonIds[0].name}',style: EduText().mediumRegular);
                                            }
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Waktu', style: EduText().mediumRegular.copyWith(
                                          color: Colors.black54)
                                        ),
                                        Text('${item.endTime} Menit',style: EduText().mediumRegular),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Jumlah Soal', style: EduText().mediumRegular.copyWith(
                                          color: Colors.black54)
                                        ),
                                        Text('${item.questionIds.length} Soal',style: EduText().mediumRegular),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Tipe', style: EduText().mediumRegular.copyWith(
                                            color: Colors.black54)
                                        ),
                                        Text('${item.type}',style: EduText().mediumRegular),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Status', style: EduText().mediumRegular.copyWith(
                                          color: Colors.black54)
                                        ),
                                        Text('$statusText',style: EduText().mediumRegular.copyWith(
                                          color: statusColor
                                        )),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Column(
                              children: <Widget>[
                                (item.showScoreOption && item.isUploaded) || item.status != 'finished'? MyButton(
                                  onTap: () {
                                    if (item.status == 'draft' || item.status == 'ongoing') {
                                      if (item.openingCode != null && item.openingCode != "") {
                                        if (item.isOpened) {
                                          _quizListBloc.add(StartQuiz(item));
                                        } else {
                                          _openingCodeDialog(item);
                                        }
                                      } else {
                                        _quizListBloc.add(StartQuiz(item));
                                      }
                                    } else {
                                      FbAnalytics().sendAnalytics('see_quiz_result');
                                      MixpanelUtils().initPlatformState('See Quiz Result');
                                      _quizListBloc.add(LoadResult(item.sId, item.quizType));
                                    }
                                  },
                                  isDisabled: false,
                                  child: !_isStartingQuiz ? Text('$buttonText', style: EduText().mediumSemiBold.copyWith(
                                    color: Colors.white
                                  )) : SpinKitThreeBounce(
                                    color: Colors.white,
                                    size: 20.0,
                                    controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
                                  ),
                                ) : Container(),
                                statusText == 'Sudah dikerjakan' ? SizedBox(height: 10.0,) : Container(),
                                statusText == 'Sudah dikerjakan' ? MyButton(
                                  onTap: () {
                                      _quizListBloc.add(UploadResult(item));
                                    },
                                    isDisabled: false,
                                    color: EduColors.red,
                                    child: Text('Unggah hasil ujian', style: EduText().mediumSemiBold.copyWith(
                                      color: Colors.white
                                    )
                                  )
                                ) : Container(),
                                SizedBox(height: 10.0,),
                                InkWell(
                                  onTap: () {
                                    _deleteDialog(item);
                                  },
                                  child: Container(
                                    height: 50.0,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      border: Border.all(width: 1.0, color: Colors.grey),
                                      borderRadius: BorderRadius.circular(50.0),
                                      color: Colors.white
                                    ),
                                    child: Center(
                                      child: Text('Hapus', style: EduText().mediumSemiBold,),
                                    )
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _loadingView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 150.0),
      child: SpinKitRing(
        color: EduColors.primary,
        size: 50.0,
        lineWidth: 3.0,
        controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
      )
    );
//    return ListView.builder(
//      shrinkWrap: true,
//      itemCount: 10,
//      itemBuilder: (context, state) {
//        return Shimmer.fromColors(
//          period: Duration(milliseconds: 500),
//          baseColor: Colors.grey[300],
//          highlightColor: Colors.grey[200],
//          child: Padding(
//            padding: const EdgeInsets.symmetric(
//              vertical: 6.0, horizontal: 16.0),
//            child: Container(
//              width: MediaQuery.of(context).size.width,
//              height: 80.0,
//              decoration: BoxDecoration(
//                borderRadius: BorderRadius.circular(10.0),
//                color: Colors.grey[200]
//              ),
//            ),
//          ),
//        );
//      },
//    );
  }

  Widget _loading() {
    return Visibility(
      visible: _showLoading,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitDualRing(
            color: Colors.white,
            size: 30.0,
            lineWidth: 3.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text('Tunggu sebentar..', style: EduText().smallRegular.copyWith(
              color: Colors.white
            )),
          )
        ],
      ),
    );
  }

  Widget _successIndicator() {
    return Visibility(
      visible: _showSuccessIndicator,
      child: Column(
        children: <Widget>[
          Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.5, color: Colors.white
              ),
              shape: BoxShape.circle
            ),
            child: Icon(Icons.check, color: Colors.white,)
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text('Berhasil mengunggah', style: EduText().smallRegular.copyWith(
                color: Colors.white
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _failedIndicator() {
    return Visibility(
      visible: _showFailedIndicator,
      child: Column(
        children: <Widget>[
          Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.5, color: Colors.white
              ),
              shape: BoxShape.circle
            ),
            child: Icon(Icons.close, color: Colors.white,)
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(_failedStatusText, style: EduText().smallRegular.copyWith(
                color: Colors.white
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}