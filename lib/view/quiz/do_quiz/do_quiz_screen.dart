import 'package:edubox_tryout/main.dart';
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/quiz/do_quiz/do_quiz_bloc/bloc.dart';
import 'package:edubox_tryout/view/quiz/quiz_submitted/quiz_submitted_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:photo_view/photo_view.dart';
import 'package:screen/screen.dart';

ResultBody resultBody;
DoQuizBloc doQuizBloc;
Items quiz;
String answer;
int position = 0;
int remaining = 0;
int durations = 0;
bool cbValue = false;
List<StudentAnswersBody> listAnswers;

class DoQuizScreen extends StatefulWidget {
  final Items quiz;
  final ResultBody resultBody;
  final List<StudentAnswersBody> listAnswer;

  const DoQuizScreen({Key key, this.quiz, this.resultBody, this.listAnswer})
      : super(key: key);

  @override
  _DoQuizScreenState createState() => _DoQuizScreenState();
}

class _DoQuizScreenState extends State<DoQuizScreen>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  FocusNode _blocCodeFocus = FocusNode();
  AnimationController _controller;
  TextEditingController _answerController;
  ScrollController _scrollController;
  bool _isSubmitted = false;
  bool _isBlocked = false;
  String get timerString {
    Duration duration = _controller.duration * _controller.value;
    return '${duration.inHours}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    Screen.keepOn(true);

    quiz = widget.quiz;
    resultBody = widget.resultBody;
    listAnswers = widget.listAnswer;
    doQuizBloc = DoQuizBloc();

    _answerController =
        new TextEditingController(text: listAnswers[position].answer);
    _scrollController = new ScrollController();

    if (resultBody.remaining != null) {
      durations = resultBody.remaining;
    } else {
      durations = quiz.endTime;
    }
    _controller = AnimationController(
        vsync: this, duration: Duration(minutes: durations));
    _controller.reverse(from: durations.toDouble());

    FbAnalytics().setCurrentScreen('Do Quiz Screen');
    FbAnalytics().sendAnalytics('do_quiz');
    MixpanelUtils().initPlatformState('Do Quiz Screen');

    WidgetsBinding.instance.addObserver(this);
    super.initState();

    if (widget.quiz.passcode != null) {
      if (widget.quiz.passcode != "") {
        if (widget.quiz.isBlocked) {
          if (!_isBlocked) {
            SchedulerBinding.instance
                .addPostFrameCallback((_) => _blockCodeDialog());
          }
        }
      }
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      if (widget.quiz.passcode != null) {
        if (widget.quiz.passcode != "") {
          if (!_isBlocked) {
            _blockCodeDialog();
          }
        }
      }
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  void dispose() {
    _onDispose();
    WidgetsBinding.instance.removeObserver(this);
    _controller.stop();
    super.dispose();
  }

  void _onDispose() {
    resultBody = null;
    quiz = null;
    answer = null;
    position = 0;
    remaining = 0;
    durations = 0;
    cbValue = false;
    listAnswers = [];
  }

  void _blockCodeDialog() {
    FocusScope.of(context).requestFocus(_blocCodeFocus);
    setState(() {
      _isBlocked = true;
    });
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return WillPopScope(
            onWillPop: () {
              Navigator.pop(dialogContext);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyApp(
                            currentPage: 1,
                          )),
                  (Route<dynamic> route) => false);
              return null;
            },
            child: CustomDialog(
              title: Strings.appName,
              content: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Column(
                  children: <Widget>[
                    Text(
                        'Ujian diblokir! Buka blokir untuk melanjutkan ujian.'),
                    SizedBox(
                      height: 5.0,
                    ),
                    Form(
                      key: _formKey,
                      child: MyTextField(
                        focusNode: _blocCodeFocus,
                        obsecureText: true,
                        hintText: 'Masukkan kode pembuka',
                        validator: (val) {
                          if (val != widget.quiz.passcode) {
                            return 'Kode pembuka salah!';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
              ),
              actionAlignment: MainAxisAlignment.end,
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.pop(dialogContext);
                      setState(() {
                        _isBlocked = false;
                      });
                    }
                  },
                  color: EduColors.primary,
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Buka',
                        style: EduText()
                            .smallSemiBold
                            .copyWith(color: Colors.white),
                      ),
                      SizedBox(
                        width: 2.0,
                      ),
                      Icon(
                        Icons.vpn_key,
                        color: Colors.white,
                        size: 18.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  void _changeQuestion() {
    if (position < 3) {
      _scrollController.animateTo(0.0,
          duration: Duration(milliseconds: 700), curve: Curves.easeOut);
    } else if (position > quiz.questionsIds.length - 4) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 700), curve: Curves.easeOut);
    } else {
      _scrollController.animateTo(60 * (position - 3).floorToDouble(),
          duration: Duration(milliseconds: 700), curve: Curves.easeOut);
    }
  }

  void _dialogSubmit() {
    var _answeredList = listAnswers.where((i) => i.state == 'dijawab').toList();
    var _doubtfulList =
        listAnswers.where((i) => i.state == 'ragu-ragu').toList();
    var _unansweredList = listAnswers.where((i) => i.state == 'belum').toList();

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            content: Padding(
              padding: EdgeInsets.only(bottom: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Container(
                        width: 70.0,
                        height: 70.0,
                        child: Image.asset('assets/quiz.png')),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Soal dijawab :',
                                  style: EduText().mediumRegular),
                              Text(' ${_answeredList.length}',
                                  style: EduText().mediumSemiBold)
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Ragu-ragu :',
                                  style: EduText().mediumRegular),
                              Text(' ${_doubtfulList.length}',
                                  style: EduText().mediumSemiBold)
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Belum dijawab :',
                                  style: EduText().mediumRegular),
                              Text(' ${_unansweredList.length}',
                                  style: EduText().mediumSemiBold)
                            ],
                          ),
                        ]),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    'Apakah kamu yakin ingin menyelesaikan ujian ?',
                    style: EduText().mediumRegular,
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
            actionAlignment: MainAxisAlignment.center,
            actions: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 95.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(width: 1.5, color: EduColors.primary)),
                  child: Center(
                      child: Text(
                    'Batal',
                    style: EduText()
                        .smallSemiBold
                        .copyWith(color: EduColors.primary),
                  )),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              InkWell(
                onTap: () {
                  FbAnalytics().sendAnalytics('submit_quiz');
                  MixpanelUtils().initPlatformState('Submit Quiz');

                  doQuizBloc.add(SubmitQuiz(
                      id: quiz.sId,
                      list: listAnswers,
                      result: resultBody,
                      quizData: quiz));
                  Navigator.pop(context);
                },
                child: Container(
                  width: 95.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: EduColors.primary,
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Center(
                      child: Text(
                    'Yakin',
                    style:
                        EduText().smallSemiBold.copyWith(color: Colors.white),
                  )),
                ),
              ),
            ],
          );
        });
  }

  _onWillPop() {
    if (widget.quiz.passcode != null && widget.quiz.passcode != "") {
      _dialogSubmit();
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            iconTheme: IconThemeData(color: EduColors.black),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(quiz.title, style: EduText().largeSemiBold),
                Text('Waktu pengerjaan ${quiz.endTime} menit',
                    style:
                        EduText().smallRegular.copyWith(color: Colors.black54))
              ],
            ),
            automaticallyImplyLeading:
                widget.quiz.passcode != null && widget.quiz.passcode != ""
                    ? false
                    : true,
            actions: [
              Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () => _dialogSubmit(),
                        borderRadius: BorderRadius.circular(5.0),
                        child: Container(
                          width: 75.0,
                          height: 35.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            border: Border.all(
                                color: EduColors.primary, width: 1.5),
                          ),
                          padding: EdgeInsets.all(5.0),
                          child: Center(
                              child: Text('Selesai',
                                  style: EduText()
                                      .mediumSemiBold
                                      .copyWith(color: EduColors.primary))),
                        ),
                      ),
                    ],
                  )),
            ],
          ),
          bottomNavigationBar: quiz.questionsIds[position].type == 'essay'
              ? null
              : Choices(
                  controller: _controller,
                  isSubmitted: _isSubmitted,
                ),
          endDrawer: _drawer(),
          body: BlocListener(
            bloc: doQuizBloc,
            listener: (context, state) {
              if (state is UpdateResultFailed || state is SubmitQuizFailed) {
                Fluttertoast.showToast(
                    msg: "Terjadi kesalahan",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    backgroundColor: Colors.grey[400],
                    textColor: Colors.white);
              }
              if (state is QuizSubmitted) {
                setState(() {
                  _isSubmitted = true;
                });
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => QuizFinishedScreen()),
                    (Route<dynamic> route) => false);
              }
            },
            child: Column(children: [_header(), _content(), _essayAnswer()]),
          )),
    );
  }

  Widget _header() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 5.0),
      child: Column(
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            AnimatedBuilder(
              animation: _controller,
              builder: (context, widget) {
                if (_controller.value == 0.0) {
                  doQuizBloc.add(SubmitQuiz(
                      id: quiz.sId,
                      list: listAnswers,
                      result: resultBody,
                      quizData: quiz));
                }
                return Text(
                  timerString,
                  style: EduText().largeSemiBold,
                );
              },
            ),
            Row(children: [
              Container(
                child: Text(
                    'Soal ${position + 1} dari ${quiz.questionsIds.length}',
                    style: EduText().mediumRegular),
              ),
              SizedBox(width: 10.0),
              GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
                child: Container(
                  width: 35.0,
                  height: 35.0,
                  decoration: BoxDecoration(
                    color: EduColors.yellow,
                    shape: BoxShape.circle,
                  ),
                  padding: EdgeInsets.all(6.0),
                  child: Image.asset('assets/drawer.png'),
                ),
              )
            ])
          ]),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(children: [
              _arrowNavigation('left',
                  Icon(Icons.arrow_back_ios, color: Colors.black, size: 20.0)),
              _horizontalNumber(),
              _arrowNavigation(
                  'right',
                  Icon(Icons.arrow_forward_ios,
                      color: Colors.black, size: 20.0))
            ]),
          )
        ],
      ),
    );
  }

  Widget _arrowNavigation(String direction, Icon icon) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        setState(() {
          if (direction == 'right') {
            if (position < quiz.questionsIds.length - 1) position++;
          } else if (direction == 'left') {
            if (position > 0) position--;
          }

          answer = null;
          cbValue = false;
          _answerController =
              new TextEditingController(text: listAnswers[position].answer);
        });
        _changeQuestion();
      },
      child: Container(
          width: 35.0,
          height: 35.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.grey[200]),
          child: icon),
    );
  }

  Widget _horizontalNumber() {
    return Expanded(
      child: Container(
          height: 50.0,
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: ListView.builder(
            controller: _scrollController,
            scrollDirection: Axis.horizontal,
            physics: BouncingScrollPhysics(),
            itemCount: quiz.questionsIds.length,
            itemBuilder: (context, index) {
              int number = index + 1;
              return Padding(
                padding: const EdgeInsets.all(3.0),
                child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    setState(() {
                      position = index;

                      answer = null;
                      cbValue = false;
                      _answerController = new TextEditingController(
                          text: listAnswers[position].answer);
                    });
                    _changeQuestion();
                  },
                  child: Container(
                    width: 50.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: index == position
                            ? EduColors.primary
                            : listAnswers[index].state == 'ragu-ragu'
                                ? EduColors.yellow
                                : listAnswers[index].state == 'dijawab'
                                    ? EduColors.green
                                    : Colors.grey[200]),
                    child: Center(
                        child: Text(number.toString(),
                            style: EduText().mediumSemiBold.copyWith(
                                color: index == position ||
                                        listAnswers[index].state == 'dijawab' ||
                                        listAnswers[index].state == 'ragu-ragu'
                                    ? Colors.white
                                    : Colors.black))),
                  ),
                ),
              );
            },
          )),
    );
  }

  Widget _content() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
        child: Container(
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey[300])),
          child: Scrollbar(
            child: ListView(physics: BouncingScrollPhysics(), children: [
              _question(),
              _options(),
            ]),
          ),
        ),
      ),
    );
  }

  Widget _question() {
    return Container(
      width: MediaQuery.of(context).size.width,
      constraints: BoxConstraints(
        minHeight: 60.0,
      ),
      padding: EdgeInsets.all(10.0),
      child: Html(
        data: """${quiz.questionsIds[position].text}""",
        defaultTextStyle: EduText().mediumSemiBold,
        onImageTap: (img) {
          print(img);
          UriData data = Uri.parse(img).data;
          var bytes = data.contentAsBytes();
          _dialogImageZoomed(bytes);
        },
      ),
    );
  }

  void _dialogImageZoomed(bytes) {
    showDialog(
        context: context,
        builder: (ctx) {
          return PhotoView(
            imageProvider: MemoryImage(bytes),
            heroAttributes: const PhotoViewHeroAttributes(
                tag: "image zoom", transitionOnUserGestures: true),
            onTapUp: (ctx, img, controller) {
              Navigator.of(ctx).pop();
            },
            backgroundDecoration: BoxDecoration(color: Colors.transparent),
            minScale: PhotoViewComputedScale.contained * 1.0,
          );
        });
  }

  Widget _options() {
    return quiz.questionsIds[position].type == "essay"
        ? Container()
        : Column(children: [
            _answerItem('A', '${quiz.questionsIds[position].choices.a}',
                quiz.questionsIds[position].choices.a != null ? true : false),
            _answerItem('B', '${quiz.questionsIds[position].choices.b}',
                quiz.questionsIds[position].choices.b != null ? true : false),
            _answerItem('C', '${quiz.questionsIds[position].choices.c}',
                quiz.questionsIds[position].choices.c != null ? true : false),
            _answerItem('D', '${quiz.questionsIds[position].choices.d}',
                quiz.questionsIds[position].choices.d != null ? true : false),
            _answerItem('E', '${quiz.questionsIds[position].choices.e}',
                quiz.questionsIds[position].choices.e != null ? true : false),
          ]);
  }

  Widget _answerItem(String choice, String data, bool isExist) {
    return Visibility(
      visible: isExist,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 50.0,
              height: 50.0,
              child: Center(child: Text(choice)),
            ),
            Expanded(
                child: Container(
              constraints: BoxConstraints(minHeight: 50.0),
              child: Center(
                child: Html(
                  data: """$data""",
                  defaultTextStyle: EduText().mediumSemiBold,
                  onImageTap: (img) {
                    UriData data = Uri.parse(img).data;
                    var bytes = data.contentAsBytes();
                    _dialogImageZoomed(bytes);
                  },
                ),
              ),
            ))
          ]),
    );
  }

  Widget _essayAnswer() {
    if (quiz.questionsIds[position].type == "essay") {
      return Padding(
        padding:
            const EdgeInsets.only(left: 7.0, right: 7.0, bottom: 4.0, top: 0.0),
        child: Row(
          children: <Widget>[
            Flexible(
                child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, bottom: 8.0),
                    child: TextFormField(
                      controller: _answerController,
                      maxLines: 3,
                      decoration: InputDecoration(
                        hintText: 'Jawab disini',
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 1.0, color: Colors.black)),
                        suffixIcon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () {
                              Duration duration =
                                  _controller.duration * _controller.value;
                              remaining = duration.inMinutes;

                              FocusScope.of(context).requestFocus(FocusNode());

                              setState(() {
                                answer = _answerController.text;
                                listAnswers[position].answer = answer;
                                listAnswers[position].state = 'dijawab';
                              });

                              doQuizBloc.add(UpdateResult(
                                  result: resultBody,
                                  list: listAnswers,
                                  remaining: remaining,
                                  id: quiz.sId));
                            },
                            child: CircleAvatar(
                              child: Icon(Icons.send, color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ))),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _drawer() {
    return Drawer(
        child: Padding(
      padding: const EdgeInsets.all(24.0),
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4, childAspectRatio: 1.0),
        shrinkWrap: true,
        itemCount: quiz.questionsIds.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  position = index;

                  answer = null;
                  cbValue = false;
                  _answerController = new TextEditingController(
                      text: listAnswers[position].answer);
                });
                _changeQuestion();
                Future.delayed(const Duration(milliseconds: 200), () {
                  Navigator.pop(context);
                });
              },
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: index == position
                        ? EduColors.primary
                        : listAnswers[index].state == 'ragu-ragu'
                            ? EduColors.yellow
                            : listAnswers[index].state == 'dijawab'
                                ? EduColors.green
                                : Colors.grey[200]),
                child: Center(
                    child: Text(
                  '${index + 1}',
                  style: EduText().mediumSemiBold.copyWith(
                      color: index == position ||
                              listAnswers[index].state == 'dijawab' ||
                              listAnswers[index].state == 'ragu-ragu'
                          ? Colors.white
                          : Colors.black),
                )),
              ),
            ),
          );
        },
      ),
    ));
  }
}

class Choices extends StatefulWidget {
  final AnimationController controller;
  final bool isSubmitted;

  const Choices({Key key, this.controller, this.isSubmitted}) : super(key: key);
  @override
  _ChoicesState createState() => _ChoicesState();
}

class _ChoicesState extends State<Choices> {
  AnimationController get _controller => widget.controller;
  bool get _isSubmitted => widget.isSubmitted;

  void _onChangedDoubtful(bool resp) {
    setState(() {
      cbValue = resp;
      if (cbValue) {
        listAnswers[position].state = 'ragu-ragu';
      } else {
        if (listAnswers[position].answer != null) {
          listAnswers[position].state = 'dijawab';
        } else {
          listAnswers[position].state = 'belum';
        }
      }

      _updateResult();
    });
  }

  void _updateResult() {
    Duration duration = _controller.duration * _controller.value;
    remaining = duration.inMinutes;

    doQuizBloc.add(UpdateResult(
        result: resultBody,
        remaining: remaining,
        list: listAnswers,
        id: quiz.sId));
  }

  @override
  void dispose() {
    if (!_isSubmitted) {
      _updateResult();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        height: 110.0,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      cbValue = !cbValue;
                      if (cbValue) {
                        listAnswers[position].state = 'ragu-ragu';
                      } else {
                        if (listAnswers[position].answer != null) {
                          listAnswers[position].state = 'dijawab';
                        } else {
                          listAnswers[position].state = 'belum';
                        }
                      }
                      _updateResult();
                    });
                  },
                  child: Container(
                    height: 30.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            '*ketuk gambar untuk memperbesar',
                            style:
                                EduText().smallRegular.copyWith(fontSize: 10.0),
                          ),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text('Ragu-ragu',
                                  style: EduText().mediumSemiBold.copyWith(
                                        fontSize: 10.0,
                                      )),
                            ),
                            Container(
                              height: 30.0,
                              width: 30.0,
                              child: Checkbox(
                                  activeColor: EduColors.yellow,
                                  value:
                                      listAnswers[position].state == 'ragu-ragu'
                                          ? true
                                          : cbValue,
                                  onChanged: _onChangedDoubtful),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _choice(
                    'A',
                    '${quiz.questionsIds[position].choices.a}',
                    quiz.questionsIds[position].choices.a != null
                        ? true
                        : false),
                _choice(
                    'B',
                    '${quiz.questionsIds[position].choices.b}',
                    quiz.questionsIds[position].choices.b != null
                        ? true
                        : false),
                _choice(
                    'C',
                    '${quiz.questionsIds[position].choices.c}',
                    quiz.questionsIds[position].choices.c != null
                        ? true
                        : false),
                _choice(
                    'D',
                    '${quiz.questionsIds[position].choices.d}',
                    quiz.questionsIds[position].choices.d != null
                        ? true
                        : false),
                _choice(
                    'E',
                    '${quiz.questionsIds[position].choices.e}',
                    quiz.questionsIds[position].choices.e != null
                        ? true
                        : false),
              ],
            ),
          ],
        ));
  }

  Widget _choice(String choice, String data, bool isExist) {
    return Visibility(
      visible: isExist,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: GestureDetector(
          onTap: () {
            setState(() {
              answer = choice;
              listAnswers[position].answer = answer;
              listAnswers[position].state = cbValue ? 'ragu-ragu' : 'dijawab';
              if (listAnswers[position].answer ==
                  quiz.questionsIds[position].keyAnswer) {
                listAnswers[position].isCorrect = true;
              } else {
                listAnswers[position].isCorrect = false;
              }

              print('Question Id : ${listAnswers[position].id}');
              print(quiz.questionsIds[position].keyAnswer);
              print(listAnswers[position].answer);
              print(listAnswers[position].isCorrect);

              _updateResult();
            });
          },
          child: Container(
              width: 50.0,
              height: 50.0,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFFcccccc),
                      blurRadius: 1.5,
                      spreadRadius: 1.0,
                      offset: Offset(0.0, 2.0),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(40.0),
                  color: listAnswers[position].answer == choice
                      ? Colors.grey
                      : answer == choice ? Colors.grey : Colors.grey[200]),
              child: Center(
                  child: Text(choice,
                      style: EduText().mediumSemiBold.copyWith(
                          fontWeight: FontWeight.bold,
                          color: listAnswers[position].answer == choice
                              ? Colors.white
                              : answer == choice
                                  ? Colors.white
                                  : Colors.black)))),
        ),
      ),
    );
  }
}
