import 'package:edubox_tryout/models/quiz.dart';
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:equatable/equatable.dart';

abstract class DoQuizEvent extends Equatable {
  const DoQuizEvent();
}

class UpdateResult extends DoQuizEvent {
  final String id;
  final ResultBody result;  
  final List<StudentAnswersBody> list;
  final int remaining;

  UpdateResult({this.result, this.list, this.id, this.remaining});

  @override  
  List<Object> get props => [result, list];
}

class SubmitQuiz extends DoQuizEvent {
  final String id;  
  final Items quizData;
  final List<StudentAnswersBody> list;
  final ResultBody result;  

  SubmitQuiz({this.id, this.quizData, this.list, this.result});

  @override  
  List<Object> get props => null;
}