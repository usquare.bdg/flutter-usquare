import 'package:equatable/equatable.dart';

abstract class DoQuizState extends Equatable {
  const DoQuizState();
}

class InitialDoQuizState extends DoQuizState {
  @override
  List<Object> get props => [];
}

class ResultUpdated extends DoQuizState {
  @override  
  List<Object> get props => [];
}

class UpdateResultFailed extends DoQuizState {
  @override  
  List<Object> get props => [];
}

class QuizSubmitted extends DoQuizState {
  final int incorrect, unanswered, correct;
  final double score;
  final bool showScore;

  QuizSubmitted({this.incorrect, this.unanswered, this.correct, this.score, this.showScore});
  @override  
  List<Object> get props => [];
}

class SubmitQuizFailed extends DoQuizState {  
  @override  
  List<Object> get props => [];
}