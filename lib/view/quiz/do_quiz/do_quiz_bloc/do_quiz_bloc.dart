import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import './bloc.dart';

class DoQuizBloc extends Bloc<DoQuizEvent, DoQuizState> {
  @override
  DoQuizState get initialState => InitialDoQuizState();

  @override
  Stream<DoQuizState> mapEventToState(
    DoQuizEvent event,
  ) async* {
    if (event is UpdateResult) {
      yield* _updateResultBodyToState(event);
    } else if (event is SubmitQuiz) {
      yield* _submitQuizToState(event);
    }
  }

  Stream<DoQuizState> _updateResultBodyToState(event) async* {
    try {
      final ResultBody resultBody = event.result;
        resultBody.studentAnswersBody = event.list;
        resultBody.remaining = event.remaining;

      await DbDao().updateResultBody(resultBody, event.id);
      yield ResultUpdated();   
    } catch (e) {
      print(e);
      yield UpdateResultFailed();
    }
  }

  Stream<DoQuizState> _submitQuizToState(event) async* {
    try {
      final List<StudentAnswersBody> listAnswers = event.list;
      final listCorrect = listAnswers.where((i) => i.isCorrect).toList();
      final unanswered  = listAnswers.where((i) => i.state == 'belum').toList();
      final Items quizData = event.quizData;
        quizData.status = 'finished';

      final score = (100 / listAnswers.length) * listCorrect.length;
      final correctAnswer = listCorrect.length;
      final incorrectAnswer = listAnswers.length - listCorrect.length;
      final ResultBody resultBody = event.result;
        resultBody.status = 'published';
        resultBody.remaining = null;
        resultBody.score = score;
        resultBody.wrong = incorrectAnswer;
        resultBody.correct = correctAnswer;
        resultBody.unanswers = unanswered.length;

      await DbDao().updateQuiz(quizData, event.id);
      await DbDao().updateResultBody(resultBody, event.id);

      print('Score = $score | CA = $correctAnswer | IA = $incorrectAnswer | UA = ${unanswered.length}');

      yield QuizSubmitted(
        correct: correctAnswer,
        incorrect: incorrectAnswer,
        score: score,
        unanswered: unanswered.length,
        showScore: quizData.showScoreOption
      );
    } catch (e) {
      yield SubmitQuizFailed();
      print(e);
    }
  }
}
