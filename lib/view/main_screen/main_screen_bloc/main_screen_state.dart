import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MainScreenState extends Equatable {}

class InitialMainScreenState extends MainScreenState {
  @override
  List<Object> get props => [];
}

class TryOutScreenLoaded extends MainScreenState {
  @override
  List<Object> get props => [];
}

class ContentScreenLoaded extends MainScreenState {
  final int number;

  ContentScreenLoaded(this.number);

  @override
  List<Object> get props => [number];
}

class TaskScreenLoaded extends MainScreenState {
  @override
  List<Object> get props => [];
}

class ProfileScreenLoaded extends MainScreenState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class HomeScreenLoaded extends MainScreenState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class FooState extends MainScreenState {
  @override
  List<Object> get props => [];
}
