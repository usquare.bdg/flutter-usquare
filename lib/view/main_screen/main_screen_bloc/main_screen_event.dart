import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MainScreenEvent extends Equatable{}

class LoadTryOutScreen extends MainScreenEvent {

  @override
  List<Object> get props => [];
}

class LoadContentScreen extends MainScreenEvent {
  final int number;

  LoadContentScreen(this.number);

  @override
  List<Object> get props => [];
}

class LoadTaskScreen extends MainScreenEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadProfileScreen extends MainScreenEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoadHomeScreen extends MainScreenEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class Foo extends MainScreenEvent {
  @override
  List<Object> get props => [];
}
