import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class MainScreenBloc extends Bloc<MainScreenEvent, MainScreenState> {
  @override
  MainScreenState get initialState => InitialMainScreenState();

  @override
  Stream<MainScreenState> mapEventToState(
    MainScreenEvent event,
  ) async* {
    if (event is LoadTryOutScreen) {
      yield TryOutScreenLoaded();
    } else if (event is LoadContentScreen) {
      yield ContentScreenLoaded(event.number);
    } else if (event is LoadTaskScreen) {
      yield TaskScreenLoaded();
    } else if (event is LoadProfileScreen) {
      yield ProfileScreenLoaded();
    } else if (event is LoadHomeScreen) {
      yield HomeScreenLoaded();
    } else if (event is Foo) {
      yield FooState();
    }
  }
}
