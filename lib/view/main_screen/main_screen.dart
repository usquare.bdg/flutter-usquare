import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/view/assignment/list_assignment_screen.dart';
import 'package:edubox_tryout/view/content/content_screen.dart';
import 'package:edubox_tryout/view/home/home_screen.dart';
import 'package:edubox_tryout/view/main_screen/main_screen_bloc/bloc.dart';
import 'package:edubox_tryout/view/profile/profile_screen.dart';
import 'package:edubox_tryout/view/quiz/quiz_list/quiz_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatefulWidget {
  final int currentPage;

  const MainScreen({Key key, this.currentPage}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  MainScreenBloc _mainScreenBloc;
  int _currentIndex = 0;
  List<Widget> _listPages;

  void _onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      _mainScreenBloc.add(Foo());
    });
  }

  @override
  void initState() {
    _listPages = List<Widget>();
    _listPages.add(HomeScreen());
    _listPages.add(QuizListScreen());
    _listPages.add(ListTaskScreen());
    _listPages.add(ContentScreen());
    _listPages.add(ProfileScreen());
    _mainScreenBloc = MainScreenBloc();
    widget.currentPage != null ? _currentIndex = widget.currentPage : 0;
    super.initState();
  }

  @override
  void dispose() {
    _setFirstTimeOpen(false);
    super.dispose();
  }

  Future<bool> _onWillPop() {
    if (_currentIndex == 0) {
      SystemNavigator.pop();
    } else {
      setState(() {
        _currentIndex = 0;
      });
    }
    _mainScreenBloc.add(Foo());
  }

  Future _setFirstTimeOpen(firstTimeOpen) async {
    await ApiClient().setFirstTimeOpen(firstTimeOpen);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: BlocProvider(
          create: (context) => _mainScreenBloc,
          child: BlocListener(
            bloc: _mainScreenBloc,
            listener: (context, state) {
              if (state is HomeScreenLoaded) {
                _onTabTapped(0);
              }
              if (state is TryOutScreenLoaded) {
                _onTabTapped(1);
              }
              if (state is ContentScreenLoaded) {
                _onTabTapped(3);
              }
              if (state is TaskScreenLoaded) {
                _onTabTapped(2);
              }
              if (state is ProfileScreenLoaded) {
                _onTabTapped(4);
              }
            },
            child: BlocBuilder(
              bloc: _mainScreenBloc,
              builder: (context, state) {
                return _listPages[_currentIndex];
              },
            )
          ),
        ),
        bottomNavigationBar: SizedBox(
          height: 55.0,
          child: BottomNavigationBar(
            onTap: _onTabTapped,
            currentIndex: _currentIndex,
            selectedItemColor: EduColors.black,
            unselectedItemColor: Colors.grey,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home,),
                title: Text('Beranda', style: TextStyle(
                  fontSize: 10.0
                ))
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.library_books),
                title: Text('Ujian', style: TextStyle(
                    fontSize: 10.0
                ))
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.assignment),
                title: Text('Tugas', style: TextStyle(
                    fontSize: 10.0
                ))
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.book),
                title: Text('Belajar', style: TextStyle(
                    fontSize: 10.0
                ))
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                title: Text('Profil', style: TextStyle(
                    fontSize: 10.0
                ))
              ),
            ],
          ),
        ),
      ),
    );
  }
}
