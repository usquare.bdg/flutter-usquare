import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/profile/add_phone_screen.dart';
import 'package:edubox_tryout/view/profile/profile_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:edubox_tryout/widgets/textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class EditProfileScreen extends StatefulWidget {
  final User user;

  const EditProfileScreen({Key key, this.user}) : super(key: key);
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  User get _user => widget.user;
  ProfileBloc _profileBloc;
  TextEditingController _usernameController, _nameController, _emailController, _institutionController, _classController, _passwordController;
  FocusNode _nameFocus = new FocusNode();
  FocusNode _emailFocus = new FocusNode();
  FocusNode _passwordFocus = new FocusNode();
  bool _cbVal = false;

  @override
  void initState() {
    _usernameController = TextEditingController(text: _user.data.username);
    _nameController = TextEditingController(text: _user.data.displayName);
    _emailController = TextEditingController(text: _user.data.email);
    _institutionController = TextEditingController(text: _user.data.studentAtInstitutions[0].name);
    _classController = TextEditingController(text: _user.data.rombelIds.last.name);
    _passwordController = TextEditingController(text: '');

    _profileBloc = new ProfileBloc();
    super.initState();
  }

  void _onSubmit() {
    _profileBloc.add(UpdateProfile(
      _nameController.text,
      _emailController.text,
      _user,
      _user.data.phoneNumber,
      _passwordController.text
    ));
  }

  void _loadingDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext dialogContext) {
        return CustomDialog(
          content: Row(
            children: <Widget>[
              SpinKitRing(
                color: EduColors.primary,
                size: 40.0,
                lineWidth: 3.0,
              ),
              Expanded(
                child: Center(
                  child: Text('Mengirim data..', style: EduText().mediumRegular,)
                ),
              )
            ],
          ),
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);
        return null;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.black),
          automaticallyImplyLeading: true,
          title: Text('Edit Profil', style: EduText().mediumBold),
        ),
        body: BlocListener(
          bloc: _profileBloc,
          listener: (context, state) {
            if (state is Loading) _loadingDialog();
            if (state is UpdateProfileFailed) {
              Navigator.of(context, rootNavigator: true).pop();
              MyToast().toast('Terjadi kesalahan');
            }
            if (state is ProfileUpdated) {
              Navigator.of(context, rootNavigator: true).pop();
              Navigator.pop(context, true);
              MyToast().toast('Profil telah diubah');
            }
          },
          child: _formView(),
        ),
        bottomSheet: InkWell(
          onTap: _onSubmit,
          child: Container(
            height: 50.0,
            color: EduColors.primary,
            child: Center(
              child: Text('Simpan', style: EduText().mediumSemiBold.copyWith(
                color: Colors.white
              ),),
            ),
          ),
        ),
      ),
    );
  }

  Widget _formView() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 50.0),
      child: ListView(
        padding: const EdgeInsets.symmetric(
            horizontal: 16.0, vertical: 10.0),
        children: <Widget>[
          _formItem(
            title: 'Username',
            enabled: false,
            controller: _usernameController,
          ),
          _formItem(
            title: 'Nama',
            enabled: false,
            focus: _nameFocus,
            controller: _nameController,
            actionType: TextInputAction.next,
            onSubmitted: (val) {
              _nameFocus.unfocus();
              FocusScope.of(context).requestFocus(_emailFocus);
            }
          ),
          _formItem(
            title: 'Email',
            enabled: true,
            focus: _emailFocus,
            controller: _emailController,
            actionType: _cbVal ? TextInputAction.next : TextInputAction.go,
            onSubmitted: (val) {
              _emailFocus.unfocus();
              if (_cbVal) FocusScope.of(context).requestFocus(_passwordFocus);
              else _onSubmit();
            }
          ),
          _phoneNumber(),
          _formItem(
            title: 'Kelas',
            enabled: false,
            controller: _classController,
          ),
          _formItem(
            title: 'Sekolah',
            enabled: false,
            controller: _institutionController,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: InkWell(
              onTap: () {
                setState(() => _cbVal = !_cbVal);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Atur ulang password', style: EduText().mediumRegular,),
                  Checkbox(
                    value: _cbVal,
                    onChanged: (bool value) {
                      setState(() => _cbVal = value);
                    },
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: _cbVal,
            child: _formItem(
              title: 'Password',
              enabled: true,
              hintText: 'Masukkan password baru',
              focus: _passwordFocus,
              controller: _passwordController,
              actionType: TextInputAction.go,
              onSubmitted: (val) {
                _passwordFocus.unfocus();
                _onSubmit();
              }
            ),
          )
        ],
      ),
    );
  }

  Widget _formItem({
    String title, hintText,
    TextEditingController controller,
    FocusNode focus,
    TextInputAction actionType,
    Function onSubmitted,
    bool enabled,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: EduText().smallSemiBold,),
          MyTextField(
            controller: controller,
            focusNode: focus,
            isEnabled: enabled,
            actionType: actionType,
            onSubmitted: onSubmitted,
            hintText: hintText ?? null,
            keyboardType: TextInputType.emailAddress,
          )
        ],
      ),
    );
  }

  Widget _phoneNumber() {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('No. Handphone', style: EduText().smallSemiBold,),
          Builder(
            builder: (context) {
              if (_user.data.phoneNumber != null) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(_user.data.phoneNumber, style: EduText().mediumRegular,),
                    FlatButton(
                      onPressed: () {
                        FbAnalytics().sendAnalytics('edit_phone');
                        MixpanelUtils().initPlatformState('Edit Phone');

                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => AddPhoneScreen(user: _user,)
                        ));
                      },
                      child: Text('Ubah No. HP', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                      ),),
                    ),
                  ],
                );
              } else {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Belum diatur', style: EduText().mediumRegular.copyWith(
                      fontStyle: FontStyle.italic
                    ),),
                    FlatButton(
                      onPressed: () {
                        FbAnalytics().sendAnalytics('add_phone');
                        MixpanelUtils().initPlatformState('Add Phone');

                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) => AddPhoneScreen(user: _user,)
                        ));
                      },
                      child: Text('+ Tambahkan No. HP', style: EduText().smallSemiBold.copyWith(
                          color: EduColors.primary
                      ),),
                    ),
                  ],
                );
              }
            },
          ),
        ],
      ),
    );
  }
}
