import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/about/about_screen.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/view/main_screen/main_screen_bloc/bloc.dart';
import 'package:edubox_tryout/view/profile/edit_profile_screen.dart';
import 'package:edubox_tryout/view/profile/profile_bloc/bloc.dart';
import 'package:edubox_tryout/view/select_server/select_server_screen.dart';
import 'package:edubox_tryout/widgets/button.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  User _user;
  ProfileBloc _profileBloc;
  bool _isLoading = true;
  bool _userNotFound = false;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Profile Screen');
    MixpanelUtils().initPlatformState('Profile Screen');

    _user = new User();
    _profileBloc = new ProfileBloc();
    _profileBloc.add(LoadProfile());
    super.initState();
  }

  Future _moveToLoginScreen() async {
    final result = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => LoginScreen()
    ));

    if (result) {
      await Future.delayed(Duration(milliseconds: 500));
      _profileBloc.add(LoadProfile());
    }
  }

  Future _moveToEditScreen() async {
    final result = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => EditProfileScreen(user: _user,)
    ));

    if (result) {
      _profileBloc.add(LoadProfile());
    }
  }

  void _logOutDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.only(
                bottom: 16.0),
            child: Text('Apakah kamu yakin ingin keluar ?', style: EduText().mediumRegular),
          ),
          actionAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        width: 1.5,
                        color: EduColors.primary
                    )
                ),
                child: Center(
                    child: Text('Batal', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                    ),)
                ),
              ),
            ),
            SizedBox(width: 10.0,),
            InkWell(
              onTap: () {
                FbAnalytics().sendAnalytics('logged_out');
                MixpanelUtils().initPlatformState('Logged Out');

                Navigator.pop(context);
                _profileBloc.add(LogOut());
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    color: EduColors.primary,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                child: Center(
                    child: Text('Yakin', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),)
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener(
        bloc: _profileBloc,
        listener: (context, state) {
          if (state is Loading) {
            setState(() => _isLoading = true);
          }
          if (state is UserNotFound) {
            setState(() {
              _user = null;
              _isLoading = false;
              _userNotFound = true;
            });
          }
          if (state is LoadProfileFailed) {
            setState(() {
              _user = null;
              _isLoading = false;
              _userNotFound = true;
            });
            MyToast().toast('Terjadi kesalahan');
          }
          if (state is ProfileLoaded) {
            setState(() {
              _user = state.user;
              _userNotFound = false;
              _isLoading = false;
            });
          }
          if (state is LoggedOut) {
            BlocProvider.of<MainScreenBloc>(context).add(LoadHomeScreen());
          }
        },
        child: _body(),
      )
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 200.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [EduColors.primary, EduColors.primaryDark],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                stops: [0.2, 1.0],
                tileMode: TileMode.clamp
              ),
            ),
          ),
          SafeArea(
            child: Padding(
              padding: EdgeInsets.only(
                  top: 60.0, left: 16.0, right: 16.0, bottom: 16.0
              ),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height / 1.35
                    ),
                    margin: EdgeInsets.only(top: 40.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[300],
                          blurRadius: 5.0,
                          offset: Offset(0.0, 5.0)
                        ),
                      ],
                    ),
                    child: Builder(
                      builder: (context) {
                        if (_isLoading) return _loadingView();
                        else if (_userNotFound) return _userNotFoundView();
                        else return _profileView();
                      },
                    )
                  ),
                  _userInitial()
                ],
              )
            ),
          ),
          Container(
            height: 80.0,
            child: AppBar(
              backgroundColor: Colors.transparent,
              title: Row(
                children: <Widget>[
                  Text('Profil Saya', style: EduText().largeBold.copyWith(
                      color: Colors.white
                  ),),
                ],
              ),
              elevation: 0.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget _profileView() {
    return Padding(
      padding: EdgeInsets.only(
          top: 50.0, left: 10.0, right: 10.0, bottom: 10.0
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _profileItem('Username', _user.data.username),
          _profileItem('Nama', _user.data.displayName),
          _profileItem('Email', _user.data.email),
          _profileItem('No. Handpone',  _user.data.phoneNumber),
          _profileItem('Kelas', _user.data.rombelIds.last.name),
          _profileItem('Sekolah', _user.data.studentAtInstitutions[0].name),
          _button()
        ],
      ),
    );
  }

  Widget _button() {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Column(
        children: <Widget>[
          MyButton(
            onTap: () {
              FbAnalytics().sendAnalytics('edit_profile');
              MixpanelUtils().initPlatformState('Edit Profile');
              _moveToEditScreen();
            },
            isDisabled: false,
            child: Text('Edit Profil', style: EduText().mediumSemiBold.copyWith(
                color: Colors.white
            ),),
          ),
          SizedBox(height: 15.0,),
          FlatButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => AboutScreen()
              ));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Tentang', style: EduText().smallSemiBold.copyWith(
                    color: EduColors.primary
                ),),
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              _logOutDialog();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Keluar', style: EduText().smallSemiBold.copyWith(
                    color: EduColors.red
                ),),
                SizedBox(width: 5.0,),
                Icon(Icons.exit_to_app, color: EduColors.red, size: 20.0,)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _profileItem(String title, subtitle) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: EduText().smallSemiBold,),
          SizedBox(height: 10.0,),
          Builder(
            builder: (context) {
              if (subtitle != null) {
                return Text(subtitle, style: EduText().mediumRegular,);
              } else {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Belum diatur', style: EduText().mediumRegular.copyWith(
                      fontStyle: FontStyle.italic
                    ),),
                    Icon(Icons.error_outline, color: EduColors.red, size: 20.0,)
                  ],
                );
              }
            },
          ),
          Divider()
        ],
      ),
    );
  }

  Widget _userNotFoundView() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('Kamu belum masuk ke ${Strings.appName}', style: EduText().mediumSemiBold,),
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
            child: Text('Silakan masuk untuk mengakses\nhalaman profil',
              style: EduText().smallRegular,
              textAlign: TextAlign.center,
            ),
          ),
          FlatButton(
            onPressed: () {
              _moveToLoginScreen();
            },
            color: EduColors.primary,
            child: Text('Masuk', style: EduText().smallSemiBold.copyWith(
                color: Colors.white
            ),),
          )
        ],
      ),
    );
  }

  Widget _userInitial() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
          width: 80.0,
          height: 80.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[400],
                  blurRadius: 4.0,
                  offset: Offset(0.0, 4.0)
              ),
            ],
          ),
          child: Builder(
            builder: (context) {
              if (_isLoading) return Container();
              else if (_user != null && _user.data.displayName.split(' ').length == 1)
                return Center(child: Text(_user.data.displayName.split(' ')[0][0].toUpperCase(), style: EduText().display1,));
              else if (_user != null && _user.data.displayName.split(' ').length > 1)
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(_user.data.displayName.split(' ')[0][0].toUpperCase(), style: EduText().superLargeBold,),
                    Text(_user.data.displayName.split(' ')[1][0].toUpperCase(), style: EduText().superLargeBold,)
                  ],
                );
              else return Center(child: Text('?', style: EduText().display1,));
            },
          )
      ),
    );
  }

  Widget _loadingView() {
    return Container(
      width: double.infinity,
      child: Center(
        child: SpinKitRing(
          color: EduColors.primary,
          size: 50.0,
          lineWidth: 3.0,
        )
      ),
    );
  }
}
