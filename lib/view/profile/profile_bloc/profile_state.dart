import 'package:edubox_tryout/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();
}

class InitialProfileState extends ProfileState {
  @override
  List<Object> get props => [];
}

class Loading extends ProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class UserNotFound extends ProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoadProfileFailed extends ProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class ProfileLoaded extends ProfileState {
  final User user;

  ProfileLoaded(this.user);
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class ProfileUpdated extends ProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class UpdateProfileFailed extends ProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoggedOut extends ProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}