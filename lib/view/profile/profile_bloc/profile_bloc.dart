import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import './bloc.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  @override
  ProfileState get initialState => InitialProfileState();

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if (event is LoadProfile) {
      yield* _loadProfileToState();
    } else if (event is LogOut) {
      yield* _logOutToState();
    } else if (event is UpdateProfile) {
      yield* _updateProfileToState(event);
    }
  }

  Stream<ProfileState> _updateProfileToState(UpdateProfile event) async* {
    yield Loading();

    try {
      final result = await ApiClient().updateUser(
          event.user.data.sId, event.displayName, event.email, event.phone);

      event.user.data.displayName = event.displayName;
      event.user.data.email = event.email;
      event.user.data.phoneNumber = event.phone;

      if (result) {
        if (event.password != null && event.password != "") {
          print("${event.user.data.sId} : ${event.password}");
          final result = await ApiClient().updatePassword(event.user.data.sId, event.password);

          if (result) {
            event.user.data.password = event.password;

            await DbDao().updateUser(event.user);

            yield ProfileUpdated();
          } else UpdateProfileFailed();
        } else {
          await DbDao().updateUser(event.user);

          yield ProfileUpdated();
        }
      } else yield UpdateProfileFailed();

    } catch(e) {
      print(e);
      yield UpdateProfileFailed();
    }
  }

  Stream<ProfileState> _logOutToState() async* {
    await ApiClient().logOut();
    await DbDao().deleteUser();

    yield LoggedOut();
  }

  Stream<ProfileState> _loadProfileToState() async* {
    try {
      final user = await DbDao().getUser();

      if (user != null) yield ProfileLoaded(user);
      else yield UserNotFound();

    } catch(e) {
      print(e);
      yield LoadProfileFailed();
    }
  }
}
