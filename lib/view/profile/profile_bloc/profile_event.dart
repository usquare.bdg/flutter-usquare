import 'package:edubox_tryout/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();
}

class LoadProfile extends ProfileEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LogOut extends ProfileEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class UpdateProfile extends ProfileEvent {
  final String displayName, email, phone, password;
  final User user;

  UpdateProfile(this.displayName, this.email, this.user, this.phone, this.password);

  @override
  // TODO: implement props
  List<Object> get props => null;
}
