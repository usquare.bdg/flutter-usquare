import 'package:edubox_tryout/main.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/profile/profile_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerificationScreen extends StatefulWidget {
  final User user;
  final String phoneNumber;
  final String verificationId;

  const VerificationScreen({Key key, this.phoneNumber, this.verificationId, this.user}) : super(key: key);
  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> with TickerProviderStateMixin {
//  final FirebaseAuth _auth = FirebaseAuth.instance;
  AnimationController _timeController;
  String get _timerString {
    Duration duration = _timeController.duration * _timeController.value;
    return '${(duration.inMinutes % 60).toString().padLeft(2, '0')} : ${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }
  bool _isSendAgain = false;
  bool _verified = false;
  String _smsCode;
  String _verificationId;
  ProfileBloc _profileBloc;

  void _loadingDialog(context) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return StatefulBuilder(
          builder: (context, dialogState) {
            return BlocListener(
              bloc: _profileBloc,
              listener: (context, state) {
                if (state is Loading) {
                  dialogState(() => _verified = true);
                }
                if (state is UpdateProfileFailed) {
                  MyToast().toast('Terjadi kesalahan saat menyimpan data');
                  Navigator.of(dialogContext, rootNavigator: true).pop();
                }
                if (state is ProfileUpdated) {
                  MyToast().toast('Berhasil menambahkan ${widget.phoneNumber}');
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                    builder: (context) => MyApp(currentPage: 4,)
                  ), (Route<dynamic> route) => false);
                }
              },
              child: CustomDialog(
                content: Container(
                  height: 50.0,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 10.0,),
                      CircularProgressIndicator(),
                      SizedBox(width: 30.0,),
                      Expanded(child: Text(_verified ? 'Menyimpan data..'  : 'Memverifikasi..', style: EduText().mediumRegular,)),
                    ],
                  ),
                )
              ),
            );
          }
        );
      },
      barrierDismissible: false
    );
  }

  @override
  void initState() {
    _verificationId = widget.verificationId;
    print(_verificationId);

    _profileBloc = new ProfileBloc();
    _timeController = AnimationController(
        duration: Duration(minutes: 2
        ),
        vsync: this
    );

    _timeController.reverse(from: 120);
    super.initState();
  }

  @override
  void dispose() {
    _timeController.stop();
    _timeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.black),
        automaticallyImplyLeading: true,
        title: Text('Verifikasi', style: EduText().mediumBold),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(
            horizontal: 16.0, vertical: 10.0
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Kami telah mengirimkan kode verifikasi ke nomor ${widget.phoneNumber}',
                style: EduText().mediumRegular
              ),
              Container(
                width: 260,
                margin: const EdgeInsets.only(top: 25),
                child: PinCodeTextField(
                  length: 6,
                  activeColor: Colors.black,
                  textStyle: EduText().largeBold,
                  inactiveColor: EduColors.primary,
                  obsecureText: false,
                  autoFocus: true,
                  animationType: AnimationType.fade,
                  shape: PinCodeFieldShape.underline,
                  animationDuration: Duration(milliseconds: 300),
                  borderRadius: BorderRadius.circular(5),
                  fieldHeight: 40,
                  fieldWidth: 35,
                  textInputType: TextInputType.phone,
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      _smsCode = value;
                    });
                    if(value.length == 6){
                      _signInWithPhoneNumber(context);
                    }
                  },
                ),
              ),
              AnimatedBuilder(
                animation: _timeController,
                builder: (context, widgets){
                  if(_timeController.value == 0.0){
                    return Container(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Center(
                        child: Text('Batas waktu berakhir!', style: EduText().smallRegular.copyWith(
                          color: EduColors.red
                        ),),
                      ),
                    );
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: Text(
                        '$_timerString',
                        style: EduText().mediumSemiBold,
                      ),
                    );
                  }
                }
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Tidak menerima kode ? ',
                      style: EduText().mediumRegular,
                    ),
                    InkWell(
                      onTap: (){
                        _verifyNumber();
                        setState(() {
                          _isSendAgain = true;
                          _timeController = AnimationController(
                              duration: Duration(minutes: 2
                              ),
                              vsync: this
                          );

                          _timeController.reverse(from: 120);
                        });
                      },
                      child: Text(
                        'Kirim ulang',
                        style: EduText().mediumSemiBold.copyWith(color: EduColors.primary),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _verifyNumber() async {
    /*setState(() {

    });

    final PhoneVerificationCompleted verificationCompleted = (AuthCredential auth){
      _auth.signInWithCredential(auth);
      print('login phone credential $auth');
    };

    final PhoneVerificationFailed verificationFailed = (AuthException authException) {
      print('phone verification failed ${authException.message}');
    };

    final PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) async {
      _verificationId = verificationId;
      print(_verificationId);
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout = (String verificationId) {
      _verificationId = verificationId;
    };

    await _auth.verifyPhoneNumber(
      phoneNumber: widget.phoneNumber,
      timeout: const Duration(minutes: 2),
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout
    );*/
  }

  void _signInWithPhoneNumber(context) async {
    /*_loadingDialog(context);

    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: _isSendAgain ? _verificationId : widget.verificationId,
        smsCode: _smsCode
      );

      final FirebaseUser user = (await _auth.signInWithCredential(credential));
      final FirebaseUser currentUser = await _auth.currentUser();
      assert(user.uid == currentUser.uid);
      setState(() {
        if(user != null){
          widget.user.data.phoneNumber = widget.phoneNumber;

          _profileBloc.add(UpdateProfile(
            widget.user.data.displayName,
            widget.user.data.email,
            widget.user,
            widget.user.data.phoneNumber,
            widget.user.data.password
          ));
        } else {
          Navigator.of(context, rootNavigator: true).pop();
        }
      });
    } catch(e){
      print(e);
      Navigator.of(context, rootNavigator: true).pop();
      MyToast().toast('Kode salah');
    }*/
  }
}
