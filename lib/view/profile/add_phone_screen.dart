import 'package:country_code_picker/country_code_picker.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/profile/verification_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:edubox_tryout/widgets/textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../main.dart';
import 'profile_bloc/bloc.dart';
import 'profile_bloc/profile_bloc.dart';

class AddPhoneScreen extends StatefulWidget {
  final User user;

  const AddPhoneScreen({Key key, this.user}) : super(key: key);
  @override
  _AddPhoneScreenState createState() => _AddPhoneScreenState();
}

class _AddPhoneScreenState extends State<AddPhoneScreen> {
  String _phoneNumber;
  String _verificationId;
  String _countryCode = '+62';
//  final FirebaseAuth _auth = FirebaseAuth.instance;
  ProfileBloc _profileBloc;

  @override
  void initState() {
    _profileBloc = new ProfileBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.black),
        automaticallyImplyLeading: true,
        title: Text('Tambah Nomor Handphone', style: EduText().mediumBold),
      ),
      bottomSheet: InkWell(
        onTap: () {
//          if (_phoneNumber.length > 5) _verifyNumber();
          _loadingDialog(context);
          _profileBloc.add(UpdateProfile(
              widget.user.data.displayName,
              widget.user.data.email,
              widget.user,
              _phoneNumber,
              ""
          ));
        },
        child: Container(
          height: 50.0,
          color: EduColors.primary,
          child: Center(
            child: Text('Selanjutnya', style: EduText().mediumSemiBold.copyWith(
              color: Colors.white
            ),),
          ),
        ),
      ),
      body: _body(),
    );
  }

  void _loadingDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return StatefulBuilder(
              builder: (context, dialogState) {
                return BlocListener(
                  bloc: _profileBloc,
                  listener: (context, state) {
                    if (state is Loading) {
//                      dialogState(() => _verified = true);
                    }
                    if (state is UpdateProfileFailed) {
                      MyToast().toast('Terjadi kesalahan saat menyimpan data');
                      Navigator.of(dialogContext, rootNavigator: true).pop();
                    }
                    if (state is ProfileUpdated) {
                      MyToast().toast('Berhasil menambahkan $_phoneNumber');
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                          builder: (context) => MyApp(currentPage: 4,)
                      ), (Route<dynamic> route) => false);
                    }
                  },
                  child: CustomDialog(
                      content: Container(
                        height: 50.0,
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: 10.0,),
                            CircularProgressIndicator(),
                            SizedBox(width: 30.0,),
                            Expanded(child: Text('Menyimpan data..', style: EduText().mediumRegular,)),
                          ],
                        ),
                      )
                  ),
                );
              }
          );
        },
        barrierDismissible: false
    );
  }

  Widget _body() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Masukkan nomor handphone', style: EduText().mediumSemiBold,),
          SizedBox(height: 5.0,),
          Text('Pastikan nomor yang dimasukkan adalah nomor yang valid dan aktif!', style: EduText().smallRegular,),
          SizedBox(height: 5.0,),
          Row(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 15.0),
                decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(3.0)
                ),
                child: CountryCodePicker(
                  onChanged: (data){
                    _countryCode = data.toString();
                  },
                  initialSelection: 'ID',
                  favorite: ['+62','ID'],
                  showCountryOnly: false,
                  showOnlyCountryWhenClosed: false,
                  alignLeft: false,
                ),
              ),
              SizedBox(width: 10.0,),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: MyTextField(
                    keyboardType: TextInputType.number,
                    actionType: TextInputAction.done,
                    onChanged: (val) {
                      setState(() {
                        _phoneNumber = val;
                      });
                    },
                  ),
                ),
              )
            ],
          ),
        ]
      )
    );
  }

  /*void _verifyNumber() async {
    setState(() {});

    final PhoneVerificationCompleted verificationCompleted = (AuthCredential auth){
      _auth.signInWithCredential(auth);
      print('login phone credential $auth');
    };

    final PhoneVerificationFailed verificationFailed = (AuthException authException) {
      print('login phone fail ${authException.message}');
      MyToast().toast('Terjadi kesalahan');
    };

    final PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) async {
      _verificationId = verificationId;

      Future.delayed(Duration(milliseconds: 500), () {
        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => VerificationScreen(
              user: widget.user,
              phoneNumber: _phoneNumber,
              verificationId: _verificationId,
            )
        ));
      });
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout = (String verificationId) {
      _verificationId = verificationId;
    };

    if(_phoneNumber[0] == "0") {
      _phoneNumber = '$_countryCode${_phoneNumber.substring(1, _phoneNumber.length)}';
    } else if (!_phoneNumber.contains(_countryCode)) {
      _phoneNumber = '$_countryCode$_phoneNumber';
    }
    print('Phone number $_phoneNumber');

    await _auth.verifyPhoneNumber(
      phoneNumber: _phoneNumber,
      timeout: const Duration(minutes: 2),
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout
    );
  }*/
}
