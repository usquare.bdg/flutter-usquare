import 'dart:async';

import 'package:edubox_tryout/models/sync.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_admob.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/view/main_screen/main_screen_bloc/bloc.dart';
import 'package:edubox_tryout/view/main_screen/main_screen_bloc/main_screen_bloc.dart';
import 'package:edubox_tryout/view/quiz/do_quiz/do_quiz_screen.dart';
import 'package:edubox_tryout/view/sync/sync_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class SyncScreen extends StatefulWidget {
  final String type;
  final BuildContext context;

  const SyncScreen({Key key, this.type, this.context}) : super(key: key);
  @override
  _SyncScreenState createState() => _SyncScreenState();
}

class _SyncScreenState extends State<SyncScreen> with TickerProviderStateMixin {
  //, WidgetsBindingObserver{
  final _formKey = GlobalKey<FormState>();
  FocusNode _openingCodeFocus = FocusNode();
  User _user;
  SyncBloc _syncBloc;
  List<Sync> _listSyncItem;
  bool _isLoading = true;
  bool _isDownloading = false;
  bool _showLoading = false;
  bool _showSuccessIndicator = false;
  bool _showFailedIndicator = false;
  bool _isStartingQuiz = false;
  int position = 0;
  int lessonIndex = 0;
  Timer _timer;
//  BannerAd bannerAd;
//  InterstitialAd interstitialAd;
  bool isPremium = false;

  @override
  void initState() {
    _listSyncItem = new List<Sync>();
    _user = new User();
    _syncBloc = new SyncBloc();
    _syncBloc.add(CheckIsPremium());
    _syncBloc.add(LoadData(widget.type));

    FbAnalytics().setCurrentScreen('Download Quiz Screen');
    MixpanelUtils().initPlatformState('Download Quiz Screen');
//    WidgetsBinding.instance.addObserver(this);
//    bannerAd = Ads().initBannerAd();
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();

    if (_syncBloc.state is Downloading) {
      (_syncBloc.state as Downloading).isCanceled = true;
    }
//    bannerAd..dispose();
//    interstitialAd..dispose();
//    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

//  @override
//  void didChangeAppLifecycleState(AppLifecycleState state) {
//    print("status state $state");
//    if (state == AppLifecycleState.paused) {
//      bannerAd..dispose();
//    }
//    if (state == AppLifecycleState.resumed) {
//      bannerAd..dispose();
//      bannerAd = Ads().initBannerAd();
//    }
//    if (state == AppLifecycleState.inactive) {
//      bannerAd..dispose();
//    }
//  }

  void _rebuildWidget() {
    _timer = Timer.periodic(Duration(milliseconds: 500), (Timer t) {
      setState(() {});
    });
  }

  Future _moveToLoginScreen() async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));

    if (result) {
      _syncBloc.add(LoadData(widget.type));
    }
  }

  void _warningDialog() {
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return CustomDialog(
            title: Strings.appName,
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Tidak bisa memulai ujian!',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Device yang digunakan terdeteksi menggunakan Floating App.',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(dialogContext).pop(),
                child: Text(
                  'Tutup',
                  style: EduText().smallSemiBold.copyWith(color: Colors.white),
                ),
                color: EduColors.primary,
              )
            ],
          );
        });
  }

  void _openingCodeDialog(item) {
    bool _isIncorrect = false;
    bool _onProcess = false;
    bool _hasRedeemed = false;
    TextEditingController _quizPassController = new TextEditingController();

    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          FocusScope.of(dialogContext).requestFocus(_openingCodeFocus);

          return StatefulBuilder(
            builder: (context, setDialogState) {
              return BlocListener(
                bloc: _syncBloc,
                listener: (context, state) {
                  if (state is PasscodeIncorrect) {
                    setDialogState(() {
                      _isIncorrect = true;
                      _onProcess = false;
                      _hasRedeemed = false;
                    });
                  }
                  if (state is QuizPassHasRedeemed) {
                    setDialogState(() {
                      _hasRedeemed = true;
                      _isIncorrect = false;
                      _onProcess = false;
                    });
                  }
                  if (state is QuizPassError) {
                    setDialogState(() {
                      _isIncorrect = false;
                      _onProcess = false;
                      _hasRedeemed = false;
                    });
                    Navigator.pop(dialogContext);
                    MyToast().toast('Terjadi kesalahan');
                  }
                  if (state is QuizPassUpdated) {
                    setDialogState(() {
                      _isIncorrect = false;
                      _onProcess = false;
                      _hasRedeemed = false;
                    });

                    Navigator.pop(dialogContext);
                    _syncBloc.add(StartQuiz(item.items.sId));
                  }
                },
                child: CustomDialog(
                  title: Strings.appName,
                  content: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              'Ujian dikunci! Buka kunci untuk memulai ujian.'),
                          SizedBox(height: 5),
                          Form(
                            key: _formKey,
                            child: MyTextField(
                              focusNode: _openingCodeFocus,
                              controller: _quizPassController,
                              obsecureText: false,
                              hintText: 'Masukkan kode pembuka',
                              errorText: _isIncorrect
                                  ? 'Kode pembuka salah!'
                                  : _hasRedeemed
                                      ? 'Kode sudah pernah digunakan!'
                                      : null,
                              validator: (val) {
                                if (val != item.items.openingCode) {
                                  return 'Kode pembuka salah!';
                                }
                                return null;
                              },
                            ),
                          ),
                        ],
                      )),
                  actionAlignment: MainAxisAlignment.end,
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          if (!isPremium) {
//                            bannerAd..dispose();
                          }
                          if (item.items.quizType == '7') {
                            setDialogState(() {
                              _onProcess = true;
                              _isIncorrect = false;
                              _hasRedeemed = false;
                            });
                            _syncBloc.add(UpdateQuizPass(
                                passcode: _quizPassController.text,
                                quizId: item.items.sId));
                          } else {
                            if (_formKey.currentState.validate()) {
                              Navigator.pop(dialogContext);
                              _syncBloc.add(StartQuiz(item.items.sId));
                            }
                          }
                        },
                        color: EduColors.primary,
                        child: !_onProcess
                            ? Row(
                                children: <Widget>[
                                  Text(
                                    'Buka',
                                    style: EduText()
                                        .smallSemiBold
                                        .copyWith(color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: 2.0,
                                  ),
                                  Icon(
                                    Icons.vpn_key,
                                    color: Colors.white,
                                    size: 18.0,
                                  ),
                                ],
                              )
                            : Container(
                                width: 25.0,
                                height: 25.0,
                                padding: EdgeInsets.all(5.0),
                                child: CircularProgressIndicator(
                                    backgroundColor: Colors.white),
                              ))
                  ],
                ),
              );
            },
          );
        });
  }

  void _alertDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: Strings.appName,
            content: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                'Untuk mengunduh Ujian kamu harus masuk ke akun ${Strings.appName} terlebih dahulu!',
                style: EduText().mediumRegular,
                textAlign: TextAlign.center,
              ),
            ),
            actionAlignment: MainAxisAlignment.center,
            actions: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Container(
                  width: 100.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(width: 1.5, color: EduColors.primary)),
                  child: Center(
                      child: Text(
                    'Kembali',
                    style: EduText()
                        .smallSemiBold
                        .copyWith(color: EduColors.primary),
                  )),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  _moveToLoginScreen();
                },
                child: Container(
                  width: 100.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: EduColors.primary,
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Center(
                      child: Text(
                    'Masuk',
                    style:
                        EduText().smallSemiBold.copyWith(color: Colors.white),
                  )),
                ),
              ),
            ],
          );
        },
        barrierDismissible: false);
  }

  void _profileIncompleteDialog() {
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return CustomDialog(
            title: Strings.appName,
            content: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'Lengkapi profil untuk memulai ujian!',
                textAlign: TextAlign.center,
              ),
            ),
            actionAlignment: MainAxisAlignment.center,
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                  Navigator.pop(context);
                  BlocProvider.of<MainScreenBloc>(widget.context)
                      .add(LoadProfileScreen());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Lengkapi Profil',
                      style:
                          EduText().smallSemiBold.copyWith(color: Colors.white),
                    ),
                  ],
                ),
                color: EduColors.primary,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (!isPremium) {
//          bannerAd..dispose();
        }

        if (_syncBloc.state is Downloading) {
          (_syncBloc.state as Downloading).isCanceled = true;
        }
        Navigator.pop(context, true);
        return null;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: GradientAppBar(
          automaticallyImplyLeading: true,
          title: Text('Unduh ${widget.type}',
              style: EduText().mediumBold.copyWith(color: Colors.white)),
          elevation: 0.0,
          gradient: LinearGradient(
              colors: [EduColors.primary, EduColors.purple],
              stops: [0.0, 1.0],
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              tileMode: TileMode.clamp),
          iconTheme: IconThemeData(color: Colors.white),
        ),
        body: Stack(
          children: <Widget>[
            BlocListener(
                bloc: _syncBloc,
                listener: (context, state) {
                  if (state is IsPremium) {
                    setState(() {
                      print("isPremium ${state.isPremium}");
                      if (state.isPremium != "approved") {
                        isPremium = false;
//                        bannerAd = Ads().initBannerAd();
                      } else {
                        isPremium = true;
                      }
                    });
                  }
                  if (state is Loading) {
                    setState(() {
                      _isLoading = true;
                    });
                  }
                  if (state is FailedToLoad) {
                    setState(() {
                      _isLoading = false;
                    });
                    Fluttertoast.showToast(
                        msg: "Terjadi kesalahan",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 1,
                        backgroundColor: Colors.grey,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  }
                  if (state is DataLoaded) {
                    setState(() {
                      _listSyncItem = state.listSyncItem;
                      _user = state.user;
                      _isLoading = false;
                    });
                  }
                  if (state is Downloading) {
                    setState(() {
                      if (!isPremium) {
//                        interstitialAd = Ads().initInterstitialAd();
                      }
                      _isDownloading = true;
                      _showLoading = true;
                      _showFailedIndicator = false;
                      _showSuccessIndicator = false;
                    });

                    _rebuildWidget();
                  }
                  if (state is QuizDownloaded) {
                    _timer.cancel();

                    setState(() {
                      position = state.index;
                      _listSyncItem[lessonIndex]
                          .quizids[position]
                          .isDownloaded = true;
                      _showLoading = false;
                      _showFailedIndicator = false;
                      _showSuccessIndicator = true;
                    });

                    Future.delayed(const Duration(seconds: 1), () {
                      setState(() {
                        _isDownloading = false;
                      });
                    });
                  }
                  if (state is DownloadFailed) {
                    _timer.cancel();

                    setState(() {
                      _showLoading = false;
                      _showFailedIndicator = true;
                      _showSuccessIndicator = false;
                    });

                    Future.delayed(const Duration(seconds: 1), () {
                      setState(() {
                        _isDownloading = false;
                      });
                    });
                  }
                  if (state is StartingQuiz) {
                    setState(() {
                      _isStartingQuiz = true;
                      _showLoading = true;
                      _showFailedIndicator = false;
                      _showSuccessIndicator = false;
                    });
                  }
                  if (state is QuizStarted) {
                    setState(() {
                      _isStartingQuiz = false;
                    });
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DoQuizScreen(
                                listAnswer: state.list,
                                quiz: state.quizData,
                                resultBody: state.resultBody)));
                  }
                  if (state is FloatingAppsDetected) {
                    setState(() {
                      _isStartingQuiz = false;
                    });
                    _warningDialog();
                  }
                  if (state is UserNotFound) {
                    _alertDialog();
                  }
                },
                child: BlocBuilder(
                  bloc: _syncBloc,
                  builder: (context, state) {
                    return SafeArea(
                        child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      padding:
                          EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                      child: Column(
                        children: <Widget>[_header(), _content()],
                      ),
                    ));
                  },
                )),
            Visibility(
              visible: _isDownloading || _isStartingQuiz,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent),
            ),
            Visibility(
              visible: _isDownloading || _isStartingQuiz,
              child: Align(
                alignment: Alignment.center,
                child: Container(
                    width: 160.0,
                    height: 120.0,
                    decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.circular(5.0)),
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _loading(_isStartingQuiz),
                        _successIndicator(),
                        _failedIndicator()
                      ],
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _header() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60.0,
      child: Text(
          'Berikut adalah daftar ${widget.type} aktif yang dapat kamu unduh',
          style: EduText().mediumSemiBold),
    );
  }

  Widget _content() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_listActiveQuiz(), _tip()],
      ),
    );
  }

  Widget _listActiveQuiz() {
    return Expanded(
      child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey[300]),
          ),
          child: _isLoading
              ? Center(
                  child: SpinKitRing(
                  color: EduColors.primary,
                  size: 50.0,
                  lineWidth: 3.0,
                  controller: AnimationController(
                      vsync: this,
                      duration: const Duration(milliseconds: 1200)),
                ))
              : _listSyncItem.length > 0
                  ? ListView.builder(
                      padding: EdgeInsets.all(16.0),
                      itemCount: _listSyncItem.length,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                  'Pelajaran ${_listSyncItem[index].lessonName}',
                                  style: EduText().mediumSemiBold),
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _listSyncItem[index].quizids.length,
                              itemBuilder: (context, i) {
                                var item = _listSyncItem[index].quizids[i];
                                return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: InkWell(
                                      onTap: () {
                                        if (!item.isDownloaded) {
                                          FbAnalytics()
                                              .sendAnalytics('download_quiz');
                                          MixpanelUtils().initPlatformState(
                                              'Download Quiz');

                                          lessonIndex = index;
                                          _syncBloc.add(DownloadSingle(
                                              item.items,
                                              i,
                                              widget.type,
                                              _listSyncItem[index].lessonName));
                                        } else {
                                          if (!item.isFinished) {
                                            if (item.items.openingCode !=
                                                    null &&
                                                item.items.openingCode != '') {
                                              if (item.items.isOpened) {
                                                _syncBloc.add(
                                                    StartQuiz(item.items.sId));
                                              } else {
                                                _openingCodeDialog(item);
                                              }
                                            } else {
                                              _syncBloc.add(
                                                  StartQuiz(item.items.sId));
                                            }
                                          }
                                        }
                                      },
                                      child: Container(
                                        constraints:
                                            BoxConstraints(minHeight: 50.0),
                                        child: Row(
                                          children: <Widget>[
                                            Image(
                                                width: 25.0,
                                                height: 25.0,
                                                image: AssetImage(
                                                    item.items.quizType == '10'
                                                        ? 'assets/book_rem.png'
                                                        : 'assets/quiz.png')),
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 8.0),
                                                child: Text(item.items.title,
                                                    style: EduText()
                                                        .mediumRegular),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Container(
                                                width: 30.0,
                                                height: 30.0,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: item.isDownloaded
                                                        ? EduColors.green
                                                        : EduColors.primary),
                                                child: !item.isDownloaded
                                                    ? Icon(
                                                        Icons.file_download,
                                                        color: Colors.white,
                                                      )
                                                    : item.isFinished
                                                        ? Icon(
                                                            Icons.check,
                                                            color: Colors.white,
                                                          )
                                                        : Icon(
                                                            Icons.play_arrow,
                                                            color: Colors.white,
                                                          ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ));
                              },
                            ),
                            SizedBox(height: 10.0),
                            index == _listSyncItem.length - 1
                                ? Container()
                                : Divider(),
                          ],
                        );
                      },
                    )
                  : Container(
                      child: Center(
                        child: Text('Tidak ada data',
                            style: EduText().mediumRegular),
                      ),
                    )),
    );
  }

  Widget _tip() {
    return Container(
        height: 50.0,
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Center(
          child: Text(
            'Tekan salah satu item di atas untuk mengunduh ${widget.type}',
            style: EduText().smallRegular.copyWith(color: Colors.black54),
          ),
        ));
  }

  Widget _loading(bool isStartingQuiz) {
    return Visibility(
      visible: _showLoading,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitDualRing(
            color: Colors.white,
            size: 30.0,
            lineWidth: 3.0,
          ),
          Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Builder(
                builder: (context) {
                  if (_syncBloc.state is Downloading) {
                    return Text(
                        '${(_syncBloc.state as Downloading).process.round()}%',
                        style: EduText()
                            .smallRegular
                            .copyWith(color: Colors.white));
                  } else {
                    return Text('Memulai ujian.. ',
                        style: EduText()
                            .smallRegular
                            .copyWith(color: Colors.white));
                  }
                },
              ))
        ],
      ),
    );
  }

  Widget _successIndicator() {
    return Visibility(
      visible: _showSuccessIndicator,
      child: Column(
        children: <Widget>[
          Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                  border: Border.all(width: 1.5, color: Colors.white),
                  shape: BoxShape.circle),
              child: Icon(
                Icons.check,
                color: Colors.white,
              )),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(
              'Berhasil mengunduh',
              style: EduText().smallRegular.copyWith(
                    color: Colors.white,
                  ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _failedIndicator() {
    return Visibility(
      visible: _showFailedIndicator,
      child: Column(
        children: <Widget>[
          Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                  border: Border.all(width: 1.5, color: Colors.white),
                  shape: BoxShape.circle),
              child: Icon(
                Icons.close,
                color: Colors.white,
              )),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(
              'Gagal mengunduh',
              style: EduText().smallRegular.copyWith(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
