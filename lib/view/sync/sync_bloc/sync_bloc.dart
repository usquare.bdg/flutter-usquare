import 'dart:async';
import 'dart:io' show Platform;

import 'package:bloc/bloc.dart';
import 'package:device_apps/device_apps.dart';
import 'package:edubox_tryout/models/question.dart';
import 'package:edubox_tryout/models/quiz.dart';
import 'package:edubox_tryout/models/quiz_list.dart' as prefix0;
import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/models/sync.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:edubox_tryout/utils/list_packages.dart';
import './bloc.dart';

class SyncBloc extends Bloc<SyncEvent, SyncState> {
  @override
  SyncState get initialState => InitialSyncState();

  @override
  Stream<SyncState> mapEventToState(
      SyncEvent event,
      ) async* {
    if (event is LoadData) {
      yield* _loadDataToState(event);
    } else if (event is DownloadSingle) {
      yield* _downloadQuizSingle(event, state);
    } else if (event is Skip) {
      yield* _skipToState();
    } else if (event is StartQuiz) {
      yield* _startQuizToState(event);
    } else if (event is UpdateQuizPass) {
      yield* _updateQuizPassToState(event);
    } else if (event is CancelDownload) {
    } else if (event is CheckIsPremium) {
      yield* _checkIsPremium();
    }
  }

  Stream<SyncState> _checkIsPremium() async* {
    try {
      final isPremium = await ApiClient().isPremium();
      yield IsPremium(isPremium);
    } catch (e) {
      print(e);
    }
  }

  Stream<SyncState> _updateQuizPassToState(event) async* {
    yield FooState();

    try {
      String quizPassId = '';
      final quizPass = await ApiClient().getQuizPass(event.quizId);
      final userId = await ApiClient().getUserId();

      if (quizPass != null) {
        bool isCorrect = false;
        bool hasRedeemed = false;

        for (var item in quizPass.data.items) {
          if (event.passcode == item.passcode) {
            isCorrect = true;
            quizPassId = item.sId;

            if (item.userId != null) {
              if (userId != item.userId.sId) {
                print('$userId <===> ${item.userId.sId}');
                hasRedeemed = true;
              }
            }
          }
        }

        if (isCorrect) {
          if (hasRedeemed) {
            yield QuizPassHasRedeemed();
          } else {
            final result = await ApiClient().updateQuizPass(quizPassId);

            if (result) {
              yield QuizPassUpdated();
            } else {
              yield QuizPassError();
            }
          }
        } else {
          yield PasscodeIncorrect();
        }
      } else {
        yield QuizPassError();
      }
    } catch (e) {
      print('error : $e');
      yield QuizPassError();
    }
  }

  Stream<SyncState> _startQuizToState(event) async* {
    yield StartingQuiz();
    try {
      List<StudentAnswersBody> listAnswers = new List<StudentAnswersBody>();
      final user = await DbDao().getUser();
      final resultBody = await DbDao().getResultBody(event.id);
      final List<QuestionIds> listQuestions = await DbDao().getAllQuestions(event.id, user.data.sId);

      bool isInstalled = false;
      if (Platform.isAndroid) {
        for (var packageName in ListPackages().packages) {
          final status = await DeviceApps.isAppInstalled(packageName);
          if (status) isInstalled = true;
        }
      }

      if (isInstalled) {
        yield FloatingAppsDetected();
      } else {
        final quiz = await DbDao().getQuiz(event.id);

        if (quiz.randomSeq) {
          listQuestions.shuffle();
        }

        quiz.status = 'ongoing';
        quiz.questionsIds = listQuestions;
        quiz.isOpened = true;

        if (resultBody != null) {
          if (quiz.randomSeq) {
            for (var question in listQuestions) {
              for (var answer in resultBody.studentAnswersBody) {
                if (answer.id == question.sId) {
                  listAnswers.add(answer);
                }
              }
            }
          } else {
            for (var answer in resultBody.studentAnswersBody) {
              listAnswers.add(answer);
            }
          }

          quiz.isBlocked = true;
          await DbDao().updateQuiz(quiz, quiz.sId);

          yield QuizStarted(
              list: listAnswers,
              quizData: quiz,
              resultBody: resultBody
          );
        } else {
          for (var question in quiz.questionsIds) {
            StudentAnswersBody body = new StudentAnswersBody();
            body.id = question.sId;
            body.answer = null;
            body.state = 'belum';
            body.isCorrect = false;
            body.type = question.type;

            listAnswers.add(body);
          }

          PeriodBody periodBody = new PeriodBody();
          periodBody.year = quiz.period.year;
          KdsBody kd = new KdsBody();
          kd.hello = 'world';
          ResultBody resultBody = new ResultBody();
          resultBody.institutionId = user.data.studentAtInstitutions[0].sId;
          resultBody.quizId = quiz.sId;
          resultBody.rombelId = user.data.rombelIds.last.sId;
          resultBody.studentId = user.data.sId;
          resultBody.studentAnswersBody = listAnswers;
          resultBody.periodBody = periodBody;
          resultBody.status = 'draft';
          resultBody.kds = [kd];
          resultBody.correct = null;
          resultBody.wrong = null;
          resultBody.unanswers = null;
          resultBody.score = null;
          resultBody.remaining = null;
          resultBody.blockedStatus = 'free';
          resultBody.isUploaded = false;
          resultBody.userId = user.data.sId;

          await DbDao().insertResultBody(resultBody);
          await DbDao().updateQuiz(quiz, quiz.sId);

          if (quiz.questionsIds.length > 0) {
            await DbDao().insertResultBody(resultBody);
            await DbDao().updateQuiz(quiz, quiz.sId);

            yield QuizStarted(
                list: listAnswers,
                quizData: quiz,
                resultBody: resultBody
            );
          } else {
            yield StartQuizFailed();
          }
        }
      }
    } catch (e) {
      print('error starting quiz : $e');
    }
  }

  Stream<SyncState> _downloadQuizSingle(DownloadSingle event, SyncState tate) async* {
    yield Downloading(false, 0.0);

    prefix0.Items item = event.items;
    item.type = event.type;
    double percentage = 100 / item.questionIds.length;
    double process = 0.0;

    try {
      int downloaded = 0;
      item.status = 'draft';
      item.isUploaded = false;
      if (item.showScoreOption == null) item.showScoreOption = false;
      if (item.randomSeq == null) item.randomSeq = false;
      item.isOpened = false;
      item.isBlocked = false;
      item.lessonName = event.lessonName;
      item.userId = await ApiClient().getUserId();

      for (var id in item.questionIds) {
        Question question = new Question();
        question = await ApiClient().getQuestion(id);
        print(id);

        if (question != null) {

          question.data.quizId = item.sId;
          question.data.userId = await ApiClient().getUserId();
          await DbDao().insertQuestion(question.data);

          downloaded++;
          print('input question : $downloaded');
          print(process);

          process = process + percentage;

          if (state is Downloading) {
            (state as Downloading).process = process;
            if ((state as Downloading).isCanceled) break;
          }
        }
      }

      print('$downloaded == ${item.questionIds.length}');

      if (downloaded == item.questionIds.length) {
        await DbDao().insertQuiz(item);
        yield QuizDownloaded(event.index);
      } else {
        await DbDao().deleteQuestions(item.sId);
        yield DownloadFailed();
      }
    } catch(e) {
      print('error download single $e');
      await DbDao().deleteQuestions(item.sId);
      yield DownloadFailed();
    }
  }

  Stream<SyncState> _loadDataToState(event) async* {
    yield Loading();
    try {
      final user = await DbDao().getUser();
      if (user != null) {
        final lessons = await ApiClient().getLessons(user.data.rombelIds.last.sId);
        final listQuizDb = await DbDao().getAllQuiz();

        List<Sync> listSync = new List<Sync>();

        if (lessons != null) {

          for(var lesson in lessons.data.items) {
            List<QuizIds> listQuizIds = new List<QuizIds>();

            QuizList quizList;
            if (event.type == 'Try Out') {
              quizList = await ApiClient().getTryOut(
                  department: user.data.rombelIds.last.department,
                  grade: user.data.rombelIds.last.grade,
                  lessonGroup: lesson.lessonGroup
              );
            } else {
              print('Insitution id : ${user.data.studentAtInstitutions[0].sId}');
              print('Lesson id : ${lesson.sId}');

              quizList = await ApiClient().getQuizList(
                  institutionId: user.data.studentAtInstitutions[0].sId,
                  lessonId: lesson.sId
              );
            }

            if (quizList != null) {
              if (quizList.data.items.length > 0) {
                print('quiz length : ${quizList.data.items.length}');

                for (var quiz in quizList.data.items) {
                  QuizIds quizIdsItem = new QuizIds();
                  quizIdsItem.isSelected = false;
                  quizIdsItem.isDownloaded = false;
                  quizIdsItem.isFinished = false;
                  quizIdsItem.items = quiz;

                  if (listQuizDb != null) {
                    for (var i in listQuizDb) {
                      if (quiz.sId == i.sId) {
                        quizIdsItem.isDownloaded = true;
                        quizIdsItem.items.isOpened = i.isOpened;
                        if (i.status == 'finished') {
                          quizIdsItem.isFinished = true;
                        }
                      }
                    }
                  }

                  if (quizIdsItem.items.questionIds != null) {
                    if (quizIdsItem.items.studentIds == null) listQuizIds.add(quizIdsItem);
                    else if (quizIdsItem.items.studentIds.length > 0) {

                      List<StudentIds> find = quizIdsItem.items.studentIds.
                      where((data) => data.sId == user.data.sId).toList();
                      for (var id in quizIdsItem.items.studentIds) {
                        print("compare ids ${user.data.sId} : ${id.sId}");
                      }

                      if (find.length > 0) listQuizIds.add(quizIdsItem);
                    } else if (quizIdsItem.items.studentIds.length == 0) listQuizIds.add(quizIdsItem);
                  }
                }

                Sync syncItem = new Sync();
                if (listQuizIds.length > 0) {
                  syncItem.lessonName = lesson.name;
                  syncItem.quizids = listQuizIds;

                  listSync.add(syncItem);
                }

              }
            }
          }

          yield DataLoaded(listSync, lessons.data.items.length, user);
        } else {
          yield FailedToLoad();
        }
      } else {
        yield UserNotFound();
      }

    } catch (e) {
      print('Error fetch data : $e');
      yield FailedToLoad();
    }
  }

  Stream<SyncState> _skipToState() async* {
    try {
      yield Skipped();
    } catch (e) {
      print('error to skip : $e');
    }
  }
}
