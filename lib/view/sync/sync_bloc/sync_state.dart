import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/result.dart';
import 'package:edubox_tryout/models/sync.dart';
import 'package:edubox_tryout/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class SyncState extends Equatable {
  const SyncState();
}

class InitialSyncState extends SyncState {
  @override
  List<Object> get props => [];
}

class Loading extends SyncState {
  @override  
  List<Object> get props => [];
}

class DataLoaded extends SyncState {
  final List<Sync> listSyncItem;
  final int lessonLength;
  final User user;

  DataLoaded(this.listSyncItem, this.lessonLength, this.user);
  @override  
  List<Object> get props => [];
}

class FailedToLoad extends SyncState {
  @override  
  List<Object> get props => [];
}

class Downloading extends SyncState {
  bool isCanceled;
  double process;

  Downloading(this.isCanceled, this.process);
  @override  
  List<Object> get props => [isCanceled, process];
}

class QuizDownloaded extends SyncState {
  final int index;

  QuizDownloaded(this.index);
  @override  
  List<Object> get props => [index];
}

class DownloadFailed extends SyncState {
  @override  
  List<Object> get props => [];
}

class Skipped extends SyncState {
  @override
  List<Object> get props => [];
}

class QuizStarted extends SyncState {
  final Items quizData;  
  final ResultBody resultBody;
  final List<StudentAnswersBody> list;

  QuizStarted({this.quizData, this.resultBody, this.list});
  
  @override  
  List<Object> get props => [];
}

class StartingQuiz extends SyncState {
  @override  
  List<Object> get props => [];
}

class StartQuizFailed extends SyncState {
  @override  
  List<Object> get props => [];
}

class FloatingAppsDetected extends SyncState {
  @override
  List<Object> get props => [];
}

class UserNotFound extends SyncState {
  @override
  List<Object> get props => [];
}

class FooState extends SyncState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class QuizPassUpdated extends SyncState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class PasscodeIncorrect extends SyncState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class QuizPassError extends SyncState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class QuizPassHasRedeemed extends SyncState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class DownloadCanceled extends SyncState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class IsPremium extends SyncState {
  final String isPremium;

  IsPremium(this.isPremium);

  @override
  // TODO: implement props
  List<Object> get props => [isPremium];
}