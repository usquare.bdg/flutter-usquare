import 'package:edubox_tryout/models/quiz_list.dart';
import 'package:edubox_tryout/models/sync.dart';
import 'package:equatable/equatable.dart';

abstract class SyncEvent extends Equatable {
  const SyncEvent();
}

class LoadData extends SyncEvent {
  final String type;

  LoadData(this.type);
  @override
  List<Object> get props => [];
}

class DownloadQuiz extends SyncEvent {
  final List<Sync> listSyncItem;

  DownloadQuiz(this.listSyncItem);
  @override  
  List<Object> get props => [];
}

class DownloadSingle extends SyncEvent {
  final Items items;
  final int index;
  final String type;
  final String lessonName;

  DownloadSingle(this.items, this.index, this.type, this.lessonName);

  @override  
  List<Object> get props => [items, index];

}

class StartQuiz extends SyncEvent {
  final String id;

  StartQuiz(this.id);
  @override
  List<Object> get props => null;
}

class Skip extends SyncEvent {
  @override  
  List<Object> get props => [];
}

class UpdateQuizPass extends SyncEvent {
  final String passcode;
  final String quizId;

  UpdateQuizPass({this.passcode, this.quizId});
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class CancelDownload extends SyncEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class CheckIsPremium extends SyncEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}