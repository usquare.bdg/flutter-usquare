import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import './bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  @override
  LoginState get initialState => InitialLoginState();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is Login) {
      yield* _mapLoginToState(event);
    }
  }

  Stream<LoginState> _mapLoginToState(event) async* {
    yield Loading();
    try {
      final bool login = await ApiClient().logIn(event.username, event.password);
      if (login) {
        await ApiClient().saveUsername(event.username);
        final user = await ApiClient().getProfile();

        if (user != null) {
          if (user.data.role[0] == 2) {
            await DbDao().insertUser(user);
            yield OnSuccess();
          } else {
            yield Alert();
          }
        } else {
          yield OnFailed(true);
        }
      } else {
        yield OnFailed(true);
      }
    } catch (e) {
      print(e);
      yield OnFailed(false);
    }
  }
}
