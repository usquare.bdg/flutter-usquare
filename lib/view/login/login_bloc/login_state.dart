import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class InitialLoginState extends LoginState {
  @override
  List<Object> get props => [];
}

class OnSuccess extends LoginState {
  @override  
  List<Object> get props => [];
}

class OnFailed extends LoginState {
  final bool isConnected;

  OnFailed(this.isConnected);
  @override
  List<Object> get props => [];
}

class Alert extends LoginState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class Loading extends LoginState {
  @override
  List<Object> get props => [];
}
