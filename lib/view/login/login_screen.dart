import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_admob.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/login/login_bloc/bloc.dart';
import 'package:edubox_tryout/view/register_student/register_student_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with TickerProviderStateMixin {
  LoginBloc _loginBloc;
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  FocusNode _usernameFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  String _username = '';  
  String _password = '';
  String _baseUrl = '';
  bool _isVisible = true;  
  bool _isDisabled = true; 
  bool _showError = false;
  bool _isLoading = false; 
//  BannerAd bannerAd;

  @override
  void initState() {
    _getUrl().then((url) {
      _baseUrl = url;
    });
    _loginBloc = new LoginBloc();
    FbAnalytics().setCurrentScreen('Login Screen');
    MixpanelUtils().initPlatformState('Login Screen');
//    bannerAd = Ads().initBannerAd();
    super.initState();
  }

  @override
  void dispose() {
//    bannerAd..dispose();
    super.dispose();
  }

  Future _getUrl() async {
    return await ApiClient().getUrl();
  }

  void _alertDialog() {
    showDialog(
      context: context,
      builder: (dialogContext) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Guru atau Admin tidak dapat mengakses ${Strings.appName}', textAlign: TextAlign.center,),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
              child: Text('Oke', style: EduText().smallSemiBold,)
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);
        return null;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          body: BlocListener(
            bloc: _loginBloc,
            listener: (context, state) {
              if (state is Alert) {
                _alertDialog();
                setState(() {
                  _isLoading = false;
                });
              }
              if (state is OnSuccess) {
                FbAnalytics().sendAnalytics('logged_in');
                MixpanelUtils().initPlatformState('Logged In');
                Navigator.pop(context, true);
                setState(() {
                  _isLoading = false;
                });
              }
              if (state is OnFailed) {
                setState(() {
                  if (state.isConnected) {
                    _showError = true;
                  } else {
                    Fluttertoast.showToast(
                        msg: "Terjadi kesalahan",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 1,
                        backgroundColor: Colors.grey,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                  }
                  _isLoading = false;
                });
              }
              if (state is Loading) {
                setState(() {
                  _isLoading = true;
                });
              }
            },
            child: Stack(
              children: <Widget>[
                SingleChildScrollView(
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          _loginForm(),
                        ],
                      ),
                    )
                ),
                Container(
                  height: 80.0,
                  child: AppBar(
                    backgroundColor: Colors.white,
                    elevation: 0.0,
                    iconTheme: IconThemeData(color: Colors.black),
                    automaticallyImplyLeading: true,
                    title: Text('Masuk ke ${Strings.appName}', style: EduText().mediumBold),
                  ),
                ),
              ],
            ),
          )
      ),
    );
  }

  Widget _logo() {
    return Container(
      height: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.appName, style: EduText().largeBold),
          SizedBox(width: 3.0,),
          /*Container(
            width: 30.0,
            height: 25.0,
            padding: EdgeInsets.all(2.0),
            decoration: BoxDecoration(
              color: EduColors.red,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0),
                  topLeft: Radius.circular(20.0)
              ),
              gradient: LinearGradient(
                  colors: [Colors.red, EduColors.red],
                  stops: [0.0, 1.0],
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  tileMode: TileMode.clamp
              ),
            ),
            child: Center(
              child: Text('Go', style: EduText().smallBold.copyWith(
                  color: Colors.white
              )),
            ),
          ),*/
        ],
      ),
    );
  }

  Widget _loginForm() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 16.0),
      child: Container(
        padding: EdgeInsets.only(
            left: 16.0, right: 16.0, top: 10.0, bottom: 10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[400],
              blurRadius: 10.0,
              offset: Offset(0.0, 3.0)
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            _logo(),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 5.0
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Username', style: EduText().mediumSemiBold,),
              ),
            ),
            MyTextField(
              controller: _usernameController,
              focusNode: _usernameFocus,
              hintText: 'Masukkan username',
              keyboardType: TextInputType.emailAddress,
              actionType: TextInputAction.next,
              onSubmitted: (term) {
                _usernameFocus.unfocus();
                FocusScope.of(context).requestFocus(_passwordFocus);
              },
              onChanged: (val) {
                setState(() {
                  _username = val;
                  if (_username.length > 0 && _password.length >= 2) {
                    _isDisabled = false;
                  } else {
                    _isDisabled = true;
                  }
                });
              },
              suffix: _username.length > 0 ? InkWell(
                onTap: () {
                  setState(() {
                    WidgetsBinding.instance.addPostFrameCallback((_) => _usernameController.clear());
                    _username = '';
                  });
                },
                child: Icon(Icons.close, color: Colors.grey[400]),
              ) : null,
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 5.0
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Password', style: EduText().mediumSemiBold,),
              ),
            ),
            MyTextField(
              controller: _passwordController,
              focusNode: _passwordFocus,
              hintText: 'Masukkan password',
              keyboardType: TextInputType.text,
              actionType: TextInputAction.done,
              onSubmitted: (term) {
                _passwordFocus.unfocus();
                if (!_isDisabled) {
                  _loginBloc.add(Login(
                    username: _usernameController.text,
                    password: _passwordController.text
                  ));
                }
              },
              onChanged: (val) {
                setState(() {
                  _password = val;
                  if (_username.length > 0 && _password.length >= 2) {
                    _isDisabled = false;
                  } else {
                    _isDisabled = true;
                  }
                });
              },
              suffix: InkWell(
                onTap: () {
                  setState(() {
                    _isVisible = !_isVisible;
                  });
                },
                child: Icon(
                  _isVisible ? Icons.visibility_off : Icons.visibility,
                  color: Colors.grey[400])
              ),
              obsecureText: _isVisible,
            ),
            SizedBox(
              height: 20.0,
            ),
            Visibility(
              visible: _showError,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Text('Username atau password salah!', style: EduText().smallRegular.copyWith(
                  color: EduColors.red
                ),),
              ),
            ),
            MyButton(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
                _loginBloc.add(Login(
                  username: _usernameController.text,
                  password: _passwordController.text
                ));
              },
              isDisabled: _isDisabled,
              child: _isLoading ? SpinKitThreeBounce(
                  color: Colors.white,
                  size: 20.0,
                  controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
                ) : Text('Masuk', style: EduText().mediumSemiBold.copyWith(
                color: Colors.white
              )),
            ),
            _baseUrl != 'http://api.edu.box' ? Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 30.0,
                    child: Center(
                      child: Text('Belum punya akun ?', style: EduText().smallRegular)),
                  ),
                  SizedBox(width: 10.0,),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => RegisterStudentScreen()
                      ));
                    },
                    child: Container(
                      height: 30.0,
                      child: Center(
                        child: Text('Daftar Akun', style: EduText().smallSemiBold.copyWith(
                          color: EduColors.primary
                        ))
                      ),
                    ),
                  ),
                ],
              ),
            ) : SizedBox(
              height: 25.0,
            ),
          ],
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height - 65);        
    path.quadraticBezierTo(size.width / 2, size.height, size.width, size.height - 65); 
    path.lineTo(size.width, 0); 
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {    
    return true;
  }
}