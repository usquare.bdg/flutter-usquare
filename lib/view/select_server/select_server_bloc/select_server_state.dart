import 'package:equatable/equatable.dart';

abstract class SelectServerState extends Equatable {
  const SelectServerState();
}

class InitialSelectServerState extends SelectServerState {
  @override
  List<Object> get props => [];
}

class Connected extends SelectServerState {
  final String type;

  Connected(this.type);
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class FailedToConnect extends SelectServerState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class Connecting extends SelectServerState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class UrlLoaded extends SelectServerState {
  final String url;

  UrlLoaded(this.url);
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class IsPremium extends SelectServerState {
  final String isPremium;

  IsPremium(this.isPremium);

  @override
  // TODO: implement props
  List<Object> get props => [isPremium];
}