import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import './bloc.dart';

class SelectServerBloc extends Bloc<SelectServerEvent, SelectServerState> {
  @override
  SelectServerState get initialState => InitialSelectServerState();

  @override
  Stream<SelectServerState> mapEventToState(
    SelectServerEvent event,
  ) async* {
    if (event is SelectedServer) {
      yield* _selectedServerToState(event);
    } else if (event is LoadUrl) {
      yield* _loadUrlToState();
    } else if (event is CheckIsPremium) {
      yield* _checkIsPremium();
    }
  }

  Stream<SelectServerState> _checkIsPremium() async* {
    try {
      final isPremium = await ApiClient().isPremium();
      yield IsPremium(isPremium);
    } catch (e) {
      print(e);
    }
  }

  Stream<SelectServerState> _loadUrlToState() async* {
    try {
      final url = await ApiClient().getUrl();
      yield UrlLoaded(url);
    } catch (e) {
      print(e);
    }
  }

  Stream<SelectServerState> _selectedServerToState(event) async* {
    yield Connecting();
    await Future.delayed(Duration(seconds: 1));

    try {
      bool result;
      String url;

      if (event.type == 'Lokal') {
        url = 'http://api.edu.box';
        result = await ApiClient().checkApi(url);
      } else {
        url = 'https://api.edubox.cloud';
        result = await ApiClient().checkApi(url);
      }

      if (result) {
        await ApiClient().saveUrl(url);

        yield Connected(event.type);
      } else {
        yield FailedToConnect();
      }
    } catch (e) {
      print('error : $e');
      yield FailedToConnect();
    }
  }
}
