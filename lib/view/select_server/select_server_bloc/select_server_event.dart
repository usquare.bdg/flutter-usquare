import 'package:equatable/equatable.dart';

abstract class SelectServerEvent extends Equatable {
  const SelectServerEvent();
}

class SelectedServer extends SelectServerEvent {
  final String type;

  SelectedServer(this.type);
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadUrl extends SelectServerEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CheckIsPremium extends SelectServerEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}
