import 'package:edubox_tryout/main.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/themes/colors.dart';
import 'package:edubox_tryout/themes/textstyles.dart';
import 'package:edubox_tryout/utils/firebase_admob.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/home/home_screen.dart';
import 'package:edubox_tryout/view/select_server/select_server_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SelectServerScreen extends StatefulWidget {
  @override
  _SelectServerScreenState createState() => _SelectServerScreenState();
}

class _SelectServerScreenState extends State<SelectServerScreen> {
  SelectServerBloc _serverBloc;
  bool _isConnecting = false;
  bool _isLoading = false;
  String _url;
  String _statusText = 'Belum Terhubung';
//  BannerAd bannerAd;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Select Server Screen');
    MixpanelUtils().initPlatformState('Select Server Screen');
    _serverBloc = new SelectServerBloc();
    if (!isFirstTimeOpen) {
      _serverBloc.add(LoadUrl());
    }
//    bannerAd = Ads().initBannerAd();
    _serverBloc.add(CheckIsPremium());
    setState(() {
      _isConnecting = true;
    });
    super.initState();
  }

  @override
  void dispose() {
//    bannerAd..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener(
        bloc: _serverBloc,
        listener: (context, state) {
          if (state is Connecting) {
            setState(() {
              _isConnecting = true;
              _statusText = 'Menghubungkan';
            });
          }
          if (state is Connected) {
            setState(() {
              _isConnecting = false;
              _statusText = 'Terhubung ke ${state.type}';

              isFirstTimeOpen = false;

              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                  builder: (context) => MyApp(currentPage: 0,)
              ), (Route<dynamic> route) => false);
            });
          }
          if (state is FailedToConnect) {
            setState(() {
              _isConnecting = false;
              _statusText = 'Gagal menghubungkan, coba lagi';
            });
          }
          if (state is UrlLoaded) {
            setState(() {
              if (state.url == 'http://api.edu.box') _statusText = 'Terhubung ke Lokal';
              else _statusText = 'Terhubung ke Online';

              _isLoading = false;
            });
          }

          if (state is IsPremium) {
            setState(() {
              print("isPremium ${state.isPremium}");
              if (state.isPremium != "approved") {
//                bannerAd = Ads().initBannerAd();
                Future.delayed(Duration(seconds: 6), (){
                  setState(() {
                    _isConnecting = false;
                  });
                });
              } else {
                _isConnecting = false;
              }
            });
          }
        },
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ClipPath(
                      clipper: MyClipper(),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 3,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [EduColors.primary, EduColors.primaryDark],
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              stops: [0.2, 1.0],
                              tileMode: TileMode.clamp
                          ),
                        ),
                        child: Center(
                          child: Text('Pilih Server', style: EduText().superLargeBold.copyWith(color: Colors.white),),
                        ),
                      )
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 30.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 40.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Pilih yang mana ?', style: EduText().largeBold,),
                                SizedBox(height: 10.0,),
                                Text('- Jika kamu mau menggunakan ${Strings.appName} dengan Internet atau Paket Data silakan pilih Server Online', style: EduText().smallRegular,),
                                SizedBox(height: 5.0,),
                                Text('- Jika kamu mau menggunakan ${Strings.appName} dengan Server Lokal di sekolah atau Edubox Smart Router tanpa koneksi Internet silakan pilih Server Lokal', style: EduText().smallRegular),
                              ],
                            ),
                          ),
                          MyButton(
                            onTap: () {
                              _serverBloc.add(SelectedServer('Online'));
                            },
                            isDisabled: _isConnecting,
                            child: Text('Server Online', style: EduText().mediumSemiBold.copyWith(
                                color: Colors.white
                            ),),
                          ),
                          SizedBox(height: 10.0,),
                          MyButton(
                            onTap: () {
                              _serverBloc.add(SelectedServer('Lokal'));
                            },
                            isDisabled: _isConnecting,
                            child: Text('Server Lokal', style: EduText().mediumSemiBold.copyWith(
                                color: Colors.white
                            ),),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 50.0,
                      width: 50.0,
                      margin: EdgeInsets.only(bottom: 10.0),
                      padding: EdgeInsets.all(5.0),
                      child: _isConnecting ? CircularProgressIndicator() : Container(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 50.0),
                      child: _isLoading ? Container() : Text(_statusText, style: EduText().mediumRegular,),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 80.0,
              child: AppBar(
                automaticallyImplyLeading: true,
                backgroundColor: Colors.transparent,
                elevation: 0.0,
                iconTheme: IconThemeData().copyWith(
                  color: Colors.white
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
