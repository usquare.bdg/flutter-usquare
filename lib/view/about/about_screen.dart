import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/home/home_screen.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {

  String versionName = "";

  @override
  void initState() {
    getVersion();
    super.initState();
  }

  getVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      versionName = "v${packageInfo.version}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ClipPath(
                      clipper: MyClipper(),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 3,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [EduColors.primary, EduColors.primaryDark],
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              stops: [0.2, 1.0],
                              tileMode: TileMode.clamp
                          ),
                        ),
                        child: Center(
                          child: Text(Strings.aboutTitle, style: EduText().superLargeBold.copyWith(color: Colors.white),),
                        ),
                      )
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(Strings.aboutDesc, style: EduText().mediumRegular, textAlign: TextAlign.center,),
                            ],
                          ),
                        ),

                        Image(image: AssetImage("assets/edusquare_logo.png"), height: 100, width: 100,),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                          child: Text(
                            versionName
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 80.0,
            child: AppBar(
              automaticallyImplyLeading: true,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              iconTheme: IconThemeData().copyWith(
                  color: Colors.white
              ),
            ),
          ),

        ],
      ),
    );
  }
}
