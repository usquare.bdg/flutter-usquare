import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PlayVideoState extends Equatable {}

class InitialPlayVideoState extends PlayVideoState {
  @override
  List<Object> get props => [];
}

class Downloading extends PlayVideoState {
  @override
  List<Object> get props => [];
}

class VideoDownloaded extends PlayVideoState {
  @override
  List<Object> get props => [];
}

class DownloadVideoFailed extends PlayVideoState {
  @override
  List<Object> get props => [];
}

class UserNotFound extends PlayVideoState {
  @override
  List<Object> get props => [];
}
