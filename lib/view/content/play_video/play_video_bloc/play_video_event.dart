import 'package:edubox_tryout/models/content.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PlayVideoEvent extends Equatable {}

class DownloadVideo extends PlayVideoEvent {
  final FileIds file;

  DownloadVideo(this.file);

  @override
  List<Object> get props => [];
}
