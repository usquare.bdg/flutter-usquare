import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:permission_handler/permission_handler.dart';
import './bloc.dart';

class PlayVideoBloc extends Bloc<PlayVideoEvent, PlayVideoState> {
  @override
  PlayVideoState get initialState => InitialPlayVideoState();

  @override
  Stream<PlayVideoState> mapEventToState(
    PlayVideoEvent event,
  ) async* {
    if (event is DownloadVideo) {
      yield* _mapDownloadVideoToState(event);
    }
  }

  Stream<PlayVideoState> _mapDownloadVideoToState(event) async* {
    yield Downloading();
    try {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      final user = await DbDao().getUser();
      if (user != null) {
        final result = await ApiClient().downloadFile(event.file);
        if (result) {
          if (event.file.meta.thumbnail != null && event.file.meta.thumbnail != "") {
            await ApiClient().downloadThumbnail(event.file);
          }
          event.file.isDownloaded = true;
          await DbDao().insertContent(event.file);

          yield VideoDownloaded();
        }
      } else {
        yield UserNotFound();
      }
    } catch (e) {
      print('download video failed $e');
      yield DownloadVideoFailed();
    }
  }
}
