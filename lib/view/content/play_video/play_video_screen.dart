import 'dart:async';
import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:dio/dio.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/content/play_video/play_video_bloc/bloc.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';

class PlayVideoScreen extends StatefulWidget {
  final List<FileIds> listVideo;
  final FileIds nowPlaying;
  final String babName, subBabName;
  final String baseUrl;

  PlayVideoScreen({Key key, this.listVideo, this.nowPlaying, this.subBabName, this.baseUrl, this.babName}) : super(key: key);
  @override
  _PlayVideoScreenState createState() => _PlayVideoScreenState();
}

class _PlayVideoScreenState extends State<PlayVideoScreen> {
  List<FileIds> get _listVideo => widget.listVideo;
  String get _baseUrl => widget.baseUrl;
  FileIds _nowPlaying;
  int _npPosition = 0;
  bool _isLoading = true;
  bool _isExpanded = false;
  bool _showBackButton = false;
  ChewieController _chewieController;
  VideoPlayerController _videoPlayerController;
  PlayVideoBloc _playVideoBloc;
  String _formattedDate;
  String _processString = '0.0';
  String _dir = '';
  bool _isDownloaded = false;
  bool _isDownloading = false;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Play Video Screen');
    FbAnalytics().sendAnalytics('play_video');
    MixpanelUtils().initPlatformState('Play Video');

    _npPosition = widget.listVideo.indexWhere((i) => i.sId == widget.nowPlaying.sId);
    _nowPlaying = widget.nowPlaying;
    print('now playing position : $_npPosition');

    _getDir().then((dir) {
      _dir = dir;
    });

    _playVideoBloc = PlayVideoBloc();
    _playVideo().then((a) {
      setState(() {
        _isLoading = false;
      });
    });
    super.initState();
  }

  Future<String> _getDir() async {
    return (await getApplicationDocumentsDirectory()).path;
  }

  Future _downloadFile() async {
    Dio dio = new Dio();

    _downloading();

    try {
      await dio.download(
        '$_baseUrl/file/${_nowPlaying.sId}', '$_dir/${_nowPlaying.name}',
        onReceiveProgress: (rec, total) {
          setState(() => _processString = ((rec / total) * 100).toStringAsFixed(0) + '%');
        }
      );

      Navigator.of(context, rootNavigator: true).pop();
    } catch(e) {
      print(e);
    }
  }

  @override
  void dispose() {
    _videoPlayerController.pause();
    _videoPlayerController.dispose();
    _chewieController.pause();
    _chewieController.dispose();
    _videoPlayerController.removeListener(_listener);
    super.dispose();
  }

  void _changeVideo() {
    setState(() {
      _isLoading = true;
    });
    _videoPlayerController.removeListener(_listener);
    _videoPlayerController.pause().then((pause) {
      Timer(new Duration(milliseconds: 100), () {
        _videoPlayerController.dispose().then((a) {
          _playVideo().then((b) {
            setState(() {
              _isLoading = false;
            });
          });
        });
      });
    });
  }

  Future _playVideo() async {
    if (_nowPlaying.isDownloaded) {
      setState(() {
        _isDownloaded = true;
      });
      String dir = (await getApplicationDocumentsDirectory()).path;
      File file = File('$dir/${_nowPlaying.name}');
      _videoPlayerController = VideoPlayerController.file(file);
    }
//    else {
//      String _videoUrl = //'https://drive.google.com/file/d/0B079ffoDnU0NaWx2bi0zSDdLVEk/view?usp=sharing';
//          '$_baseUrl/file/' + _nowPlaying.sId;
//      _videoPlayerController = VideoPlayerController.network(_videoUrl);
//    }

    _videoPlayerController.addListener(_listener);

    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      aspectRatio: 16/9,
      looping: false,
      autoInitialize: true,
      autoPlay: true,
      errorBuilder: (context, err) {
        return Center(
          child: Text(
            err,
            style: EduText().smallRegular.copyWith(
              color: Colors.white
            ),
          ),
        );
      }
    );
  }

  void _listener() {
    setState(() {
      if (_videoPlayerController.value.isPlaying) {
        _showBackButton = false;
      } else {
        _showBackButton = true;
      }
    });
  }

  void _alertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.only(
                bottom: 16.0),
            child: Text('Untuk mengunduh Video kamu harus masuk ke akun ${Strings.appName} terlebih dahulu!',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          ),
          actionAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        width: 1.5,
                        color: EduColors.primary
                    )
                ),
                child: Center(
                    child: Text('Kembali', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                    ),)
                ),
              ),
            ),
            SizedBox(width: 10.0,),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => LoginScreen()
                ));
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    color: EduColors.primary,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                child: Center(
                    child: Text('Masuk', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),)
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  void _downloading() {
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: 'Mengunduh video',
          content: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: SpinKitThreeBounce(
              color: EduColors.primary,
              size: 24.0,
            ),
          ),
        );
      },
      barrierDismissible: false
    );
  }

  void _confirmationDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          title: 'Konfirmasi',
          content: Padding(
            padding: const EdgeInsets.only(
                bottom: 16.0),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    _nowPlaying.meta.thumbnail != "" ? Image.network(
                      '$_baseUrl/file/${_nowPlaying.meta.thumbnail}',
                      fit: BoxFit.cover,
                      width: 120.0,
                      height: 70.0,
                    ) : Container(
                      width: 120.0,
                      height: 70.0,
                      color: Colors.grey[300],
                      child: Center(
                        child: Icon(Icons.play_circle_outline, size: 36.0, color: Colors.grey,),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 2.0,
                            horizontal: 4.0
                        ),
                        color: Colors.black54,
                        child: Text(
                          '${(_nowPlaying.meta.duration / 60).round()}.${(_nowPlaying.meta.duration % 60).toString().padLeft(2, '0')}',
                          style: EduText().smallSemiBold.copyWith(
                              color: Colors.white,
                              fontSize: 10.0
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 20.0,),
                Text('Unduh\n \"${_nowPlaying.meta.title}\" ?', style: EduText().mediumRegular,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actionAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        width: 1.5,
                        color: EduColors.primary
                    )
                ),
                child: Center(
                    child: Text('Batal', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                    ),)
                ),
              ),
            ),
            SizedBox(width: 10.0,),
            InkWell(
              onTap: () {
                Navigator.pop(context);
//                _chewieController.pause();
                _playVideoBloc.add(DownloadVideo(_nowPlaying));
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    color: EduColors.primary,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                child: Center(
                    child: Text('Unduh', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),)
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  Future _onWillPop() async {
    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context) {
    _formattedDate = DateFormat('dd MMMM yyyy').format(_nowPlaying.createdAt);

    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: Theme(
        data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.grey[200]
        ),
        child: SafeArea(
          child: Scaffold(
            body: BlocListener(
              bloc: _playVideoBloc,
              listener: (context, state) {
                if (state is Downloading) {
                  setState(() {
                    _isDownloading = true;
                  });
//                  _downloading();
                }
                if (state is UserNotFound) {
                  Navigator.pop(context);
                  _alertDialog();
                }
                if (state is DownloadVideoFailed) {
                  setState(() {
                    _isDownloading = false;
                  });
//                  Navigator.pop(context);
                  MyToast().toast('Gagal mengunduh video');
                }
                if (state is VideoDownloaded) {
                  setState(() {
                    _isDownloading = false;
                  });
//                  Navigator.pop(context);
                  MyToast().toast('Berhasil mengunduh video');
                  setState(() {
                    _isDownloaded = true;
                    _nowPlaying.isDownloaded = true;
                    _listVideo[_npPosition].isDownloaded = true;

                  });
                  _playVideo().then((a) {
                    setState(() {
                      _isLoading = false;
                    });
                  });
                }
              },
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      _isDownloading ? AspectRatio(
                        aspectRatio: 16/9,
                        child: Container(
                          color: Colors.black,
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SpinKitThreeBounce(
                                  color: Colors.white,
                                  size: 24.0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Text(
                                    "Mengunduh Video",
                                    style: EduText().largeRegular.copyWith(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ) : _isDownloaded ? _isLoading ? AspectRatio(
                        aspectRatio: 16/9,
                        child: Container(
                          color: Colors.black,
                        ),
                      ) : Chewie(
                        controller: _chewieController,
                      ) : AspectRatio(
                        aspectRatio: 16/9,
                        child: Container(
                          color: Colors.black,
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                InkWell(
                                  onTap: (){
                                    _confirmationDialog();
                                  },
                                  child: Icon(
                                    Icons.play_circle_outline,
                                    color: Colors.white,
                                    size: 64.0,
                                  )
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView(
                          children: <Widget>[
                            _header(),
                            Divider(),
                            Container(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Daftar putar video', style: EduText().mediumSemiBold,)
                            ),
                            _playList()
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    height: 60.0,
                    child: AppBar(
                      backgroundColor: Colors.transparent,
                      elevation: 0.0,
                      leading: Visibility(
                        visible: _showBackButton,
                        child: InkWell(
                          onTap: () => Navigator.pop(context),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.black54,
                              ),
                              child: BackButtonIcon(),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ),
        ),
      ),
    );
  }

  Widget _header() {
    return InkWell(
      onTap: () {
        setState(() {
          _isExpanded = !_isExpanded;
        });
      },
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(
            left: 16.0, right: 16.0,
            bottom: 8.0, top: 16.0
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text('${_nowPlaying.meta.title}',
                    style: EduText().largeBold,
                    maxLines: 4,
                  ),
                  SizedBox(height: 10.0,),
                  Text('Diunggah tanggal $_formattedDate', style: EduText().smallRegular,),
                  _isExpanded ? Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text('Deskripsi', style: EduText().smallSemiBold),
                        SizedBox(height: 5.0),
                        Html(
                          data: """${_nowPlaying.meta.description}""",
                          padding: EdgeInsets.all(0.0),
                        ),
                        SizedBox(height: 15.0),

                        Text('Bab', style: EduText().smallSemiBold),
                        SizedBox(height: 5.0),
                        Text('${widget.babName}', style: EduText().smallRegular.copyWith(
                            color: EduColors.primary
                        ),),
                        SizedBox(height: 15.0),

                        Text('Sub-bab', style: EduText().smallSemiBold),
                        SizedBox(height: 5.0),
                        Text('${widget.subBabName}', style: EduText().smallRegular.copyWith(
                            color: EduColors.primary
                        ),),
                      ],
                    ),
                  ) : Container(),
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0),
                    child: Text(_isExpanded ? 'SEMBUNYIKAN DETAIL' : 'LIHAT DETAIL', style: EduText().smallSemiBold.copyWith(
                        color: Colors.grey
                    ),),
                  )
                ],
              ),
            ),
            Visibility(
              visible: false,
              child: InkWell(
                onTap: _nowPlaying.isDownloaded ? null : () {
                  _confirmationDialog();
                },
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 50.0,
                      height: 35.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _nowPlaying.isDownloaded ? EduColors.green : EduColors.primary,
                      ),
                      child: Icon(
                        _nowPlaying.isDownloaded ? Icons.check : Icons.file_download,
                        color: Colors.white,
                        size: 18.0,),
                    ),
                    SizedBox(height: 5.0,),
                    Text(
                      _nowPlaying.isDownloaded ? 'Diunduh' : 'Unduh',
                      style: EduText().smallSemiBold.copyWith(
                          fontSize: 10.0
                      ),)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _playList() {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: _listVideo.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            setState(() {
              _npPosition = index;
              _isExpanded = false;
              _nowPlaying = _listVideo[index];
              _changeVideo();
            });
          },
          child: Container(
            constraints: BoxConstraints(
              minHeight: 100.0,
            ),
            height: 100.0,
            color: _npPosition == index ? Colors.grey[200] : Colors.transparent,
            child: Row(
              children: <Widget>[
                Container(
                  width: 40.0,
                  child: Center(
                    child: Text('${index+1}', style: EduText().smallRegular,),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 75.0,
                    child: Row(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
//                            _listVideo[index].meta.thumbnail != "" ? Image.network(
//                              '$_baseUrl/file/${_listVideo[index].meta.thumbnail}',
//                              fit: BoxFit.cover,
//                              width: 120.0,
//                              height: 70.0,
//                            ) : Container(
//                              width: 120.0,
//                              color: Colors.grey[300],
//                              child: Center(
//                                child: Icon(Icons.play_circle_outline, size: 36.0, color: Colors.grey,),
//                              ),
//                            ),
                            _thumbnail(_listVideo[index]),
                            Positioned(
                              bottom: 0,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0
                                ),
                                color: Colors.black54,
                                child: Text(
                                  '${(_listVideo[index].meta.duration / 60).round()}.${(_listVideo[index].meta.duration % 60).toString().padLeft(2, '0')}',
                                  style: EduText().smallSemiBold.copyWith(
                                      color: Colors.white,
                                      fontSize: 10.0
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(width: 10.0,),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('${_listVideo[index].meta.title}',
                                  style: EduText().mediumSemiBold,
                                  maxLines: 3,
                                ),
//                                _listVideo[index].isDownloaded ? Container(
//                                  padding: EdgeInsets.symmetric(
//                                    vertical: 5.0,
//                                    horizontal: 8.0
//                                  ),
//                                  decoration: BoxDecoration(
//                                    borderRadius: BorderRadius.circular(30.0),
//                                    color: Colors.grey[300]
//                                  ),
//                                  child: Row(
//                                    mainAxisSize: MainAxisSize.min,
//                                    children: <Widget>[
//                                      Text('Diunduh', style: EduText().smallSemiBold.copyWith(
////                                        color: Colors.white,
//                                        fontSize: 10.0
//                                      ),),
//                                      Icon(Icons.check, size: 16.0, color: EduColors.black,)
//                                    ],
//                                  ),
//                                ) : Container()
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  ),
                ),
              ],
            ),
          ),
        );
      }
    );
  }

  Widget _thumbnail(FileIds item) {
    if (item.isDownloaded) {
      File file = File('$_dir/${item.meta.thumbnail}.jpg');

      if (item.meta.thumbnail != null && item.meta.thumbnail != "") {
        return Image.file(
          file,
          fit: BoxFit.cover,
          width: 120.0,
          height: 70.0,
        );
      } else {
        return Container(
          width: 120.0,
          height: 70.0,
          color: Colors.grey[300],
          child: Center(
            child: Icon(Icons.play_circle_outline, size: 36.0, color: Colors.grey,),
          ),
        );
      }
    } else {
      if (item.meta.thumbnail != "") {
        return Image.network(
          '$_baseUrl/file/${item.meta.thumbnail}',
          fit: BoxFit.cover,
          width: 120.0,
          height: 70.0,
        );
      } else {
        return Container(
          width: 120.0,
          height: 70.0,
          color: Colors.grey[300],
          child: Center(
            child: Icon(Icons.play_circle_outline, size: 36.0, color: Colors.grey,),
          ),
        );
      }
    }
  }
}
