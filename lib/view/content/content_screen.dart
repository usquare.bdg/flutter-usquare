import 'dart:io';

import 'package:edubox_tryout/models/lesson.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/view/content/content_bloc/bloc.dart';
import 'package:edubox_tryout/view/content/select_chapter/select_chapter_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'list_content/list_content_screen.dart';

class ContentScreen extends StatefulWidget {
  @override
  _ContentScreenState createState() => _ContentScreenState();
}

class _ContentScreenState extends State<ContentScreen> with TickerProviderStateMixin{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  AnimationController _controller;
  Animation _fadeAnimation;
  ContentBloc _contentBloc;
  bool _isLoading = true;
  bool _isFailed = false;
  List<LessonItems> _listCategory;
  List<String> _assetNames;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Content Screen');
    MixpanelUtils().initPlatformState('Content Screen');

    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _fadeAnimation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _contentBloc = ContentBloc()
      ..add(LoadCategory());
    _listCategory =  List<LessonItems>();
    _assetNames = List<String>();
    super.initState();
  }

  Future _onRefresh() async {
    _contentBloc.add(LoadCategory());
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent
      ),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            children: <Widget>[
              Text('Konten Belajar', style: EduText().largeBold,),
            ],
          ),
          elevation: 0.0,
        ),
        body: BlocListener(
            bloc: _contentBloc,
            listener: (context, state) {
              if (state is Loading) {
                setState(() {
                  _isLoading = true;
                });
              }
              if (state is LoadCategoryFailed) {
                setState(() {
                  _isLoading = false;
                  _isFailed = true;
                  print('load category failed');
                });
              }
              if (state is CategoryIsEmpty) {
                setState(() {
                  _isLoading = false;
                  _isFailed = false;
                  print('category is empty');
                });
              }
              if (state is CategoryLoaded) {
                _controller.forward();
                setState(() {
                  _listCategory = state.listCategory;
                  _assetNames = state.assetNames;
                  _isLoading = false;
                  _isFailed = false;
                });
              }
            },
            child: _isLoading ? _loadingView() : _isFailed ? _listFailedView() : ScrollConfiguration(
              behavior: MyBehavior(),
              child: RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: _onRefresh,
                child: ListView(
                  padding: const EdgeInsets.only(
                      left: 11.0, right: 11.0, bottom: 11.0, top: 11.0
                  ),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 5.0),
                      child: Text('Pilih Kategori', style: EduText().mediumSemiBold),
                    ),
                    SizedBox(height: 15.0,),
                    GridView.count(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        crossAxisCount: 2,
                        children: List.generate(_listCategory.length, (index) {
                          var item = _listCategory[index];
                          return _categoryItem(item, index);
                        })
                    )
                  ],
                ),
              ),
            )
        ),
      ),
    );
  }

  Widget _categoryItem(item, index) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Opacity(
          opacity: _fadeAnimation.value,
          child: InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => SelectChapterScreen(
                    categoryName: item.name,
                    lessonId: item.materialId,)
              ));
            },
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [EduColors.primary, EduColors.purple],
                        stops: [0.2, 1.0],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(5.0)
                ),
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(_assetNames[index], width: 50.0,),
                    SizedBox(height: 20.0,),
                    Text('${item.name}', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),
                      textAlign: TextAlign.center,),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _loadingView() {
    return Center(
        child: SpinKitRing(
          color: EduColors.primary,
          size: 50.0,
          lineWidth: 3.0,
          controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
        )
    );
  }

  Widget _listFailedView() {
    return  Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/no-wifi.png', width: 80.0),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Text('Kamu sedang offline', style: EduText().mediumSemiBold,),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
              child: Text('Lihat hasil download untuk menikmati konten\ntanpa internet.',
                style: EduText().smallRegular,
                textAlign: TextAlign.center,
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ListContentScreen(
                      subBabName: 'Tidak diketahui',
                      subBabId: 'Tidak diketahui',
                      babName: 'Tidak diketahui',
                      onlyDownloadedFile: true,
                    )
                ));
              },
              color: EduColors.primary,
              child: Text('Lihat hasil download', style: EduText().smallSemiBold.copyWith(
                  color: Colors.white
              ),),
            ),
            SizedBox(height: 5.0),
            InkWell(
              onTap: () {
                _contentBloc.add(LoadCategory());
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Text('Muat ulang', style: EduText().smallSemiBold.copyWith(
                    color: EduColors.primary
                ),),
              ),
            )
          ],
        ),
      ),
    );
  }
}
