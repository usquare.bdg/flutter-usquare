import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ContentEvent extends Equatable {}

class LoadCategory extends ContentEvent {
  @override
  List<Object> get props => [];
}

