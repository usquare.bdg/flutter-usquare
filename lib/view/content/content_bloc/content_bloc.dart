import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/models/lesson.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:flutter/services.dart';
import './bloc.dart';

class ContentBloc extends Bloc<ContentEvent, ContentState> {
  @override
  ContentState get initialState => InitialContentState();

  @override
  Stream<ContentState> mapEventToState(
    ContentEvent event,
  ) async* {
    if (event is LoadCategory) {
      yield* _loadCategoryToState();
    }
  }

  Stream<ContentState> _loadCategoryToState() async* {
    yield Loading();
    try {
      List<LessonItems> listCategory = List<LessonItems>();
      List<String> assetNames = List<String>();
      final category = await ApiClient().getNoAuthLessons();
//      final user = await DbDao().getUser();
//      if (user != null) {
//        final category = await ApiClient().getLessons(user.data.rombelIds.last.sId);
      if (category != null) {
        category.data.items.removeWhere((i) => i.slug == 'pengumuman');

        for (var categoryItem in category.data.items) {
          listCategory.add(categoryItem);
        }

        for (var item in listCategory) {
          try {
            await rootBundle.load('assets/${item.slug}.png');
            assetNames.add('assets/${item.slug}.png');
          } catch (e) {
            print(e);
            assetNames.add('assets/default_category_icon.png');
          }
        }

        yield CategoryLoaded(listCategory, assetNames);
      } else {
        yield LoadCategoryFailed();
      }
//      } else {
//        yield LoginRequire();
//      }

    } catch (e) {
      print(e);
      yield LoadCategoryFailed();
    }
  }
}
