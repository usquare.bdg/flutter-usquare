import 'package:edubox_tryout/models/lesson.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ContentState extends Equatable {}

class InitialContentState extends ContentState {
  @override
  List<Object> get props => [];
}

class CategoryLoaded extends ContentState {
  final List<LessonItems> listCategory;
  final List<String> assetNames;

  CategoryLoaded(this.listCategory, this.assetNames);
  @override
  List<Object> get props => [];
}

class LoadCategoryFailed extends ContentState {
  @override
  List<Object> get props => [];
}

class CategoryIsEmpty extends ContentState {
  @override
  List<Object> get props => [];
}

class Loading extends ContentState {
  @override
  List<Object> get props => [];
}

class LoginRequire extends ContentState {
  @override
  // TODO: implement props
  List<Object> get props => [];

}