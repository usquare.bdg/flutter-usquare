import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/content/play_video/play_video_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ListVideoView extends StatefulWidget {
  final List<FileIds> listVideo;
  final String babName, subBabName;
  final String baseUrl, dir;

  const ListVideoView(
      {Key key,
      this.listVideo,
      this.subBabName,
      this.baseUrl,
      this.babName,
      this.dir})
      : super(key: key);
  @override
  _ListVideoViewState createState() => _ListVideoViewState();
}

class _ListVideoViewState extends State<ListVideoView> {
  List<FileIds> get _listVideo => widget.listVideo;
  String get _baseUrl => widget.baseUrl;
  String get _dir => widget.dir;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          splashColor: Colors.transparent, highlightColor: Colors.grey[200]),
      child: Scaffold(
        body: _listVideo.length > 0
            ? ScrollConfiguration(
                behavior: MyBehavior(),
                child: ListView(
                  children: <Widget>[
                    _listVideos(),
                  ],
                ),
              )
            : _listEmptyView(),
      ),
    );
  }

  Widget _listVideos() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        itemCount: _listVideo.length,
        itemBuilder: (context, index) {
          return _videoItem(index);
        },
      ),
    );
  }

  Widget _videoItem(index) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PlayVideoScreen(
                      listVideo: _listVideo,
                      nowPlaying: _listVideo[index],
                      subBabName: widget.subBabName,
                      babName: widget.babName,
                      baseUrl: _baseUrl,
                    )));
      },
      child: Padding(
        padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
            top: 10.0,
            bottom: index == _listVideo.length - 1 ? 25.0 : 10.0),
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 70.0,
            child: Row(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    _thumbnail(_listVideo[index]),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 4.0),
                        color: Colors.black54,
                        child: Center(
                          child: Text(
                            '${(_listVideo[index].meta.duration / 60).round()}.${(_listVideo[index].meta.duration % 60).toString().padLeft(2, '0')}',
                            style: EduText()
                                .smallSemiBold
                                .copyWith(color: Colors.white, fontSize: 10.0),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  width: 10.0,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        _listVideo[index].meta.title,
                        style: EduText().mediumSemiBold,
                        maxLines: 2,
                      ),
//                    SizedBox(height: 2.0,),
//                    Text(_listVideo[index].speaker, style: EduText().smallRegular)
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  Widget _thumbnail(FileIds item) {
    if (item.isDownloaded) {
      File file = File('$_dir/${item.meta.thumbnail}.jpg');

      if (item.meta.thumbnail != null && item.meta.thumbnail != "") {
        return Image.file(
          file,
          fit: BoxFit.cover,
          width: 120.0,
          height: 70.0,
        );
      } else {
        return Container(
          width: 120.0,
          height: 70.0,
          color: Colors.grey[300],
          child: Center(
            child: Icon(
              Icons.play_circle_outline,
              size: 36.0,
              color: Colors.grey,
            ),
          ),
        );
      }
    } else {
      if (item.meta.thumbnail != "") {
        return CachedNetworkImage(
            imageUrl: '${widget.baseUrl}/file/${item.meta.thumbnail}',
            imageBuilder: (context, imageProvider) => Container(
                  height: 70.0,
                  width: 120.0,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      )),
                ),
            placeholder: (context, url) => _loadingImage('video'),
            errorWidget: (context, url, error) => _imageError('video'));
      } else {
        return Container(
          width: 120.0,
          height: 70.0,
          color: Colors.grey[300],
          child: Center(
            child: Icon(
              Icons.play_circle_outline,
              size: 36.0,
              color: Colors.grey,
            ),
          ),
        );
      }
    }
  }

  Widget _listEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/empty.png', width: 60.0),
          SizedBox(height: 40.0),
          Container(
            child: Text(
              'Tidak ada video',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _imageError(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
          child: Icon(
        Icons.error,
        color: Colors.grey,
      )),
    );
  }

  Widget _loadingImage(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
        child: Shimmer.fromColors(
          child: Text(Strings.appName,
              style: EduText()
                  .mediumBold
                  .copyWith(fontSize: type == 'video' ? 14.0 : 8.0)),
          period: Duration(milliseconds: 500),
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[200],
        ),
      ),
    );
  }
}
