import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/colors.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/content/open_book/open_book_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ListEbookView extends StatefulWidget {
  final List<FileIds> listBook;
  final String baseUrl, dir;

  const ListEbookView({Key key, this.listBook, this.baseUrl, this.dir})
      : super(key: key);

  @override
  _ListEbookViewState createState() => _ListEbookViewState();
}

class _ListEbookViewState extends State<ListEbookView> {
  List<FileIds> get _listBook => widget.listBook;
  String get _baseUrl => widget.baseUrl;
  String get _dir => widget.dir;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          splashColor: Colors.transparent, highlightColor: Colors.grey[200]),
      child: Scaffold(
        body: _listBook.length > 0
            ? ScrollConfiguration(
                behavior: MyBehavior(),
                child: ListView(
                  children: <Widget>[_listContent()],
                ),
              )
            : _listEmptyView(),
      ),
    );
  }

  Widget _listContent() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        itemCount: _listBook.length,
        itemBuilder: (context, index) {
          return _bookItem(index);
        },
      ),
    );
  }

  Widget _bookItem(index) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OpenBookScreen(
                      file: _listBook[index],
                      listBook: _listBook,
                    )));
      },
      child: Padding(
        padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
            top: 10.0,
            bottom: index == _listBook.length - 1 ? 25.0 : 10.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 90.0,
          child: Row(
            children: <Widget>[
              _thumbnail(_listBook[index]),
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _listBook[index].meta.title,
                      style: EduText().mediumSemiBold,
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text('${_listBook[index].meta.pages} halaman',
                        style: EduText().smallRegular),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _thumbnail(FileIds item) {
    if (item.isDownloaded) {
      File file = File('$_dir/${item.meta.thumbnail}.jpg');

      return Container(
          width: 60.0,
          height: 90.0,
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Image.file(
              file,
              fit: BoxFit.cover,
            ),
          ));
    } else {
      if (item.meta.thumbnail != "") {
        return ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: CachedNetworkImage(
                imageUrl: '${widget.baseUrl}/file/${item.meta.thumbnail}',
                imageBuilder: (context, imageProvider) => Container(
                      width: 60.0,
                      height: 90.0,
                      decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(5.0),
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          )),
                    ),
                placeholder: (context, url) => _loadingImage('book'),
                errorWidget: (context, url, error) => _imageError('book')));
      } else {
        return Container(
            width: 60.0,
            height: 90.0,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Center(
              child: Icon(
                Icons.book,
                size: 36,
                color: Colors.grey,
              ),
            ));
      }
    }
  }

  Widget _listEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/empty.png', width: 60.0),
          SizedBox(height: 40.0),
          Container(
            child: Text(
              'Tidak ada buku',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _loadingImage(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
        child: Shimmer.fromColors(
          child: Text(Strings.appName,
              style: EduText()
                  .mediumBold
                  .copyWith(fontSize: type == 'video' ? 14.0 : 8.0)),
          period: Duration(milliseconds: 500),
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[200],
        ),
      ),
    );
  }

  Widget _imageError(type) {
    return Container(
      width: type == 'video' ? 120.0 : 60.0,
      height: type == 'video' ? 70 : 90,
      color: Colors.grey[200],
      child: Center(
          child: Icon(
        Icons.error,
        color: Colors.grey,
      )),
    );
  }
}
