import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/view/content/play_audio/play_audio_screen.dart';
import 'package:flutter/material.dart';

class ListAudioView extends StatefulWidget {
  final List<FileIds> listAudio;
  final String subBabName;
  final String baseUrl;

  const ListAudioView({Key key, this.listAudio, this.subBabName, this.baseUrl}) : super(key: key);
  @override
  _ListAudioViewState createState() => _ListAudioViewState();
}

class _ListAudioViewState extends State<ListAudioView> {
  List<FileIds> get _listAudio => widget.listAudio;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.grey[200]
      ),
      child: Scaffold(
        body: _listAudio.length > 0 ? ScrollConfiguration(
          behavior: MyBehavior(),
          child: ListView(
            children: <Widget>[
              _listAudios(),
            ],
          ),
        ) : _listEmptyView(),
      ),
    );
  }

  Widget _listAudios() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.symmetric(
            horizontal: 16.0
        ),
        itemCount: _listAudio.length,
        itemBuilder: (context, index) {
          return _audioItem(index);
        },
      ),
    );
  }

  Widget _audioItem(index) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => PlayAudioScreen(
            listAudio: _listAudio,
            nowPlaying: _listAudio[index],
            subBabName: widget.subBabName,
            baseUrl: widget.baseUrl,
          )
        ));
      },
      child: Padding(
        padding: EdgeInsets.only(
            left: 8.0, right: 8.0,
            top: 8.0, bottom: index == _listAudio.length - 1 ? 25.0 : 8.0),
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 70.0,
            child: Row(
              children: <Widget>[
                Container(
                  width: 60.0,
                  height: 60.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey[300],
                  ),
                  child: Center(
                    child: Icon(Icons.play_circle_filled, size: 36.0, color: Colors.grey,),
                  ),
                ),
                SizedBox(width: 10.0,),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(_listAudio[index].meta.title, style: EduText().mediumSemiBold, maxLines: 2,)),
                ),
                SizedBox(width: 10.0,),
                Text('${(_listAudio[index].meta.duration / 60).round()}.${(_listAudio[index].meta.duration % 60).toString().padLeft(2, '0')}', style: EduText().smallSemiBold)
              ],
            )
        ),
      ),
    );
  }

  Widget _listEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
              'assets/empty.png',
              width: 60.0
          ),
          SizedBox(height: 40.0),
          Container(
            child: Text('Tidak ada audio',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}