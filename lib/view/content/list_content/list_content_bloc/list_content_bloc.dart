import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:path_provider/path_provider.dart';
import './bloc.dart';

class ListContentBloc extends Bloc<ListContentEvent, ListContentState> {
  @override
  ListContentState get initialState => InitialListContentState();

  @override
  Stream<ListContentState> mapEventToState(
      ListContentEvent event,
      ) async* {
    if (event is LoadPlaylist) {
      yield* _loadPlaylistToState(event);
    }
  }

  Stream<ListContentState> _loadPlaylistToState(event) async* {
    yield Loading();
    try {
      int tabPosition = 0;
      List<FileIds> listVideo = new List<FileIds>();
      List<FileIds> listBook = new List<FileIds>();
      List<FileIds> listAudio = new List<FileIds>();

      final baseUrl = await ApiClient().getUrl();
      final dir = (await getApplicationDocumentsDirectory()).path;
      final listFileDb = await DbDao().getContent();

      if (event.onlyDownloadedFile) {
        if (listFileDb != null) {
          for (var fileDb in listFileDb) {
            var extension = fileDb.name.split('.');
            fileDb.isDownloaded = true;

            if (extension.last == 'mp4') {
              listVideo.add(fileDb);
            } else if (extension.last == 'pdf') {
              listBook.add(fileDb);
            } else if (extension.last == 'mp3' || extension.last == 'ogg') {
              listAudio.add(fileDb);
            }
          }
        }

        if (listAudio.length > listVideo.length && listAudio.length > listBook.length)
          tabPosition = 1;
        else if (listBook.length > listVideo.length && listBook.length > listAudio.length)
          tabPosition = 2;
        else tabPosition = 0;

        yield PlaylistLoaded(listVideo, listBook, listAudio, baseUrl, dir, tabPosition);

      } else {
        final result = await ApiClient().getNoAuthPlayListContent(subBabId: event.subBabId);

        if (result != null) {
          if (result.data.fileIds.length > 0) {
            for (var file in result.data.fileIds) {
              var extension = file.name.split('.');
              file.isDownloaded = false;

              if (listFileDb != null) {
                for (var fileDb in listFileDb) {
                  if (file.name == fileDb.name) {
                    file.isDownloaded = true;
                  }
                }
              }

              if (extension.last == 'mp4') {
                listVideo.add(file);
              } else if (extension.last == 'pdf') {
                listBook.add(file);
              } else if (extension.last == 'mp3' || extension.last == 'ogg') {
                listAudio.add(file);
              }
            }
          }

          if (listAudio.length > listVideo.length && listAudio.length > listBook.length)
            tabPosition = 1;
          else if (listBook.length > listVideo.length && listBook.length > listAudio.length)
            tabPosition = 2;
          else tabPosition = 0;

          print(tabPosition);

          yield PlaylistLoaded(listVideo, listBook, listAudio, baseUrl, dir, tabPosition);
        } else {
          yield LoadPlaylistFailed();
        }
      }
    } catch(e) {
      print(e);
      yield LoadPlaylistFailed();
    }
  }
}
