import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ListContentEvent extends Equatable {}

class LoadPlaylist extends ListContentEvent {
  final String subBabId;
  final bool onlyDownloadedFile;

  LoadPlaylist({this.onlyDownloadedFile, this.subBabId});

  @override
  List<Object> get props => [];
}

