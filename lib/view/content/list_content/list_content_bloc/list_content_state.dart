import 'package:edubox_tryout/models/content.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ListContentState extends Equatable {}

class InitialListContentState extends ListContentState {
  @override
  List<Object> get props => [];
}

class PlaylistLoaded extends ListContentState {
  final List<FileIds> listVideo;
  final List<FileIds> listBook;
  final List<FileIds> listAudio;
  final String baseUrl;
  final String dir;
  final int tabPosition;

  PlaylistLoaded(this.listVideo, this.listBook, this.listAudio, this.baseUrl, this.dir, this.tabPosition);

  @override
  List<Object> get props => [];
}

class LoadPlaylistFailed extends ListContentState {
  @override
  List<Object> get props => [];
}

class Loading extends ListContentState {
  @override
  List<Object> get props => [];
}
