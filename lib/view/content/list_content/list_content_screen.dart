import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/textstyles.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/view/content/list_content/list_audio/list_audio_view.dart';
import 'package:edubox_tryout/view/content/list_content/list_content_bloc/bloc.dart';
import 'package:edubox_tryout/view/content/list_content/list_ebook/list_ebook_view.dart';
import 'package:edubox_tryout/view/content/list_content/list_video/list_video_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class ListContentScreen extends StatefulWidget {
  final bool onlyDownloadedFile;
  final String babName, subBabName, subBabId, lessonId;

  const ListContentScreen({Key key, this.subBabName, this.subBabId, this.babName, this.onlyDownloadedFile, this.lessonId}) : super(key: key);
  @override
  _ListContentScreenState createState() => _ListContentScreenState();
}

class _ListContentScreenState extends State<ListContentScreen> with TickerProviderStateMixin {
  List<FileIds> _listVideo;
  List<FileIds> _listBook;
  List<FileIds> _listAudio;
  ListContentBloc _contentBloc;
  TabController _tabController;
  bool _isLoading = true;
  bool _isFailed = false;
  bool _isInit = true;
  String _baseUrl = '';
  String _dir = '';
  int _tabIndex = 0;

  @override
  void initState() {
    _contentBloc = ListContentBloc()
      ..add(LoadPlaylist(
          subBabId: widget.subBabId,
          onlyDownloadedFile: widget.onlyDownloadedFile
      ));

    _tabController = new TabController(length: 3, vsync: this, initialIndex: 0);
    _tabController.addListener(_tabListener);
    _listVideo = new List<FileIds>();
    _listBook = new List<FileIds>();
    _listAudio = new List<FileIds>();
    super.initState();
  }

  _tabListener() => print(_tabController.index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: _floatingActionBtn(),
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [EduColors.primary, EduColors.purple],
            stops: [0.0, 1.0],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            tileMode: TileMode.clamp
        ),
        title: Text(widget.onlyDownloadedFile ? 'Hasil Unduh' : '${widget.subBabName}', style: EduText().mediumBold.copyWith(
            color: Colors.white
        ),),
        bottom: TabBar(
          indicatorWeight: 3.0,
          controller: _tabController,
          indicatorPadding: EdgeInsets.symmetric(horizontal: 20.0),
          tabs: <Widget>[
            Tab(
              child: Text('Video', style: EduText().mediumSemiBold.copyWith(color: Colors.white),),
            ),
            Tab(
              child: Text('Audio', style: EduText().mediumSemiBold.copyWith(color: Colors.white),),
            ),
            Tab(
              child: Text('Buku', style: EduText().mediumSemiBold.copyWith(color: Colors.white),),
            )
          ],
        ),
      ),
      body: BlocListener(
        bloc: _contentBloc,
        listener: (context, state) {
          if (state is Loading) {
            setState(() {
              _isLoading = true;
            });
          }
          if (state is LoadPlaylistFailed) {
            setState(() {
              _isLoading = false;
              _isFailed = true;
            });
          }
          if (state is PlaylistLoaded) {
            setState(() {
              print(_isInit);
              _listVideo = state.listVideo;
              _listBook = state.listBook;
              _listAudio = state.listAudio;
              _baseUrl = state.baseUrl;
              if (_isInit) {
                _tabIndex = state.tabPosition;
                print('apapa');
                _tabController.animateTo(_tabIndex, duration: Duration(seconds: 1), curve: Curves.easeOut);
              }
              _dir = state.dir;
              _isFailed = false;
              _isLoading = false;
              _isInit = false;
            });
          }
        },
        child: _isLoading ? _loadingView() : TabBarView(
          controller: _tabController,
          children: <Widget>[
            !_isFailed ? ListVideoView(
              listVideo: _listVideo,
              babName: widget.babName,
              subBabName: widget.subBabName,
              baseUrl: _baseUrl,
              dir: _dir,
            ) : _loadFailedView(),
            !_isFailed ? ListAudioView(
              listAudio: _listAudio,
              subBabName: widget.subBabName,
              baseUrl: _baseUrl,
            ) : _loadFailedView(),
            !_isFailed ? ListEbookView(
              listBook: _listBook,
              baseUrl: _baseUrl,
              dir: _dir,
            ) : _loadFailedView()
          ],
        ),
      ),
    );
  }

  Widget _loadingView() {
    return Center(
        child: SpinKitRing(
          color: EduColors.primary,
          size: 50.0,
          lineWidth: 3.0,
          controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
        )
    );
  }

  Widget _loadFailedView() {
    return  Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/no-wifi.png', width: 80.0),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Text('Kamu sedang offline', style: EduText().mediumSemiBold,),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
              child: Text('Lihat hasil download untuk menikmati konten\ntanpa internet.',
                style: EduText().smallRegular,
                textAlign: TextAlign.center,
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ListContentScreen(
                      subBabName: 'Tidak diketahui',
                      subBabId: 'Tidak diketahui',
                      babName: 'Tidak diketahui',
                      onlyDownloadedFile: true,
                    )
                ));
              },
              color: EduColors.primary,
              child: Text('Lihat hasil download', style: EduText().smallSemiBold.copyWith(
                  color: Colors.white
              ),),
            ),
            SizedBox(height: 5.0),
            InkWell(
              onTap: () {
                _contentBloc.add(LoadPlaylist(
                    subBabId: widget.subBabId,
                    onlyDownloadedFile: widget.onlyDownloadedFile
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Text('Muat ulang', style: EduText().smallSemiBold.copyWith(
                    color: EduColors.primary
                ),),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _floatingActionBtn() {
    return InkWell(
      borderRadius: BorderRadius.circular(50.0),
      onTap: () => _contentBloc.add(LoadPlaylist(
          subBabId: widget.subBabId,
          onlyDownloadedFile: widget.onlyDownloadedFile
      )),
      child: Container(
        width: 60.0,
        height: 60.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
              colors: [EduColors.primary, EduColors.purple],
              stops: [0.0, 1.0],
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              tileMode: TileMode.clamp
          ),
          boxShadow: [
            BoxShadow(
                color: Colors.grey,
                blurRadius: 6.0,
                offset: Offset(0.0, 4.0)
            )
          ],
        ),
        child: Icon(Icons.refresh, color: Colors.white,),
      ),
    );
  }
}

