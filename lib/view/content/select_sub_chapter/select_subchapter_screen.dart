import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/view/content/list_content/list_content_screen.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class SelectSubChapterScreen extends StatefulWidget {
  final String materialId;
  final String chapterName;
  final List<Sub> subChapter;
  final String lessonId;

  const SelectSubChapterScreen({Key key, this.materialId, this.chapterName,
    this.subChapter, this.lessonId}) : super(key: key);
  @override
  _SelectSubChapterScreenState createState() => _SelectSubChapterScreenState();
}

class _SelectSubChapterScreenState extends State<SelectSubChapterScreen> with TickerProviderStateMixin {
  List<Sub> get _subChapter => widget.subChapter;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        automaticallyImplyLeading: true,
        title: Text(widget.chapterName, style: EduText().mediumBold.copyWith(
            color: Colors.white
        )),
        elevation: 0.0,
        gradient: LinearGradient(
            colors: [EduColors.primary, EduColors.primaryDark],
            stops: [0.0, 1.0],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            tileMode: TileMode.clamp
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: _subChapter.length > 0 ? Scrollbar(
        child: ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(
              left: 16.0, right: 16.0, bottom: 16.0, top: 22.0),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: Column(
                children: <Widget>[
                  Text('Pilih Sub-bab', style: EduText().mediumSemiBold,),
                  SizedBox(height: 10.0,),
                  Container(
                    height: 2.5,
                    width: 75.0,
                    color: EduColors.primary,
                  )
                ],
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: _subChapter.length,
              itemBuilder: (context, index) {
                var item = _subChapter[index];
                return _listItem(item);
              },
            )
          ],
        ),
      ) : _listEmptyView()
    );
  }

  Widget _listItem(item) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => ListContentScreen(
            subBabName: item.name,
            subBabId: item.sId,
            babName: widget.chapterName,
            onlyDownloadedFile: false,
            lessonId: widget.lessonId,
          )
        ));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 80.0,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[300],
                  blurRadius: 6.0,
                  offset: Offset(0.0, 4.0)
              )
            ],
          ),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text('${item.name}',
                          style: EduText().mediumSemiBold,
                          overflow: TextOverflow.ellipsis,
                        )
                    ),
                    Icon(Icons.arrow_forward_ios, color: EduColors.black,)
                  ],
                ),
              )
          ),
        ),
      ),
    );
  }

  Widget _listEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
              'assets/empty.png',
              width: 60.0
          ),
          SizedBox(height: 40.0),
          Container(
            child: Text('Tidak ada data',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
