import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import './bloc.dart';

class OpenBookBloc extends Bloc<OpenBookEvent, OpenBookState> {
  @override
  OpenBookState get initialState => InitialOpenBookState();

  @override
  Stream<OpenBookState> mapEventToState(
    OpenBookEvent event,
  ) async* {
    if (event is DownloadBook) {
      yield* _downloadBookToState(event);
    }
    if (event is OpenBook) {
      yield* _readBookToState(event);
    }
  }

  Stream<OpenBookState> _downloadBookToState(event) async* {
    try {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      final result = await ApiClient().downloadFile(event.file);
      if (result) {
        if (event.file.meta.thumbnail != "") {
          await ApiClient().downloadThumbnail(event.file);
        }
        event.file.isDownloaded = true;
        await DbDao().insertContent(event.file);

        yield BookDownloaded();
      } else {
        yield DownloadBookFailed();
      }
    } catch(e) {
      print('download failed : $e');
      yield DownloadBookFailed();
    }
  }

  Stream<OpenBookState> _readBookToState(event) async* {
    try {
      final String dir = (await getApplicationDocumentsDirectory()).path;
      final String path = '$dir/${event.filename}';

      yield BookOpened(path);
    } catch(e) {
      print('open failed : $e');
    }
  }
}
