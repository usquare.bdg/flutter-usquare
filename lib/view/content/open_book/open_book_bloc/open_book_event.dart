import 'package:edubox_tryout/models/content.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class OpenBookEvent extends Equatable {}

class DownloadBook extends OpenBookEvent {
  final FileIds file;

  DownloadBook(this.file);

  @override
  List<Object> get props => [];
}

class OpenBook extends OpenBookEvent {
  final String filename;

  OpenBook(this.filename);
  @override
  List<Object> get props => [];
}
