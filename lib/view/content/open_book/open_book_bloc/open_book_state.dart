import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class OpenBookState extends Equatable {}

class InitialOpenBookState extends OpenBookState {
  @override
  List<Object> get props => [];
}

class BookDownloaded extends OpenBookState {
  @override
  List<Object> get props => [];
}

class DownloadBookFailed extends OpenBookState {
  @override
  List<Object> get props => [];
}

class BookOpened extends OpenBookState {
  final String path;

  BookOpened(this.path);

  @override
  List<Object> get props => [];
}

class Loading extends OpenBookState {
  @override
  List<Object> get props => [];
}