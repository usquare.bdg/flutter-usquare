import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/content/open_book/open_book_bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class OpenBookScreen extends StatefulWidget {
  final FileIds file;
  final List<FileIds> listBook;

  const OpenBookScreen({Key key, this.file, this.listBook}) : super(key: key);
  @override
  _OpenBookScreenState createState() => _OpenBookScreenState();
}

class _OpenBookScreenState extends State<OpenBookScreen> with TickerProviderStateMixin{
  String _pdfPath;
  bool _isLoading = true;
  OpenBookBloc _openBookBloc;
  int _npPosition;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Read Book Screen');
    FbAnalytics().sendAnalytics('read_book');
    MixpanelUtils().initPlatformState('Read Book');
    _npPosition = widget.listBook.indexWhere((i) => i.sId == widget.file.sId);

    _openBookBloc = OpenBookBloc();
    if (widget.file.isDownloaded) {
      _openBookBloc.add(OpenBook(widget.file.name));
    } else {
      _openBookBloc.add(DownloadBook(widget.file));
    }

    print(widget.file.isDownloaded);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _openBookBloc,
      listener: (context, state) {
        if (state is DownloadBookFailed) {
          setState(() {
            _isLoading = false;
          });
          MyToast().toast('Gagal mengambil data');
          Navigator.pop(context);
        }
        if (state is BookDownloaded) {
          MyToast().toast('Buku berhasil diunduh');
          _openBookBloc.add(OpenBook(widget.file.name));
        }
        if (state is BookOpened) {
          setState(() {
            _pdfPath = state.path;
            widget.listBook[_npPosition].isDownloaded = true;
            _isLoading = false;
          });
        }
      },
      child: !_isLoading ? PDFViewerScaffold(
        appBar: GradientAppBar(
          gradient: LinearGradient(
            colors: [EduColors.primary, EduColors.primaryDark],
            stops: [0.0, 1.0],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            tileMode: TileMode.clamp
          ),
          title: Text(widget.file.meta.title, style: EduText().mediumBold.copyWith(
            color: Colors.white
          ),),
        ),
        path: _pdfPath,
      ) : Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: SpinKitRing(
            color: EduColors.primary,
            size: 50.0,
            lineWidth: 3.0,
            controller: AnimationController(vsync: this, duration: Duration(milliseconds: 500)),
          ),
        ),
      ),
    );
  }
}
