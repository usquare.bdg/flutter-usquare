import 'package:audioplayers/audioplayers.dart';
import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/scroll_behavior.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/utils/toast.dart';
import 'package:edubox_tryout/view/content/play_audio/play_audio_bloc/bloc.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path_provider/path_provider.dart';

class PlayAudioScreen extends StatefulWidget {
  final FileIds nowPlaying;
  final List<FileIds> listAudio;
  final String subBabName;
  final String baseUrl;

  const PlayAudioScreen({Key key, this.nowPlaying, this.listAudio, this.subBabName, this.baseUrl}) : super(key: key);

  @override
  _PlayAudioScreenState createState() => _PlayAudioScreenState();
}

class _PlayAudioScreenState extends State<PlayAudioScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String get _baseUrl => widget.baseUrl;
  PlayAudioBloc _audioBloc;
  FileIds _nowPlaying;
  List<FileIds> get _listAudio => widget.listAudio;
  AudioPlayer _audioPlayer;
  bool _isPaused = false;
  int _npPosition = 0;
  Duration _duration;
  Duration _position;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Play Audio Screen');
    FbAnalytics().sendAnalytics('play_audio');
    MixpanelUtils().initPlatformState('Play Audio');

    _audioBloc = PlayAudioBloc();
    _npPosition = widget.listAudio.indexWhere((i) => i.sId == widget.nowPlaying.sId);
    _nowPlaying = widget.nowPlaying;
    _initPlayer();
    _playAudio();
    super.initState();
  }

  @override
  void dispose() {
    _audioPlayer.stop();
    _audioPlayer.dispose();
    super.dispose();
  }

  void _initPlayer() {
    _audioPlayer = AudioPlayer();
    _duration = Duration();
    _position = Duration();

    _audioPlayer.onDurationChanged.listen((Duration d) {
      setState(() {
        _duration = d;
      });
    });

    _audioPlayer.onAudioPositionChanged.listen((Duration p) {
      setState(() {
        _position = p;
      });
    });

    _audioPlayer.onPlayerError.listen((event) {
      setState(() {
        _position = Duration(seconds: 0);
        _isPaused = true;
      });
    });

    _audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        _position = Duration(seconds: 0);
        if (_npPosition < _listAudio.length - 1) {
          _npPosition++;
          _nowPlaying = _listAudio[_npPosition];
          _playAudio();
        } else {
          _isPaused = true;
        }
      });
    });
  }

  void _playAudio() async {
    if (_nowPlaying.isDownloaded) {
      final _directory = await getApplicationDocumentsDirectory();
      await _audioPlayer.play('${_directory.path}/${_nowPlaying.name}', isLocal: true);
    } else {
      await _audioPlayer.play('$_baseUrl/file/${_nowPlaying.sId}');
    }
  }

  void _downloading() {
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: 'Mengunduh audio',
          content: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: SpinKitThreeBounce(
              color: EduColors.primary,
              size: 24.0,
            ),
          ),
        );
      },
      barrierDismissible: false
    );
  }

  void _alertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          title: Strings.appName,
          content: Padding(
            padding: const EdgeInsets.only(
                bottom: 16.0),
            child: Text('Untuk mengunduh Audio kamu harus masuk ke akun ${Strings.appName} terlebih dahulu!',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          ),
          actionAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        width: 1.5,
                        color: EduColors.primary
                    )
                ),
                child: Center(
                    child: Text('Kembali', style: EduText().smallSemiBold.copyWith(
                        color: EduColors.primary
                    ),)
                ),
              ),
            ),
            SizedBox(width: 10.0,),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => LoginScreen()
                ));
              },
              child: Container(
                width: 100.0,
                height: 40.0,
                decoration: BoxDecoration(
                    color: EduColors.primary,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                child: Center(
                    child: Text('Masuk', style: EduText().smallSemiBold.copyWith(
                        color: Colors.white
                    ),)
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  void _confirmationDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: 'Konfirmasi',
            content: Padding(
              padding: const EdgeInsets.only(
                  bottom: 16.0),
              child: Column(
                children: <Widget>[
                  Container(
                    width: 70.0,
                    height: 70.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey[300],
                    ),
                    child: Center(
                      child: Icon(Icons.play_circle_outline, size: 36.0, color: Colors.grey,),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text('Unduh\n \"${_nowPlaying.meta.title}\" ?', style: EduText().mediumRegular,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            actionAlignment: MainAxisAlignment.center,
            actions: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 100.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(
                          width: 1.5,
                          color: EduColors.primary
                      )
                  ),
                  child: Center(
                      child: Text('Batal', style: EduText().smallSemiBold.copyWith(
                          color: EduColors.primary
                      ),)
                  ),
                ),
              ),
              SizedBox(width: 10.0,),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  _audioBloc.add(DownloadAudio(_nowPlaying));
                },
                child: Container(
                  width: 100.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: EduColors.primary,
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  child: Center(
                      child: Text('Unduh', style: EduText().smallSemiBold.copyWith(
                          color: Colors.white
                      ),)
                  ),
                ),
              ),
            ],
          );
        }
    );
  }

  Future _onWillPop() async {
    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: Scaffold(
        key: _scaffoldKey,
        endDrawer: _playList(),
        body: BlocListener(
          bloc: _audioBloc,
          listener: (context, state) {
            if (state is Downloading) {
              _downloading();
            }
            if (state is UserNotFound) {
              Navigator.pop(context);
              _alertDialog();
            }
            if (state is DownloadAudioFailed) {
              Navigator.pop(context);
              MyToast().toast('Gagal mengunduh audio');
            }
            if (state is AudioDownloaded) {
              Navigator.pop(context);
              MyToast().toast('Berhasil mengunduh audio');
              setState(() {
                _nowPlaying.isDownloaded = true;
                _listAudio[_npPosition].isDownloaded = true;
              });
            }
          },
          child: Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + kToolbarHeight),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [EduColors.primary, EduColors.primaryDark],
                            stops: [0.2, 1.0],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            tileMode: TileMode.clamp
                          ),
                        ),
                        child: Center(
                          child: Image.asset(
                            'assets/audio.png',
                            width: 120.0,
                            height: 120.0,
                          ),
                        ),
                      )
                    ),
                    Expanded(
                      child: _controllerView()
                    )
                  ],
                ),
              ),
              Container(
                height: 80.0,
                child: AppBar(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  iconTheme: IconThemeData(
                    color: Colors.white
                  ),
                  centerTitle: true,
                  title: Text(widget.subBabName, style: EduText().mediumBold.copyWith(
                    color: Colors.white
                  ),),
                  actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _scaffoldKey.currentState.openEndDrawer();
                      },
                      icon: Icon(Icons.playlist_play))
                  ],
                ),
              )
            ],
          ),
        )
      ),
    );
  }

  Widget _controllerView() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Center(
                child: Text(
                  _nowPlaying.meta.title, style: EduText().largeSemiBold,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                ),
              )
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Text('${_position.inMinutes % 60}:${(_position.inSeconds % 60).toString().padLeft(2, '0')}', style: EduText().smallRegular,),
                Expanded(
                  child: Slider(
                    activeColor: EduColors.primary,
                    inactiveColor: Colors.grey[200],
                    value: _position.inSeconds.toDouble(),
                    min: 0.0,
                    max: _nowPlaying.meta.duration.toDouble(),
                    onChanged: (val) {
                      setState(() {
                        Duration newPosition = Duration(seconds: val.toInt());

                        _audioPlayer.seek(newPosition);
                      });
                    },
                  ),
                ),
                Text('${(_nowPlaying.meta.duration / 60).round()}.${(_nowPlaying.meta.duration % 60).toString().padLeft(2, '0')}', style: EduText().smallRegular)
              ],
            )
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    setState(() {
                      if (_npPosition > 0) {
                        _npPosition--;
                        _nowPlaying = _listAudio[_npPosition];
                        _playAudio();
                        if (_isPaused) {
                          _isPaused = false;
                        }
                      }
                    });
                  },
                  borderRadius: BorderRadius.circular(50.0),
                  child: Container(
                    width: 60.0,
                    height: 60.0,
                    child: Icon(Icons.skip_previous, color: EduColors.black, size: 36.0,),
                  ),
                ),
                SizedBox(width: 20.0,),
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: () {
                    setState(() {
                      _isPaused ? _audioPlayer.resume() : _audioPlayer.pause();
                      _isPaused = !_isPaused;
                    });
                  },
                  child: Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        colors: [EduColors.primary, EduColors.primaryDark],
                        stops: [0.2, 1.0],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        tileMode: TileMode.clamp
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        _isPaused ? Icons.play_arrow : Icons.pause,
                        size: 41.0,
                        color: Colors.white,),
                    ),
                  ),
                ),
                SizedBox(width: 20.0,),
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: () {
                    setState(() {
                      if (_npPosition < _listAudio.length - 1) {
                        _npPosition++;
                        _nowPlaying = _listAudio[_npPosition];
                        _playAudio();
                        if (_isPaused) {
                          _isPaused = false;
                        }
                      }
                    });
                  },
                  child: Container(
                    width: 60.0,
                    height: 60.0,
                    child: Icon(Icons.skip_next, color: EduColors.black, size: 36.0,),
                  ),
                ),
              ],
            )
          ),
          Expanded(
            child: Center(
              child: !_nowPlaying.isDownloaded ? OutlineButton(
                onPressed: () {
                  _confirmationDialog();
                },
                borderSide: BorderSide(
                  width: 2.0, color: EduColors.primary,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Unduh Audio', style: EduText().smallBold.copyWith(
                      color: EduColors.primary
                    ),),
                    SizedBox(width: 5.0,),
                    Icon(Icons.file_download, color: EduColors.primary, size: 18.0,)
                  ],
                ),
              ) : Container(
                height: 40.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50.0),
                  color: Colors.grey[200]
                ),
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Diunduh', style: EduText().smallSemiBold),
                    SizedBox(width: 5.0,),
                    Icon(Icons.check, color: EduColors.black, size: 20.0,)
                  ],
                ),
              )
            )
          ),
        ],
      ),
    );
  }

  Widget _playList() {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                backgroundColor: Colors.transparent,
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.close),
                ),
                iconTheme: IconThemeData(
                  color: EduColors.black,
                ),
                actions: <Widget>[
                  Container()
                ],
                elevation: 0.0,
                centerTitle: true,
                title: Text('Daftar putar audio', style: EduText().mediumBold,)
              ),
              SizedBox(height: 20.0,),
              ScrollConfiguration(
                behavior: MyBehavior(),
                child: ListView.builder(
                  itemCount: _listAudio.length,
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          _nowPlaying = _listAudio[index];
                          _npPosition = index;
                          _playAudio();
                          _isPaused = false;
                          Navigator.pop(context);
                        });
                      },
                      child: Container(
                        color: index == _npPosition ? Colors.grey[200] : Colors.transparent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              child: Center(
                                child: Text('${index+1}.', style: EduText().smallRegular.copyWith(
                                  fontWeight: index == _npPosition ? FontWeight.w600 : FontWeight.normal
                                ),),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 60.0,
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(_listAudio[index].meta.title, style: EduText().mediumRegular.copyWith(
                                    fontWeight: index == _npPosition ? FontWeight.w600 : FontWeight.normal
                                  ),)
                                )
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            Container(
                              width: 40.0,
                              child: Text('${(_listAudio[index].meta.duration / 60).round()}.${(_listAudio[index].meta.duration % 60).toString().padLeft(2, '0')}',
                                style: EduText().smallRegular.copyWith(
                                  fontWeight: _nowPlaying.name == _listAudio[index].name ? FontWeight.w600 : FontWeight.normal
                                )),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
