import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/services/db_dao.dart';
import 'package:permission_handler/permission_handler.dart';
import './bloc.dart';

class PlayAudioBloc extends Bloc<PlayAudioEvent, PlayAudioState> {
  @override
  PlayAudioState get initialState => InitialPlayAudioState();

  @override
  Stream<PlayAudioState> mapEventToState(
    PlayAudioEvent event,
  ) async* {
    if (event is DownloadAudio) {
      yield* _downloadAudioToState(event);
    }
  }

  Stream<PlayAudioState> _downloadAudioToState(event) async* {
    yield Downloading();
    try {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      final user = await DbDao().getUser();
      if (user != null) {
        final result = await ApiClient().downloadFile(event.file);
        if (result) {
          if (event.file.meta.thumbnail != "") {
            await ApiClient().downloadThumbnail(event.file);
          }

          event.file.isDownloaded = true;
          await DbDao().insertContent(event.file);

          yield AudioDownloaded();
        } else {
          yield DownloadAudioFailed();
        }
      } else {
        yield UserNotFound();
      }
    } catch (e) {
      print('download failed $e');
      yield DownloadAudioFailed();
    }
  }
}
