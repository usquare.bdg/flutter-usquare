import 'package:edubox_tryout/models/content.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PlayAudioEvent extends Equatable {}

class DownloadAudio extends PlayAudioEvent {
  final FileIds file;

  DownloadAudio(this.file);

  @override
  List<Object> get props => [];
}
