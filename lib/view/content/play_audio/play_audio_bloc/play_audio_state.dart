import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PlayAudioState extends Equatable{}

class InitialPlayAudioState extends PlayAudioState {
  @override
  List<Object> get props => [];
}

class AudioDownloaded extends PlayAudioState {
  @override
  List<Object> get props => [];
}

class DownloadAudioFailed extends PlayAudioState {
  @override
  List<Object> get props => [];
}

class UserNotFound extends PlayAudioState {
  @override
  List<Object> get props => [];
}

class Downloading extends PlayAudioState {
  @override
  List<Object> get props => [];
}
