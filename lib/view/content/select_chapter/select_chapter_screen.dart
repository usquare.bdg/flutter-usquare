import 'package:edubox_tryout/models/content.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/view/content/list_content/list_content_screen.dart';
import 'package:edubox_tryout/view/content/select_chapter/select_chapter_bloc/bloc.dart';
import 'package:edubox_tryout/view/content/select_sub_chapter/select_subchapter_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class SelectChapterScreen extends StatefulWidget {
  final String categoryName;
  final String lessonId;
  final String sId;

  const SelectChapterScreen({Key key, this.categoryName, this.lessonId, this.sId}) : super(key: key);
  @override
  _SelectChapterScreenState createState() => _SelectChapterScreenState();
}

class _SelectChapterScreenState extends State<SelectChapterScreen> with TickerProviderStateMixin{
  SelectChapterBloc _selectChapterBloc;
  EduboxContent _chapter;
  bool _isLoading = true;
  bool _isEmpty = false;

  @override
  void initState() {
    _chapter = EduboxContent();
    _selectChapterBloc = SelectChapterBloc()
      ..add(LoadContent(widget.lessonId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        automaticallyImplyLeading: true,
        title: Text(widget.categoryName, style: EduText().mediumBold.copyWith(
          color: Colors.white
        )),
        elevation: 0.0,
        gradient: LinearGradient(
            colors: [EduColors.primary, EduColors.primaryDark],
            stops: [0.0, 1.0],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            tileMode: TileMode.clamp
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: BlocListener(
        bloc: _selectChapterBloc,
        listener: (context, state) {
          if (state is Loading) {
            setState(() {
              _isLoading = true;
            });
          }
          if (state is LoadContentFailed) {
            setState(() {
              _isLoading = false;
              _chapter = null;
            });
          }
          if (state is ContentIsEmpty) {
            setState(() {
              _isLoading = false;
              _isEmpty = true;
            });
          }
          if (state is ContentLoaded) {
            setState(() {
              _isLoading = false;
              _isEmpty = false;
              _chapter = state.chapter;
            });
          }
        },
        child: _isLoading ? _loadingView() : _isEmpty ? _listEmptyView() : _chapter != null ? Scrollbar(
          child: ListView(
            physics: BouncingScrollPhysics(),
            padding: EdgeInsets.only(
                left: 16.0, right: 16.0, bottom: 16.0, top: 22.0),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Column(
                  children: <Widget>[
                    Text('Pilih Bab', style: EduText().mediumSemiBold,),
                    SizedBox(height: 10.0,),
                    Container(
                      height: 2.5,
                      width: 50.0,
                      color: EduColors.primary,
                    )
                  ],
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _chapter.data.sub.length,
                itemBuilder: (context, index) {
                  var item = _chapter.data.sub[index];
                  return _listItem(item);
                },
              )
            ],
          ),
        ) : _loadFailedView()
      ),
    );
  }

  Widget _listItem(item) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => SelectSubChapterScreen(
            materialId: item.sId,
            chapterName: item.name,
            subChapter: item.sub,
            lessonId: widget.sId,
          )
        ));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 80.0,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[300],
                  blurRadius: 6.0,
                  offset: Offset(0.0, 4.0)
              )
            ],
          ),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('${item.name}',
                      style: EduText().mediumSemiBold,
                      overflow: TextOverflow.ellipsis,
                    )
                  ),
                  Icon(Icons.arrow_forward_ios, color: EduColors.black,)
                ],
              ),
              )
          ),
        ),
      ),
    );
  }

  Widget _loadingView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: SpinKitRing(
          color: EduColors.primary,
          size: 50.0,
          lineWidth: 3.0,
          controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
        ),
      )
    );
  }

  Widget _listEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
              'assets/empty.png',
              width: 80.0
          ),
          SizedBox(height: 40.0),
          Container(
            child: Text('Tidak ada data',
              style: EduText().mediumRegular,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _loadFailedView() {
    return  Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/no-wifi.png', width: 80.0),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Text('Kamu sedang offline', style: EduText().mediumSemiBold,),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
              child: Text('Lihat hasil download untuk menikmati konten\ntanpa internet.',
                style: EduText().smallRegular,
                textAlign: TextAlign.center,
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => ListContentScreen(
                    subBabName: 'Tidak diketahui',
                    subBabId: 'Tidak diketahui',
                    babName: 'Tidak diketahui',
                    onlyDownloadedFile: true,
                  )
                ));
              },
              color: EduColors.primary,
              child: Text('Lihat hasil download', style: EduText().smallSemiBold.copyWith(
                  color: Colors.white
              ),),
            ),
            SizedBox(height: 5.0),
            InkWell(
              onTap: () {
                _selectChapterBloc.add(LoadContent(widget.lessonId));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Text('Muat ulang', style: EduText().smallSemiBold.copyWith(
                    color: EduColors.primary
                ),),
              ),
            )
          ],
        ),
      ),
    );
  }
}