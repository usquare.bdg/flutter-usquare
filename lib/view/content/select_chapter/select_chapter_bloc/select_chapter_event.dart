import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SelectChapterEvent extends Equatable {}

class LoadContent extends SelectChapterEvent {
  final String lessonId;

  LoadContent(this.lessonId);
  @override
  List<Object> get props => [];
}
