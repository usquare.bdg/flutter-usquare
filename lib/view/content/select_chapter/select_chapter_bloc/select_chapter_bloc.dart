import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import 'bloc.dart';

class SelectChapterBloc extends Bloc<SelectChapterEvent, SelectChapterState> {
  @override
  SelectChapterState get initialState => InitialSelectChapterState();

  @override
  Stream<SelectChapterState> mapEventToState(
    SelectChapterEvent event,
  ) async* {
    if (event is LoadContent) {
      yield* _loadContentToState(event);
    }
  }

  Stream<SelectChapterState> _loadContentToState(event) async* {
    yield Loading();
    try {
      final result = await ApiClient().getNoAuthChapter(event.lessonId);
      if (result != null) {
        if (result.data.sub.length > 0) {
          yield ContentLoaded(result);
        } else {
          yield ContentIsEmpty();
        }
      } else {
        yield LoadContentFailed();
      }
    } catch(e) {
      print(e);
      yield LoadContentFailed();
    }
  }
}
