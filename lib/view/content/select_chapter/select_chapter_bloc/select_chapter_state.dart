import 'package:edubox_tryout/models/content.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SelectChapterState extends Equatable {}

class InitialSelectChapterState extends SelectChapterState {
  @override
  List<Object> get props => [];
}

class ContentLoaded extends SelectChapterState {
  final EduboxContent chapter;

  ContentLoaded(this.chapter);
  @override
  List<Object> get props => [];
}

class LoadContentFailed extends SelectChapterState {
  @override
  List<Object> get props => [];
}

class ContentIsEmpty extends SelectChapterState {
  @override
  List<Object> get props => [];
}

class Loading extends SelectChapterState {
  @override
  List<Object> get props => [];
}