import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_tryout/services/api_client.dart';
import './bloc.dart';

class RegisterStudentBloc extends Bloc<RegisterStudentEvent, RegisterStudentState> {
  @override
  RegisterStudentState get initialState => InitialRegisterStudentState();

  @override
  Stream<RegisterStudentState> mapEventToState(
    RegisterStudentEvent event,
  ) async* {
    if (event is SubmitForm) {
      yield* _submitFormToState(event);
    }
  }

  Stream<RegisterStudentState> _submitFormToState(event) async* {
    yield Loading();
    try {
      final result = await ApiClient().createStudent(event.body);
      if (result) {
        yield OnSuccess();
      } else {
        yield OnFailed();
      }
    } catch (e) {
      print(e);
      yield OnFailed();
    }
  }
}
