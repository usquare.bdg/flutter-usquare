import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterStudentState extends Equatable{}

class InitialRegisterStudentState extends RegisterStudentState {
  @override
  List<Object> get props => [];
}

class OnSuccess extends RegisterStudentState {
  @override
  List<Object> get props => [];
}

class OnFailed extends RegisterStudentState {
  @override
  List<Object> get props => [];
}

class Loading extends RegisterStudentState {
  @override
  List<Object> get props => [];
}
