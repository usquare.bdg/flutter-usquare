import 'package:edubox_tryout/models/user.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterStudentEvent extends Equatable {}

class SubmitForm extends RegisterStudentEvent {
  final CreateStudentBody body;

  SubmitForm(this.body);
  @override
  List<Object> get props => [];
}
