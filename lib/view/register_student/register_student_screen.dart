import 'package:edubox_tryout/models/user.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_analytics.dart';
import 'package:edubox_tryout/utils/mixpanel_utlis.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/view/register_student/register_student_bloc/bloc.dart';
import 'package:edubox_tryout/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class RegisterStudentScreen extends StatefulWidget {
  final bool isFromLoginScreen;

  const RegisterStudentScreen({Key key, this.isFromLoginScreen}) : super(key: key);
  @override
  _RegisterStudentScreenState createState() => _RegisterStudentScreenState();
}

class _RegisterStudentScreenState extends State<RegisterStudentScreen> {
  RegisterStudentBloc _registerStudentBloc;
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _displayNameController = TextEditingController();
  FocusNode _usernameFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _displayNameFocus = FocusNode();
  String _username = '', _password = '', _email = '', _displayName = '';
  bool _isLoading = false;
  bool _isDisabled = true;
  bool _isVisible = true;
  bool _showLoading = false;
  bool _showFailedIndicator = false;
  bool _showSuccessIndicator = false;

  @override
  void initState() {
    FbAnalytics().setCurrentScreen('Register Screen');
    MixpanelUtils().initPlatformState('Register Screen');
    _registerStudentBloc = RegisterStudentBloc();
    super.initState();
  }

  void _onSubmit() {
    FbAnalytics().sendAnalytics('submit_register');
    MixpanelUtils().initPlatformState('Submit Register');

    List<String> rombelIds = [
      '17e60b54-d3d2-447c-9be1-1f141bd2e4db',
      '04ee48d5-1537-4acf-a089-6234e419a242',
      '84d9b6c2-c8b3-4acf-a687-38425710ddd7',
      '267bcccd-ba77-4079-9de5-eeab525c659f',
      'd5604ea9-3bd0-4a90-a98b-74b5cebace43',
      '67197e92-43a4-4844-8a86-6f099906b9bc',
      '2c58f3ec-a986-4327-831c-5678b48c680a',
      '90603e0f-e497-4546-b9c3-6daaec3f392e',
      '613c53f0-aac3-4e5d-9c33-67795c78178e',
      'a64030a9-c1c1-4e12-84b7-c719e301a77a',
      '205f4085-4aca-4222-a245-b4a0bc72a3b9',
      'da729c16-fa4a-44df-8af8-9132cb6dcd34',
      '0784605a-4ff0-438d-aceb-bd296a6f1636',
      '1685f952-41f9-47ea-8724-df2b2407beb6',
      'd2f3581d-fa52-434f-897f-1f77b606f4c3',
      'e920bb11-6d5c-4e2b-a99c-41c7ac91ad64'
    ];

    CreateStudentBody _studentBody = CreateStudentBody();
      _studentBody.username = _usernameController.text;
      if (_email.length == 0) {
        _studentBody.email = '${_usernameController.text}@mail.id';
      } else {
        _studentBody.email = _emailController.text;
      }
      _studentBody.password = _passwordController.text;
      _studentBody.displayName = _displayNameController.text;
      _studentBody.institutionId = 'cad5b9da-57d6-41f4-9ce3-142655cb3847';
      _studentBody.rombelIds = rombelIds;

    _registerStudentBloc.add(SubmitForm(_studentBody));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);
        return null;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: <Widget>[
              BlocListener(
                bloc: _registerStudentBloc,
                listener: (context, state) {
                  if (state is OnSuccess) {
                    setState(() {
                      _showLoading = false;
                      _showFailedIndicator = false;
                      _showSuccessIndicator = true;
                    });

                    Future.delayed(const Duration(seconds: 1), () {
                      setState(() {
                        _isLoading = false;
                      });
                      Navigator.pop(context);
                    });
                  }
                  if (state is OnFailed) {
                    setState(() {
                      _showLoading = false;
                      _showFailedIndicator = true;
                      _showSuccessIndicator = false;
                    });

                    Future.delayed(const Duration(seconds: 1), () {
                      setState(() {
                        _isLoading = false;
                      });
                    });
                  }
                  if (state is Loading) {
                    setState(() {
                      _isLoading = true;
                      _showLoading = true;
                      _showFailedIndicator = false;
                      _showSuccessIndicator = false;
                    });
                  }
                },
                child: Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            _registerForm(),
                          ],
                        ),
                      )
                    ),
                    Container(
                      height: 80.0,
                      child: AppBar(
                        backgroundColor: Colors.white,
                        elevation: 0.0,
                        iconTheme: IconThemeData(color: Colors.black),
                        automaticallyImplyLeading: true,
                        title: Text('Daftar akun ${Strings.appName}', style: EduText().mediumBold),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: _isLoading,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.transparent
                ),
              ),
              Visibility(
                visible: _isLoading,
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                      width: 160.0,
                      height: 120.0,
                      decoration: BoxDecoration(
                          color: Colors.black54,
                          borderRadius: BorderRadius.circular(5.0)
                      ),
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _loading(),
                          _successIndicator(),
                          _failedIndicator()
                        ],
                      )
                  ),
                ),
              )
            ],
          )
      ),
    );
  }

  Widget _registerForm() {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: 16.0),
      child: Container(
        padding: EdgeInsets.only(
            left: 16.0, right: 16.0, top: 20.0, bottom: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
                color: Colors.grey[400],
                blurRadius: 10.0,
                offset: Offset(0.0, 3.0)
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10.0,),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 5.0
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Username', style: EduText().mediumSemiBold,),
              ),
            ),
            MyTextField(
              controller: _usernameController,
              focusNode: _usernameFocus,
              hintText: 'Masukkan username',
              keyboardType: TextInputType.emailAddress,
              actionType: TextInputAction.next,
              onSubmitted: (term) {
                _usernameFocus.unfocus();
                FocusScope.of(context).requestFocus(_passwordFocus);
              },
              onChanged: (val) {
                setState(() {
                  _username = val;
                  if (_username.length > 0 && _password.length >= 5 && _displayName.length > 0) {
                    _isDisabled = false;
                  } else {
                    _isDisabled = true;
                  }
                });
              },
              suffix: _username.length > 0 ? InkWell(
                onTap: () {
                  setState(() {
                    WidgetsBinding.instance.addPostFrameCallback((_) => _usernameController.clear());
                    _username = '';
                  });
                },
                child: Icon(Icons.close, color: Colors.grey[400]),
              ) : null,
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 5.0
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Password', style: EduText().mediumSemiBold,),
              ),
            ),
            MyTextField(
              controller: _passwordController,
              focusNode: _passwordFocus,
              hintText: 'Masukkan password',
              keyboardType: TextInputType.text,
              actionType: TextInputAction.next,
              onSubmitted: (term) {
                _passwordFocus.unfocus();
                FocusScope.of(context).requestFocus(_displayNameFocus);
              },
              onChanged: (val) {
                setState(() {
                  _password = val;
                  if (_username.length > 0 && _password.length >= 5 && _displayName.length > 0) {
                    _isDisabled = false;
                  } else {
                    _isDisabled = true;
                  }
                });
              },
              suffix: InkWell(
                  onTap: () {
                    setState(() {
                      _isVisible = !_isVisible;
                    });
                  },
                  child: Icon(
                      _isVisible ? Icons.visibility_off : Icons.visibility,
                      color: Colors.grey[400])
              ),
              obsecureText: _isVisible,
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 5.0
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Nama Lengkap', style: EduText().mediumSemiBold,),
              ),
            ),
            MyTextField(
              controller: _displayNameController,
              focusNode: _displayNameFocus,
              hintText: 'Masukkan nama lengkap',
              keyboardType: TextInputType.text,
              actionType: TextInputAction.next,
              onSubmitted: (term) {
                _displayNameFocus.unfocus();
                FocusScope.of(context).requestFocus(_emailFocus);
              },
              onChanged: (val) {
                setState(() {
                  _displayName = val;
                  if (_username.length > 0 && _password.length >= 5 &&  _displayName.length > 0) {
                    _isDisabled = false;
                  } else {
                    _isDisabled = true;
                  }
                });
              },
              suffix: _displayName.length > 0 ? InkWell(
                onTap: () {
                  setState(() {
                    WidgetsBinding.instance.addPostFrameCallback((_) => _displayNameController.clear());
                    _displayName = '';
                  });
                },
                child: Icon(Icons.close, color: Colors.grey[400]),
              ) : null,
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 5.0
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Email', style: EduText().mediumSemiBold,),
              ),
            ),
            MyTextField(
              controller: _emailController,
              focusNode: _emailFocus,
              hintText: 'Masukkan email',
              keyboardType: TextInputType.emailAddress,
              actionType: TextInputAction.done,
              onSubmitted: (term) {
                _emailFocus.unfocus();
                if (!_isDisabled) {
                  _onSubmit();
                }
              },
              onChanged: (val) {
                setState(() {
                  _email = val;
                  if (_username.length > 0 && _password.length >= 5 && _displayName.length > 0) {
                    _isDisabled = false;
                  } else {
                    _isDisabled = true;
                  }
                });
              },
              suffix: _email.length > 0 ? InkWell(
                onTap: () {
                  setState(() {
                    WidgetsBinding.instance.addPostFrameCallback((_) => _emailController.clear());
                    _email = '';
                  });
                },
                child: Icon(Icons.close, color: Colors.grey[400]),
              ) : null,
            ),
            SizedBox(
              height: 20.0,
            ),
            MyButton(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
                _onSubmit();
              },
              isDisabled: _isDisabled,
              child: Text('Daftarkan Akun', style: EduText().mediumSemiBold.copyWith(
                  color: Colors.white
              )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _loading() {
    return Visibility(
      visible: _showLoading,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitDualRing(
            color: Colors.white,
            size: 30.0,
            lineWidth: 3.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text('Tunggu sebentar..', style: EduText().smallRegular.copyWith(
                color: Colors.white
            )),
          )
        ],
      ),
    );
  }

  Widget _successIndicator() {
    return Visibility(
      visible: _showSuccessIndicator,
      child: Column(
        children: <Widget>[
          Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 1.5, color: Colors.white
                  ),
                  shape: BoxShape.circle
              ),
              child: Icon(Icons.check, color: Colors.white,)
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(
              'Berhasil mendaftarkan akun', style: EduText().smallRegular.copyWith(
              color: Colors.white,
            ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _failedIndicator() {
    return Visibility(
      visible: _showFailedIndicator,
      child: Column(
        children: <Widget>[
          Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 1.5, color: Colors.white
                  ),
                  shape: BoxShape.circle
              ),
              child: Icon(Icons.close, color: Colors.white,)
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text('Gagal mendaftarkan akun', style: EduText().smallRegular.copyWith(
                color: Colors.white
            ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
