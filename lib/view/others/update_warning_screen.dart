import 'package:edubox_tryout/main.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/firebase_admob.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:edubox_tryout/view/login/login_screen.dart';
import 'package:edubox_tryout/view/select_server/select_server_screen.dart';
import 'package:edubox_tryout/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';

class UpdateWarningScreen extends StatefulWidget {
  final bool forceUpdate;
  final String currentVersion, latestVersion;

  const UpdateWarningScreen({Key key, this.forceUpdate, this.currentVersion, this.latestVersion}) : super(key: key);
  @override
  _UpdateWarningScreenState createState() => _UpdateWarningScreenState();
}

class _UpdateWarningScreenState extends State<UpdateWarningScreen> {

  Future<void> _launchStore() async {
    LaunchReview.launch(
      androidAppId: 'com.square',
      iOSAppId: '1485393841'
    );
  }

  @override
  void initState() {
    isFirstTimeOpen = false;

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            ClipPath(
              clipper: MyClipper(),
              child: Container(
                height: MediaQuery.of(context).size.height / 3,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [EduColors.primary, EduColors.primaryDark],
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: [0.2, 1.0],
                      tileMode: TileMode.clamp
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Versi Sekarang', style: EduText().largeBold.copyWith(color: Colors.white),),
                    SizedBox(height: 10.0,),
                    Text(widget.currentVersion, style: EduText().superLargeBold.copyWith(color: Colors.white),)
                  ],
                ),
              )
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    Text('Versi baru telah tersedia', style: EduText().largeBold, textAlign: TextAlign.center,),
                    SizedBox(height: 15.0,),
                    Text('Terdapat pembaharuan di ${Strings.appName}, klik update untuk mendapatkan versi terbaru',
                      style: EduText().mediumRegular,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10.0,),
                    Text('Pembaharuan dapat berupa fitur baru, perbaikan pada beberapa bug, dan lainnya',
                      style: EduText().mediumRegular,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right:15.0, left: 15.0, bottom: 10.0),
              child: MyButton(
                onTap: () {
                  _launchStore();
                },
                isDisabled: false,
                child: Text('Update ke versi ${widget.latestVersion}', style: EduText().mediumSemiBold.copyWith(color: Colors.white),),
              ),
            ),
            Visibility(
              visible: !widget.forceUpdate,
              child: Padding(
                padding: const EdgeInsets.only(right:15.0, left: 15.0, bottom: 10.0),
                child: InkWell(
                  borderRadius: BorderRadius.circular(30.0),
                  onTap: () {
                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                        builder: (context) => MyApp(currentPage: 0,)
                    ), (Route<dynamic> route) => false);
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(width: 1.0),
                      borderRadius: BorderRadius.circular(30.0)
                    ),
                    child: Center(
                      child: Text('Lain kali', style: EduText().mediumSemiBold,),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}
