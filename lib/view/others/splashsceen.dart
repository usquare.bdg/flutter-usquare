import 'package:edubox_tryout/services/api_client.dart';
import 'package:edubox_tryout/themes/themes.dart';
import 'package:edubox_tryout/utils/strings.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    saveUrl();
    super.initState();
  }

  Future<void> saveUrl() async {
    await ApiClient().saveUrl(Strings.baseUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(Strings.appName, style: EduText().display1.copyWith(
                    color: EduColors.primary,
                    fontWeight: FontWeight.bold
                ),),
                /*Container(
                  width: 45.0,
                  height: 40.0,
                  padding: EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                    color: EduColors.red,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50.0),
                        bottomRight: Radius.circular(50.0),
                        topLeft: Radius.circular(50.0)
                    ),
                    gradient: LinearGradient(
                        colors: [Colors.red, EduColors.red],
                        stops: [0.0, 1.0],
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        tileMode: TileMode.clamp
                    ),
                  ),
                  child: Center(
                    child: Text('Go', style: EduText().superLargeMedium.copyWith(
                        color: Colors.white
                    )),
                  ),
                ),*/
              ],
            ),
          ),
        ],
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          width: double.infinity,
          child: Text(
            "Powered by Edubox PRO",
            style: EduText().smallRegular,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}